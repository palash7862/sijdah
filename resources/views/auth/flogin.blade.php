@extends('layouts.master')

@section('title','Login / Regoster')

@section('content')
    <!-- ========================================= MAIN ========================================= -->
    <main id="authentication" class="inner-bottom-md">
        <div class="container">
            <div class="row">
                @if(Session::has('msg-success'))
                    <p class="alert ">{{ Session::get('msg-success') }}</p>
                @endif
                <div class="col-md-6">
                    <section class="section sign-in inner-right-xs">
                        <h2 class="bordered">Sign In</h2>
                        <form role="form" class="login-form cf-style-1" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="field-row {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label>Email Address</label>
                                <input type="email" class="le-input" name="email" value="{{ old('email') }}" required
                                       autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.field-row -->

                            <div class="field-row {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label>Password</label>
                                <input type="password" class="le-input" name="password" required>
                            </div><!-- /.field-row -->

                            <div class="field-row remember-forget clearfix">
                                <span class="pull-left">
                                    <input type="checkbox" class="le-checbox auto-width inline"
                                           name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <span class="bold">Remember me</span>
                                </span>
                                <span class="pull-right">
                                    <a href="{{ route('password.request') }}" class="content-color bold">Forgotten Password ?</a>
                                </span>
                            </div>

                            <div class="buttons-holder">
                                <button type="submit" class="le-button huge">Sign In</button>
                            </div><!-- /.buttons-holder -->
                        </form><!-- /.cf-style-1 -->

                    </section><!-- /.sign-in -->
                </div><!-- /.col -->

                <div class="col-md-6">
                    <section class="section register inner-left-xs">
                        <h2 class="bordered">Create New Account</h2>

                        <form role="form" class="register-form cf-style-1" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="field-row {{ $errors->has('username') ? ' has-error' : '' }}">
                                <label>Full Name</label>
                                <input type="text" class="le-input" name="username" value="{{ old('username') }}" required
                                       autofocus>
                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.field-row -->

                            <div class="field-row {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label>Email Address</label>
                                <input type="text" class="le-input" name="email" value="{{ old('email') }}" required
                                       autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.field-row -->

                            <div class="field-row {{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label>Mobile No</label>
                                <input type="text" class="le-input" name="phone" value="{{ old('phone') }}" required
                                       autofocus>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.field-row -->

                            <div class="field-row {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label>Password</label>
                                <input type="password" class="le-input" name="password" value="" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.field-row -->

                            <div class="field-row">
                                <label>Confirm Password</label>
                                <input type="password" class="le-input" name="password_confirmation" value="" required>
                            </div><!-- /.field-row -->

                            <div class="buttons-holder">
                                <button type="submit" class="le-button huge">Sign Up</button>
                            </div><!-- /.buttons-holder -->
                        </form>
                    </section><!-- /.register -->

                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </main><!-- /.authentication -->
    <!-- ========================================= MAIN : END ========================================= -->
@endsection


@section('scripts')
    <script src="{{ asset('assets/js/jquery_i18n/CLDRPluralRuleParser.js') }}"></script>
    <script src="{{ asset('assets/js/jquery_i18n/jquery.i18n.js') }}"></script>
    <script src="{{ asset('assets/js/jquery_i18n/jquery.i18n.messagestore.js') }}"></script>
    <script src="{{ asset('assets/js/jquery_i18n/jquery.i18n.fallbacks.js') }}"></script>
    <script src="{{ asset('assets/js/jquery_i18n/jquery.i18n.language.js') }}"></script>
    <script src="{{ asset('assets/js/jquery_i18n/jquery.i18n.parser.js') }}"></script>
    <script src="{{ asset('assets/js/jquery_i18n/jquery.i18n.emitter.js') }}"></script>
    <script src="{{ asset('assets/js/jquery_i18n/jquery.i18n.emitter.bidi.js') }}"></script>
    <script src="{{ asset('assets/js/jquery_i18n/main-jquery_i18n.js') }}"></script>
@endsection