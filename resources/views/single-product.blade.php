@extends('layouts.master')

@section('title',$product->title_en)

@section('content')
    @if($product)
        <div id="single-product">
            <div class="container">

                <div class="col-lg-12 no-margin">
                    <div class="breadcrumbArea">
                        <ul>
                            <li><a href="{{route('siteroot')}}" class="lang-switch" data-en="Home" data-bn="নীড়">নীড়</a></li>
                            <li><span class="lang-switch" data-en="Cart" data-bn="ব্যাগ">ব্যাগ</span></li>
                        </ul>
                    </div>
                </div>

                <div class="no-margin col-md-4">
                    <div class="product-thumb">
                        <div class="read-more"></div>
                        <img class="img-responsive" alt="" src="assets/images/blank.gif"
                             data-echo="{{asset(getImageSizeById($product->featured_img_id, 'full'))}}"/>
                    </div><!-- /.single-product-gallery-item -->
                </div><!-- /.gallery-holder -->
                <div class="no-margin col-md-8 body-holder">
                    <div class="body">

                        <div class="title"><h1 class="lang-switch" data-en="{{$product->title_en}}" data-bn="{{$product->title_bn}}">{{$product->title_bn}}</h1></div>
                        <div class="productmeta">
                            <p>
                                <span class="lang-switch label" data-en="Writer:" data-bn="লেখক">লেখক </span>
                                <span class="value">
								@if($product->writers)
                                        @foreach($product->writers as $writer)
                                            <a href="{{url('/writer/'.$writer->id)}}">
                                            <span class="lang-switch" data-en="{{json_decode($writer->name)->en}}" data-bn="{{json_decode($writer->name)->bn}}"> {{json_decode($writer->name)->bn}}</span></a>
                                            @if(!$loop->last)
                                                ,
                                            @endif
                                        @endforeach
                                    @endif
                                </span>
                            </p>
                            <p>
                                <span class="lang-switch label" data-en="Publiser:"
                                      data-bn="প্রকাশক">প্রকাশক</span>
                                <span class="value">
								@if($product->publisher_id)
                                <a href="{{url('/publisher/'.$product->publisher_id)}}">
                                        <span class="lang-switch"
                                              data-en="{{json_decode($product->publishers->name)->en}}"
                                              data-bn="{{json_decode($product->publishers->name)->bn}}"> {{json_decode($product->publishers->name)->bn}}</span></a>
                                @endif
                                </span>
                            </p>
                            <p>
                                <span class="label lang-switch" data-en="Our Price" data-bn="ছাড়ে  মুল্য"> ছাড়ে  মুল্য</span>
                                <span class="value mainPrice lang-digit" data-val="{{ $product->reg_price }}">৳ {{enToBn($product->reg_price)}}</span>
                            </p>
                            <p>
                                <span class="label lang-switch" data-en="Regular Price" data-bn="বাজার মুল্য"> বাজার মুল্য</span>
                                <span class="value regularPrice lang-digit" data-val="{{ $product->dis_price }}">৳ {{enToBn($product->dis_price)}}</span>
                            </p>
                            <p>
                                <span class="label lang-switch" data-en="Product Rating" data-bn="রেটিং">রেটিং</span>
                                <span class="regularPrice">
                                        <span class='starrr' id='starrr' readonly="readonly"></span>
                                        <input type='hidden' name='rating' value='{{$avg}}' id='star2_input' readonly/>
                                </span>
                            </p>
                        </div>

                        <div class="qnt-holder col-md-8">
                            <div class="le-quantity col-md-3">
                                <form>
                                    <a class="minus btn-reduce-item" data-id="{{ $product->id }}"></a>
                                    <input name="quantity" readonly="readonly" type="text" value="1"/>
                                    <a class="plus btn-increase-item" data-id="{{ $product->id }}"></a>
                                </form>
                            </div>
                            <div class=" col-md-9">
                                <div class="">
                                    <a href="#" data-pid="{{$product->id}}"
                                       class="le-button huge lang-switch btn-add-item"
                                       data-bn="ব্যাগে যোগ করুন" data-en="Add to cart">ব্যাগে যোগ করুন</a>
                                </div>
                            </div>
                        </div><!-- /.qnt-holder -->

                    </div><!-- /.body -->

                </div><!-- /.body-holder -->
            </div><!-- /.container -->
        </div><!-- /.single-product -->

        <!-- ========================================= SINGLE PRODUCT TAB ========================================= -->
        <section id="single-product-tab">
            <div class="container">
                <div class="tab-holder">

                    <ul class="nav nav-tabs simple">
                        <li class="active"><a class="lang-switch" href="#description" data-toggle="tab"
                                              data-en="Description" data-bn="বর্ণনা">বর্ণনা</a></li>
                        <li><a class="lang-switch" href="#reviews" data-toggle="tab"
                               data-en="Reviews ( {{$comments->count()}} )" data-bn="মন্তব্য( {{$comments->count()}} )">মন্তব্য( {{enToBn($comments->count())}} )</a></li>
                    </ul><!-- /.nav-tabs -->

                    <div class="tab-content">
                        <div class="tab-pane active" id="description">
                            {{strip_tags($product->description_bn)}}

                        </div><!-- /.tab-pane #description -->

                        <div class="tab-pane" id="reviews">
                            <div class="comments">
                                @foreach($comments as $comment)
                                    <div class="comment-item">
                                        <div class="row no-margin">
                                            <div class="col-lg-1 col-xs-12 col-sm-2 no-margin">
                                                <div class="avatar">
                                                    <img alt="avatar"
                                                         src="{{asset('assets/images/default-avatar.jpg')}}">
                                                </div><!-- /.avatar -->
                                            </div><!-- /.col -->

                                            <div class="col-xs-12 col-lg-11 col-sm-10 no-margin">
                                                <div class="comment-body">
                                                    <div class="meta-info">
                                                        <div class="author inline">
                                                            <a href="#" class="bold">{{$comment->user->username}}</a>
                                                        </div>
                                                    </div><!-- /.meta-info -->
                                                    <p class="comment-text">
                                                        {{$comment->massage}}
                                                    </p><!-- /.comment-text -->
                                                </div><!-- /.comment-body -->

                                            </div><!-- /.col -->

                                        </div><!-- /.row -->
                                    </div><!-- /.comment-item -->
                                @endforeach
                            </div><!-- /.comments -->
                            <div class="add-review row">
                                <div class="col-sm-8 col-xs-12">
                                    @if(Auth::check())
                                        {!! Form::open(['route' => 'book.comment', 'class' => 'contact-form' ,'id'=>'contact-form']) !!}
                                        <div class="new-review-form">
                                            <div class="review">
                                                <h2 class="lang-switch" data-en="Add review" data-bn="রিভিউ দিন">রিভিউ দিন</h2>
                                                <div class='starrr' id='star2'></div>
                                                <input type='hidden' name='rating' value='' id='star2_input'/>

                                            </div>

                                            <input name="product_id" type="hidden" value="{{$product->id}}">


                                            <div class="field-row">
                                                <label class="lang-switch" data-en="your review" data-bn="আপানার রিভিউ">আপানার রিভিউ</label>
                                                <textarea name="comment" rows="8" class="le-input"></textarea>
                                            </div><!-- /.field-row -->

                                            <div class="buttons-holder">
                                                <button type="submit" class="le-button huge lang-switch"
                                                        data-en="submit" data-bn="সাবমিট">সাবমিট
                                                </button>
                                            </div><!-- /.buttons-holder -->


                                        </div><!-- /.new-review-form -->
                                        {!! Form::close() !!}
                                    @else
                                        <span class="lang-switch" data-en="You are not log in. please"
                                              data-bn="আপনি   লগড ইন নাই, দয়া করে">আপনি   লগড ইন নাই, দয়া করে</span> <a href="{{url('/login')}}" class="lang-switch" data-en="Login" data-bn="লগ ইন">লগ ইন</a> <span class="lang-switch"
                                                                                data-en="to continue." data-bn="করুন">করুন</span>
                                    @endif
                                </div><!-- /.col -->
                            </div><!-- /.add-review -->

                        </div><!-- /.tab-pane #reviews -->
                    </div><!-- /.tab-content -->

                </div><!-- /.tab-holder -->
            </div><!-- /.container -->
        </section><!-- /#single-product-tab -->
        <!-- ========================================= SINGLE PRODUCT TAB : END ========================================= -->
        @if($product->medias->count()> 0)
            <div class="read-once-shdaw"></div>
            <div class="read-once-warp">
                <div class="inner-once">
                    <ul>
                        @foreach($product->medias as $media)
                            <li><img src="{{asset($media->source)}}" alt=""></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    @endif
@endsection


@section('scripts')
    <!-- JavaScripts placed at the end of the document so the pages load faster -->
    <script src="{{ asset('assets/js/buttons.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/js/starrr.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".read-once-shdaw").click(function () {
                $('.read-once-shdaw').fadeOut();
                $('.read-once-warp').fadeOut();
                $('body').css({'overflow': 'auto'});
            });
            $(".product-thumb").click(function () {
                $('body').css({'overflow': 'hidden'});
                $('.read-once-shdaw').fadeIn();
                $('.read-once-warp').fadeIn();
            });
            $("#star .stars").click(function () {
                var label = $("label[for='" + $(this).attr('id') + "']");
                $("#feedback").text(label.attr('title'));
                $(this).attr("checked");
            });
            $('.star').hover(function (e) {
                e.preventDefault();
                $(this).children().css('color', '#25960c');
                $(this).prevAll().children().css('color', '#25960c');
                $(this).nextAll().children().css('color', '#ddd');
                var ind = $(this).index() + 1;
                $(this).closest('div').find('input').val(ind);
            });
            var $s2input = $('#star2_input');
            $('#star2').starrr({
                max: 5,
                rating: $s2input.val(),
                change: function (e, value) {
                    $s2input.val(value).trigger('input');
                }
            });
            var curentRet = $("#star2_input").val();
            $('#starrr').starrr({
                rating: curentRet,
                readOnly: true
            })
        });
    </script>
@endsection
