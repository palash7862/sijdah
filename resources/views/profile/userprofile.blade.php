@extends('layouts.master')

@section('title','profile')

@section('content')
    <div class="animate-dropdown">
        <!-- ========================================= BREADCRUMB ========================================= -->

        <!-- ========================================= BREADCRUMB : END ========================================= -->
    </div>

    <!-- ========================================= CONTENT ========================================= -->
    <section id="profile-page">
        <div class="container">
            <div class="row profile">
                <div class="col-md-3">
                    <div class="profile-sidebar">

                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li class="active">
                                    <a href="{{ route('user.profile')}}">
                                        <i class="glyphicon glyphicon-home"></i>
                                        Account Overview </a>
                                </li>
                                <li >
                                    <a href="{{ route('change.password')}}">
                                        <i class="glyphicon glyphicon-user"></i>
                                        Change password </a>
                                </li>
                                <li>
                                    <a href="{{ route('user.orders')}}">
                                        <i class="glyphicon glyphicon-ok"></i>
                                        Order History </a>
                                </li>

                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="profile-content">
                            <div class="health-post-wapper">
                                <div class="main-content">
                                    <div class="panel panel-default user-panel">
                                        <div class="panel-heading">
                                            <h3 class="profile-your">Your Profile</h3>
                                            <h3 class="edit-profile-url">
                                                <a href="{{route('edit.profile')}}">Edit</a>
                                            </h3>
                                        </div>

                                        <div class="panel-body">
                                            <div class="profile-detiels">
                                                <table class="table">
                                                    <tr>
                                                        <td><h5>User Name</h5></td>
                                                        <td>{{Auth::user()->username}}</td>
                                                    </tr>

                                                    <tr>
                                                        <td><h5>Name</h5></td>
                                                        <td>{{Auth::user()->name}}</td>
                                                    </tr>

                                                    <tr>
                                                        <td><h5>Your Email</h5></td>
                                                        <td>{{Auth::user()->email}}</td>
                                                    </tr>

                                                    <tr>
                                                        <td><h5>User Phone</h5></td>
                                                        <td>{{Auth::user()->phone}}</td>
                                                    </tr>

                                                    <tr>
                                                        <td><h5>User Address</h5></td>
                                                        <td>{{Auth::user()->address}}</td>
                                                    </tr>
                                                </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /# -->
    <!-- ========================================= CONTENT : END ========================================= -->


@endsection