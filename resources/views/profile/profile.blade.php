@extends('layouts.master')

@section('title','profile')

@section('content')
    <div class="animate-dropdown">
        <!-- ========================================= BREADCRUMB ========================================= -->

        <!-- ========================================= BREADCRUMB : END ========================================= -->
    </div>

    <!-- ========================================= CONTENT ========================================= -->
    <section id="profile-page">
        <div class="container">
            <div class="row profile">
                <div class="col-md-3">
                    <div class="profile-sidebar">

                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li class="active">
                                    <a href="{{ route('user.profile')}}">
                                        <i class="glyphicon glyphicon-home"></i>
                                        Account Overview </a>
                                </li>
                                <li >
                                    <a href="{{ route('change.password')}}">
                                        <i class="glyphicon glyphicon-user"></i>
                                        Change password </a>
                                </li>
                                <li>
                                    <a href="{{ route('user.orders')}}">
                                        <i class="glyphicon glyphicon-ok"></i>
                                        Order History </a>
                                </li>

                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="profile-content">
                        Some user related content goes here...
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /# -->
    <!-- ========================================= CONTENT : END ========================================= -->


@endsection