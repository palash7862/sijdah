@extends('layouts.master')

@section('title','profile')

@section('content')
    <div class="animate-dropdown">
        <!-- ========================================= BREADCRUMB ========================================= -->

        <!-- ========================================= BREADCRUMB : END ========================================= -->
    </div>

    <!-- ========================================= CONTENT ========================================= -->
    <section id="profile-page">
        <div class="container">
            <div class="row profile">
                <div class="col-md-3">
                    <div class="profile-sidebar">

                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li>
                                    <a href="{{ route('user.profile')}}">
                                        <i class="glyphicon glyphicon-home"></i>
                                        Account Overview </a>
                                </li>
                                <li>
                                    <a href="{{ route('change.password')}}">
                                        <i class="glyphicon glyphicon-user"></i>
                                        Change Password </a>
                                </li>
                                <li class="active">
                                    <a href="{{ route('user.orders')}}">
                                        <i class="glyphicon glyphicon-ok"></i>
                                        Order History </a>
                                </li>

                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="profile-content">
                        <div class="health-post-wapper">
                            <div class="main-content">
                                <div class="panel panel-default user-panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Orders List</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="profile-detiels">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Amount(in Tk)</th>
                                                        <th>Date</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($orders as $key => $order)
                                                    <tr>
                                                        <td>{{ $key+1 }}</td>
                                                        <td>{{ $order->total_price}}</td>
                                                        <td>{{ $order->created_at}}</td>
                                                        <td>{{ $order->status}}</td>
                                                        <td>
                                                            <a href="{{url('/user/order/'.$order->id)}}">
                                                                View Detail
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /# -->
    <!-- ========================================= CONTENT : END ========================================= -->


@endsection