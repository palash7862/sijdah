@extends('layouts.master')

@section('title','profile')

@section('content')
    <div class="animate-dropdown">
        <!-- ========================================= BREADCRUMB ========================================= -->

        <!-- ========================================= BREADCRUMB : END ========================================= -->
    </div>

    <!-- ========================================= CONTENT ========================================= -->
    <section id="profile-page">
        <div class="container">
            <div class="row profile">
                <div class="col-md-3">
                    <div class="profile-sidebar">

                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li >
                                    <a href="{{ route('user.profile')}}">
                                        <i class="glyphicon glyphicon-home"></i>
                                        Account Overview </a>
                                </li>
                                <li class="active">
                                    <a href="{{ route('change.password')}}">
                                        <i class="glyphicon glyphicon-user"></i>
                                        Change password </a>
                                </li>
                                <li>
                                    <a href="{{ route('user.orders')}}">
                                        <i class="glyphicon glyphicon-ok"></i>
                                        Order History </a>
                                </li>

                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="profile-content">
                        <div class="health-post-wapper">
                            <div class="main-content">
                                <div class="panel panel-default user-panel">
                                    <div class="panel-heading">
                                        <h3 class="profile-your" align="center">Change Password</h3>
                                    </div>

                                    <div class="panel-body">
                                        {!! Form::open(['route' => 'update.password', 'class' => 'profile-update-form']) !!}
                                        <div class="profile-detiels">
                                            <table class="table">
                                                <tr>
                                                    <td><h5>Current Password</h5></td>
                                                    <td><input type="text" name="current_password"></td>
                                                </tr>

                                                <tr>
                                                    <td><h5>New Password</h5></td>
                                                    <td><input type="text" name="new_password"></td>
                                                </tr>

                                                <tr>
                                                    <td><h5>Confirm Password</h5></td>
                                                    <td><input type="text" name="confirm_password"></td>
                                                </tr>

                                                <tr>
                                                    <td></td>
                                                    <td><input type="submit" name="submit" value="submit"></td>
                                                </tr>
                                            </table>

                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /# -->
    <!-- ========================================= CONTENT : END ========================================= -->

@endsection