@extends('layouts.master')

@section('title','Shop')

@section('content')

<section id="category-grid">
    <div class="container">
        <div class="col-lg-12 no-margin">
            <div class="breadcrumbArea">
                <ul>
                    <li><a href="{{route('siteroot')}}">Home</a></li>
                    @if($writer)
                        <li><span class="lang-switch" data-en="{{json_decode($writer->name)->en}}" data-bn="{{json_decode($writer->name)->bn}}"> {{json_decode($writer->name)->bn}}</span></li>
                    @else
                        <li><span class="lang-switch" data-en="Writer" data-bn="লেখক">লেখক</span></li>
                    @endif
                </ul>
            </div>
        </div>
        <!-- ========================================= SIDEBAR ========================================= -->
        <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">

            <!-- ========================================= PRODUCT FILTER ========================================= -->
            <!-- <div class="widget">
                <h1>Product Filters</h1>
                <div class="body bordered">
                    <div class="price-filter">
                        <h2>Price</h2>
                        <hr>
                        <div class="price-range-holder">

                            <input type="text" class="price-slider" data-slider-min="0" data-slider-max="10000" data-slider-step="10" value="" >

                            <span class="min-max" id="price-range">
                                Price: ৳ 0 - ৳10000
                            </span>
                            <span class="filter-button">
                                <a href="#">Filter</a>
                            </span>
                        </div>
                    </div><!-- /.price-filter --

                    <div class="category-filter">
                        <h2>Type</h2>
                        <hr>
                        <ul>
                            <li>
                                <input class="le-checkbox" name="type[]" type="checkbox" value="" checked="checked"><label>All</label>
                            </li>
                            <li>
                                <input class="le-checkbox p_type" name="type[]" type="checkbox" value="Book"><label>Book</label>
                            </li>
                            <li>
                                <input class="le-checkbox p_type" name="type[]" type="checkbox" value="Clothing"><label>Cloth</label>
                            </li>
                            <li>
                                <input class="le-checkbox p_type" name="type[]" type="checkbox" value="FoodGroceries"> <label>Food</label>
                            </li>
                        </ul>
                    </div>
                    <div class="category-filter">
                        <h2>Rating</h2>
                        <hr>
                        <ul>
                            <li><input class="le-radio" name="rating" type="radio" value="0" checked="checked">Any</li>
                            <li><input class="le-radio p_rating" name="rating" type="radio" value="5">5.00</li>
                            <li><input class="le-radio p_rating" name="rating" type="radio" value="4">above 4.00</li>
                            <li><input class="le-radio p_rating" name="rating" type="radio" value="3">above 3.00</li>
                            <li><input class="le-radio p_rating" name="rating" type="radio" value="2">above 2.00</li>
                            <li><input class="le-radio p_rating" name="rating" type="radio" value="1">above 1.00</li>
                        </ul>
                    </div>

                </div>
            </div -->>
        <!-- ================== PRODUCT FILTER : END ==================== -->

        <!-- ======================== CATEGORY TREE ============================ -->
            <div class="widget accordion-widget category-accordions">
                <h1 class="border lang-switch" data-en="All Categories" data-bn="সমস্ত  ক্যাটাগরি সমুহ">সমস্ত  ক্যাটাগরি সমুহ</h1>
                <div class="accordion">


                    @foreach($categories as $category)  
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle lang-switch" data-toggle="collapse" href="#collapse_{{$category->id}}" data-en="{{ $category->name_en}}" data-bn="{{ $category->name_bn}}">
                                {{ $category->name_bn}}
                            </a>
                        </div>
                        @if(count($category->childs))
                            @include('partial.tree',['childs' => $category->childs])
                        @endif
                    </div><!-- /.category-accordions -->
                    @endforeach
                </div><!-- /.accordion -->
            </div><!-- /.category-accordions -->
            <!-- =================== CATEGORY TREE : END ================== -->
        </div>
<!-- ===================== SIDEBAR : END ========================================= -->

<!-- ===================== CONTENT : START ===================== -->
        <div class="col-xs-12 col-sm-9 no-margin wide sidebar">
            <section id="gaming">
                <div class="grid-list-products">
                    <div class="info-box">
                        <div class="thumb"></div>
                        @if($writer)
                            <h3 class="lang-switch" data-en="{{json_decode($writer->name)->en}}" data-bn="{{json_decode($writer->name)->bn}}"> {{json_decode($writer->name)->bn}}</h3>
                            <p>{{$writer->description}}</p>
                        @endif
                    </div>
                    <div class="tab-content">
                        <div id="grid-view" class="products-grid fade tab-pane in active">
                            <div class="product-grid-holder">
                                <div class="row no-margin" id="search-res-container">
                                    @if($products != false)
                                        @foreach($products as $product)
                                            <div class="product-single">
                                                <div class="box-shadow"></div>
                                                <div class="product-element-top">
                                                    <a href="{{route('product.single', $product->id)}}">
                                                        <div class="product-labels labels-rectangular">
                                                            <span class="onsale product-label lang-digit" data-val="{{(int)(($product->dis_price-$product->reg_price)*100/$product->reg_price)}}%">{{enToBn((int)(($product->dis_price-$product->reg_price)*100/$product->reg_price))}}%</span>
                                                        </div>
                                                        <img src="{{asset(getImageSizeById($product->featured_img_id, 'full'))}}" width="430" height="490" alt="">
                                                    </a>

                                                    @php
                                                        if($product->product_type == 'Book'){
                                                            echo '<div class="writer-names">';
                                                            $writers = App\Product::find($product->id)->writers;
                                                            foreach($writers as $writer){
                                                            echo '<a href="'.url("/writer/".$writer->id).'"><span class="lang-switch" data-en="'.json_decode($writer->name)->en.'" data-bn="'.json_decode($writer->name)->bn.'">'.json_decode($writer->name)->bn.'</span></a>';
                                                            }
                                                            echo '</div>';
                                                        }
                                                    @endphp
                                                </div>
                                                <div class="product-information">
                                                    <h3 class="product-title lang-switch" data-bn="{{$product->title_bn}}" data-en="{{$product->title_en}}">
                                                        <a href="{{route('product.single', $product->id)}}">{{$product->title_bn}}</a>
                                                    </h3>
                                                    <div class="product-rating-price">
                                                        <div class="wrapp-product-price">
                                                            <div class="star-rating">
                                                                <span style="width:80%">Rated <strong class="rating">4.00</strong> out of 5</span>
                                                            </div>
                                                            <span class="price">
                                                                <span class="lang-digit" data-val="৳ {{$product->reg_price}}">৳ {{enToBn($product->reg_price)}}</span>
                                                                <strong class="lang-digit" data-val="৳ {{$product->dis_price}}">৳ {{enToBn($product->dis_price)}}</strong>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="fade-in-block">
                                                        <a href="" data-pid="{{$product->id}}" class="lang-switch btn-add-item cart-button" data-bn="ব্যাগে যোগ করুন" data-en="Add to cart">ব্যাগে যোগ করুন</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif

                                </div><!-- /.row -->
                            </div><!-- /.product-grid-holder -->

                            <div class="pagination-holder">
                                <div class="row">
                                    @if($products != false)
                                    <div class="col-xs-12 col-sm-6 text-left">
                                        {{ $products->links() }}
                                    </div>

                                    <div class="col-xs-12 col-sm-6">
                                        <div class="result-counter">
                                            <span class="lang-switch" data-en="Showing" data-bn="দেখানো হচ্ছে">দেখানো হচ্ছে</span> <span class="lang-digit" data-val="{{$products->firstItem()}}-{{$products->lastItem()}}">{{enToBn($products->firstItem())}} - {{enToBn($products->lastItem())}}</span> <span class="lang-switch" data-en="of" data-bn="/">/</span> <span class="lang-digit" data-val="{{ $products->total()}}">{{enToBn($products->total())}}</span> <span class="lang-switch" data-en="results" data-bn="পণ্য">পণ্য</span>
                                        </div>
                                    </div>
                                    @endif
                                </div><!-- /.row -->
                            </div><!-- /.pagination-holder -->
                        </div><!-- /.products-grid #grid-view -->
                    </div><!-- /.tab-content -->
                </div><!-- /.grid-list-products -->

            </section><!-- /#gaming -->
        </div><!-- /.col -->
        <!-- ========================================= CONTENT : END ========================================= -->
    </div><!-- /.container -->
</section><!-- /#category-grid -->
@endsection
@section('scripts')
<script>
const curl = window.location.origin;
var price_range = "";
var type = new Set();
var rating = "";
var sortby=0;
var pagination=0;

$('.price-slider').on("slideStop",function(){
    price_range= $('.price-slider').val();
    $('#price-range').text('Price: ৳ ' + price_range.replace(',',' - ৳ '));
    apply_filter();
});

$('.p_type').on("change",function(){
    type.clear();
    $('.p_type:checked').each(function(v) {
        type.add(this.value);
    });
    apply_filter();
});

$('.p_rating').on("change",function(){
    rating=$(".p_rating:checked" ).val();
    apply_filter();
});

$('#sortby').on("change",function(){
    sortby=$("#sortby").val();
    apply_filter();
});
$('#pagination').on("change",function(){
    pagination=$("#pagination").val();
    apply_filter();
});

function apply_filter()
{
    url = window.location.origin + "/search/?";
    if(price_range!="")
        url += 'pricebetween='+price_range+'&';
    if(type.size != 0)
    {
        for (var v of type) {
          url += 'type[]='+v+'&';
        }
    }
    if(rating!="")
        url += 'rating='+rating+'&';
    if(sortby)
        url += 'orderby='+sortby+'&';
    if(pagination)
        url += 'pagination='+pagination+'&';

    //console.log(url);

    $.ajax({
        type: "GET",
        dataType: 'JSON',
        url: url,
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#search-res-container').empty();
     
            $.each(data,function(key,val){

                var discount = parseInt((val.dis_price-val.reg_price)*100/val.reg_price);
                var wr="";

                if(val.product_type=='Book'){
                    var writers = val.writersName.split("|");
                    var writersId = val.writersId.split("|");
                    wr += '<div class="writer-names">';
                    
                    for(i=0;i<writers.length;i++)
                    {
                        if(i>0)
                            wr += ", ";

                        wr += '<a href="'+curl+'/writer/'+ writersId[i]+'"><span class="lang-switch" data-en="'+writers[i].en +'" data-bn="'+writers[i].bn +'">'+JSON.parse(writers[i]).en +'</span></a>';    
                    }
                    wr += '</div>';
                }  

                $('#search-res-container').append(
                '<div class="product-single">'+
                '<div class="box-shadow"></div>'+
                '<div class="product-element-top">'+
                '<a href="'+curl+'/product/'+val.id +'">'+
                '<div class="product-labels labels-rectangular">'+
                '<span class="onsale product-label lang-digit" data-val="'+discount+'%">'+discount+'%</span>'+
                '</div>'+
                '<img src="'+ val.source+'" width="430" height="490" alt="">'+
                '</a>'+
                wr +
        
                '</div>'+
                '<div class="product-information">'+
                '<h3 class="product-title lang-switch" data-bn="'+val.title_bn+'" data-en="'+val.title_en+'">'+
                '<a href="'+curl+'/product/'+val.id+'">'+val.title_en+'</a>'+
                '</h3>'+
                '<div class="product-rating-price">'+
                '<div class="wrapp-product-price">'+
                '<div class="star-rating">'+
                '<span style="width:80%">Rated <strong class="rating">4.00</strong> out of 5</span>'+
                '</div>'+
                '<span class="price">'+
                '<span class="lang-digit" data-val="৳ '+ val.reg_price +'">৳ '+ val.reg_price +'</span>'+
                '<strong class="lang-digit" data-val="৳ '+ val.dis_price +'">৳ '+ val.dis_price +'</strong>'+
                '</span>'+
                '</div>'+
                '</div>'+

                '<div class="fade-in-block">'+
                '<a href="" data-pid="'+ val.id+'" class="lang-switch btn-add-item cart-button" data-bn="ব্যাগে যোগ করুন" data-en="Add to cart">Add to cart</a>'+
                '</div>'+
                '</div>'+
                '</div>'
                );
            });

            update_language();

        },
        error: function(req, status, err) {
            swal({
                type: 'error',
                title:'Oops...',
                text: err,
            });
        }
    });
}

</script>
@endsection
