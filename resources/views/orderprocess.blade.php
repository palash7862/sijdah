<form id="sslcommerz_form" class="hidden" action="https://sandbox.sslcommerz.com/gwprocess/v3/process.php" method="post">
    <input name="store_id" readonly="" value="test_thyrocarebd" type="hidden">
    <input name="total_amount" readonly="" value="{{$totalAmount}}" type="hidden">
    <input name="tran_id" readonly="" value="{{$tranId}}" type="hidden">

    <input name="success_url" readonly="" value="{{route('pay.val.front')}}" type="hidden">
    <input name="fail_url" readonly="" value="{{route('pay.faild.front')}}" type="hidden">
    <input name="cancel_url" readonly="" value="{{route('pay.cancel.front')}}" type="hidden">
</form>
<script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery-migrate-1.4.1.min.js') }}"></script>
<script type="text/javascript">
    window.onload = function(){
        document.forms['sslcommerz_form'].submit();
    }
</script>