@extends('layouts.master')

@section('title','Checkout')

@section('content')
<!-- ========================================= CONTENT ========================================= -->
<section id="checkout-page">
    <div class="container">
        <div class="col-lg-12 no-margin">
            <div class="breadcrumbArea">
                <ul>
                    <li><a href="{{route('siteroot')}}">Home</a></li>
                    <li><a href="{{url('/cart')}}">Cart</a></li>
                    <li><span>Checkout</span></li>
                </ul>
            </div>
        </div>
        {!! Form::open(['route' => 'order.create', 'class'=>'form-horizontal form-bordered checkoutForm' ]) !!}
        <div class="col-xs-12 no-margin">
            <div class="col-xs-12 col-md-6">
                <div class="billing-address">
                @if(Auth::user() && $user = Auth::user())
                    @if(!empty($user->billing_country) && !empty($user->billing_city) &&
                    !empty($user->billing_area) && !empty($user->billing_postcode) &&
                    !empty($user->billing_address) && !empty($user->phone))
                            <input type="hidden" name="billingAddress" value="1">
                        @else
                            <input type="hidden" name="billingAddress" value="0">
                        @endif
                    <h2 class="border h1">billing address</h2>
                    <div class="row field-row">
                        <div class="col-xs-12">
                            <label>Full Name*</label>
                            <input class="le-input" value="{{$user->username}}" >
                        </div>
                    </div><!-- /.field-row -->

                    <div class="row field-row">
                        <div class="col-xs-12">
                            <label>Country</label>
                            @php
                                $country = ['Australia', 'Bangladesh', 'United States']
                            @endphp
                            <select name="billingCountry" id="selectCountry" class="le-input" >
                                <option>Select Country</option>
                                @foreach($country as $cty)
                                    @if($cty == trim($user->billing_country))
                                        <option value="{{$cty}}" selected>{{$cty}}</option>
                                    @else
                                        <option value="{{$cty}}">{{$cty}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div><!-- /.field-row -->

                    <div class="row field-row">
                        <div class="col-xs-12 col-sm-6">
                            <label>City</label>
                            <input class="le-input" name="billingCity" value="{{$user->billing_city}}" >
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <label>Area</label>
                            <input class="le-input" name="billingArea" value="{{$user->billing_area}}" >
                        </div>
                    </div><!-- /.field-row -->

                    <div class="row field-row">
                        <div class="col-xs-12 col-sm-6">
                            <label>Post Code / Zip*</label>
                            <input class="le-input" name="billingPostcode" value="{{$user->billing_postcode}}" >
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <label>Phone Number*</label>
                            <input class="le-input" name="billingPhone" value="{{$user->phone}}">
                        </div>
                    </div><!-- /.field-row -->
                    <div class="row field-row">
                        <div class="col-xs-12">
                            <label>Address*</label>
                            <textarea class="le-input" name="billingAddress" >{{$user->billing_address}}</textarea>
                        </div>
                    </div><!-- /.field-row -->
                @endif
            </div><!-- /.billing-address -->
            </div>
            <div class="col-xs-12 col-md-6 ">
                <section id="shipping-address">
                <h2 class="border h1">shipping address</h2>
                    <div class="row field-row">
                        <div class="col-xs-12">
                            <input  class="le-checkbox big" type="checkbox" name="addressType"  />
                            <a class="simple-link bold" href="#">ship to different address?</a>
                        </div>
                    </div><!-- /.field-row -->
                    <div class="ship-address-block">
                        <div class="row field-row">
                            <div class="col-xs-12">
                                <label>Recive Parson Name*</label>
                                <input class="le-input" name="shipParson" >
                            </div>
                        </div><!-- /.field-row -->
                        <div class="row field-row">
                            <div class="col-xs-12">
                                <label>Country</label>
                                @php
                                    $country = ['Australia', 'Bangladesh', 'United States']
                                @endphp
                                <select name="billingCountry" id="selectCountry" class="le-input" >
                                    <option>Select Country</option>
                                    @foreach($country as $cty)
                                            <option value="{{$cty}}">{{$cty}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div><!-- /.field-row -->
                        <div class="row field-row">
                            <div class="col-xs-12 col-sm-6">
                                <label>City</label>
                                <input class="le-input" name="shipCity" >
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label>Area</label>
                                <input class="le-input" name="shipArea" >
                            </div>
                        </div><!-- /.field-row -->
                        <div class="row field-row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Post Code / Zip*</label>
                                <input class="le-input" name="shipPostcode" >
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label>Phone Number*</label>
                                <input class="le-input" name="shipPhone">
                            </div>
                        </div><!-- /.field-row -->
                        <div class="row field-row">
                            <div class="col-xs-12">
                                <label>Address*</label>
                                <textarea class="le-input" name="shipAddress" ></textarea>
                            </div>
                        </div><!-- /.field-row -->
                    </div><!-- /.billing-address -->
            </section><!-- /#shipping-address -->
            </div>
        </div>
        <div class="col-xs-12 no-margin">

            <div id="payment-method-options">

                    <div class="payment-method-option">
                        <input class="le-radio" type="radio" name="paymentType" value="cash">
                        <div class="radio-label bold ">Cash On Dalivery</div>
                    </div><!-- /.payment-method-option -->

                    <div class="payment-method-option">
                        <input class="le-radio" type="radio" name="paymentType" value="online" checked>
                        <div class="radio-label bold ">Online Payment</div>
                    </div><!-- /.payment-method-option -->
            </div><!-- /#payment-method-options -->

            <div class="place-order-button">
                <button class="le-button big formSubmit">place order</button>
            </div><!-- /.place-order-button -->
        </div><!-- /.col -->
        {!! Form::close() !!}
    </div><!-- /.container -->
</section><!-- /#checkout-page -->
<!-- ========================================= CONTENT : END ========================================= -->
@endsection
@section('scripts')
<script type="text/javascript">
    (function($) {
        var countrys = [
            {name: 'Australia', code: 'AU'},
            {name: 'Bangladesh', code: 'BD'},
            {name: 'United States', code: 'US'},
        ]

//    if($('#selectCountry').length){
//        var activeVal = $(this).data('val');
//        var countryHtml = '<option>Select Country</option>';
//        for (var i=0; i<countrys.length;i++){
//            if(activeVal == countrys[i].name){
//                console.log(activeVal);
//                console.log(countrys[i].name);
//                countryHtml += '<option value="'+countrys[i].name+'" checked>'+countrys[i].name+'</option>';
//            } else {
//                countryHtml += '<option value="'+countrys[i].name+'">'+countrys[i].name+'</option>';
//            }
//        }
//        $('#selectCountry').append(countryHtml);
//        $('.selectCountry').append(countryHtml);
//    }
        $(".checkoutForm").on('submit', function(e){
            var address =  true;
            if($('input[name=addressType]').is(':checked')){

                $('.ship-address-block, .billing-address').find('input, select, textarea').each(function (index, value) {
                    if( ($.trim($(value).val().toLowerCase())== '') ||
                        ($.trim($(value).val().toLowerCase())=='select country') ){
                        address = false;
                        console.log($(value).val().toLowerCase());
                    }
                });
                if(address !== true){
                    e.preventDefault();
                }
            }else{
                $('.billing-address').find('input, select, textarea').each(function (index, value) {
                    if( ($.trim($(value).val().toLowerCase())== '') ||
                        ($.trim($(value).val().toLowerCase())=='select country') ){
                        address = false;
                        console.log($(value).val().toLowerCase());
                    }
                });
                if(address !== true){
                    e.preventDefault();
                }
            }
        });

    })(jQuery);
</script>
@endsection
