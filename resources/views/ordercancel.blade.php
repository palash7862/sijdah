@extends('layouts.master')

@section('title','Checkout')

@section('content')
    
    <!-- ========================================= CONTENT ========================================= -->
    <section id="checkout-page">
        <div class="container">
            <div class="col-xs-12 no-margin">
                <h1 class="lang-switch" data-en="Your order has been canceled! Try again!!" data-bn="আপনার অর্ডার ব্যার্থ হয়েছে ! আবার   চেস্টা করুন" >আপনার অর্ডার ব্যার্থ হয়েছে ! আবার   চেস্টা করুন</h1>
            </div>
        </div><!-- /.container -->
    </section><!-- /#checkout-page -->
    <!-- ========================================= CONTENT : END ========================================= -->
@endsection
@section('scripts')

@endsection
