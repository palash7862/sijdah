<!DOCTYPE html>
@php
$phone = App\Setting::where('name','phone')->first();
$sitename = App\Setting::where('name','sitename')->first();
$favicon = App\Setting::where('name','favicon')->first();
$logo = App\Setting::where('name','logo')->first();
$address_en = App\Setting::where('name','address_en')->first();
$address_bn = App\Setting::where('name','address_bn')->first();

$facebook = App\Setting::where('name','facebook')->first();
$twitter = App\Setting::where('name','twitter')->first();
$linkedin = App\Setting::where('name','linkedin')->first();
$pinterest = App\Setting::where('name','pinterest')->first();
$dribbble = App\Setting::where('name','dribbble')->first();
$stumbleupon = App\Setting::where('name','stumbleupon')->first();
$vk = App\Setting::where('name','vk')->first();

$footerWidget_1 = App\Setting::where('name','footerWidget_1')->first();
$footerWidget_2 = App\Setting::where('name','footerWidget_2')->first();
$footerWidget_3 = App\Setting::where('name','footerWidget_3')->first();

@endphp


@if(session()->exists('lang'))
    <html lang="{{session('lang')}}">
@else
    <html lang="bn">
@endif

@include('partial.head')

<body>
    <!-- Top bar -->
    <div class="top-banner" id="top-banner" style="display: none;">
        <img src="{{ asset('assets/images/cross.png') }}" style="height: 40px;" align="right" id="top-banner-cross">
    </div><!-- /Top bar -->
    @include('partial.mobilenav')
    <div class="wrapper">

        @include('partial.header')

        <!-- category show -->
    
        @yield('content')
        <!-- category show end -->

        @include('partial.footer')
    </div><!-- /.wrapper -->



        <!-- JavaScripts placed at the end of the document so the pages load faster -->
        <script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery-migrate-1.4.1.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.menu-aim.js') }}"></script>

        <script src="{{ asset('assets/js/bootstrap-hover-dropdown.min.js') }}"></script>
        <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('assets/js/css_browser_selector.min.js') }}"></script>
        <script src="{{ asset('assets/js/echo.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.easing-1.3.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap-slider.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.raty.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.prettyPhoto.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.customSelect.min.js') }}"></script>
        <script src="{{ asset('assets/js/wow.min.js') }}"></script>

        <script src="{{ asset('assets/js/scripts.js') }}"></script>

        <!-- counterup js-->
        <script src="{{ asset('assets/product_slider/js/jquery.countdown.js') }}"></script>
        <!--main js-->
        <script src="{{ asset('assets/product_slider/js/main.js') }}"></script>

        <!-- Sweet Alart js-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <!--Custom js-->

        <!-- Language Switch Js -->
        <script src="{{ asset('assets/js/language-switch.js') }}"></script>
        <!-- Add to Cart Js -->
        <script src="{{ asset('assets/js/live-search.js') }}"></script>
        <!-- Live search Js -->
        <script src="{{ asset('assets/js/add-to-cart.js') }}"></script>
        @yield('scripts')
    </body>
</html>
