@extends('layouts.master')

@section('title','Cart')

@section('styles')
    <style type="text/css">
        #cupon-widget .placeholder {
            text-transform: none
        }
    </style>
@endsection

@section('content')
    <section id="cart-page">
        <div class="container">
            <div class="col-lg-12 no-margin">
                <div class="breadcrumbArea">
                    <ul>
                        <li><a href="{{route('siteroot')}}" class="lang-switch" data-en="Home" data-bn="নীড়">নীড়</a></li>
                        <li><span class="lang-switch" data-en="Cart" data-bn="ব্যাগ">ব্যাগ</span></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-12 no-margin mialstone">
                <ul class="{{\Cart::count()}}"> 
                    @if( (app('request')->input('stap') == 1) && (\Cart::count() > 0))
                        <li class="stap_done"><a href="{{route('siteroot')}}" class="lang-switch" data-en="01 . Cart" data-bn="ব্যাগ">ব্যাগ</a></li>
                        <li><span class="lang-switch" data-en="02 . Sign In" data-bn="সাইন ইন">সাইন ইন</span></li>
                        <li><span class="lang-switch" data-en="03 . Address" data-bn="অর্ডার ঠিকানা">অর্ডার ঠিকানা</span></li>
                        <li><span class="lang-switch" data-en="04 . Payment" data-bn="মূল্য পরিশোধ">মূল্য পরিশোধ</span></li>
                        <li><span class="lang-switch" data-en="05 . Order Status" data-bn="অর্ডার">অর্ডার</span></li>
                    @elseif(app('request')->input('stap') == 3)
                        <li class="stap_done"><a href="{{route('siteroot')}}" class="lang-switch" data-en="01 . Cart" data-bn="ব্যাগ">ব্যাগ</a></li>
                        <li class="stap_done"><span class="lang-switch" data-en="02 . Sign In" data-bn="সাইন ইন">সাইন ইন</span></li>
                        <li><span class="lang-switch" data-en="03 . Address" data-bn="অর্ডার ঠিকানা">অর্ডার ঠিকানা</span></li>
                        <li><span class="lang-switch" data-en="04 . Payment" data-bn="মূল্য পরিশোধ">মূল্য পরিশোধ</span></li>
                        <li><span class="lang-switch" data-en="05 . Order Status" data-bn="অর্ডার">অর্ডার</span></li>
                    @elseif(app('request')->input('stap') ==4 )
                            <li class="stap_done"><a href="{{route('siteroot')}}" class="lang-switch" data-en="01 . Cart" data-bn="ব্যাগ">ব্যাগ</a></li>
                            <li class="stap_done"><span class="lang-switch" data-en="02 . Sign In" data-bn="সাইন ইন">সাইন ইন</span></li>
                            <li class="stap_done"><span class="lang-switch" data-en="03 . Address" data-bn="অর্ডার ঠিকানা">অর্ডার ঠিকানা</span></li>
                            <li><span class="lang-switch" data-en="04 . Payment" data-bn="মূল্য পরিশোধ">মূল্য পরিশোধ</span></li>
                            <li><span class="lang-switch" data-en="05 . Order Status" data-bn="অর্ডার">অর্ডার</span></li>
                    @elseif( (app('request')->input('stap') ==5) && (\Session::has('OrderPlace') == true) )
                        <li class="stap_done"><a href="{{route('siteroot')}}" class="lang-switch" data-en="01 . Cart" data-bn="ব্যাগ">ব্যাগ</a></li>
                        <li class="stap_done"><span class="lang-switch" data-en="02 . Sign In" data-bn="সাইন ইন">সাইন ইন</span></li>
                        <li class="stap_done"><span class="lang-switch" data-en="03 . Address" data-bn="অর্ডার ঠিকানা">অর্ডার ঠিকানা</span></li>
                        <li class="stap_done"><span class="lang-switch" data-en="04 . Payment" data-bn="মূল্য পরিশোধ">মূল্য পরিশোধ</span></li>
                        <li class="stap_done"><span class="lang-switch" data-en="05 . Order Status" data-bn="অর্ডার">অর্ডার</span></li>
                    @else
                        @if(\Cart::count() > 0 )
                        <li class="stap_done"><span class="lang-switch" data-en="01 . Cart" data-bn="ব্যাগ">ব্যাগ</span></li>  
                        @else
                        <li><span class="lang-switch" data-en="01 . Cart" data-bn="ব্যাগ">ব্যাগ</span></li>  
                        @endif
                        <li><span class="lang-switch" data-en="02 . Sign In" data-bn="সাইন ইন">সাইন ইন</span></li>
                        <li><span class="lang-switch" data-en="03 . Address" data-bn="অর্ডার ঠিকানা">অর্ডার ঠিকানা</span></li>
                        <li><span class="lang-switch" data-en="04 . Payment" data-bn="মূল্য পরিশোধ">মূল্য পরিশোধ</span></li>
                        <li><span class="lang-switch" data-en="05 . Order Status" data-bn="অর্ডার">অর্ডার</span></li>
                    @endif
                </ul>
            </div>
            <!-- ========================================= CONTENT ========================================= -->
            @if(app('request')->input('stap') < 2 || !app('request')->input('stap') )
                <div class="col-xs-12 col-md-12 no-margin" id="items-holder">

                    @foreach($products as $product)
                        <div class="row no-margin cart-item">
                            <div class="col-xs-12 col-sm-2 no-margin">
                                <a href="#" class="thumb-holder">
                                    <img class="lazy" alt="" src="{{ $product->options->featuredImg }}"/>
                                </a>
                            </div>

                            <div class="col-xs-12 col-sm-5 ">
                                <div class="title">
                                    <a href="#" class="lang-switch" data-en="{{ $product->name }}"
                                       data-bn="{{ $product->options->title_bn }}">{{ $product->options->title_bn }}</a>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-3 no-margin">
                                <div class="quantity">
                                    <div class="le-quantity">
                                        <form>
                                            <a class="minus btn-reduce-item" data-pid="{{ $product->id }}"></a>
                                            <input name="quantity" readonly="readonly" type="text"
                                                   value="{{ $product->qty }}"/>
                                            <a class="plus btn-increase-item" data-pid="{{ $product->id }}"></a>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-2 no-margin cart-item-close-div">
                                <div class="price lang-digit" data-val="৳ {{ $product->subtotal }}">
                                    ৳ {{ enToBn($product->subtotal) }}
                                </div>
                                <a href="#" class="close-btn btn-remove-item" data-pid="{{ $product->id }}"><i class="fa fa-times" aria-hidden="true"></i></a>
                            </div>
                        </div><!-- /.cart-item -->
                    @endforeach
                </div>
                @if(\Cart::count() > 0 )
                <div class="col-xs-12 col-md-12 no-margin sidebar">
                    <div class="col-xs-12 col-md-6 no-margin pull-left">
                        <div id="cupon-widget" class="widget">
                            <h1 class="border lang-switch" data-en="use coupon" data-bn="কুপন আছে?">কুপন আছে?</h1>
                            <div class="body">
                                <form>
                                    <div class="inline-input">
                                        <input data-placeholder="উদাহরণ: BOOKFAIR18" type="text" id="input-coupon-code"/>
                                        <button class="le-button lang-switch" type="submit" id="btn-coupon-code"
                                                data-en="Apply" data-bn="আছে">আছে
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.widget -->
                    </div>
                    <div class="col-xs-12 col-md-6 no-margin pull-right">
                        <div class="widget cart-summary pull-right">
                            <div class="body">
                                <ul class="tabled-data no-border inverse-bold">
                                    <li>
                                        <label class="lang-switch" data-en="cart subtotal" data-bn="খরচ">খরচ</label>
                                        <div class="value pull-right lang-digit" id="cart-subtotal-2"
                                             data-val="{{ $cartSubtotal }}">{{ enToBn((int)$cartSubtotal) }}</div>
                                    </li>
                                    <li>
                                        @php
                                        $dhaka=App\Setting::where('name','ship_dhaka')->first();
                                        $outside =App\Setting::where('name','ship_outside')->first();

                                        $a=1;
                                        $sp = $dhaka->description;

                                        if(session()->has('area')){
                                            if(session('area')=='ship_outside'){
                                                $a = 2;
                                                $sp = $outside->description;
                                            }
                                        }
                                        @endphp

                                        <label for="input-shipping" class="lang-switch" data-en="Select area:"
                                               data-bn="এলাকা নির্বাচন করুন">এলাকা নির্বাচন করুন:</label>
                                        <select class="form-control" id="input-shipping">
                                            <option class="lang-switch" type="1" data-en="Inside Dhaka"
                                                    data-bn="ঢাকার ভেতরে" charge="{{ $dhaka->description}}">ঢাকার ভেতরে
                                            </option>
                                            <option class="lang-switch" type="2" data-en="Outside Dhaka"
                                                    data-bn="ঢাকার বাহিরে" charge="{{$outside->description }}"
                                                    @if($a==2) selected @endif>ঢাকার বাহিরে
                                            </option>
                                        </select>
                                    </li>
                                    <li>
                                        <label class="lang-switch" data-en="shipping"
                                               data-bn="পাঠানোর চার্জ">পাঠানোর চার্জ</label>
                                        <div class="value pull-right lang-digit" id="cart-shipping"
                                             data-val="{{ $sp}}">{{ enToBn($sp) }}</div>
                                    </li>
                                    <li>
                                        <label class="lang-switch" data-en="Discount" data-bn="ছাড়">ছাড়</label>
                                        <div class="value pull-right text-success lang-digit" id="cart-discount"
                                             data-val="-{{ $discount }}">-{{ enToBn($discount) }}</div>
                                    </li>
                                    <li>
                                        <label class="lang-switch" data-en="Total Amount" data-bn="মোট খরচ">মোট খরচ</label>
                                        <div class="value pull-right lang-digit" id="cart-total"
                                             data-val="{{ $cartTotal }}">{{ enToBn($cartTotal) }}</div>
                                    </li>
                                </ul>
                            </div>
                        </div><!-- /.widget -->
                    </div>

                </div><!-- /.sidebar -->
                @else
                    <div class="col-md-12 cart-page-empty">
                        <h2 class="lang-switch" data-en="Your Cart Empty" data-bn="আপনার ব্যাগ খালি">আপনার ব্যাগ খালি</h2>
                    </div>
                @endif
                
                <div class="col-md-12 no-margin">
                    <div class="cart-page-footer-btns">
                        <a class="shop le-button block lang-switch" href="{{ route('shop') }}"
                           data-en="continue shopping" data-bn="আরো কেনাকাটা করুন">আরো কেনাকাটা করুন</a>
                        @if(\Cart::count() > 0 )
                        @if(Auth::check())
                            <a class="next le-button lang-switch" href="{{ route('cart', array('stap'=>3))}}"
                               data-en="checkout"
                               data-bn="পরিশোধ করুন">পরিশোধ করুন</a>
                        @else
                            <a class="next le-button lang-switch" href="{{ route('cart', array('stap'=>2))}}"
                               data-en="checkout"
                               data-bn="পরিশোধ করুন">পরিশোধ করুন</a>
                        @endif
                         @endif
                    </div>
                </div> 
            @endif

            @if(app('request')->input('stap') ==3 )
                {!! Form::open(['route' => 'cart.address', 'class'=>'form-horizontal form-bordered checkoutForm' ]) !!}
                <div class="col-xs-12 no-margin">
                    <div class="col-xs-12 col-md-6">
                        <div class="billing-address">
                            @if(Auth::user() && $user = Auth::user())
                                @if(!empty($user->billing_country) && !empty($user->billing_city) &&
                                !empty($user->billing_area) && !empty($user->billing_postcode) &&
                                !empty($user->billing_address) && !empty($user->phone))
                                    <input type="hidden" name="billingAddress" value="1">
                                @else
                                    <input type="hidden" name="billingAddress" value="0">
                                @endif
                                <h2 class="border h1 lang-switch" data-en="billing address" data-bn=" বিলিং ঠিকানা"> বিলিং ঠিকানা</h2>
                                <div class="row field-row">
                                    <div class="col-xs-12">
                                        <label class="lang-switch" data-en="Full Name*" data-bn="নাম">নাম</label>
                                        <input class="le-input" value="{{$user->username}}">
                                    </div>
                                </div><!-- /.field-row -->

                                <div class="row field-row">
                                    <div class="col-xs-12 col-sm-6">
                                        <label class="lang-switch" data-en="City" data-bn="বিভাগ">বিভাগ</label>
                                        <select name="billingCityCheck" id="selectDivision" class="le-input" data-active-id="{{$user->billing_city}}">
                                        </select>
                                        <input type="hidden" name="billingCity" value="{{$user->billing_area}}">
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <label class="lang-switch" data-en="Area" data-bn="জেলা">জেলা</label>
                                        <select name="billingAreaCheck" id="selectDistic" class="le-input" data-active-id="{{$user->billing_city}}">
                                        </select>
                                        <input type="hidden" name="billingArea" value="{{$user->billing_area}}">
                                    </div>
                                </div><!-- /.field-row -->

                                <div class="row field-row">
                                    <div class="col-xs-12 col-sm-12">
                                        <label class="lang-switch" data-en="Phone Number*" data-bn="মোবাইল নাম্বার*">মোবাইল নাম্বার*</label>
                                        <input class="le-input" name="billingPhone" value="{{$user->phone}}">
                                    </div>
                                </div><!-- /.field-row -->
                                <div class="row field-row">
                                    <div class="col-xs-12">
                                        <label class="lang-switch" data-en="Address" data-bn="ঠিকানা*">ঠিকানা*</label>
                                        <textarea class="le-input" name="billingAddress">{{$user->billing_address}}</textarea>
                                    </div>
                                </div><!-- /.field-row -->
                            @endif
                        </div><!-- /.billing-address -->
                    </div>
                    <div class="col-xs-12 col-md-6 ">
                        <section id="shipping-address">
                            <h2 class="border h1 lang-switch" data-en="shipping address" data-bn="শিপিং ঠিকানা">শিপিং ঠিকানা</h2>
                            <div class="row field-row">
                                <div class="col-xs-12">
                                    <input class="le-checkbox big" type="checkbox" name="addressType"/>
                                    <a class="simple-link bold lang-switch" href="#" data-en="ship to different address?" data-bn="অন্য ঠিকানায় পাঠাতে চান?">অন্য ঠিকানায় পাঠাতে চান?</a>
                                </div>
                            </div><!-- /.field-row -->
                            <div class="ship-address-block">
                                <div class="row field-row">
                                    <div class="col-xs-12">
                                        <label class="lang-switch" data-en="Recive Parson Name*" data-bn="পণ্য গ্রহণকারীর নাম*">পণ্য গ্রহণকারীর নাম*</label>
                                        <input class="le-input" name="shipParson">
                                    </div>
                                </div><!-- /.field-row -->

                                <div class="row field-row">
                                    <div class="col-xs-12 col-sm-6">
                                        <label class="lang-switch" data-en="City" data-bn="বিভাগ">বিভাগ</label>
                                        <select name="shipCityCheck" id="selectShipDivision" class="le-input">
                                        </select>
                                        <input type="hidden" name="shipCity" value="">
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <label class="lang-switch" data-en="Area" data-bn="জেলা">জেলা</label>
                                        <select name="shipCityCheck" id="selectShipDistic" class="le-input" >
                                        </select>
                                        <input type="hidden" name="shipArea" value="">
                                    </div>
                                </div><!-- /.field-row -->
                                <div class="row field-row">
                                    <div class="col-xs-12 col-sm-12">
                                        <label class="lang-switch" data-en="Phone Number*" data-bn="মোবাইল নাম্বার*">মোবাইল নাম্বার*</label>
                                        <input class="le-input" name="shipPhone">
                                    </div>
                                </div><!-- /.field-row -->
                                <div class="row field-row">
                                    <div class="col-xs-12">
                                        <label class="lang-switch" data-en="Address*" data-bn="ঠিকানা*">ঠিকানা*</label>
                                        <textarea class="le-input" name="shipAddress"></textarea>
                                    </div>
                                </div><!-- /.field-row -->
                            </div><!-- /.billing-address -->
                        </section><!-- /#shipping-address -->
                    </div>
                </div>
                <div class="col-md-12 no-margin">
                    <div class="cart-page-footer-btns">
                        <a class="shop le-button block lang-switch" href="{{ route('shop') }}"
                           data-en="continue shopping" data-bn="আরো কেনাকাটা করুন">আরো কেনাকাটা করুন</a>
                        <button class="next formSubmit le-button lang-switch" data-en="checkout"
                               data-bn="পরিশোধ করুন">পরিশোধ করুন</button>
                    </div>
                </div>
                {!! Form::close() !!}
            @endif

            @if(app('request')->input('stap') ==4 )
                {!! Form::open(['route' => 'order.create', 'class'=>'form-horizontal form-bordered checkoutForm' ]) !!}
                    <div class="col-xs-12 no-margin">

                        <div id="payment-method-options">

                            <div class="payment-method-option">
                                <input class="le-radio" type="radio" name="paymentType" value="cash">
                                <div class="radio-label bold ">
                                    <img src="{{asset('assets/images/cash.png')}}">
                                    <span class="lang-switch" data-en="Cash On Dalivery" data-bn="ডেলিভারির সময় পরিশোধ">ডেলিভারির সময় পরিশোধ</span>
                                </div>
                            </div><!-- /.payment-method-option -->

                            <div class="payment-method-option">
                                <input class="le-radio" type="radio" name="paymentType" value="online" checked>
                                <div class="radio-label bold ">
                                    <img src="{{asset('assets/images/ssl_commerz_logo.png')}}"><span class="lang-switch" data-en="Online Payment" data-bn="অনলাইনে পরিশোধ">অনলাইনে পরিশোধ</span>
                                </div>
                            </div><!-- /.payment-method-option -->
                        </div><!-- /#payment-method-options --> 
                    </div><!-- /.col -->
                    <div class="col-md-12 no-margin">
                        <div class="cart-page-footer-btns">
                            <a class="shop le-button block lang-switch" href="{{ route('shop') }}"
                               data-en="continue shopping" data-bn="আরো কেনাকাটা করুন">আরো কেনাকাটা করুন</a>
                            <button class="next formSubmit le-button lang-switch"
                                    data-en="checkout"
                                    data-bn="পরিশোধ করুন">পরিশোধ করুন</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            @endif
            @if( (app('request')->input('stap') ==5) && (\Session::has('OrderPlace') == true) )
                <div class="col-xs-12 no-margin">
                    <h2 class="lang-switch" data-en="{{ \Session::get('OrderPlace') }}" data-bn="আপনার অর্ডার  সম্পন্ন হয়েছে">আপনার অর্ডার  সম্পন্ন হয়েছে</h2>
                </div>
            @endif

        <!-- ================================== SIDEBAR : END ============================= -->
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('assets/js/order.js')}}"></script>
@endsection