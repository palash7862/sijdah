@extends('layouts.master')

@section('title','404!')

@section('styles')
<style>
	.info-404 h2 {
		font-size: 64px !important;
		font-weight: 700 !important;
		line-height: 80px !important;
	}
</style>
@endsection
@section('content')
<main id="faq" class="inner">
	<div class="container">
		<div class="row">
			<div class="col-md-8 center-block">
				<div class="info-404 text-center">
					<h2 class="primary-color inner-bottom-xs lang-switch" data-en="Page Not Found" data-bn="পেজটি  খুজে পাওয়া যাচ্ছে  না">পেজটি  খুজে পাওয়া যাচ্ছে  না</h2>
					<p class="lead lang-switch" data-en="We are sorry, the page you've requested is not available." data-bn="দুঃখিত! আপনি যে পেজের  রিকুয়েস্ট করেছেন সেটি দেখানো সম্ভব  হচ্ছে না">দুঃখিত! আপনি যে পেজের  রিকুয়েস্ট করেছেন সেটি দেখানো সম্ভব  হচ্ছে না</p>
					<div class="text-center">
						<a href="" class="btn-lg huge"><i class="fa fa-home"></i> <span class=" lang-switch" data-en="Go to Home Page" data-bn="মূল পাতায় ফিরে যান">মূল পাতায় ফিরে যান</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
@endsection