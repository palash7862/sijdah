@extends('admin.layouts.master')

@section('title') Categorys @stop

@section('styles') 
<style>
	.btn-app {
		padding: 5px 5px;
		margin: 0 0 5px 5px;
		min-width: 45px;
		height: 45px;
	}
	.user-create-form .form-group {
	  clear: both;
	  display: block;
	  float: left;
	  margin-bottom: 40px;
	  width: 100%;
	}
	.user-create-form .form-group > label {
	  color: #888;
	  float: left;
	  font-size: 20px;
	  font-weight: normal;
	  width: 20%;
	}
	.user-create-form .form-group .input-group {
	  float: left;
	  width: 40%;
	}
	.error-msg {
	  color: red;
	  display: inline-block;
	  padding: 5px 10px 5px 30px;
	  text-transform: capitalize;
	}
	.form-horizontal .form-group {
		margin-right: 0px;
		margin-left: 0px;
	}
	.cat-input-label{
		display: block; width: 100%; text-align: left !important; margin-bottom: 20px;
	}
	.btn-full{
		width: 100% !important;
	}
</style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Categorys 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Categorys</li>
      </ol>
    </section> 
    <!-- Main content --> 
        <section class="content">
          <div class="row">
			<div class="col-md-4">
				<div class="box">
					<div class="box-body">  
						@if (Route::currentRouteName() == 'cats.edit') 					
							{!! Form::open(['route' => 'cats.update', 'class'=>'form-horizontal form-bordered' ]) !!}
						@else
							{!! Form::open(['route' => 'cats.create', 'class'=>'form-horizontal form-bordered' ]) !!}
						@endif
							<!-- Page Title -->
							@if (Route::currentRouteName() == 'cats.edit') 
								@foreach($cats as $cat)
								<input type="hidden" name="catName" value="{{ $cat->id }}" />
								<div class="form-group">
									<label for="cat name" class="cat-input-label control-label">Category Name ( English )</label>
									<input type="text" class="form-control" name="name_en" value="{{ $cat->name_en }}" placeholder="Enter the Category Name...">
									@if($errors->first('name_en'))
										<strong class="error-msg">{{ $errors->first('name_en') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label for="cat name" class="cat-input-label control-label">Category Name ( Bangla )</label>
									<input type="text" class="form-control" name="name" value="{{ $cat->name_bn }}" placeholder="Enter the Category Name...">
									@if($errors->first('name_bn'))
										<strong class="error-msg">{{ $errors->first('name_bn') }} </strong>
									@endif
								</div>
								<div class="form-group ">
									<label class="cat-input-label">Select Parent</label>
									<select class="form-control" name="parent_id">
										<option value="">Select Parent</option>
										@foreach($parents as $parent)
											@if( $cat->parent_id == $parent->id )
												<option value="{{ $parent->id }}" selected>{{ $parent->name_en }} ( {{ $parent->name_en }} )</option>
											@else
												<option value="{{ $parent->id }}">{{ $parent->name_en }} ( {{ $parent->name_en }} )</option>
											@endif
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea name="description" class="form-control" rows="3" placeholder="Enter Your Category Description">{{ $cat->description }}</textarea>
								</div>
								@endforeach
							@else

								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Category Name ( English )</label>
									<input type="text" class="form-control" name="name_en" placeholder="Enter The Category Name...">
									@if($errors->first('name_en'))
										<strong class="error-msg">{{ $errors->first('name_en') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Category Name ( Bangls )</label>
									<input type="text" class="form-control" name="name_bn" placeholder="Enter The Category Name...">
									@if($errors->first('name_bn'))
										<strong class="error-msg">{{ $errors->first('name_bn') }} </strong>
									@endif
								</div>
								<div class="form-group ">
									<label class="cat-input-label">Select Parent</label>
									<select class="form-control" name="parent_id">
										<option value="">Select Parent</option>
										@foreach($parents as $parent)
											<option value="{{ $parent->id }}">{{ $parent->name_en }} ( {{ $parent->name_en }} )</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea name="description" class="form-control" rows="3" placeholder="Enter Your Category Description"></textarea>
								</div>
							@endif
							
							<div class="form-group form-actions">
								<div class="cat-input-label">
									@if (Route::currentRouteName() == 'cats.edit')
										<button type="submit" id="add" class="btn btn-info btn-full">Update</button>
									@else
										<button type="submit" id="add" class="btn btn-info btn-full">Create</button>
									@endif
								</div>
							</div>
						{!! Form::close() !!}
					</div>
					<!-- /.box-body -->

				</div><!-- /.box -->
			</div>
            <!-- right column -->
            <div class="col-md-8">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-body"> 
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Category Name ( English )</th>
							<th>Category Name ( Bangla )</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
					<tbody> 
						@foreach($cats as $cat)
						<tr>
                            <td>{{ $cat->id }}</td>
                            <td>{{ $cat->name_en }}</td>
                            <td>{{ $cat->name_bn }}</td>
                            <td>
								@if (Route::currentRouteName() != 'cats.edit')
								<a href="{{ route('cats.edit', array($cat->id)) }}" class="btn btn-app" >
									<i class="fa fa-edit"></i>
									Edit
								</a>
								@endif
								<a href="{{ route('cats.delete', array($cat->id)) }}" class="btn delete-cat btn-app" >
									<i class="fa fa-trash-o"></i>
									Delete
								</a> 
							</td>
                        </tr>
						@endforeach
					</tbody> 
					<tfoot>
                        <tr>
                            <th>#ID</th>
                            <th>Category Name ( English )</th>
                            <th>Category Name ( Bangla )</th>
                            <th class="text-center">Options</th>
                        </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.box-body -->

              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content   where('id', $id) -->
	<!-- /.content -->

@stop
@section('scripts')   
<!-- page script -->
	<script>
	  $(function () {
	      $('.delete-cat').on('click', function (event) {
	          var rlink = $(this).attr('href');
              event.preventDefault();
              swal({
                  title: 'Are you sure?',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, delete it!'
              }).then((result) => {
                  if (result.value) {
                  	window.location.href = rlink;
				  }
			  });
          });
//			$('#example2').DataTable({
//			  "paging": true,
//			  "lengthChange": false,
//			  "searching": false,
//			  "ordering": true,
//			  "info": true,
//			  "autoWidth": false
//			});
	  });
	</script> 
@stop