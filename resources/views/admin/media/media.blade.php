@extends('admin.layouts.master')

@section('title') Create Test @stop

@section('styles')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/dist/css/dropzone.css') }}">
    <style>
        .test-create-form .form-group, .test-create-form .input-group {
            width: 100%;
        }
        .test-create-form .cats-list-checkbox {
            display: block;
            max-height: 400px;
            overflow-x: auto;
            overflow-y: scroll;
            width: 100%;
        }
        .test-create-form .cats-list-checkbox > label {
            display: block;
            padding: 5px;
            width: 100%;
        }
        .error-msg {
            color: red;
            display: inline-block;
            padding: 5px 10px 5px 30px;
            text-transform: capitalize;
        }
        #multipleupload .dropzone {
            border: 2px dashed #0087f7;
            padding: 16px 20px;
            text-align: center;
        }

        /******************** Switch Button Css **********************/
        .switch-field {
            padding: 0px;
            overflow: hidden;
        }

        .switch-title {
            margin-bottom: 6px;
        }

        .switch-field input {
            position: absolute !important;
            clip: rect(0, 0, 0, 0);
            height: 1px;
            width: 1px;
            border: 0;
            overflow: hidden;
        }

        .switch-field label {
            float: left;
        }

        .switch-field label {
            display: inline-block;
            width: 33.3333%;
            background-color: #e4e4e4;
            color: rgba(0, 0, 0, 0.6);
            font-size: 14px;
            font-weight: normal;
            text-align: center;
            text-shadow: none;
            padding: 6px 14px;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
            box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
            -webkit-transition: all 0.1s ease-in-out;
            -moz-transition:    all 0.1s ease-in-out;
            -ms-transition:     all 0.1s ease-in-out;
            -o-transition:      all 0.1s ease-in-out;
            transition:         all 0.1s ease-in-out;
        }

        .switch-field label:hover {
            cursor: pointer;
        }

        .switch-field input:checked + label {
            background-color: #3c8dbc;
            -webkit-box-shadow: none;
            box-shadow: none;
            color: #ffffff;
        }

        .switch-field label:first-of-type {
            border-radius: 4px 0 0 4px;
        }

        .switch-field label:last-of-type {
            border-radius: 0 4px 4px 0;
        }
        .form-group .inner-group {
            width: calc( 100%/2 - 20px );
            float: left;
            margin-right: 20px;
        }
        .form-group .inner-group.colum-4 {
            width: calc( 100%/4 - 20px );
        }
        .form-group {
            margin-bottom: 25px;
        }
        .Clothing-product-input, .FoodGroceries-product-input {
            display: none;
        }
        .media-view-warp .single-media {
            float: left;
            width: 20%;
        }
        .media-view-warp .single-media {
            float: left;
            width: 17%;
            text-align: center;
            border: 1px solid rgba(0, 0, 0, 0.1);
            margin-left: 10px;
            margin-right: 10px;
            padding: 13px;
            margin-bottom: 20px;
        }
        .media-view-warp .single-media img {
            max-width: 100%;
        }
        .dropzone.dz-clickable .dz-message{
            display: block !important;
        }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Media
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Media</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- <pre>
                //print_r($errors);
                </pre> -->
                <div class="box">

                    {!! Form::open(['route' => 'product.create']) !!}
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                        <div id="multipleupload" class="multipleupload dropzone" action="{{ route('upload.add') }}">
                                            <div class="dropzone"  >
                                                <div class="dz-message" >
                                                    <h3>Drop images here or click to upload.</h3>
                                                </div>
                                            </div>
                                        </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="media-view-warp">
                                    @if($mediaAll)
                                        @foreach($mediaAll as $media)
                                            <div class="single-media">
                                                <img src="{{asset(get_image_size('media-thumb', $media->source))}}" alt="{{$media->title}}">
                                                <a href="{{route('upload.delete', array($media->id))}}"  class="media-delete btn btn-block btn-danger btn-xs">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <!--
    <div class="dropzone"  >
        <div class="dz-message" >
            <img id="post-thumb-prev" src="" alt="" />
            <h3>Drop images here or click to upload.</h3>
        </div>
    </div>

    -->
@stop
@section('scripts')
    <!-- Select2 -->
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('admin/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('admin/dist/js/dropzone.js') }}"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <!-- page script -->
    <script>
        Dropzone.autoDiscover = false;
        $(document).ready(function () {
            $('.media-delete').on('click', function (event) {
                    var rlink = $(this).attr('href');
                    event.preventDefault();
                    swal({
                        title: 'Are you sure?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.value) {
                        window.location.href = rlink;
                    }
                });
            });

            var imageCount = 1;
            var actionUrl  =  $(".multipleupload").attr('action');
            var myDropzone =  $("#multipleupload").dropzone( {
                url: actionUrl,
                maxFilesize: 99, // MB
                paramName: "files",
                uploadMultiple:true,
                //previewsContainer: false,
                //addRemoveLinks: true,
                uploadprogress: function(file, progress, bytesSent){
                    var prevHtml = '<div class="single-media" data-imageid="'+imageCount+'" ><img  src="'+file.dataURL+'" width="80px"><input type="radio" name="thumb_future_id" value="" ><span class="remove">Remove</span></div>';
                    var mediaPrev = $("#multipleupload").parents('.post-thumb-field').find(".media-preview");
                    mediaPrev.append(prevHtml);
                    console.log(file);
                },

                success: function(file, response, done){
                    var mediaPrev = $("#multipleupload").parents('.post-thumb-field').find(".media-preview");
                    mediaPrev.find('.single-media[data-imageid='+imageCount+']')
                        .attr('data-mediaid', response.medialId)
                        .append('<input type="hidden" name="product_img[]" value="'+response.medialId+'" >');

                    mediaPrev.find('.single-media[data-imageid='+imageCount+']')
                        .find("input[name=thumb_future_id]").attr('value', response.medialId);
                    imageCount++;
                }
            });

            /*
                sending: function(file, xhr, formData){
                    formData.append( '_token', $("input[name=_token]").val() );
                },
            */
        });
    </script>
@stop