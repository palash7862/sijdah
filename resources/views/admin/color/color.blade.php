@extends('admin.layouts.master')

@section('title') Categorys @stop

@section('styles') 
<style>
	.user-create-form .form-group {
	  clear: both;
	  display: block;
	  float: left;
	  margin-bottom: 40px;
	  width: 100%;
	}
	.user-create-form .form-group > label {
	  color: #888;
	  float: left;
	  font-size: 20px;
	  font-weight: normal;
	  width: 20%;
	}
	.user-create-form .form-group .input-group {
	  float: left;
	  width: 40%;
	}
	.error-msg {
	  color: red;
	  display: inline-block;
	  padding: 5px 10px 5px 30px;
	  text-transform: capitalize;
	}
	.form-horizontal .form-group {
		margin-right: 0px;
		margin-left: 0px;
	}
	.cat-input-label{
		display: block; width: 100%; text-align: left !important; margin-bottom: 20px;
	}
	.btn-full{
		width: 100% !important;
	}
</style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Color</h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Color</li>
      </ol>
    </section> 
    <!-- Main content --> 
        <section class="content">
          <div class="row">
			<div class="col-md-4">
				<div class="box">
					<div class="box-body">  
						@if (Route::currentRouteName() == 'color.edit')
							{!! Form::open(['route' => 'color.update', 'class'=>'form-horizontal form-bordered' ]) !!}
						@else
							{!! Form::open(['route' => 'color.create', 'class'=>'form-horizontal form-bordered' ]) !!}
						@endif
							<!-- Page Title -->
							@if (Route::currentRouteName() == 'color.edit')
								@foreach($color as $scolor)
								<input type="hidden" name="colorId" value="{{ $scolor->id }}" />
								<div class="form-group">
									<label for="cat name" class="cat-input-label control-label">Color name</label>
									<input type="text" class="form-control" name="name" value="{{ $scolor->name }}" placeholder="Enter the Color Name">
									@if($errors->first('name')) 
										<strong class="error-msg">{{ $errors->first('name') }} </strong> 
									@endif
								</div>
								<div class="form-group">
									<label for="cat name" class="cat-input-label control-label">Color Code</label>
									<input type="text" class="form-control" name="color_code" value="{{ $scolor->color_code }}" placeholder="Enter the Color Code">
									@if($errors->first('color_code'))
										<strong class="error-msg">{{ $errors->first('color_code') }} </strong>
									@endif
								</div>
								@endforeach
							@else
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Color Name</label>
									<input type="text" class="form-control" name="name" placeholder="Enter the Color Name">
									@if($errors->first('name'))
										<strong class="error-msg">{{ $errors->first('name') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Color Code</label>
									<input type="color" class="form-control" name="color_code" placeholder="Enter the Color Code">
									@if($errors->first('color_code'))
										<strong class="error-msg">{{ $errors->first('color_code') }} </strong>
									@endif
								</div>
							@endif
							
							<div class="form-group form-actions">
								<div class="cat-input-label">
									@if (Route::currentRouteName() == 'color.edit')
										<button type="submit" id="add" class="btn btn-info btn-full">Update</button>
									@else
										<button type="submit" id="add" class="btn btn-info btn-full">Create</button>
									@endif
								</div>
							</div>
						{!! Form::close() !!}
					</div>
					<!-- /.box-body -->

				</div><!-- /.box -->
			</div>
            <!-- right column -->
            <div class="col-md-8">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-body"> 
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Color name</th>
                            <th>Color Code</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
					<tbody> 
						@foreach($colors as $color)
						<tr>
                            <th>{{ $color->id }}</th>
                            <th>{{ $color->name }}</th>
                            <th>{{ $color->color_code }}</th>
                            <th>
								@if (Route::currentRouteName() != 'color.edit')
								<a href="{{ route('color.edit', array($color->id)) }}" class="btn btn-app" >
									<i class="fa fa-edit"></i>
									Edit
								</a>
								@endif
								<a href="{{ route('color.delete', array($color->id)) }}" class="btn delete-color btn-app" >
									<i class="fa fa-trash-o"></i>
									Delete
								</a> 
							</th>
                        </tr>
						@endforeach
					</tbody> 
					<tfoot>
                        <tr>
                            <th>#ID</th>
                            <th>Color name</th>
                            <th>Color Code</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.box-body -->

              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content   where('id', $id) -->
	<!-- /.content -->

@stop
@section('scripts')   
<!-- page script -->
<script>
    $(function () {
        $('.delete-color').on('click', function (event) {
            var rlink = $(this).attr('href');
            event.preventDefault();
            swal({
                title: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                window.location.href = rlink;
            }
        });
        });
//			$('#example2').DataTable({
//			  "paging": true,
//			  "lengthChange": false,
//			  "searching": false,
//			  "ordering": true,
//			  "info": true,
//			  "autoWidth": false
//			});
    });
</script>
@stop