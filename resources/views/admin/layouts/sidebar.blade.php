<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->username}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="@if( (Route::currentRouteName() == 'dashboard' ) ) active @endif">
                <a href="{{ route('dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

            <li class="treeview @if( (Route::currentRouteName() == 'orders.placed' ) ||
        ( Route::currentRouteName() == 'order.single') ) active @endif">
                <a href="">
                    <i class="fa fa-circle-o text-red"></i> <span>Orders</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if( (Route::currentRouteName() == 'orders.placed') ) active @endif">
                        <a href="{{ route('orders.placed') }}"><i class="fa fa-circle-o"></i> Orders Placed </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'order.processing') ) active @endif">
                        <a href="{{ route('order.processing') }}"><i class="fa fa-circle-o"></i> Orders Processing </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'order.shipped') ) active @endif">
                        <a href="{{ route('order.shipped') }}"><i class="fa fa-circle-o"></i> Orders Shipped </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'order.delivered') ) active @endif">
                        <a href="{{ route('order.delivered') }}"><i class="fa fa-circle-o"></i> Orders Delivered </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'order.cancel') ) active @endif">
                        <a href="{{ route('order.cancel') }}"><i class="fa fa-circle-o"></i> Orders Cancel </a>
                    </li>
                </ul>
            </li>

            <li class="treeview
                @if( (Route::currentRouteName() == 'product.create' ) || ( Route::currentRouteName() == 'product.all')
                || ( Route::currentRouteName() == 'product.edit')  || ( Route::currentRouteName() == 'writer.all')
                || ( Route::currentRouteName() == 'writer.edit')  || ( Route::currentRouteName() == 'writer.create')
                || ( Route::currentRouteName() == 'publisher.all') || ( Route::currentRouteName() == 'publisher.create')
                || ( Route::currentRouteName() == 'publisher.edit') || ( Route::currentRouteName() == 'cats.all')
                || ( Route::currentRouteName() == 'cats.edit') || ( Route::currentRouteName() == 'cats.create')
                || ( Route::currentRouteName() == 'color.edit') || ( Route::currentRouteName() == 'color.all')
                || ( Route::currentRouteName() == 'color.create'))
                    active
@endif">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Product</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if( (Route::currentRouteName() == 'product.create') ) active @endif">
                        <a href="{{ route('product.create') }}"><i class="fa fa-circle-o"></i> Product Add </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'product.all' ) ) active @endif">
                        <a href="{{ route('product.all') }}"><i class="fa fa-circle-o"></i> Product All </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'writer.all' ) ||
                            (Route::currentRouteName() == 'writer.create' ) ||
                            (Route::currentRouteName() == 'writer.edit' ) ) active @endif">
                        <a href="{{ route('writer.all') }}"><i class="fa fa-circle-o"></i> Writer </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'publisher.all' ) ||
                            (Route::currentRouteName() == 'publisher.create' ) ||
                            (Route::currentRouteName() == 'publisher.edit' )) active @endif">
                        <a href="{{ route('publisher.all') }}"><i class="fa fa-circle-o"></i> Publisher </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'cats.all' ) ||
                            (Route::currentRouteName() == 'cats.create' ) ||
                            (Route::currentRouteName() == 'cats.edit' ) ) active @endif">
                        <a href="{{ route('cats.all') }}"><i class="fa fa-circle-o"></i> Category </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'color.all' ) ||
                            (Route::currentRouteName() == 'color.create' ) ||
                            (Route::currentRouteName() == 'color.edit' ) ) active @endif">
                        <a href="{{ route('color.all') }}"><i class="fa fa-circle-o"></i> Color </a>
                    </li>
                </ul>
            </li>

            <li class="treeview @if( (Route::currentRouteName() == 'coupon.all' ) ||
        ( Route::currentRouteName() == 'coupon.create') || ( Route::currentRouteName() == 'promotion.index') ) active @endif">
                <a href="#">
                    <i class="fa fa-circle-o text-red"></i> <span>Offers</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if( (Route::currentRouteName() == 'coupon.all') ) active @endif">
                        <a href="{{ route('coupon.all') }}"><i class="fa fa-circle-o"></i> Coupons </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'promotion.index')||(Route::currentRouteName() == 'promotion.create') ) active @endif">
                        <a href="{{ route('promotion.index') }}"><i class="fa fa-circle-o"></i> Promotions </a>
                    </li>
                </ul>
            </li>

            <li class="treeview @if( (Route::currentRouteName() == 'all.page' ) ||
        ( Route::currentRouteName() == 'create.page') ) active @endif">
                <a href="#">
                    <i class="fa fa-circle-o text-red"></i> <span>Pages</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if( (Route::currentRouteName() == 'all.page') ) active @endif">
                        <a href="{{ route('all.page') }}"><i class="fa fa-circle-o"></i> All Pages </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'create.page' ) ) active @endif">
                        <a href="{{ route('create.page') }}"><i class="fa fa-circle-o"></i> Add page </a>
                    </li>
                </ul>
            </li>
            <li class="treeview @if( (Route::currentRouteName() == 'user.all' ) ||
        ( Route::currentRouteName() == 'user.add') || ( Route::currentRouteName() == 'user.add') ) active @endif">
                <a href="#">
                    <i class="fa fa-circle-o text-red"></i> <span>Users</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if( (Route::currentRouteName() == 'user.all') ) active @endif">
                        <a href="{{ route('user.all') }}"><i class="fa fa-circle-o"></i> All User </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'user.add' ) ) active @endif">
                        <a href="{{ route('user.add') }}"><i class="fa fa-circle-o"></i> Add User </a>
                    </li>
                </ul>
            </li>
            <li class="@if( (Route::currentRouteName() == 'section.all' ) ) active @endif">
                <a href="{{ route('section.all') }}"><i class="fa fa-circle-o text-red "></i>
                    <span>Home Page Sections</span></a>
            </li>
            <li @if( (Route::currentRouteName() == 'upload' ) ) active @endif><a href="{{ route('upload') }}"><i
                            class="fa fa-circle-o text-yellow"></i> <span>Media</span></a></li>

            <li class="treeview @if(
                (Route::currentRouteName() == 'writer.list' ) ||
                (Route::currentRouteName() == 'writer.sales' ) ||
                (Route::currentRouteName() == 'publisher.list' ) ||
                (Route::currentRouteName() == 'publisher.sales' )
            ) active @endif">
                <a href="#">
                    <i class="fa fa-circle-o text-red"></i> <span>Reports</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if( (Route::currentRouteName() == 'writer.list') ||
                    (Route::currentRouteName() == 'writer.sales') ) active @endif">
                        <a href="{{ route('writer.list') }}"><i class="fa fa-circle-o"></i> Writer Books Report</a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'publisher.list') ||
                    (Route::currentRouteName() == 'publisher.sales') ) active @endif">
                        <a href="{{ route('publisher.list') }}"><i class="fa fa-circle-o"></i> Publisher Books
                            Report</a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'date.view') ||
                    (Route::currentRouteName() == 'date.sales') ) active @endif">
                        <a href="{{ route('date.view') }}"><i class="fa fa-circle-o"></i> Date Sales Report</a>
                    </li>
                </ul>
            </li>
            <li class="treeview @if( (Route::currentRouteName() == 'shipping.index' ) ||
        ( Route::currentRouteName() == 'contact.index') || ( Route::currentRouteName() == 'footerWidget.index' )||( Route::currentRouteName() == 'socialProfile.index' ) ) active @endif">
                <a href="#">
                    <i class="fa fa-circle-o text-red"></i> <span>Settings</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if( (Route::currentRouteName() == 'general.index') ) active @endif">
                        <a href="{{ route('general.index') }}"><i class="fa fa-circle-o"></i> General Setting</a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'contact.index') ) active @endif">
                        <a href="{{ route('contact.index') }}"><i class="fa fa-circle-o"></i> Contact Info </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'shipping.index') ) active @endif">
                        <a href="{{ route('shipping.index') }}"><i class="fa fa-circle-o"></i> Shipping Charges </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'footerWidget.index') ) active @endif">
                        <a href="{{ route('footerWidget.index') }}"><i class="fa fa-circle-o"></i> Footer Widget </a>
                    </li>
                    <li class="@if( (Route::currentRouteName() == 'socialProfile.index') ) active @endif">
                        <a href="{{ route('socialProfile.index') }}"><i class="fa fa-circle-o"></i> Social Profiles </a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>