@extends('admin.layouts.master')

@section('title') Section @stop

@section('styles')
    <style>
        .user-create-form .form-group {
            clear: both;
            display: block;
            float: left;
            margin-bottom: 40px;
            width: 100%;
        }

        .user-create-form .form-group > label {
            color: #888;
            float: left;
            font-size: 20px;
            font-weight: normal;
            width: 20%;
        }

        .user-create-form .form-group .input-group {
            float: left;
            width: 40%;
        }

        .error-msg {
            color: red;
            display: inline-block;
            padding: 5px 10px 5px 30px;
            text-transform: capitalize;
        }

        .form-horizontal .form-group {
            margin-right: 0px;
            margin-left: 0px;
        }

        .cat-input-label {
            display: block;
            width: 100%;
            text-align: left !important;
            margin-bottom: 20px;
        }

        .btn-full {
            width: 100% !important;
        }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <h1>
            Section
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Section</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="box">
                    <div class="box-body">
                    @if (Route::currentRouteName() == 'section.edit')
                        {!! Form::open(['route' => 'section.update', 'class'=>'form-horizontal form-bordered' ]) !!}
                    @else
                        {!! Form::open(['route' => 'section.create', 'class'=>'form-horizontal form-bordered' ]) !!}
                    @endif
                    <!-- Page Title -->
                        @if (Route::currentRouteName() == 'section.edit')
                            @foreach($ssection as $sc)
                                <input type="hidden" name="section_id" value="{{ $sc->id }}"/>
                                <div class="form-group">
                                    <label for="cat name" class="cat-input-label control-label">Section Title
                                        (English)</label>
                                    <input type="text" class="form-control" name="title_en" value="{{ $sc->title_en }}">
                                    @if($errors->first('title_en'))
                                        <strong class="error-msg">{{ $errors->first('title_en') }} </strong>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="cat name" class="cat-input-label control-label">Section Title
                                        (Bangla)</label>
                                    <input type="text" class="form-control" name="title_bn" value="{{ $sc->title_bn }}">
                                    @if($errors->first('title_bn'))
                                        <strong class="error-msg">{{ $errors->first('title_bn') }} </strong>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Categories</label>
                                    <div class="form-group">
                                        @php
                                            $oldCat = unserialize($sc->categories);
                                        @endphp
                                        @foreach($categories as $category)
                                            @if(in_array($category->id,$oldCat))
                                                <label><input type="checkbox" name="categories[]"
                                                              value="{{$category->id}}" checked>{{$category->name_en}}
                                                </label>
                                            @else
                                                <label><input type="checkbox" name="categories[]"
                                                              value="{{$category->id}}">{{$category->name_en}}</label>
                                            @endif
                                        @endforeach
                                    </div>
                                    @if($errors->first('categories'))
                                        <strong class="error-msg">{{ $errors->first('categories') }} </strong>
                                    @endif
                                </div>
                            @endforeach
                        @else
                            <div class="form-group">
                                <label for="cat name" class="cat-input-label control-label">Section Title
                                    (English)</label>
                                <input type="text" class="form-control" name="title_en" value="{{ old('title_en') }}">
                                @if($errors->first('title_en'))
                                    <strong class="error-msg">{{ $errors->first('title_en') }} </strong>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="cat name" class="cat-input-label control-label">Section Title
                                    (Bangla)</label>
                                <input type="text" class="form-control" name="title_bn" value="{{ old('title_bn') }}">
                                @if($errors->first('title_bn'))
                                    <strong class="error-msg">{{ $errors->first('title_bn') }} </strong>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Categories</label>
                                <div class="form-group">
                                    @foreach($categories as $category)
                                        <label><input type="checkbox" name="categories[]"
                                                      value="{{$category->id}}">{{$category->name_en}}</label>
                                    @endforeach
                                </div>
                            </div>
                            @if($errors->first('categories'))
                                <strong class="error-msg">{{ $errors->first('categories') }} </strong>
                            @endif
                        @endif


                        <div class="form-group form-actions">
                            <div class="cat-input-label">
                                @if (Route::currentRouteName() == 'section.edit')
                                    <button type="submit" id="add" class="btn btn-info ">Update</button>
                                    <a href="{{route('section.all')}}" class="btn btn-default">Cancel</a>
                                @else
                                    <button type="submit" id="add" class="btn btn-info btn-full">Create</button>
                                @endif
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box-body -->

                </div><!-- /.box -->
            </div>
            <!-- right column -->
            <div class="col-md-8">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Section name</th>
                                <th>Categories</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($sections)
                            @foreach($sections as $section)
                                <tr>
                                    <td>{{ $section->id }}</td>
                                    <td>{{ $section->title_en }}</td>
                                    <td>
                                        @forelse(unserialize($section->categories) as $cat)
                                            {{ App\Category::find($cat)->name_en }},
                                        @empty
                                            <p>(empty!)</p>
                                        @endforelse
                                    </td>
                                    <td>
                                        @if (Route::currentRouteName() != 'section.edit')
                                            <a href="{{ route('section.edit', array($section->id)) }}"
                                               class="btn btn-app">
                                                <i class="fa fa-edit"></i>
                                                Edit
                                            </a>
                                        @endif
                                        <a href="{{ route('section.delete', array($section->id)) }}"
                                           class="btn delete-homesection btn-app delete-section">
                                            <i class="fa fa-trash-o"></i>
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>#ID</th>
                                <th>Section name</th>
                                <th>Categories</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->

                </div><!-- /.box -->

            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content   where('id', $id) -->
    <!-- /.content -->

@stop
@section('scripts')
    <!-- page script -->

    <script>
        $(function () {
            //$('#example2').DataTable();
            $('.delete-homesection').on('click', function (event) {
                var rlink = $(this).attr('href');
                event.preventDefault();
                swal({
                    title: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) = > {
                    if(result.value
            )
                {
                    window.location.href = rlink;
                }
            })
                ;
            });
        });
    </script>
    <script>
        //	  $(function () {
        //		$('#example2').DataTable({
        //		  "paging": true,
        //		  "lengthChange": false,
        //		  "searching": false,
        //		  "ordering": true,
        //		  "info": true,
        //		  "autoWidth": false
        //		});
        //	  });
    </script>
    <script>
        //		$('.delete-section').click(function(e){
        //			e.preventDefault();
        //			var delete_url = this.href;
        //			var elm = $(this).closest('tr');
        //			swal({
        //			      title: "Are you sure?",
        //			      text: "Are you sure that you want to delete this section?",
        //			      type: "warning",
        //			      showCancelButton: true,
        //			      closeOnConfirm: false,
        //			      confirmButtonText: "delete it!",
        //			      confirmButtonColor: "#ec6c62"
        //			    }, function() {
        //			      $.ajax({
        //			        url: delete_url,
        //			        type: "GET"
        //			      })
        //			      .done(function(data) {
        //			      	elm.remove();
        //			        swal("Deleted!", "Section was successfully deleted!", "success");
        //			      })
        //			      .error(function(data) {
        //			        swal("Oops", "We couldn't connect to the server!", "error");
        //			      });
        //			    });
        //		});
    </script>

@stop