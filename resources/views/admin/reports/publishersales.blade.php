@extends('admin.layouts.master')

@section('title') Writers Reports @stop

@section('styles')
    <style>
        .user-create-form .form-group {
            clear: both;
            display: block;
            float: left;
            margin-bottom: 40px;
            width: 100%;
        }

        .user-create-form .form-group > label {
            color: #888;
            float: left;
            font-size: 20px;
            font-weight: normal;
            width: 20%;
        }

        .user-create-form .form-group .input-group {
            float: left;
            width: 40%;
        }

        .error-msg {
            color: red;
            display: inline-block;
            padding: 5px 10px 5px 30px;
            text-transform: capitalize;
        }

        .form-horizontal .form-group {
            margin-right: 0px;
            margin-left: 0px;
        }

        .cat-input-label {
            display: block;
            width: 100%;
            text-align: left !important;
            margin-bottom: 20px;
        }

        .btn-full {
            width: 100% !important;
        }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Writers
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Writers</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-body">
                        @if( (Route::currentRouteName() == 'publisher.list') )
                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Writer Name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($publishers as $publisher)
                                    <tr>
                                        <td>{{ $publisher->id }}</td>
                                        <td>{{ json_decode($publisher->name)->en }} ({{ json_decode($publisher->name)->bn }}
                                            )
                                        </td>
                                        <td>
                                            <a href="{{ route('publisher.sales', array($publisher->id)) }}" class="btn btn-app" >
                                                <i class="fa fa-edit"></i>
                                                View Report
                                            </a>
                                            <a href="{{ route('publisher.sales.export', array($publisher->id)) }}" class="btn btn-app" >
                                                <i class="fa fa-edit"></i>
                                                Export
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#ID</th>
                                    <th>Writer Name</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        @endif
                        @if( (Route::currentRouteName() == 'publisher.sales') )
                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Writer Name</th>
                                    <th>Product Name</th>
                                    <th>Total Sales</th>
                                    <th>Total Income</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($publisherSell as $publisher)
                                    <tr>
                                        <td>{{ $publisher->id }}</td>
                                        <td>{{ json_decode($publisher->name)->en }} ({{ json_decode($publisher->name)->bn }}
                                            )
                                        </td>
                                        <td>{{ $publisher->title_en }}</td>
                                        <td>{{ $publisher->total_sales }}</td>
                                        <td>{{ $publisher->total_amount }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#ID</th>
                                    <th>Writer Name</th>
                                    <th>Product Name</th>
                                    <th>Total Sales</th>
                                    <th>Total Income</th>
                                </tr>
                                </tfoot>
                            </table>
                        @endif
                    </div>
                    <!-- /.box-body -->

                </div><!-- /.box -->

            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content   where('id', $id) -->
    <!-- /.content -->

@stop
@section('scripts')
    <!-- page script -->
    <script>
        $(function () {
//			$('#example2').DataTable({
//			  "paging": true,
//			  "lengthChange": false,
//			  "searching": false,
//			  "ordering": true,
//			  "info": true,
//			  "autoWidth": false
//			});
        });
    </script>
@stop