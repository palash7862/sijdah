@extends('admin.layouts.master')

@section('title') Date Sales Report @stop

@section('styles')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet"
          href="{{ asset('admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .user-create-form .form-group {
            clear: both;
            display: block;
            float: left;
            margin-bottom: 40px;
            width: 100%;
        }

        .user-create-form .form-group > label {
            color: #888;
            float: left;
            font-size: 20px;
            font-weight: normal;
            width: 20%;
        }

        .user-create-form .form-group .input-group {
            float: left;
            width: 40%;
        }

        .error-msg {
            color: red;
            display: inline-block;
            padding: 5px 10px 5px 30px;
            text-transform: capitalize;
        }

        .form-horizontal .form-group {
            margin-right: 0px;
            margin-left: 0px;
        }

        .cat-input-label {
            display: block;
            width: 100%;
            text-align: left !important;
            margin-bottom: 20px;
        }
        .input-group.submit-btn {
            overflow: hidden;
            display: block;
            padding-top: 23px;
        }
        .btn-full {
            width: 100% !important;
        }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Date Sales Report
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Date Sales Report</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            {!! Form::open(['route' => 'date.sales', 'class'=>'form-horizontal form-bordered' ]) !!}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>From Date:</label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        @if(isset($nowDate))
                                            <input type="text" class="form-control pull-right" id="datepicker"
                                                   name="from_date" value="{{$nowDate}}">
                                        @else
                                            <input type="text" class="form-control pull-right" id="datepicker"
                                                   name="from_date">
                                        @endif
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>To Date:</label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        @if(isset($oldDate))
                                            <input type="text" class="form-control pull-right" id="datepicker2"
                                                   name="to_date" value="{{$oldDate}}">
                                        @else
                                            <input type="text" class="form-control pull-right" id="datepicker2"
                                                   name="to_date">
                                        @endif
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="input-group submit-btn">
                                        <input type="submit" class="btn btn-block btn-primary btn-flat">
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                @if( Route::currentRouteName() == 'date.sales')
                                <a href="{{ route('date.sales.export', array($nowDate, $oldDate)) }}" class="btn btn-block btn-primary btn-flat" >
                                    Export
                                </a>
                                @endif
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Product Name</th>
                                        <th>Writers</th>
                                        <th>Total Sales</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if( Route::currentRouteName() == 'date.sales')
                                        @foreach($products as $product)
                                            <tr>
                                                <td>{{ $product->id }}</td>
                                                <td>{{ $product->title_en }}</td>
                                                <td>
                                                    @php
                                                    $writerArray = explode('|', $product->writersName);

                                                        foreach ($writerArray as $writer){
                                                            echo '('.json_decode($writer)->en.','.json_decode($writer)->bn.' )<br>';
                                                        }
                                                    @endphp
                                                </td>
                                                <td>{{ $product->total_sales }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Product Name</th>
                                        <th>Writers</th>
                                        <th>Total Sales</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                </div><!-- /.box -->

            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content   where('id', $id) -->
    <!-- /.content -->

@stop
@section('scripts')
    <!-- bootstrap datepicker -->
    <script src="{{ asset('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- page script -->
    <script>
        $(function () {
//Date picker
            var d = new Date();

            var currDate = d.getDate();
            var currMonth = d.getMonth();
            var currYear = d.getFullYear();

            var dateStr = currYear + "-" + currMonth + "-" + currDate;
            var datepickerOne = $('#datepicker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });
//datepickerOne.val(dateStr);
//Date picker
            $('#datepicker2').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            })
//			$('#example2').DataTable({
//			  "paging": true,
//			  "lengthChange": false,
//			  "searching": false,
//			  "ordering": true,
//			  "info": true,
//			  "autoWidth": false
//			});
        });
    </script>
@stop