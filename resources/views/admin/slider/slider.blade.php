@extends('admin.layouts.master')

@section('title') Slider Settings @stop

@section('styles') 
<style>
	.user-create-form .form-group {
	  clear: both;
	  display: block;
	  float: left;
	  margin-bottom: 40px;
	  width: 100%;
	}
	.user-create-form .form-group > label {
	  color: #888;
	  float: left;
	  font-size: 20px;
	  font-weight: normal;
	  width: 20%;
	}
	.user-create-form .form-group .input-group {
	  float: left;
	  width: 40%;
	}
	.error-msg {
	  color: red;
	  display: inline-block;
	  padding: 5px 10px 5px 30px;
	  text-transform: capitalize;
	}
	.form-horizontal .form-group {
		margin-right: 0px;
		margin-left: 0px;
	}
	.cat-input-label{
		display: block; width: 100%; text-align: left !important; margin-bottom: 20px;
	}
	.btn-full{
		width: 100% !important;
	}
</style>
<style>
	.media-fream-warp {
			display: none;
			position: fixed;
			width: 100%;
			top: 0px;
			bottom: 0px;
			left: 0px;
			right: 0px;
			z-index: 9999999;
			padding: 100px 200px;
			background: rgba(0, 0, 0, 0.6);
		}
		body.overflow-hidden{
			overflow: hidden !important;
		}
		.media-close-shadow {
			position: fixed;
			width: 100%;
			height: 100%;
			z-index: -1;
			left: 0px;
			right: 0px;
			top: 0px;
			bottom: 0px;
			background: rgba(0, 0, 0, 0.5);
		}
		.media-fream-inner {
			background: #ffffff;
			z-index: 1;
		}
		.media-all {
			width: 100%;
			overflow: auto;
			padding: 21px;
			box-shadow: inset 0px 0px 15px rgba(0, 0, 0, 0.5);
			height: 400px;
		}
		.media-all .single-media {
			float: left;
			width: 15%;
			margin: 8px 8px;
			/* box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.5); */
		}
		.media-all .single-media img {
			max-width: 100%;
		}
		.media-fream-inner .header {
			padding: 15px 25px;
		}
		.media-fream-inner .header h3 {
			margin: 0px;
		}
		.media-fream-inner .footer {
			background: #FCFCFC;
			min-height: 50px;
			padding: 8px 25px;
			text-align: right;
			border-top: 1px solid #ddd;
		}
		.media-all .single-media.active {
			box-shadow: 0px 0px 2px 3px #3c8dbc;
			border: 1px solid #ffffff;
		}
		.post-thumb-field table tbody tr td img {
			width: 50px;
		}
</style>
@stop

@section('content')
	<div class="media-fream-warp" data-url="{{route('media.all')}}">
		<div class="media-fream-inner">
			<div class="header">
				<h3>Media Manager</h3>
			</div>
			<div class="media-all"></div>
			<div class="footer">
				<button class="btn btn-primary btn-flat media-insert">Insert</button>
			</div>
		</div>
		<div class="media-close-shadow"></div>
	</div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sliderslider Setting 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#settings"><i class="fa fa-cog"></i> Settings</a></li>
        <li class="active">Sliderslider</li>
      </ol>
    </section> 
    <!-- Main content --> 
        <section class="content">
          <div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="box">
					{!! Form::open(['route' => 'slider.update', 'class'=>'form-horizontal form-bordered' ]) !!}
					<div class="box-body"> 
						<div id="slider-holder">

							@if(count($sliders))
							@foreach(json_decode($sliders->first()->description)->title as $key => $title)
							<div class="col-md-12 ">
								<div class="col-md-6">
									<div class="form-group">
										<label for="cat name" class="cat-input-label control-label">#SLider</label>
										<input type="hidden" value="{{json_decode($sliders->first()->description)->sliderId[$key]}}" class="slider_id" name="sliderId[]" required>
										<input type="text" class="form-control" name="title[]" value="{{$title }}" placeholder="slider title..." required>
									</div>
									<div class="form-group">
										<textarea class="form-control" name="description[]" required>{{json_decode($sliders->first()->description)->description[$key]}}</textarea>
									</div>
									<a href="#" class="btn btn-default add-image-btn pull-left">insert image</a> 
									<a href="#" class="btn btn-danger delete-slide-btn pull-right">Delete Slide</a>
								</div>
								<div class="col-md-6" id="image-holder">
									<img src="{{ asset(getImageSizeById(json_decode($sliders->first()->description)->sliderId[$key],'cart-thumb')) }}">
								</div>
								<hr>
							</div>

							@endforeach
							@else
							<div class="col-md-12 ">
								<div class="col-md-6">
									<div class="form-group">
										<label for="cat name" class="cat-input-label control-label">#SLider</label>
										<input type="hidden" value="" class="slider_id" name="sliderId[]" required>
										<input type="text" class="form-control" name="title[]" value="" placeholder="slider title..." required>
									</div>
									<div class="form-group">
										<textarea class="form-control" name="description[]" required></textarea>
									</div>
									<a href="#" class="btn btn-default add-image-btn pull-left">insert image</a> 
									<a href="#" class="btn btn-danger delete-slide-btn pull-right">Delete Slide</a>
								</div>
								<div class="col-md-6" id="image-holder">
									
								</div>
								<hr>
							</div>
							@endif

						</div>
					

						<div class="form-group form-actions">
							<div class="cat-input-label">
								<a href="#" class="btn btn-success pull left add-slider">Add New</a>
								<button type="submit" id="add" class="btn btn-info pull-right">Update</button>
							</div>
						</div>
						
					</div>
					<!-- /.box-body -->
					{!! Form::close() !!}

				</div><!-- /.box -->
			</div>
          </div>   <!-- /.row -->
        </section><!-- /.content   where('id', $id) -->
	<!-- /.content -->

@stop

@section('scripts')
<script>
	var imgHolder;
	var imgInput;
	$(document).ready(function(){
		
	$(".add-image-btn").live('click', function (event){
	event.preventDefault();

	imgHolder = $(this).parent().next();
	imgInput = $(this).siblings().children('.slider_id');

	

	var fream = $('.media-fream-warp');
	fream.slideDown();
	$("body").addClass("overflow-hidden");
	var getMediaUrl = fream.data("url");
	$.ajax({
		url: getMediaUrl, //this is your uri
		type: 'get', //this is your method
		dataType: 'json',
		success: function(response){
			fream.find(".media-all").empty();
			var html = "";
			for (var i=0;i<response.data.length;i++){
				var id 		= response.data[i].id;
				html += '<div class="single-media" data-id="'+id+'"><img  src="'+response.data[i].source+'" alt="" ></div>';
			}
			fream.find(".media-all").append(html);
			}
		});
	});
	$(".media-all").find(".single-media").live('click', function () {
		$(this).addClass("active").siblings().removeClass("active");
	});
	$(".media-fream-warp").find(".media-insert").live('click', function () {
		console.log(imgHolder);
		
		var mediaWarp = $(this).parents(".footer").prev(".media-all");
		var mediaId = mediaWarp.find(".single-media.active").data('id');
		var mediasrc = mediaWarp.find(".single-media.active").children("img").attr('src');

		imgHolder.html('<img src="'+ mediasrc +'" width="200">');
		imgInput.val(mediaId);

		$(".media-fream-warp").slideUp();
		$("body").removeClass("overflow-hidden");
		$(this).unbind( "click" );
	});

	$(".post-thumb-field > table").find(".porduct-media").live('click', function (event) {
		event.preventDefault();
		$(this).parents("tr").remove();
	});
	$(".media-fream-warp .media-close-shadow").live('click', function () {
		$(this).parent('.media-fream-warp').slideUp();
		$("body").removeClass("overflow-hidden");
		$(".media-fream-warp").find(".media-insert").unbind( "click" );
	});

	$('.add-slider').live("click",function(){
		$('#slider-holder').append(
			'<div class="col-md-12">'+
				'<div class="col-md-6">'+
					'<div class="form-group">'+
						'<label for="cat name" class="cat-input-label control-label">#SLider</label>'+
						'<input type="hidden" value="" class="slider_id" name="sliderId[]" required>'+
						'<input type="text" class="form-control" name="title[]" value="" placeholder="slider title..." required>'+
					'</div>'+
					'<div class="form-group">'+
						'<textarea class="form-control" name="description[]" required></textarea>'+
					'</div>'+
					'<a href="#" class="btn btn-default add-image-btn pull-left">insert image</a> '+
					'<a href="#" class="btn btn-danger delete-slide-btn pull-right">Delete Slide</a>'+
				'</div>'+
				'<div class="col-md-6" class="image-holder">'+
					
				'</div>'+
			'</div><hr>'
		);
	});

	$('.delete-slide-btn').live("click",function(){
		$(this).parent().parent().remove();
	});

	$('#slider-holder').sortable();
});
</script>
@endsection