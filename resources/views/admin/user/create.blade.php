@extends('admin.layouts.master')

@section('title') Create User @stop

@section('styles')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('public/admin-assets/plugins/select2/select2.min.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('public/admin-assets/plugins/iCheck/all.css') }}">
    <style>
        .user-create-form .form-group {
            clear: both;
            display: block;
            float: left;
            margin-bottom: 40px;
            width: 100%;
        }

        .user-create-form .form-group > label {
            color: #888;
            float: left;
            font-size: 20px;
            font-weight: normal;
            width: 35%;
        }

        .user-create-form .form-group .input-group {
            float: left;
            width: 65%;
        }

        .error-msg {
            color: red;
            display: inline-block;
            padding: 5px 10px 5px 30px;
            text-transform: capitalize;
        }

        #multipleupload .dropzone {
            border: 2px dashed #0087f7;
            padding: 16px 20px;
            text-align: center;
        }

        .form-group.user-thumb-group > label,
        .form-group.user-thumb-group .input-group {
            width: 100%;
        }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Create User
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Create User</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <!-- <pre>
                //print_r($errors);
                </pre> -->
                <div class="box">
                    {!! Form::open(['route' => 'user.create', 'class' => 'user-create-form']) !!}
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label>User Name:</label>

                                <div class="input-group">
                                    <input type="text" class="form-control" name="username"
                                           value="{{ old('username') }}">
                                </div>
                                @if($errors->first('username'))
                                    <strong class="error-msg">{{ $errors->first('username') }} </strong>
                            @endif
                            <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <div class="form-group">
                                <label>User Email:</label>

                                <div class="input-group">
                                    <input type="text" class="form-control" name="email">
                                </div>
                                @if($errors->first('email'))
                                    <strong class="error-msg">{{ $errors->first('email') }} </strong>
                            @endif
                            <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <div class="form-group">
                                <label>User Password:</label>

                                <div class="input-group">
                                    <input type="text" class="form-control" name="password">
                                </div>
                                @if($errors->first('password'))
                                    <strong class="error-msg">{{ $errors->first('password') }} </strong>
                            @endif
                            <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <div class="form-group">
                                <label>Re-Password:</label>

                                <div class="input-group">
                                    <input type="text" class="form-control" name="repassword">
                                </div>
                                @if($errors->first('repassword'))
                                    <strong class="error-msg">{{ $errors->first('repassword') }} </strong>
                            @endif
                            <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <div class="form-group">
                                <label>User Rule:</label>

                                <div class="input-group">
                                    <select class="form-control select2" name="rule" style="width: 100%;">
                                        @foreach($roles as $role)
                                            <option value="{{ $role }}">{{ $role }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->first('rule'))
                                        <strong class="error-msg">{{ $errors->first('rule') }} </strong>
                                @endif
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <!-- phone mask -->
                            <div class="form-group">
                                <label> Phone:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="phone">
                                </div>
                                @if($errors->first('phone'))
                                        <strong class="error-msg">{{ $errors->first('phone') }} </strong>
                                @endif
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <div class="form-group">
                                <label> Billing City:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="billing_city">
                                </div>
                                @if($errors->first('billing_city'))
                                        <strong class="error-msg">{{ $errors->first('billing_city') }} </strong>
                                @endif
                            <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <div class="form-group">
                                <label> Billing Area:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="billing_area">
                                </div>
                                @if($errors->first('billing_area'))
                                    <strong class="error-msg">{{ $errors->first('billing_area') }} </strong>
                            @endif
                            <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <div class="form-group">
                                <label>User Address:</label>
                                <div class="input-group">
                                    <textarea class="form-control" name="billing_address"></textarea>
                                </div>
                                @if($errors->first('billing_address'))
                                    <strong class="error-msg">{{ $errors->first('billing_address') }} </strong>
                            @endif
                            <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <div class="form-group">
                                <label>User Acount Status:</label>

                                <div class="input-group">
                                    <select class="form-control select2" name="status" style="width: 100%;">
                                            <option value="active">Active</option>
                                            <option value="deactivate">Deactivate</option>
                                            <option value="suspended">Suspended</option>
                                    </select>
                                </div>
                                @if($errors->first('status'))
                                        <strong class="error-msg">{{ $errors->first('status') }} </strong>
                                @endif
                            <!-- /.input group -->
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-block btn-default btn-flat">Submit</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@stop
@section('scripts')
    <!-- Select2 -->
    <script src="{{ asset('public/admin-assets/plugins/select2/select2.full.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('public/admin-assets/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('public/admin-assets/dist/js/dropzone.js') }}"></script>
    <!-- page script -->
    <script>
        //Initialize Select2 Elements
        $(".select2").select2();
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    </script>
    <script>
        $(document).ready(function () {
            var actionUrl = $(".multipleupload").attr('data-action');
            Dropzone.autoDiscover = false;
            $("#multipleupload").dropzone({
                paramName: "picture",
                url: actionUrl,
                uploadMultiple: false,
                previewsContainer: false,
                addRemoveLinks: true,
                sending: function (file, xhr, formData) {
                    formData.append('_token', $("input[name=_token]").val());
                },
                success: function (file, response, done) {
                    console.log("aaaaaaaaaaa");
                    $("#multipleupload").parents('.post-thumb-field').find('#post-thumb-input').attr("value", response.imageName);
                    $("#multipleupload").parents('.post-thumb-field').find('#post-thumb-prev').attr("src", response.image);
                }
            });
        });
    </script>
@stop