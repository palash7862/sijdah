@extends('admin.layouts.master')

@section('title') All User @stop

@section('styles') 
<style>
	.admin-thumb{
		width: 50px;
	}
</style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
			All User 
		</h1>
      	<ol class="breadcrumb">
    		<li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    		<li class="active">All User</li>
      	</ol>
    </section> 
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header"> 
					</div>
					<!-- /.box-header -->
					<div class="box-body">  
						<table id="example2" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Thumb</th> 
									<th>Name</th> 
									<th>Email</th> 
									<th>Phone</th> 
									<th>Role</th> 
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead> 
							<tbody>  
								@foreach($users as $user)
								<tr>
									<td> {{ $user->id }} </td>
									<td>  
										<img src="">
									</td> 
									<td>{{ $user->username }}</td>
									<td>{{ $user->email }}</td> 
									<td>{{ $user->phone }}</td>
									<td>{{ $user->level }}</td>
									<td>
										<a href="{{ route('user.edit', array($user->id)) }}" class="btn btn-app" >
										<i class="fa fa-edit"></i>
										Edit
										</a>
										<a href="{{ route('user.delete', array($user->id)) }}" class="btn delete-user btn-app" >
											<i class="fa fa-trash-o"></i>
											Delete
										</a>
									</td>
								</tr> 
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>ID</th>
									<th>Thumb</th> 
									<th>Name</th> 
									<th>Email</th> 
									<th>Phone</th> 
									<th>Role</th> 
									<th>Status</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
					</div>
				<!-- /.box-body -->
				</div>
			</div>
		</div>
    </section>
    <!-- /.content -->

@stop
@section('scripts') 
	<!-- page script -->
<script>
	$(function () {
		//$('#example2').DataTable();
		$('.delete-user').on('click', function (event) {
			var rlink = $(this).attr('href');
			event.preventDefault();
			swal({
				title: 'Are you sure?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
				if (result.value) {
				window.location.href = rlink;
			}
		});
		});
	});
</script>
@stop