@extends('admin.layouts.master')

@section('title','Subscribers')

@section('styles') 
<style>
	.user-create-form .form-group {
	  clear: both;
	  display: block;
	  float: left;
	  margin-bottom: 40px;
	  width: 100%;
	}
	.user-create-form .form-group > label {
	  color: #888;
	  float: left;
	  font-size: 20px;
	  font-weight: normal;
	  width: 20%;
	}
	.user-create-form .form-group .input-group {
	  float: left;
	  width: 40%;
	}
	.error-msg {
	  color: red;
	  display: inline-block;
	  padding: 5px 10px 5px 30px;
	  text-transform: capitalize;
	}
	.form-horizontal .form-group {
		margin-right: 0px;
		margin-left: 0px;
	}
	.cat-input-label{
		display: block; width: 100%; text-align: left !important; margin-bottom: 20px;
	}
	.btn-full{
		width: 100% !important;
	}
</style>
@endsection

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Subscribers 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Subscribers</li>
      </ol>
    </section> 
    <!-- Main content --> 
        <section class="content">
          <div class="row">
            <!-- right column -->
            <div class="col-md-8 col-md-offset-2">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-body"> 
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Email</th>
                            <th>Status</th> 
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
					<tbody> 
						@foreach($subscribers as $subscriber)
						<tr>
                            <td>{{ $subscriber->id }}</td>  
                            <td>{{ $subscriber->email }}</td>  
                            <td>{{ $subscriber->status }}</td> 
                            <td>
								<a href="{{ route('subscriber.delete', array($subscriber->id)) }}" class="btn delete-subscriber btn-app" >
									<i class="fa fa-trash-o"></i>
									Delete
								</a> 
							</td>
                        </tr>
						@endforeach
					</tbody> 
					<tfoot>
                        <tr>
                            <th>#ID</th>
                            <th>Email</th>
                            <th>Status</th> 
                            <th class="text-center">Action</th>
                        </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.box-body -->

              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content   where('id', $id) -->
	<!-- /.content -->

@stop
