@extends('admin.layouts.master')

@section('title') Coupons @stop

@section('styles')
<style>
	.user-create-form .form-group {
	  clear: both;
	  display: block;
	  float: left;
	  margin-bottom: 40px;
	  width: 100%;
	}
	.user-create-form .form-group > label {
	  color: #888;
	  float: left;
	  font-size: 20px;
	  font-weight: normal;
	  width: 20%;
	}
	.user-create-form .form-group .input-group {
	  float: left;
	  width: 40%;
	}
	.error-msg {
	  color: red;
	  display: inline-block;
	  padding: 5px 10px 5px 30px;
	  text-transform: capitalize;
	}
	.form-horizontal .form-group {
		margin-right: 0px;
		margin-left: 0px;
	}
	.cat-input-label{
		display: block; width: 100%; text-align: left !important; margin-bottom: 20px;
	}
	.btn-full{
		width: 100% !important;
	}
</style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Coupons 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Coupons</li>
      </ol>
    </section>

    <!-- Main content --> 
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

    <section class="content">
      <div class="row">
		<div class="col-md-4">
			<div class="box">
				<div class="box-body">  
					@if (Route::currentRouteName() == 'coupon.edit') 					
						{!! Form::open(['route' => 'coupon.update', 'class'=>'form-horizontal form-bordered' ]) !!}
					@else
						{!! Form::open(['route' => 'coupon.create', 'class'=>'form-horizontal form-bordered' ]) !!}
					@endif

					<!-- Page Title -->
					@if (Route::currentRouteName() == 'coupon.edit') 

						<input type="hidden" name="coupon_id" value="{{ $cp->id }}" />
						<div class="form-group">
							<label for="title" class="control-label cat-input-label" >Title</label>
							<input type="text" class="form-control" name="title" value="{{$cp->title}}">
							@if($errors->first('title'))
								<strong class="error-msg">{{ $errors->first('title') }} </strong>
							@endif
						</div>
						<div class="form-group">
							<label for="coupon-code" class="control-label">Coupon Code</label>
							<input type="text" class="form-control" name="couponcode" value="{{ $cp->couponcode }}">
								@if($errors->first('couponcode'))
									<strong class="error-msg">{{ $errors->first('couponcode') }} </strong>
								@endif
						</div>
						<div class="form-group">
							<label class="custom-control custom-radio"> Coupon Type: 
							  <input id="radio1" name="type" type="radio" class="custom-control-input" value="fixed" @if($cp->type == 'fixed') checked @endif> Fixed
							</label>
							<label class="custom-control custom-radio">
							  <input id="radio2" name="type" type="radio" class="custom-control-input" value="percentage" @if($cp->type == 'percentage') checked @endif> Percentage
							</label>
							@if($errors->first('type'))
								<strong class="error-msg">{{ $errors->first('type') }} </strong>
							@endif
						</div>
						<div class="form-group">
							<label for="amount" class="control-label cat-input-label" >Amount/Percentage</label>
							<input type="text" class="form-control" name="amount" value="{{ (int)$cp->amount }}">
							@if($errors->first('amount'))
								<strong class="error-msg">{{ $errors->first('amount') }} </strong>
							@endif
						</div>
						<div class="form-group">
							<label for="date" class="control-label cat-input-label" >Title</label>
							<input type="date" class="form-control" name="expired_date" value="{{ $cp->expired_date }}">
							@if($errors->first('date'))
								<strong class="error-msg">{{ $errors->first('date') }} </strong>
							@endif
						</div>		

					@else
						<div class="form-group">
							<label for="title" class="control-label cat-input-label" >Title</label>
							<input type="text" class="form-control" name="title" placeholder="Coupon title...">
							@if($errors->first('title'))
								<strong class="error-msg">{{ $errors->first('title') }} </strong>
							@endif
						</div>
						<div class="form-group">
							<label for="coupon-code" class="control-label">Coupon Code</label>
							<input type="text" class="form-control" name="couponcode" placeholder="Coupon code(alpha numeric)...">
								@if($errors->first('couponcode'))
									<strong class="error-msg">{{ $errors->first('couponcode') }} </strong>
								@endif
						</div>
						<div class="form-group">
							<label class="custom-control custom-radio"> Coupon Type: 
							  <input id="radio1" name="type" type="radio" class="custom-control-input" value="fixed" checked> Fixed
							</label>
							<label class="custom-control custom-radio">
							  <input id="radio2" name="type" type="radio" class="custom-control-input" value="percentage"> Percentage
							</label>
							@if($errors->first('type'))
								<strong class="error-msg">{{ $errors->first('type') }} </strong>
							@endif
						</div>
						<div class="form-group">
							<label for="amount" class="control-label cat-input-label" >Amount/Percentage</label>
							<input type="text" class="form-control" name="amount" placeholder="coupon amount...">
							@if($errors->first('amount'))
								<strong class="error-msg">{{ $errors->first('amount') }} </strong>
							@endif
						</div>
						<div class="form-group">
							<label for="date" class="control-label cat-input-label" >Title</label>
							<input type="date" class="form-control" name="expired_date" value="{{ date('Y-m-d') }}">
							@if($errors->first('date'))
								<strong class="error-msg">{{ $errors->first('date') }} </strong>
							@endif
						</div>


					@endif

					<div class="form-group form-actions">
						<div class="cat-input-label">
							@if (Route::currentRouteName() == 'coupon.edit')
								<button type="submit" id="add" class="btn btn-info btn-full">Update</button>
							@else
								<button type="submit" id="add" class="btn btn-info btn-full">Create</button>
							@endif
						</div>
					</div>
					{!! Form::close() !!}
				</div>
				<!-- /.box-body -->

			</div><!-- /.box -->
		</div>

        <!-- right column -->
        <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box">
            <div class="box-body"> 
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#ID</th>
                        <th>Title</th>
                        <th>Coupon Code</th>
                        <th>Type</th>
                        <th>Amount</th> 
                        <th>Expired Date</th>   
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
				<tbody> 
					@foreach($coupons as $coupon)
					<tr>
                        <td>{{ $coupon->id }}</td>  
                        <td>{{ $coupon->title }}</td>  
                        <td>{{ $coupon->couponcode }}</td>
                        <td>{{ $coupon->type }}</td>
                        <td>{{ (int)$coupon->amount }}</td>
                        <td>{{ $coupon->expired_date }}</td> 
                        <td> 
							@if (Route::currentRouteName() != 'coupon.edit')
							<a href="{{ route('coupon.edit', array($coupon->id)) }}" class="btn btn-app" >
								<i class="fa fa-edit"></i>
								Edit
							</a>
							@endif
							<a href="{{ route('coupon.delete', array($coupon->id)) }}" class="btn btn-app delete-coupon" >
								<i class="fa fa-trash-o"></i>
								Delete
							</a> 
						</td>
                    </tr>
					@endforeach
				</tbody> 
				<tfoot>
                    <tr>
                        <th>#ID</th>
                        <th>Title</th>
                        <th>Coupon Code</th>
                        <th>Type</th>
                        <th>Amount</th> 
                        <th>Expired Date</th>   
                        <th class="text-center">Action</th>
                    </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->

          </div><!-- /.box -->

        </div><!--/.col (right) -->
      </div>   <!-- /.row -->
    </section><!-- /.content   where('id', $id) -->
	<!-- /.content -->

@stop
@section('scripts')   
<!-- page script -->
	<script>
//	  $(function () {
//		$('#example2').DataTable({
//		  "paging": true,
//		  "lengthChange": false,
//		  "searching": false,
//		  "ordering": true,
//		  "info": true,
//		  "autoWidth": false
//		});
//	  });
	</script> 
	<script>
        $(function () {
			$('.delete-coupon').on('click', function (event) {
				var rlink = $(this).attr('href');
				event.preventDefault();
				swal({
					title: 'Are you sure?',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
					if (result.value) {
						window.location.href = rlink;
					}
				});
			});
        });
//		$('.delete-section').on('click',function(e){
//			e.preventDefault();
//			var delete_url = this.href;
//			var elm = $(this).closest('tr');
//			swal({
//			      title: "Are you sure?",
//			      text: "Are you sure that you want to delete this section?",
//			      type: "warning",
//			      showCancelButton: true,
//			      closeOnConfirm: false,
//			      confirmButtonText: "delete it!",
//			      confirmButtonColor: "#ec6c62"
//			    }, function() {
//			      $.ajax({
//			        url: delete_url,
//			        type: "GET"
//			      })
//			      .done(function(data) {
//			      	elm.remove();
//			        swal("Deleted!", "Section was successfully deleted!", "success");
//			      })
//			      .error(function(data) {
//			        swal("Oops", "We couldn't connect to the server!", "error");
//			      });
//			    });
//		});
	</script>

@stop