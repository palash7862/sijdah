@extends('admin.layouts.master')

@section('title') Categorys @stop

@section('styles') 
<style>
	.user-create-form .form-group {
	  clear: both;
	  display: block;
	  float: left;
	  margin-bottom: 40px;
	  width: 100%;
	}
	.user-create-form .form-group > label {
	  color: #888;
	  float: left;
	  font-size: 20px;
	  font-weight: normal;
	  width: 20%;
	}
	.user-create-form .form-group .input-group {
	  float: left;
	  width: 40%;
	}
	.error-msg {
	  color: red;
	  display: inline-block;
	  padding: 5px 10px 5px 30px;
	  text-transform: capitalize;
	}
	.form-horizontal .form-group {
		margin-right: 0px;
		margin-left: 0px;
	}
	.cat-input-label{
		display: block; width: 100%; text-align: left !important; margin-bottom: 20px;
	}
	.btn-full{
		width: 100% !important;
	}
</style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Categorys 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Publishers</li>
      </ol>
    </section> 
    <!-- Main content --> 
        <section class="content">
          <div class="row">
			<div class="col-md-4">
				<div class="box">
					<div class="box-body">  
						@if (Route::currentRouteName() == 'publisher.edit') 					
							{!! Form::open(['route' => 'publisher.update', 'class'=>'form-horizontal form-bordered' ]) !!}
						@else
							{!! Form::open(['route' => 'publisher.create', 'class'=>'form-horizontal form-bordered' ]) !!}
						@endif
							<!-- Page Title -->
							@if (Route::currentRouteName() == 'publisher.edit') 
								@foreach($spublisher as $publisher)
								<input type="hidden" name="publisher_id" value="{{ $publisher->id }}" />
								<div class="form-group">
									<label for="cat name" class="cat-input-label control-label">Publisher name</label>
									<input type="text" class="form-control" name="name[en]" value="{{ json_decode($publisher->name)->en }}" placeholder="Enter the Publisher title...">
									@if($errors->first('name')) 
										<strong class="error-msg">{{ $errors->first('name.en') }} </strong>
									@endif
								</div>
									<div class="form-group">
										<label for="cat name" class="cat-input-label control-label">Publisher Name (Bangla)</label>
										<input type="text" class="form-control" name="name[bn]" value="{{ json_decode($publisher->name)->bn }}" placeholder="Enter the Publisher title...">
										@if($errors->first('name'))
											<strong class="error-msg">{{ $errors->first('name.bn') }} </strong>
										@endif
									</div>
								<div class="form-group">
									<label>Description</label>
									<textarea name="description" class="form-control" rows="3" placeholder="Enter Your Publisher Description">{{ $publisher->description }}</textarea>
								</div>
								@endforeach
							@else
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Publisher Name (English)</label>
									<input type="text" class="form-control" name="name[en]" placeholder="Enter the Publisher title...">
									@if($errors->first('name.en'))
										<strong class="error-msg">{{ $errors->first('name.en') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Publisher Name (Bangla)</label>
									<input type="text" class="form-control" name="name[bn]" placeholder="Enter the Publisher title...">
									@if($errors->first('name[bn]'))
										<strong class="error-msg">{{ $errors->first('name[bn]') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea name="description" class="form-control" rows="3" placeholder="Enter Your Publisher Description"></textarea>
								</div>
							@endif
							
							<div class="form-group form-actions">
								<div class="cat-input-label">
									@if (Route::currentRouteName() == 'publisher.edit')
										<button type="submit" id="add" class="btn btn-info btn-full">Update</button>
									@else
										<button type="submit" id="add" class="btn btn-info btn-full">Create</button>
									@endif
								</div>
							</div>
						{!! Form::close() !!}
					</div>
					<!-- /.box-body -->

				</div><!-- /.box -->
			</div>
            <!-- right column -->
            <div class="col-md-8">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-body"> 
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Publisher Name</th>
                            <th>Description</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
					<tbody> 
						@foreach($publishers as $publisher)
						<tr>
                            <th>{{ $publisher->id }}</th>  
                            <th>{{ json_decode($publisher->name)->en }} ( {{ json_decode($publisher->name)->bn }} )</th>
                            <th>{{ $publisher->description }}</th>
                            <th> 
								@if (Route::currentRouteName() != 'publisher.edit')
								<a href="{{ route('publisher.edit', array($publisher->id)) }}" class="btn btn-app" >
									<i class="fa fa-edit"></i>
									Edit
								</a>
								@endif
								<a href="{{ route('publisher.delete', array($publisher->id)) }}" class="btn delete-publisher btn-app" >
									<i class="fa fa-trash-o"></i>
									Delete
								</a> 
							</th>
                        </tr>
						@endforeach
					</tbody> 
					<tfoot>
                        <tr>
                            <th>#ID</th>
                            <th>Publisher Name</th>
                            <th>Description</th>
                            <th class="text-center">Options</th>
                        </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.box-body -->

              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content   where('id', $id) -->
	<!-- /.content -->

@stop
@section('scripts')   
<!-- page script -->
<script>
    $(function () {
        $('.delete-publisher').on('click', function (event) {
            var rlink = $(this).attr('href');
            event.preventDefault();
            swal({
                title: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                window.location.href = rlink;
            }
        });
        });
//			$('#example2').DataTable({
//			  "paging": true,
//			  "lengthChange": false,
//			  "searching": false,
//			  "ordering": true,
//			  "info": true,
//			  "autoWidth": false
//			});
    });
</script>
@stop