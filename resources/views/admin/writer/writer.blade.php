@extends('admin.layouts.master')

@section('title') Writers @stop

@section('styles') 
<style>
	.user-create-form .form-group {
	  clear: both;
	  display: block;
	  float: left;
	  margin-bottom: 40px;
	  width: 100%;
	}
	.user-create-form .form-group > label {
	  color: #888;
	  float: left;
	  font-size: 20px;
	  font-weight: normal;
	  width: 20%;
	}
	.user-create-form .form-group .input-group {
	  float: left;
	  width: 40%;
	}
	.error-msg {
	  color: red;
	  display: inline-block;
	  padding: 5px 10px 5px 30px;
	  text-transform: capitalize;
	}
	.form-horizontal .form-group {
		margin-right: 0px;
		margin-left: 0px;
	}
	.cat-input-label{
		display: block; width: 100%; text-align: left !important; margin-bottom: 20px;
	}
	.btn-full{
		width: 100% !important;
	}
</style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Writers 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Writers</li>
      </ol>
    </section> 
    <!-- Main content --> 
        <section class="content">
          <div class="row">
			<div class="col-md-4">
				<div class="box">
					<div class="box-body">  
						@if (Route::currentRouteName() == 'writer.edit') 					
							{!! Form::open(['route' => 'writer.update', 'class'=>'form-horizontal form-bordered' ]) !!}
						@else
							{!! Form::open(['route' => 'writer.create', 'class'=>'form-horizontal form-bordered' ]) !!}
						@endif
							<!-- Page Title -->
							@if (Route::currentRouteName() == 'writer.edit') 
								@foreach($swriter as $wr)
								<input type="hidden" name="writer_id" value="{{ $wr->id }}" />
								<div class="form-group">
									<label for="cat name" class="cat-input-label control-label">Writer name(English)</label>
									<input type="text" class="form-control" name="name_en" value="{{ json_decode($wr->name)->en }}" placeholder="Enter the Writer name...">
									@if($errors->first('name')) 
										<strong class="error-msg">{{ $errors->first('name') }} </strong> 
									@endif
								</div>
								<div class="form-group">
									<label for="cat name" class="cat-input-label control-label">Writer name (Bangla)</label>
									<input type="text" class="form-control" name="name_bn" value="{{ json_decode($wr->name)->bn }}" placeholder="Enter the Writer name...">
									@if($errors->first('name')) 
										<strong class="error-msg">{{ $errors->first('name') }} </strong> 
									@endif
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea name="description" class="form-control" rows="3" placeholder="Enter Your Writer Description">{{ $wr->description }}</textarea>
								</div>
								@endforeach
							@else
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Writer name(English)</label>
									<input type="text" class="form-control" name="name_en" placeholder="Enter the Writer name...">
									@if($errors->first('name'))
										<strong class="error-msg">{{ $errors->first('name') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Writer name(Bangla)</label>
									<input type="text" class="form-control" name="name_bn" placeholder="Enter the Writer name...">
									@if($errors->first('name'))
										<strong class="error-msg">{{ $errors->first('name') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea name="description" class="form-control" rows="3" placeholder="Enter Your Writer Description"></textarea>
								</div>
							@endif
							
							<div class="form-group form-actions">
								<div class="cat-input-label">
									@if (Route::currentRouteName() == 'writer.edit')
										<button type="submit" id="add" class="btn btn-info btn-full">Update</button>
									@else
										<button type="submit" id="add" class="btn btn-info btn-full">Create</button>
									@endif
								</div>
							</div>
						{!! Form::close() !!}
					</div>
					<!-- /.box-body -->

				</div><!-- /.box -->
			</div>
            <!-- right column -->
            <div class="col-md-8">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-body"> 
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Writer name</th>
                            <th>Description</th> 
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
					<tbody> 
						@foreach($writers as $writer)
						<tr>
                            <th>{{ $writer->id }}</th>  
                            <th>{{ json_decode($writer->name)->en }} ({{ json_decode($writer->name)->bn }})</th>  
                            <th>{{ $writer->description }}</th> 
                            <th> 
								@if (Route::currentRouteName() != 'writer.edit')
								<a href="{{ route('writer.edit', array($writer->id)) }}" class="btn btn-app" >
									<i class="fa fa-edit"></i>
									Edit
								</a>
								@endif
								<a href="{{ route('writer.delete', array($writer->id)) }}" class="btn delete-writer btn-app" >
									<i class="fa fa-trash-o"></i>
									Delete
								</a> 
							</th>
                        </tr>
						@endforeach
					</tbody> 
					<tfoot>
                        <tr>
                            <th>#ID</th>
                            <th>Writer name</th>
                            <th>Description</th> 
                            <th class="text-center">Action</th>
                        </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.box-body -->

              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content   where('id', $id) -->
	<!-- /.content -->

@stop
@section('scripts')   
<!-- page script -->
<script>
    $(function () {
        $('.delete-writer').on('click', function (event) {
            var rlink = $(this).attr('href');
            event.preventDefault();
            swal({
                title: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                	window.location.href = rlink;
				}
			});
        });
//			$('#example2').DataTable({
//			  "paging": true,
//			  "lengthChange": false,
//			  "searching": false,
//			  "ordering": true,
//			  "info": true,
//			  "autoWidth": false
//			});
    });
</script>
@stop