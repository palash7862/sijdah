@extends('admin.layouts.master')

@section('title') Contact Settings @stop

@section('styles') 
<style>
	.user-create-form .form-group {
	  clear: both;
	  display: block;
	  float: left;
	  margin-bottom: 40px;
	  width: 100%;
	}
	.user-create-form .form-group > label {
	  color: #888;
	  float: left;
	  font-size: 20px;
	  font-weight: normal;
	  width: 20%;
	}
	.user-create-form .form-group .input-group {
	  float: left;
	  width: 40%;
	}
	.error-msg {
	  color: red;
	  display: inline-block;
	  padding: 5px 10px 5px 30px;
	  text-transform: capitalize;
	}
	.form-horizontal .form-group {
		margin-right: 0px;
		margin-left: 0px;
	}
	.cat-input-label{
		display: block; width: 100%; text-align: left !important; margin-bottom: 20px;
	}
	.btn-full{
		width: 100% !important;
	}
</style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Contact 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#settings"><i class="fa fa-cog"></i> Settings</a></li>
        <li class="active">Contact</li>
      </ol>
    </section> 
    <!-- Main content --> 
        <section class="content">
          <div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="box">
					<div class="box-body">  
											
						{!! Form::open(['route' => 'contact.update', 'class'=>'form-horizontal form-bordered' ]) !!}
						<!-- Page Title -->
						<div class="form-group">
							<label for="cat name" class="cat-input-label control-label">Phone</label>
							<input type="text" class="form-control" name="phone" value=" @isset($phone) {{ $phone->description }} @endisset" >
							@if($errors->first('phone')) 
								<strong class="error-msg">{{ $errors->first('phone') }} </strong> 
							@endif
						</div>
						<div class="form-group">
							<label for="cat name" class="cat-input-label control-label">Email</label>
							<input type="text" class="form-control" name="email" value=" @isset($email) {{ $email->description }} @endisset" >
							@if($errors->first('email')) 
								<strong class="error-msg">{{ $errors->first('email') }} </strong> 
							@endif
						</div>
						<div class="form-group">
							<label for="cat name" class="cat-input-label control-label">Address(English)</label>
							<input type="text" class="form-control" name="address_en" value=" @isset($address_en) {{ $address_en->description }} @endisset" >
							@if($errors->first('address_en')) 
								<strong class="error-msg">{{ $errors->first('address_en') }} </strong> 
							@endif
						</div>
						<div class="form-group">
							<label for="cat name" class="cat-input-label control-label">Address(Bangla)</label>
							<input type="text" class="form-control" name="address_bn" value=" @isset($address_bn) {{ $address_bn->description}} @endisset" >
							@if($errors->first('address_bn')) 
								<strong class="error-msg">{{ $errors->first('address_bn') }} </strong> 
							@endif
						</div>
						<div class="form-group form-actions">
							<div class="cat-input-label">
									<button type="submit" id="add" class="btn btn-info btn-full">Update</button>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
					<!-- /.box-body -->

				</div><!-- /.box -->
			</div>
          </div>   <!-- /.row -->
        </section><!-- /.content   where('id', $id) -->
	<!-- /.content -->

@stop