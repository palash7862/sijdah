@extends('admin.layouts.master')

@section('title') Shiping Settings @stop

@section('styles') 
<style>
	.user-create-form .form-group {
	  clear: both;
	  display: block;
	  float: left;
	  margin-bottom: 40px;
	  width: 100%;
	}
	.user-create-form .form-group > label {
	  color: #888;
	  float: left;
	  font-size: 20px;
	  font-weight: normal;
	  width: 20%;
	}
	.user-create-form .form-group .input-group {
	  float: left;
	  width: 40%;
	}
	.error-msg {
	  color: red;
	  display: inline-block;
	  padding: 5px 10px 5px 30px;
	  text-transform: capitalize;
	}
	.form-horizontal .form-group {
		margin-right: 0px;
		margin-left: 0px;
	}
	.cat-input-label{
		display: block; width: 100%; text-align: left !important; margin-bottom: 20px;
	}
	.btn-full{
		width: 100% !important;
	}
</style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Shipping 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#settings"><i class="fa fa-cog"></i> Settings</a></li>
        <li class="active">Shipping</li>
      </ol>
    </section> 
    <!-- Main content --> 
        <section class="content">
          <div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="box">
					<div class="box-body">  
											
						{!! Form::open(['route' => 'shipping.update', 'class'=>'form-horizontal form-bordered' ]) !!}
						<!-- Page Title -->
						<div class="form-group">
							<label for="cat name" class="cat-input-label control-label">Shipping Charge inside Dhaka</label>
							<input type="text" class="form-control" name="ship_dhaka" value=" @isset($ship_dhaka) {{ $ship_dhaka->description }} @endisset" >
							@if($errors->first('ship_dhaka')) 
								<strong class="error-msg">{{ $errors->first('ship_dhaka') }} </strong> 
							@endif
						</div>
						<div class="form-group">
							<label for="cat name" class="cat-input-label control-label">Shipping Charge outside Dhaka</label>
							<input type="text" class="form-control" name="ship_outside" value=" @isset($ship_outside) {{ $ship_outside->description }} @endisset" >
							@if($errors->first('ship_outside')) 
								<strong class="error-msg">{{ $errors->first('ship_outside') }} </strong> 
							@endif
						</div>
						<div class="form-group form-actions">
							<div class="cat-input-label">
									<button type="submit" id="add" class="btn btn-info btn-full">Update</button>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
					<!-- /.box-body -->

				</div><!-- /.box -->
			</div>
          </div>   <!-- /.row -->
        </section><!-- /.content   where('id', $id) -->
	<!-- /.content -->

@stop