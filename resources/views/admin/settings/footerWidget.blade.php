@extends('admin.layouts.master')

@section('title') Shiping Settings @stop

@section('styles') 
<style>
	.user-create-form .form-group {
	  clear: both;
	  display: block;
	  float: left;
	  margin-bottom: 40px;
	  width: 100%;
	}
	.user-create-form .form-group > label {
	  color: #888;
	  float: left;
	  font-size: 20px;
	  font-weight: normal;
	  width: 20%;
	}
	.user-create-form .form-group .input-group {
	  float: left;
	  width: 40%;
	}
	.error-msg {
	  color: red;
	  display: inline-block;
	  padding: 5px 10px 5px 30px;
	  text-transform: capitalize;
	}
	.form-horizontal .form-group {
		margin-right: 0px;
		margin-left: 0px;
	}
	.cat-input-label{
		display: block; width: 100%; text-align: left !important; margin-bottom: 20px;
	}
	.btn-full{
		width: 100% !important;
	}
	.form-group { 
	    overflow: hidden;
	}
</style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Shipping 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#settings"><i class="fa fa-cog"></i> Settings</a></li>
        <li class="active">Shipping</li>
      </ol>
    </section> 
    <!-- Main content --> 
        <section class="content">
          <div class="row">
			<div class="col-md-8 col-md-offset-2">
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@else
				<div class="alert alert-success alert-dismissible"">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  You can change the position of urls by Drag and Drop!
				</div>
				@endif

				<!-- ============================Widget 1 =========================== -->
				
				<div class="panel panel-success">
					{!! Form::open(['route' => 'footerWidget.update', 'class'=>'form-horizontal form-bordered' ]) !!}
				    <div class="panel-heading"> 
				    	<input type="hidden" name="widget_id" value="1">

		    	    	<div class="form-group">
		    				<label for="cat name" class="cat-input-label control-label">Footer Left Title</label>
		    				<input type="text" class="form-control" name="widget_title" value="@isset($footerWidget_1) {{ json_decode($footerWidget_1->description)->widget_title }} @endisset" >
		    				@if($errors->first('widget_title')) 
		    					<strong class="error-msg">{{ $errors->first('widget_title') }} </strong> 
		    				@endif
		    			</div>
				    </div>

				    <div class="panel-body footer-links">
				    	@if($footerWidget_1)
				    	@foreach(json_decode($footerWidget_1->description)->title as $key=> $title) 
				    	<div class="form-group">
				    		<div class="col-md-1">
				    			<label class="cat-input-label control-label">
				    				<a href="#" class="remove-url"><i class="fa fa-trash"></i></a>
				    			</label>
				    		</div>
				    		<div class="col-md-4">
				    			<input type="text" class="form-control" name="title[]" value="{{ $title }}" placeholder="title...">
				    		</div>
				    		<div class="col-md-7">
				    			<input type="text" class="form-control" name="url[]" value="{{ json_decode($footerWidget_1->description)->url[$key] }}" placeholder="url of page  (ex. http://...">
				    		</div>
				    	</div>
				    	@endforeach
				    	@else
				    	<div class="form-group">
				    		<div class="col-md-1">
				    			<label class="cat-input-label control-label">
				    				<a href="#" class="remove-url"><i class="fa fa-trash"></i></a>
				    			</label>
				    		</div>
				    		<div class="col-md-4">
				    			<input type="text" class="form-control" name="title[]" value="" placeholder="title...">
				    		</div>
				    		<div class="col-md-7">
				    			<input type="text" class="form-control" name="url[]" value="" placeholder="url of page  (ex. http://...">
				    		</div>
				    	</div>
				    	@endif

				    </div>

				    <div class="panel-footer">
				    	<button class="btn btn-info pull-right">Update</button>
				    	
				    	<a class="btn btn-success add-url">Add New</a>	
				    </div>
				    {!! Form::close() !!}
				 </div>



				 <!-- ============================Widget 2 =========================== -->
				 <div class="panel panel-success">
					{!! Form::open(['route' => 'footerWidget.update', 'class'=>'form-horizontal form-bordered' ]) !!}
				    <div class="panel-heading"> 
				    	<input type="hidden" name="widget_id" value="2">

		    	    	<div class="form-group">
		    				<label for="cat name" class="cat-input-label control-label">Footer Middle Title</label>
		    				<input type="text" class="form-control" name="widget_title" value="@isset($footerWidget_2) {{ json_decode($footerWidget_2->description)->widget_title }} @endisset" >
		    				@if($errors->first('widget_title')) 
		    					<strong class="error-msg">{{ $errors->first('widget_title') }} </strong> 
		    				@endif
		    			</div>
				    </div>

				    <div class="panel-body footer-links">
				    	@if($footerWidget_2)
				    	@foreach(json_decode($footerWidget_2->description)->title as $key=> $title) 
				    	<div class="form-group">
				    		<div class="col-md-1">
				    			<label class="cat-input-label control-label">
				    				<a href="#" class="remove-url"><i class="fa fa-trash"></i></a>
				    			</label>
				    		</div>
				    		<div class="col-md-4">
				    			<input type="text" class="form-control" name="title[]" value="{{ $title }}" placeholder="title...">
				    		</div>
				    		<div class="col-md-7">
				    			<input type="text" class="form-control" name="url[]" value="{{ json_decode($footerWidget_2->description)->url[$key] }}" placeholder="url of page  (ex. http://...">
				    		</div>
				    	</div>
				    	@endforeach
				    	@else
				    	<div class="form-group">
				    		<div class="col-md-1">
				    			<label class="cat-input-label control-label">
				    				<a href="#" class="remove-url"><i class="fa fa-trash"></i></a>
				    			</label>
				    		</div>
				    		<div class="col-md-4">
				    			<input type="text" class="form-control" name="title[]" value="" placeholder="title...">
				    		</div>
				    		<div class="col-md-7">
				    			<input type="text" class="form-control" name="url[]" value="" placeholder="url of page  (ex. http://...">
				    		</div>
				    	</div>
				    	@endif

				    </div>

				    <div class="panel-footer">
				    	<button class="btn btn-info pull-right">Update</button>
				    	
				    	<a class="btn btn-success add-url">Add New</a>	
				    </div>
				    {!! Form::close() !!}
				 </div>
				 <!-- ============================Widget 3 =========================== -->

				 <div class="panel panel-success">
					{!! Form::open(['route' => 'footerWidget.update', 'class'=>'form-horizontal form-bordered' ]) !!}
				    <div class="panel-heading"> 
				    	<input type="hidden" name="widget_id" value="3">

		    	    	<div class="form-group">
		    				<label for="cat name" class="cat-input-label control-label">Footer Right Title</label>
		    				<input type="text" class="form-control" name="widget_title" value="@isset($footerWidget_3) {{ json_decode($footerWidget_3->description)->widget_title }} @endisset" >
		    				@if($errors->first('widget_title')) 
		    					<strong class="error-msg">{{ $errors->first('widget_title') }} </strong> 
		    				@endif
		    			</div>
				    </div>

				    <div class="panel-body footer-links">
				    	@if($footerWidget_3)
				    	@foreach(json_decode($footerWidget_3->description)->title as $key=> $title) 
				    	<div class="form-group">
				    		<div class="col-md-1">
				    			<label class="cat-input-label control-label">
				    				<a href="#" class="remove-url"><i class="fa fa-trash"></i></a>
				    			</label>
				    		</div>
				    		<div class="col-md-4">
				    			<input type="text" class="form-control" name="title[]" value="{{ $title }}" placeholder="title...">
				    		</div>
				    		<div class="col-md-7">
				    			<input type="text" class="form-control" name="url[]" value="{{ json_decode($footerWidget_3->description)->url[$key] }}" placeholder="url of page  (ex. http://...">
				    		</div>
				    	</div>
				    	@endforeach
				    	@else
				    	<div class="form-group">
				    		<div class="col-md-1">
				    			<label class="cat-input-label control-label">
				    				<a href="#" class="remove-url"><i class="fa fa-trash"></i></a>
				    			</label>
				    		</div>
				    		<div class="col-md-4">
				    			<input type="text" class="form-control" name="title[]" value="" placeholder="title...">
				    		</div>
				    		<div class="col-md-7">
				    			<input type="text" class="form-control" name="url[]" value="" placeholder="url of page  (ex. http://...">
				    		</div>
				    	</div>
				    	@endif

				    </div>

				    <div class="panel-footer">
				    	<button class="btn btn-info pull-right">Update</button>
				    	
				    	<a class="btn btn-success add-url">Add New</a>	
				    </div>
				    {!! Form::close() !!}
				 </div>
				 <!-- ============================Widget 2 =========================== -->

				 
			</div>
          </div>   <!-- /.row -->
        </section><!-- /.content   where('id', $id) -->
	<!-- /.content -->

@stop

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script>
$('.footer-links').sortable();

$('.add-url').on("click", function(){
	$(this).parent().siblings('.panel-body').append(
		'<div class="form-group">'+
    		'<div class="col-md-1">'+
    			'<label class="cat-input-label control-label">'+
    				'<a href="#" class="remove-url"><i class="fa fa-trash"></i></a>'+
    			'</label>'+
    		'</div>'+
    		'<div class="col-md-4">'+
    			'<input type="text" class="form-control" name="title[]" value="" placeholder="title...">'+
    		'</div>'+
    		'<div class="col-md-7">'+
    			'<input type="text" class="form-control" name="url[]" value="" placeholder="url of page  (ex. http://...">'+
    		'</div>'+
    	'</div>'
	);
});

$('.remove-url').live("click",function(){
	$(this).closest('.form-group').remove();
});
</script>
@stop