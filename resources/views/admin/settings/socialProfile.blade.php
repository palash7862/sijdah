@extends('admin.layouts.master')

@section('title') Social Profile @stop

@section('styles') 
<style>
	.user-create-form .form-group {
	  clear: both;
	  display: block;
	  float: left;
	  margin-bottom: 40px;
	  width: 100%;
	}
	.user-create-form .form-group > label {
	  color: #888;
	  float: left;
	  font-size: 20px;
	  font-weight: normal;
	  width: 20%;
	}
	.user-create-form .form-group .input-group {
	  float: left;
	  width: 40%;
	}
	.error-msg {
	  color: red;
	  display: inline-block;
	  padding: 5px 10px 5px 30px;
	  text-transform: capitalize;
	}
	.form-horizontal .form-group {
		margin-right: 0px;
		margin-left: 0px;
	}
	.cat-input-label{
		display: block; width: 100%; text-align: left !important; margin-bottom: 20px;
	}
	.btn-full{
		width: 100% !important;
	}
</style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Scial Profile 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#settings"><i class="fa fa-cog"></i> Settings</a></li>
        <li class="active">Social Profile</li>
      </ol>
    </section> 
    <!-- Main content --> 
        <section class="content">
          <div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="box">
					<div class="box-body">  
											
						{!! Form::open(['route' => 'socialProfile.update', 'class'=>'form-horizontal form-bordered' ]) !!}
						<!-- Page Title -->
						<div class="form-group">
							<div class="input-group">
							  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-facebook"></i></span>
							  <input type="text" class="form-control" placeholder="url..." aria-describedby="basic-addon1" name="facebook" value="@isset($facebook) {{ $facebook->description }} @endisset">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
							  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-twitter"></i></span>
							  <input type="text" class="form-control" placeholder="url..." aria-describedby="basic-addon1" name="twitter" value="@isset($twitter) {{ $twitter->description }} @endisset">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
							  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-pinterest"></i></span>
							  <input type="text" class="form-control" placeholder="url..." aria-describedby="basic-addon1" name="pinterest" value="@isset($pinterest) {{ $pinterest->description }} @endisset">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
							  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-linkedin"></i></span>
							  <input type="text" class="form-control" placeholder="url..." aria-describedby="basic-addon1" name="linkedin" value="@isset($linkedin) {{ $linkedin->description }} @endisset">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
							  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-stumbleupon"></i></span>
							  <input type="text" class="form-control" placeholder="url..." aria-describedby="basic-addon1" name="stumbleupon" value="@isset($stumbleupon) {{ $stumbleupon->description }} @endisset">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
							  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-dribbble"></i></span>
							  <input type="text" class="form-control" placeholder="url..." aria-describedby="basic-addon1" name="dribbble" value="@isset($dribbble) {{ $dribbble->description }} @endisset">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
							  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-vk"></i></span>
							  <input type="text" class="form-control" placeholder="url..." aria-describedby="basic-addon1" name="vk" value="@isset($vk) {{ $vk->description }} @endisset">
							</div>
						</div>


						<div class="form-group form-actions">
							<div class="cat-input-label">
									<button type="submit" id="add" class="btn btn-info btn-full">Update</button>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
					<!-- /.box-body -->

				</div><!-- /.box -->
			</div>
          </div>   <!-- /.row -->
        </section><!-- /.content   where('id', $id) -->
	<!-- /.content -->

@stop