@extends('admin.layouts.master')

@section('title') Writers @stop

@section('styles') 
<style>
	.user-create-form .form-group {
	  clear: both;
	  display: block;
	  float: left;
	  margin-bottom: 40px;
	  width: 100%;
	}
	.user-create-form .form-group > label {
	  color: #888;
	  float: left;
	  font-size: 20px;
	  font-weight: normal;
	  width: 20%;
	}
	.user-create-form .form-group .input-group {
	  float: left;
	  width: 40%;
	}
	.error-msg {
	  color: red;
	  display: inline-block;
	  padding: 5px 10px 5px 30px;
	  text-transform: capitalize;
	}
	.form-horizontal .form-group {
		margin-right: 0px;
		margin-left: 0px;
	}
	.cat-input-label{
		display: block; width: 100%; text-align: left !important; margin-bottom: 20px;
	}
	.btn-full{
		width: 100% !important;
	}
</style>
<style>
	.media-fream-warp {
			display: none;
			position: fixed;
			width: 100%;
			top: 0px;
			bottom: 0px;
			left: 0px;
			right: 0px;
			z-index: 9999999;
			padding: 100px 200px;
			background: rgba(0, 0, 0, 0.6);
		}
		body.overflow-hidden{
			overflow: hidden !important;
		}
		.media-close-shadow {
			position: fixed;
			width: 100%;
			height: 100%;
			z-index: -1;
			left: 0px;
			right: 0px;
			top: 0px;
			bottom: 0px;
			background: rgba(0, 0, 0, 0.5);
		}
		.media-fream-inner {
			background: #ffffff;
			z-index: 1;
		}
		.media-all {
			width: 100%;
			overflow: auto;
			padding: 21px;
			box-shadow: inset 0px 0px 15px rgba(0, 0, 0, 0.5);
			height: 400px;
		}
		.media-all .single-media {
			float: left;
			width: 15%;
			margin: 8px 8px;
			/* box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.5); */
		}
		.media-all .single-media img {
			max-width: 100%;
		}
		.media-fream-inner .header {
			padding: 15px 25px;
		}
		.media-fream-inner .header h3 {
			margin: 0px;
		}
		.media-fream-inner .footer {
			background: #FCFCFC;
			min-height: 50px;
			padding: 8px 25px;
			text-align: right;
			border-top: 1px solid #ddd;
		}
		.media-all .single-media.active {
			box-shadow: 0px 0px 2px 3px #3c8dbc;
			border: 1px solid #ffffff;
		}
		.post-thumb-field table tbody tr td img {
			width: 50px;
		}
</style>
@stop

@section('content')
	<div class="media-fream-warp" data-url="{{route('media.all')}}">
		<div class="media-fream-inner">
			<div class="header">
				<h3>Media Manager</h3>
			</div>
			<div class="media-all"></div>
			<div class="footer">
				<button class="btn btn-primary btn-flat media-insert">Insert</button>
			</div>
		</div>
		<div class="media-close-shadow"></div>
	</div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Promotions 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Promotions</li>
      </ol>
    </section> 
    <!-- Main content --> 
        <section class="content">
          <div class="row">
			<div class="col-md-4">
				<div class="box">
					<div class="box-body">  
						@if (Route::currentRouteName() == 'promotion.edit')
						{{ Form::open(array('method'=>'PUT','route' => ['promotion.update', $spromotion->id],'class'=>'form-horizontal form-bordered')) }} 
						@else
							{!! Form::open(['route' => ['promotion.store'], 'class'=>'form-horizontal form-bordered' ]) !!}
						@endif
							<!-- Page Title -->
							@if (Route::currentRouteName() == 'promotion.edit') 
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Promotion Title</label>
									<input type="text" class="form-control" name="title" value="{{ $spromotion->title}}">
									@if($errors->first('title'))
										<strong class="error-msg">{{ $errors->first('title') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Image Source</label>
									<a href="#" class="add-image btn btn-success">Add Image</a>
									<input type="hidden" class="form-control" name="source" value="{{$spromotion->source}}" id="img-src">
									<div id="image-holder"><img src="{{ asset(get_image_size('media-thumb',$img->source))}}"></div>
									@if($errors->first('source'))
										<strong class="error-msg">{{ $errors->first('source') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Promo starts at</label>
									<input type="date" class="form-control" name="starts_at" value={{$spromotion->starts_at}} >
									@if($errors->first('starts_at'))
										<strong class="error-msg">{{ $errors->first('starts_at') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Promo starts at</label>
									<input type="date" class="form-control" name="ends_at" value={{$spromotion->ends_at}} >
									@if($errors->first('ends_at'))
										<strong class="error-msg">{{ $errors->first('ends_at') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label>Status</label>
									<input type="radio" name="status" value="active" @if($spromotion->status=='active') checked @endif >Active
									<input type="radio" name="status" value="inactive" @if($spromotion->status=='inactive') checked @endif>Inactive
									@if($errors->first('status'))
										<strong class="error-msg">{{ $errors->first('status') }} </strong>
									@endif
								</div>
							@else
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Promotion Title</label>
									<input type="text" class="form-control" name="title" placeholder="Enter promo title">
									@if($errors->first('title'))
										<strong class="error-msg">{{ $errors->first('title') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Image Source</label>
									<a href="#" class="add-image btn btn-success">Add Image</a>
									<input type="hidden" class="form-control" name="source" value="" id="img-src">
									<div id="image-holder"></div>
									@if($errors->first('source'))
										<strong class="error-msg">{{ $errors->first('source') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Promo starts at</label>
									<input type="date" class="form-control" name="starts_at" value={{$curDate}} >
									@if($errors->first('starts_at'))
										<strong class="error-msg">{{ $errors->first('starts_at') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label for="cat name" class="control-label cat-input-label" >Promo starts at</label>
									<input type="date" class="form-control" name="ends_at" value={{$nextDate}} >
									@if($errors->first('ends_at'))
										<strong class="error-msg">{{ $errors->first('ends_at') }} </strong>
									@endif
								</div>
								<div class="form-group">
									<label>Status</label>
									<input type="radio" name="status" value="active">Active
									<input type="radio" name="status" value="inactive">Inactive
									@if($errors->first('status'))
										<strong class="error-msg">{{ $errors->first('status') }} </strong>
									@endif
								</div>
							@endif
							
							<div class="form-group form-actions">
								<div class="cat-input-label">
									@if (Route::currentRouteName() == 'promotion.edit')
										<button type="submit" id="add" class="btn btn-info btn-full">Update</button>
									@else
										<button type="submit" id="add" class="btn btn-info btn-full">Create</button>
									@endif
								</div>
							</div>
						{!! Form::close() !!}
					</div>
					<!-- /.box-body -->

				</div><!-- /.box -->
			</div>
            <!-- right column -->
            <div class="col-md-8">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-body"> 
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Starts at</th>
                            <th>Ends at</th> 
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
					<tbody>
					@foreach($promotions as $promotion)
						<tr>
                            <td>{{$promotion->id}}</td>
                            <td>{{$promotion->title}}</td>
                            <td><img src="{{asset(get_image_size('media-thumb',$promotion->m_src))}}" class="image-respossive" width="120"></td>
                            <td>{{$promotion->status}}</td>
                            <td>{{$promotion->starts_at}}</td>
                            <td>{{$promotion->ends_at}}</td> 
                            <td class="text-center">
                            	@if (Route::currentRouteName() != 'promotion.edit')
								<a href="{{ route('promotion.edit', array($promotion->id)) }}" class="btn btn-app" >
									<i class="fa fa-edit"></i>
									Edit
								</a>
								@endif
								{{Form::open([ 'method'  => 'DELETE', 'route' => [ 'promotion.destroy', $promotion->id ] ])}}
                                {{ Form::hidden('id', $promotion->id) }}
                                {{Form::button('<i class="fa fa-trash-o"></i>', array('type' => 'submit', 'class' => 'btn btn-app'))}}
                            	{{ Form::close() }} 
                            </td>
                        </tr>
					@endforeach 
						
					</tbody> 
					<tfoot>
                        <tr>
                            <th>#ID</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Starts at</th>
                            <th>Ends at</th> 
                            <th class="text-center">Action</th>
                        </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.box-body -->

              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content   where('id', $id) -->
	<!-- /.content -->

@stop
@section('scripts')   
<!-- page script -->
<script>
    $(function () {
        $('.delete-promotion').on('click', function (event) {
            var rlink = $(this).attr('href');
            event.preventDefault();
            swal({
                title: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                	window.location.href = rlink;
				}
			});
        });
    });
</script>
<script>
	$(document).ready(function(){
		
	$(".add-image").on('click', function (event){
	event.preventDefault();
	var fream = $('.media-fream-warp');
	fream.slideDown();
	$("body").addClass("overflow-hidden");
	var getMediaUrl = fream.data("url");
	$.ajax({
		url: getMediaUrl, //this is your uri
		type: 'get', //this is your method
		dataType: 'json',
		success: function(response){
			fream.find(".media-all").empty();
			var html = "";
			for (var i=0;i<response.data.length;i++){
				var id 		= response.data[i].id;
				html += '<div class="single-media" data-id="'+id+'"><img  src="'+response.data[i].source+'" alt="" ></div>';
			}
			fream.find(".media-all").append(html);
			}
		});
	});
	$(".media-all").find(".single-media").live('click', function () {
		$(this).addClass("active").siblings().removeClass("active");
	});
	$(".media-fream-warp").find(".media-insert").live('click', function () {
		
		var mediaWarp = $(this).parents(".footer").prev(".media-all");
		var mediaId = mediaWarp.find(".single-media.active").data('id');
		var mediasrc = mediaWarp.find(".single-media.active").children("img").attr('src');

		$('#image-holder').html('<img src="'+ mediasrc +'" max-width="100" max-height="100">');
		$('#img-src').val(mediaId);

/*
		//if ( (mediaId > 0) && (mediasrc.length > 0)){
			$(".post-thumb-field > table")
				.find("tbody")
				.append('<tr><td><img src="'+mediasrc+'"><input type="hidden" name="product_media[]" value="'+mediaId+'"></td><td><input type="radio" name="featured_img_id" value="'+mediaId+'"></td><td><button class="porduct-media">Remove</button></td></tr>');*/
			$(".media-fream-warp").slideUp();
			$("body").removeClass("overflow-hidden");
			$(this).unbind( "click" );
		//}
	});
	$(".post-thumb-field > table").find(".porduct-media").live('click', function (event) {
		event.preventDefault();
		$(this).parents("tr").remove();
	});
	$(".media-fream-warp .media-close-shadow").live('click', function () {
		$(this).parent('.media-fream-warp').slideUp();
		$("body").removeClass("overflow-hidden");
		$(".media-fream-warp").find(".media-insert").unbind( "click" );
	});
});
</script>
@stop