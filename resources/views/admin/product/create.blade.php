@extends('admin.layouts.master')

@section('title') Create Test @stop

@section('styles')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/dist/css/dropzone.css') }}">
    <style>
        .test-create-form .form-group, .test-create-form .input-group {
            width: 100%;
        }

        .test-create-form .cats-list-checkbox {
            display: block;
            max-height: 400px;
            overflow-x: auto;
            overflow-y: scroll;
            width: 100%;
        }

        .test-create-form .cats-list-checkbox > label {
            display: block;
            padding: 5px;
            width: 100%;
        }

        .error-msg {
            color: red;
            display: inline-block;
            padding: 5px 10px 5px 30px;
            text-transform: capitalize;
        }

        #multipleupload .dropzone {
            border: 2px dashed #0087f7;
            padding: 16px 20px;
            text-align: center;
        }

        /******************** Switch Button Css **********************/
        .switch-field {
            padding: 0px;
            overflow: hidden;
        }

        .switch-title {
            margin-bottom: 6px;
        }

        .switch-field input {
            position: absolute !important;
            clip: rect(0, 0, 0, 0);
            height: 1px;
            width: 1px;
            border: 0;
            overflow: hidden;
        }

        .switch-field label {
            float: left;
        }

        .switch-field label {
            display: inline-block;
            width: 33.3333%;
            background-color: #e4e4e4;
            color: rgba(0, 0, 0, 0.6);
            font-size: 14px;
            font-weight: normal;
            text-align: center;
            text-shadow: none;
            padding: 6px 14px;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
            box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
            -webkit-transition: all 0.1s ease-in-out;
            -moz-transition: all 0.1s ease-in-out;
            -ms-transition: all 0.1s ease-in-out;
            -o-transition: all 0.1s ease-in-out;
            transition: all 0.1s ease-in-out;
        }

        .switch-field label:hover {
            cursor: pointer;
        }

        .switch-field input:checked + label {
            background-color: #3c8dbc;
            -webkit-box-shadow: none;
            box-shadow: none;
            color: #ffffff;
        }

        .switch-field label:first-of-type {
            border-radius: 4px 0 0 4px;
        }

        .switch-field label:last-of-type {
            border-radius: 0 4px 4px 0;
        }

        .form-group .inner-group {
            width: calc(100% / 2 - 20px);
            float: left;
            margin-right: 20px;
        }

        .form-group .inner-group.colum-4 {
            width: calc(100% / 4 - 20px);
        }

        .form-group {
            margin-bottom: 25px;
        }

        .Clothing-product-input, .FoodGroceries-product-input {
            display: none;
        }

        .media-fream-warp {
            display: none;
            position: fixed;
            width: 100%;
            top: 0px;
            bottom: 0px;
            left: 0px;
            right: 0px;
            z-index: 9999999;
            padding: 100px 200px;
            background: rgba(0, 0, 0, 0.6);
        }

        body.overflow-hidden {
            overflow: hidden !important;
        }

        .media-close-shadow {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: -1;
            left: 0px;
            right: 0px;
            top: 0px;
            bottom: 0px;
            background: rgba(0, 0, 0, 0.5);
        }

        .media-fream-inner {
            background: #ffffff;
            z-index: 1;
        }

        .media-all {
            width: 100%;
            overflow: auto;
            padding: 21px;
            box-shadow: inset 0px 0px 15px rgba(0, 0, 0, 0.5);
            height: 400px;
        }

        .media-all .single-media {
            float: left;
            width: 15%;
            margin: 8px 8px;
            /* box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.5); */
        }

        .media-all .single-media img {
            max-width: 100%;
        }

        .media-fream-inner .header {
            padding: 15px 25px;
        }

        .media-fream-inner .header h3 {
            margin: 0px;
        }

        .media-fream-inner .footer {
            background: #FCFCFC;
            min-height: 50px;
            padding: 8px 25px;
            text-align: right;
            border-top: 1px solid #ddd;
        }

        .media-all .single-media.active {
            box-shadow: 0px 0px 2px 3px #3c8dbc;
            border: 1px solid #ffffff;
        }

        .post-thumb-field table tbody tr td img {
            width: 50px;
        }
        .input-group.post-future-image {
            text-align: center;
            padding: 25px 25px;
            border: 1px solid rgba(0, 0, 0, 0.1);
        }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Create Product
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Product</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- <pre>
                //print_r($errors);
                </pre> -->
                <div class="box">

                    {!! Form::open(['route' => 'product.create', 'class' => 'test-create-form']) !!}
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-8 left-colum-warp">
                                <div class="form-group clearfix">
                                    <div class="inner-group">
                                        <label>Product Name(EN):</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="title_en">
                                        </div>
                                        @if($errors->first('title_bn'))
                                            <strong class="error-msg">{{ $errors->first('title_en') }} </strong>
                                        @endif
                                    </div>
                                    <div class="inner-group">
                                        <label>Product Name (BD):</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="title_bn">
                                        </div>
                                        @if($errors->first('title_bn'))
                                            <strong class="error-msg">{{ $errors->first('title_bn') }} </strong>
                                        @endif
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->

                                <div class="form-group clearfix">
                                    <label class="switch-title">Select Product Type</label>
                                    <div class="switch-field">
                                        <div class="input-group">
                                            <input type="radio" id="switch_3_left" name="product_type" value="Book"
                                                   checked/>
                                            <label for="switch_3_left">Book</label>
                                            <input type="radio" id="switch_3_center" name="product_type"
                                                   value="Clothing"/>
                                            <label for="switch_3_center">Clothing</label>
                                            <input type="radio" id="switch_3_right" name="product_type"
                                                   value="FoodGroceries"/>
                                            <label for="switch_3_right">Food & Groceries</label>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.form group -->

                                <div class="Book-product-input">

                                    <div class="form-group clearfix">
                                        <label>Select Writer :</label>

                                        <div class="input-group">
                                            @foreach($writers as $writer)
                                                <label>
                                                    <input type="checkbox" name="writers[]" class="flat-red"
                                                           value="{{ $writer->id }}">
                                                    {{ json_decode($writer->name)->en }}
                                                    ( {{ json_decode($writer->name)->bn }} )
                                                </label>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- /.form group -->

                                    <div class="form-group clearfix">
                                        <label>Select Publishers :</label>

                                        <div class="input-group">
                                            <select name="publisher_id" class="form-control">
                                                @foreach($publishers as $pbs)
                                                    <option value="{{ $pbs->id }}">{{ json_decode($pbs->name)->en }}
                                                        ( {{ json_decode($pbs->name)->bn }} )
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- /.form group -->

                                    <div class="form-group clearfix">
                                        <label>Page Number:</label>

                                        <div class="input-group">
                                            <input type="text" class="form-control" name="page_number">
                                        </div>
                                        @if($errors->first('page_number'))
                                            <strong class="error-msg">{{ $errors->first('page_number') }} </strong>
                                    @endif
                                    <!-- /.input group -->
                                    </div>
                                    <!-- /.form group -->
                                </div>

                                <div class="Clothing-product-input">
                                    <div class="form-group clearfix">
                                        <div class="inner-group">
                                            <label>Clothing Type(English):</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="clothing_type[en]">
                                            </div>
                                        </div>
                                        <div class="inner-group">
                                            <label>Clothing Type(Bangla):</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="clothing_type[bd]">
                                            </div>
                                        </div>
                                        @if($errors->first('clothing_type'))
                                            <strong class="error-msg">{{ $errors->first('clothing_type') }} </strong>
                                    @endif
                                    <!-- /.input group -->
                                    </div>
                                    <!-- /.form group -->

                                    <div class="form-group clearfix">
                                        <div class="inner-group">
                                            <label>Size:</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="size">
                                            </div>
                                        </div>
                                        <div class="inner-group">
                                            <label>Color:</label>
                                            <div class="form-group">
                                                @foreach($colors as $color)
                                                    <label>
                                                        <input type="checkbox" class="flat-red" name="colors[]"
                                                               value="{{$color->id}}">
                                                        {{$color->name}}
                                                    </label>
                                                @endforeach
                                            </div>
                                        </div>
                                        @if($errors->first('size'))
                                            <strong class="error-msg">{{ $errors->first('size') }} </strong>
                                    @endif
                                    <!-- /.input group -->
                                    </div>
                                    <!-- /.form group -->
                                </div>

                                <div class="FoodGroceries-product-input">
                                    <div class="form-group clearfix">
                                        <label>Weight:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="weight">
                                        </div>
                                        @if($errors->first('weight'))
                                            <strong class="error-msg">{{ $errors->first('weight') }} </strong>
                                    @endif
                                    <!-- /.input group -->
                                    </div>
                                    <!-- /.form group -->
                                </div>

                                <div class="form-group clearfix">
                                    <div class="inner-group colum-4">
                                        <label>Regular Price:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="reg_price">
                                        </div>
                                        @if($errors->first('reg_price'))
                                            <strong class="error-msg">{{ $errors->first('reg_price') }} </strong>
                                        @endif
                                    </div>
                                    <div class="inner-group colum-4">
                                        <label>Sale Price:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="dis_price">
                                        </div>
                                        @if($errors->first('dis_price'))
                                            <strong class="error-msg">{{ $errors->first('dis_price') }} </strong>
                                        @endif
                                    </div>
                                    <div class="inner-group colum-4">
                                        <label>Diller Price:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="dil_price">
                                        </div>
                                        @if($errors->first('dil_price'))
                                            <strong class="error-msg">{{ $errors->first('dil_price') }} </strong>
                                        @endif
                                    </div>
                                    <div class="inner-group colum-4">
                                        <label>Stock In:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="stock">
                                        </div>
                                        @if($errors->first('stock'))
                                            <strong class="error-msg">{{ $errors->first('stock') }} </strong>
                                        @endif
                                    </div>
                                </div>
                                <!-- /.form group -->

                                <div class="form-group clearfix">
                                    <label>Product Content(English):</label>

                                    <div class="input-group">
                                        <textarea id="test-content" name="description_en"></textarea>
                                    </div>
                                    <!-- /.input group -->
                                </div>

                                <div class="form-group clearfix">
                                    <label>Product Content(Bangla):</label>

                                    <div class="input-group">
                                        <textarea id="test-content2" name="description_bn"></textarea>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Select Category :</label>

                                    <div class="input-group">
                                        <div class="cats-list-checkbox">
                                            @foreach($cats as $cat)
                                                <label>
                                                    <input type="checkbox" name="cats[]" class="flat-red"
                                                           value="{{ $cat->id }}">
                                                    {{ $cat->name_en }} ( {{ $cat->name_bn }} )
                                                </label>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->

                                <div class="form-group">
                                    <label> Product Thumb:</label>

                                    <div class="input-group post-thumb-field">
                                        <a href="" class="add-image btn btn-block btn-default btn-flat">Add Product
                                            Images</a>
                                    </div>
                                    <!-- /.input group -->
                                    <div class="input-group post-future-image">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label> Product Book Image:</label>

                                    <div class="input-group post-thumb-field">
                                        <a href="" class="add-page-image btn btn-block btn-default btn-flat">Add Product
                                            Book Page</a>
                                    </div>
                                    <!-- /.input group -->
                                    <div class="input-group post-thumb-field">
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th>Image Preview</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.form group -->
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@stop
@section('extracontent')
    <div class="media-fream-warp" data-url="{{route('media.all')}}">
        <div class="media-fream-inner">
            <div class="header">
                <h3>Media Manager</h3>
            </div>
            <div class="media-all"></div>
            <div class="footer">
                <button class="btn btn-primary btn-flat media-insert">Insert</button>
            </div>
        </div>
        <div class="media-close-shadow"></div>
    </div>
@stop
@section('scripts')
    <!-- Select2 -->
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('admin/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('admin/dist/js/dropzone.js') }}"></script>
    <script src="{{ asset('admin/dist/js/media.js') }}"></script>
    <!-- CK Editor -->
    <script src="{{ asset('admin/bower_components/ckeditor/ckeditor.js') }}"></script>
    <script>
        $(document).ready(function () {
            CKEDITOR.replace('test-content');
            CKEDITOR.replace('test-content2');
            //$(".select2").select2();
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            $('.Book-product-input').slideDown();
            $('.Clothing-product-input').slideUp();
            $('.FoodGroceries-product-input').slideUp();

            $('.switch-field').find('input[type="radio"]').on('click', function (event) {
                var $this = $(this),
                    parentWarp = $this.parents('.left-colum-warp'),
                    currentTabName = $this.attr('value');
                if (currentTabName == 'Book') {
                    parentWarp.find('.Book-product-input').slideDown();
                    parentWarp.find('.Clothing-product-input').slideUp();
                    parentWarp.find('.FoodGroceries-product-input').slideUp();
                } else if (currentTabName == 'Clothing') {
                    parentWarp.find('.Clothing-product-input').slideDown();
                    parentWarp.find('.Book-product-input').slideUp();
                    parentWarp.find('.FoodGroceries-product-input').slideUp();
                } else if (currentTabName == 'FoodGroceries') {
                    parentWarp.find('.FoodGroceries-product-input').slideDown();
                    parentWarp.find('.Clothing-product-input').slideUp();
                    parentWarp.find('.Book-product-input').slideUp();
                }
            });
        });
    </script>
@stop