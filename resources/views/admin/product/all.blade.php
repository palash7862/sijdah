@extends('admin.layouts.master')
@section('title') All Test @stop

@section('styles')
    <style>
        .admin-thumb {
            width: 50px;
        }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            All Test
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">All Products</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Thumb</th>
                                <th>Title</th>
                                <th>Product Type</th>
                                <th>Stock In</th>
                                <th>Review</th>
                                <th>Regular Price</th>
                                <th>Discunt Percentage</th>
                                <th>Diller Price</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td> {{ $product->id }} </td>
                                    <td>
                                        <!--
                                        @-if( $test->picture != "" && $testImg = json_decode($test->picture) )
                                            <img class="admin-thumb" src="{-{ adminUrl($testImg->thumb) }-}" />
                                        @-else
                                            <img class="admin-thumb" src="{-{ adminUrl('tests/service-4.png') }-}" />
                                        @-endif -->
                                    </td>
                                    <td>
                                        <strong>English : </strong>( {{ $product->title_en }} )<br>
                                        <strong>Bangla : </strong>( {{$product->title_bn}} )
                                    </td>
                                    <td>{{ $product->product_type }}</td>
                                    <td>{{$product->stock}}</td>
                                    <td>{{$product->review}}</td>
                                    <td>{{$product->reg_price}}</td>
                                    <td>{{$product->dis_price}}%</td>
                                    <td>{{$product->dil_price}}</td>
                                    <td>
                                        <a href="{{ route('product.edit', array($product->id)) }}" class="btn btn-app">
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        <a href="{{ route('product.delete', array($product->id)) }}"
                                           class="btn btn-app delete-product">
                                            <i class="fa fa-trash-o"></i>
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Thumb</th>
                                <th>Title</th>
                                <th>Product Type</th>
                                <th>Stock In</th>
                                <th>Review</th>
                                <th>Regular Price</th>
                                <th>Discunt Percentage</th>
                                <th>Diller Price</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@stop
@section('scripts')
    <!-- page script -->
    <script>
        $(function () {
            $('.delete-product').on('click', function (event) {
                var rlink = $(this).attr('href');
                event.preventDefault();
                swal({
                    title: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                    window.location.href = rlink;
                }
            });
            });
            //$('#example2').DataTable();
        });
    </script>
@stop