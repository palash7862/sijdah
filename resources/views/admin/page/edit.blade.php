@extends('admin.layouts.master')

@section('title') Create Test @stop

@section('styles')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/iCheck/all.css') }}">
    <style>

        .test-create-form .form-group, .test-create-form .input-group {
            width: 100%;
        }
        .test-create-form .cats-list-checkbox {
            display: block;
            max-height: 400px;
            overflow-x: auto;
            overflow-y: scroll;
            width: 100%;
        }
        .test-create-form .cats-list-checkbox > label {
            display: block;
            padding: 5px;
            width: 100%;
        }
        .error-msg {
            color: red;
            display: inline-block;
            padding: 5px 10px 5px 30px;
            text-transform: capitalize;
        }
        #multipleupload .dropzone {
            border: 2px dashed #0087f7;
            padding: 16px 20px;
            text-align: center;
        }

        /******************** Switch Button Css **********************/
        .switch-field {
            padding: 0px;
            overflow: hidden;
        }

        .switch-title {
            margin-bottom: 6px;
        }

        .switch-field input {
            position: absolute !important;
            clip: rect(0, 0, 0, 0);
            height: 1px;
            width: 1px;
            border: 0;
            overflow: hidden;
        }

        .switch-field label {
            float: left;
        }

        .switch-field label {
            display: inline-block;
            width: 33.3333%;
            background-color: #e4e4e4;
            color: rgba(0, 0, 0, 0.6);
            font-size: 14px;
            font-weight: normal;
            text-align: center;
            text-shadow: none;
            padding: 6px 14px;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
            box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
            -webkit-transition: all 0.1s ease-in-out;
            -moz-transition:    all 0.1s ease-in-out;
            -ms-transition:     all 0.1s ease-in-out;
            -o-transition:      all 0.1s ease-in-out;
            transition:         all 0.1s ease-in-out;
        }

        .switch-field label:hover {
            cursor: pointer;
        }

        .switch-field input:checked + label {
            background-color: #3c8dbc;
            -webkit-box-shadow: none;
            box-shadow: none;
            color: #ffffff;
        }

        .switch-field label:first-of-type {
            border-radius: 4px 0 0 4px;
        }

        .switch-field label:last-of-type {
            border-radius: 0 4px 4px 0;
        }
        .form-group .inner-group {
            width: calc( 100%/2 - 20px );
            float: left;
            margin-right: 20px;
        }
        .form-group .inner-group.colum-4 {
            width: calc( 100%/4 - 20px );
        }
        .form-group {
            margin-bottom: 25px;
        }

        .display-block{
            display: block;
        }
        .display-none{
            display: none;
        }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Product
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Product</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- <pre>
                //print_r($errors);
                </pre> -->
                <div class="box">

                    {!! Form::open(['route' => 'update.page', 'class' => 'test-create-form']) !!}
                    @foreach($page as $data)
                        <input type="hidden" name="page_id" value="{{$data->id}}">
                        <div class="box-header">
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-8 left-colum-warp">
                                    <div class="form-group clearfix">

                                        <div class="inner-group">
                                            <label>Product Name(EN):</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="title[en]" value="{{ $data->title_en }}" >
                                            </div>
                                        </div>
                                        <div class="inner-group">
                                            <label>Product Name (BD):</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="title[bn]" value="{{ $data->title_bn }}" >
                                            </div>
                                        </div>
                                    <!-- /.input group -->
                                    </div>


                                    <!-- /.form group -->

                                    <div class="form-group clearfix">
                                        <label>Page Content(English):</label>
                                        <div class="input-group">
                                            <textarea class="test-content" name="content[en]"> {{ $data->content_en  }}</textarea>
                                        </div>
                                        <!-- /.input group -->
                                    </div>

                                    <div class="form-group clearfix">
                                        <label>Page Content(Bangla):</label>

                                        <div class="input-group">
                                            <textarea class="test-content" name="content[bn]">{{$data->content_bn }}</textarea>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.form group -->
                                </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    @endforeach
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@stop
@section('scripts')
    <!-- Select2 -->
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('admin/plugins/iCheck/icheck.min.js') }}"></script>
    <!--<script src="{-{ asset('admin/dist/js/dropzone.js') }-}"></script>-->
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <!-- page script -->
    <script>

        tinymce.init({
            selector: 'textarea',
            height: 300,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
            ]
        });


    </script>
@stop