@extends('admin.layouts.master')

@section('title') Create Test @stop

@section('styles')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/dist/css/dropzone.css') }}">
    <style>
        .test-create-form .form-group, .test-create-form .input-group {
            width: 100%;
        }
        .test-create-form .cats-list-checkbox {
            display: block;
            max-height: 400px;
            overflow-x: auto;
            overflow-y: scroll;
            width: 100%;
        }
        .test-create-form .cats-list-checkbox > label {
            display: block;
            padding: 5px;
            width: 100%;
        }
        .error-msg {
            color: red;
            display: inline-block;
            padding: 5px 10px 5px 30px;
            text-transform: capitalize;
        }
        #multipleupload .dropzone {
            border: 2px dashed #0087f7;
            padding: 16px 20px;
            text-align: center;
        }

        /******************** Switch Button Css **********************/
        .switch-field {
            padding: 0px;
            overflow: hidden;
        }

        .switch-title {
            margin-bottom: 6px;
        }

        .switch-field input {
            position: absolute !important;
            clip: rect(0, 0, 0, 0);
            height: 1px;
            width: 1px;
            border: 0;
            overflow: hidden;
        }

        .switch-field label {
            float: left;
        }

        .switch-field label {
            display: inline-block;
            width: 33.3333%;
            background-color: #e4e4e4;
            color: rgba(0, 0, 0, 0.6);
            font-size: 14px;
            font-weight: normal;
            text-align: center;
            text-shadow: none;
            padding: 6px 14px;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
            box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
            -webkit-transition: all 0.1s ease-in-out;
            -moz-transition:    all 0.1s ease-in-out;
            -ms-transition:     all 0.1s ease-in-out;
            -o-transition:      all 0.1s ease-in-out;
            transition:         all 0.1s ease-in-out;
        }

        .switch-field label:hover {
            cursor: pointer;
        }

        .switch-field input:checked + label {
            background-color: #3c8dbc;
            -webkit-box-shadow: none;
            box-shadow: none;
            color: #ffffff;
        }

        .switch-field label:first-of-type {
            border-radius: 4px 0 0 4px;
        }

        .switch-field label:last-of-type {
            border-radius: 0 4px 4px 0;
        }
        .form-group .inner-group {
            width: calc( 100%/2 - 20px );
            float: left;
            margin-right: 20px;
        }
        .form-group .inner-group.colum-4 {
            width: calc( 100%/4 - 20px );
        }
        .form-group {
            margin-bottom: 25px;
        }
        .Clothing-product-input, .FoodGroceries-product-input {
            display: none;
        }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Create Page
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add page</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">

                    {!! Form::open(array('route' => 'store.page', 'class' => 'test-create-form')) !!}
                    <div class="box-header">
                        @if( Session::has('status'))

                            <h2 style="color: darkgreen;">{{ Session::get('status') }}</h2>

                        @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-8 left-colum-warp">
                                <div class="form-group clearfix">
                                    <div class="inner-group">
                                        <label>Page Title (English):</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="title[en]">
                                        </div>

                                        @if( $errors->first('title.en') )
                                            <strong class="error-msg">{{ $errors->first('title.en') }} </strong>
                                        @endif
                                    </div>
                                    <div class="inner-group">
                                        <label>Page Title (Bangla):</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="title[bn]" >
                                        </div>
                                        @if( $errors->first('title.bn') )
                                            <strong class="error-msg">{{ $errors->first('title.bn') }} </strong>
                                        @endif
                                    </div>
                                    <div class="inner-group">
                                        <label>Slug:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="slug" >
                                        </div>
                                        @if( $errors->first('slug') )
                                            <strong class="error-msg">{{ $errors->first('slug') }} </strong>
                                        @endif
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->


                                <div class="form-group clearfix">
                                    <label>Page Content(English):</label>

                                    <div class="input-group">
                                        <textarea class="test-content" name="content[en]"></textarea>
                                        @if( $errors->first('content.en') )
                                            <strong class="error-msg">{{ $errors->first('content.en') }} </strong>
                                        @endif
                                    </div>
                                    <!-- /.input group -->
                                </div>

                                <div class="form-group clearfix">
                                    <label>Page Content(Bangla):</label>

                                    <div class="input-group">
                                        <textarea class="test-content" name="content[bn]"></textarea>
                                        @if( $errors->first('content.bn') )
                                            <strong class="error-msg">{{ $errors->first('content.bn') }} </strong>
                                        @endif
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <!--
    <div class="dropzone"  >
        <div class="dz-message" >
            <img id="post-thumb-prev" src="" alt="" />
            <h3>Drop images here or click to upload.</h3>
        </div>
    </div>

    -->
@stop
@section('scripts')
    <!-- Select2 -->
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('admin/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('admin/dist/js/dropzone.js') }}"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <!-- page script -->
    <script>

        tinymce.init({
            selector: 'textarea',
            height: 300,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
            ]
        });
        Dropzone.autoDiscover = false;
        $(document).ready(function () {
            //Initialize Select2 Elements
            //$(".select2").select2();
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            $('.Book-product-input').slideDown();
            $('.Clothing-product-input').slideUp();
            $('.FoodGroceries-product-input').slideUp();

            $('.switch-field').find('input[type="radio"]').on('click', function (event) {
                var $this 			= $(this),
                    parentWarp 		= $this.parents('.left-colum-warp'),
                    currentTabName 	= $this.attr('value');
                if(currentTabName == 'Book'){
                    parentWarp.find('.Book-product-input').slideDown();
                    parentWarp.find('.Clothing-product-input').slideUp();
                    parentWarp.find('.FoodGroceries-product-input').slideUp();
                }else if(currentTabName == 'Clothing'){
                    parentWarp.find('.Clothing-product-input').slideDown();
                    parentWarp.find('.Book-product-input').slideUp();
                    parentWarp.find('.FoodGroceries-product-input').slideUp();
                }else if(currentTabName == 'FoodGroceries') {
                    parentWarp.find('.FoodGroceries-product-input').slideDown();
                    parentWarp.find('.Clothing-product-input').slideUp();
                    parentWarp.find('.Book-product-input').slideUp();
                }
            });


            var imageCount = 1;
            var actionUrl  =  $(".multipleupload").attr('action');
            var myDropzone =  $("#multipleupload").dropzone( {
                url: actionUrl,
                paramName: "productThumb",
                uploadMultiple:false,
                previewsContainer: false,
                addRemoveLinks: true,
                uploadprogress: function(file, progress, bytesSent){
                    var prevHtml = '<div class="single-media" data-imageid="'+imageCount+'" data-mediaid=""><img src="'+file.dataURL+'"><span class="remove">Remove</span></div>';
                    var mediaPrev = $("#multipleupload").parents('.post-thumb-field').find(".media-preview");
                    mediaPrev.append(prevHtml);
                    console.log(file);
                },

                success: function(file, response, done){
                    var mediaPrev = $("#multipleupload").parents('.post-thumb-field').find(".media-preview");
                    mediaPrev.find('.single-media[data-imageid='+imageCount+']')
                        .attr('data-mediaid', response.medialId)
                        .append('<input type="hidden" name="product_img[]" value="'+response.medialId+'" >');

                    mediaPrev.find('.single-media[data-imageid='+imageCount+']').
                        imageCount++;
                }
            });

            /*
                sending: function(file, xhr, formData){
                    formData.append( '_token', $("input[name=_token]").val() );
                },
            */
        });
    </script>
@stop