@extends('admin.layouts.master')
@section('title') All Test @stop

@section('styles')
    <style>
        .admin-thumb{
            width: 50px;
        }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            All Test
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">All page</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title(English)</th>
                                <th>Title(Bangla)</th>
                                <th>View</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                        @if(isset($page))
                            @foreach($page as $product)
                                <tr>
                                    <td>{{ $product->id }}</td>
                                    <td>{{ $product->title_en }}</td>
                                    <td>{{ $product->title_bn}}</td>
                                    <td>{{ $product->slug}}</td>
                                    <td>
                                        <a href="{{ route('edit.page', array($product->id)) }}" class="btn btn-app" >
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        <a href="{{ route('page.delete', array($product->id)) }}" class="btn delete-page btn-app" >
                                            <i class="fa fa-trash-o"></i>
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Title(English)</th>
                                <th>Title(Bangla)</th>
                                <th>View</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@stop
@section('scripts')
    <!-- page script -->
    <script>
        $(function () {
            //$('#example2').DataTable();
            $('.delete-page').on('click', function (event) {
                var rlink = $(this).attr('href');
                event.preventDefault();
                swal({
                    title: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        window.location.href = rlink;
                    }
                });
            });
        });
    </script>
@stop