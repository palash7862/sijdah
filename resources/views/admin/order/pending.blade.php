@extends('admin.layouts.master') 

@section('title') Dashboard @stop

@section('styles') 
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('public/admin-assets/plugins/select2/select2.min.css') }}">
<style>
	.admin-thumb{
		width: 50px;
	}
	.table-bordered > thead > tr > th.table-name {
	  background-color: #00a65a;
	  color: #ffffff;
	  font-size: 35px;
	  text-align: center;
	}
	.table-bordered > thead > tr > th, 
	.table-bordered > tfoot > tr > th {
	  font-size: 16px;
	}
</style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Orders
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('adminHome') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('orders') }}">Orders</a></li>
        <li class="active">Order Detiels</li>
      </ol>
    </section> 
    <!-- Main content -->
    <section class="content">
		<div class="row">
			@if(Route::currentRouteName() == 'order.panding.up')
			<div class="col-xs-12"> 
				<div class="box"> 
				{!! Form::open(['route' => 'order.panding.update', 'class' => 'test-create-form']) !!} 
				<input type="hidden" name="orderId" value="{{ $Order->id }}">
					<div class="box-header"> 
						<h2 style="margin: 0px;">Update To Processing</h2> 
					</div>
					<div class="box-body">  
						<div class="row"> 
							<div class="col-md-6">  
								<div class="form-group">
									<label>Barcode : </label>

									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control" name="barcode" value="{{ old('barcode') }}" > 
									</div>
									@if($errors->first('barcode')) 
										<strong class="error-msg">{{ $errors->first('barcode') }} </strong> 
									@endif
									<!-- /.input group -->
								</div>
								<!-- /.form group -->  
							</div>  
							<div class="col-md-6">  
								<div class="form-group">
									<label>Labcode : </label>

									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control" name="labcode" value="{{ old('labcode') }}" > 
									</div>
									@if($errors->first('labcode')) 
										<strong class="error-msg">{{ $errors->first('labcode') }} </strong> 
									@endif
									<!-- /.input group -->
								</div>
								<!-- /.form group -->  
							</div> 
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Update To Processing</button>
					</div>
					<!-- /.box-body --> 
				{!! Form::close() !!}
				</div>
			</div>
			@endif
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header"> 
					</div>
					<!-- /.box-header -->
					<div class="box-body">  
						<div class="col-md-6">
							<table id="example2" class="table table-bordered table-striped">
								<thead>
									<tr> 
										<th colspan="2" class="table-name">Order Detiels</th>   
									</tr>
									<tr> 
										<th>Property</th>   
										<th>Value</th>   
									</tr>
								</thead> 
								<tbody>   
									<tr> 
										<td>Order Id</td>  
										<td>{{ $Order->id }}</td>  
									</tr>
									<tr> 
										<td>User Type</td>  
										<td>{{ $Order->userType }}</td>  
									</tr>  
									<tr> 
										<td>User Name</td>  
										@if($Order->userType == 'Member') 
											<td>{{ $Order->user->name }}</td>
										@elseif($Order->userType == 'Guest') 
											<td>{{ $Order->guest->name }}</td> 
										@endif  
									</tr> 
									<tr> 
										<td>User Email</td>  
										@if($Order->userType == 'Member') 
											<td>{{ $Order->user->email }}</td>
										@elseif($Order->userType == 'Guest') 
											<td>{{ $Order->guest->email }}</td> 
										@endif  
									</tr>
									<tr> 
										<td>User Phone</td>  
										@if($Order->userType == 'Member') 
											<td>{{ $Order->user->mobile }}</td>
										@elseif($Order->userType == 'Guest') 
											<td>{{ $Order->guest->mobile }}</td> 
										@endif  
									</tr>  
									<tr> 
										<td>User Address</td>  
										@if($Order->userType == 'Member') 
											<td>{{ $Order->user->address }}</td>
										@elseif($Order->userType == 'Guest') 
											<td>{{ $Order->guest->address }}</td> 
										@endif   
									</tr> 
									<tr> 
										<td>Order Date</td>  
										<td>{{ $Order->created_at }}</td>  
									</tr> 
								</tbody>
								<tfoot>
									<tr> 
										<th>Property</th>   
										<th>Value</th>   
									</tr>
								</tfoot>
							</table>
						</div>
						<div class="col-md-6">
							<table id="example2" class="table table-bordered table-striped">
								<thead>
									<tr> 
										<th colspan="2" class="table-name">Payment Detiels</th>   
									</tr>
									<tr> 
										<th>Property</th>   
										<th>Value</th>   
									</tr>
								</thead> 
								<tbody> 
								@if($Order->order_type == 'ssl')  
									<tr> 
										<td>Payment Store Id</td>  
										<td>{{ $Order->payment->id }}</td>  
									</tr> 
									<tr> 
										<td>Tran Id</td>  
										<td>{{ $Order->payment->tran_id }}</td>  
									</tr>
									<tr> 
										<td>Bank Tran Id</td>  
										<td>{{ $Order->payment->bank_tran_id }}</td>  
									</tr> 
									<tr> 
										<td>Card Type</td>  
										<td>{{ $Order->payment->card_type }}</td>  
									</tr> 
									<tr> 
										<td>Card Issuer Country</td>  
										<td>{{ $Order->payment->card_issuer_country }}</td>  
									</tr> 
									<tr> 
										<td>Currency Amount </td>  
										<td>{{ $Order->payment->currency_amount }}</td>  
									</tr> 
									<tr> 
										<td>Tran Date</td>  
										<td>{{ $Order->payment->tran_date }}</td>  
									</tr>
									@else
										<tr>
											<td colspan="2"><h2 class="text-center">Cash On Delivery</h2></td>
										</tr>
										<tr>
											<td>Amount</td>
											<td>&#x9f3; {{$Order->total}}</td>
										</tr>
									@endif  
								</tbody>
								<tfoot>
									<tr> 
										<th>Property</th>   
										<th>Value</th>   
									</tr>
								</tfoot>
							</table>
						</div>

						<div class="col-md-12">
							<table id="example2" class="table table-bordered table-striped">
								<thead>
									<tr> 
										<th colspan="6" class="table-name">Test Or Health List</th> 
									</tr> 
									<tr> 
										<th>ID</th>   
										<th>Test Image</th>   
										<th>Test Type</th>   
										<th>Test Name</th>   
										<th>Test Short Name</th>   
										<th>Price</th>   
									</tr>
								</thead> 
								<tbody>
									@if($tests = json_decode($Order->products) ) 
										@foreach($tests as $test)  
											<tr>   
												<td>{{ $test->id }}</td>
												<td>
													<img src="{{ $test->attributes->imgPath }}" alt="" width="150px">
												</td>
												<td>{{ $test->attributes->product_type }}</td> 
												<td>{{ $test->name }}</td> 

												@if($test->attributes->product_type == 'Test')  
													<td>{{ $test->attributes->short_name }}</td>
												@else
													<td>Not Available</td>
												@endif 

												<td>{{ $test->price }}</td>   
											</tr> 
										@endforeach
									@endif
								</tbody>
								<tfoot>
									<tr> 
										<th>ID</th>   
										<th>Test Image</th>   
										<th>Test Type</th>   
										<th>Test Name</th>   
										<th>Test Short Name</th>   
										<th>Price</th>   
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				<!-- /.box-body -->
				</div>
			</div>
		</div>
    </section>
    <!-- /.content -->

@stop
@section('scripts') 
<!-- Select2 -->
<script src="{{ asset('public/admin-assets/plugins/select2/select2.full.min.js') }}"></script>
	<!-- page script -->
<script>
$(".select2").select2();
	$(function () { 
		$('#example2').DataTable({
		  "paging": true,
		  "lengthChange": false,
		  "searching": false,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});
	});
</script>
@stop