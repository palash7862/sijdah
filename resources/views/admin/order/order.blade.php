@extends('admin.layouts.master')
@section('title') Single Order @stop

@section('styles') 
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/select2.min.css') }}">
<style>
	.admin-thumb{
		width: 50px;
	}
	.table-bordered > thead > tr > th.table-name {
	  background-color: #00a65a;
	  color: #ffffff;
	  font-size: 35px;
	  text-align: center;
	}
	.table-bordered > thead > tr > th, 
	.table-bordered > tfoot > tr > th {
	  font-size: 16px;
	}
</style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Orders
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('orders') }}">Orders</a></li>
        <li class="active">Order Detiels</li>
      </ol>
    </section> 
    <!-- Main content -->
    <section class="content">
		<div class="row"> 
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header"> 
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<table id="example2" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th colspan="2" class="table-name">Order User Detiels</th>
										</tr>
										<tr>
											<th>Property</th>
											<th>Value</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Order Id</td>
											<td>{{ $Order->id }}</td>
										</tr>
										<tr>
											<td>User Name</td>
											<td>{{ $Order->user->username }}</td>
										</tr>
										<tr>
											<td>User Email</td>
											<td>{{ $Order->user->email }}</td>
										</tr>
										<tr>
											<td>User Phone</td>
											<td>{{ $Order->user->phone }}</td>
										</tr>
										<tr>
											<td>Order Date</td>
											<td>{{ $Order->created_at }}</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-md-6">
								<table id="example2" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th colspan="2" class="table-name">Payment Detiels</th>
										</tr>
										<tr>
											<th>Property</th>
											<th>Value</th>
										</tr>
									</thead>
									<tbody>
									@if($Order->payment_type == 'online')
										<tr>
											<td>Tran Id</td>
											<td>{{ $Order->payment->tran_id }}</td>
										</tr>
										<tr>
											<td>Bank Tran Id</td>
											<td>{{ $Order->payment->bank_tran_id }}</td>
										</tr>
										<tr>
											<td>Card Type</td>
											<td>{{ $Order->payment->card_type }}</td>
										</tr>
										<tr>
											<td>Card Issuer Country</td>
											<td>{{ $Order->payment->card_issuer_country }}</td>
										</tr>
										<tr>
											<td>Currency Amount </td>
											<td>{{ $Order->payment->currency_amount }}</td>
										</tr>
										<tr>
											<td>Tran Date</td>
											<td>{{ $Order->payment->tran_date }}</td>
										</tr>
										@else
											<tr>
												<td colspan="2"><h2 class="text-center">Cash On Delivery</h2></td>
											</tr>
											<tr>
												<td>Amount</td>
												<td>&#x9f3; {{$Order->total}}</td>
											</tr>
										@endif
									</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
							<table id="example2" class="table table-bordered table-striped">
								<thead>
									<tr> 
										<th colspan="6" class="table-name">Order Product List</th>
									</tr> 
									<tr> 
										<th>ID</th>   
										<th>Product Image</th>
										<th>Product Name</th>
										<th>Sell Price</th>
										<th>Product Quality</th>
									</tr>
								</thead> 
								<tbody>
										@foreach($Order->orderProduct as $product)
											<tr>
												<td>{{ $product->product_id }}</td>
												<td>
													@if($product->product->featured_img_id
														&& $fid=$product->product->featured_img_id)
														<img src="{{asset(get_image_size('cart-thumb',$product->product->futureImageSrc($fid)))}}">
													@endif
												</td>
												<td>{{ $product->product->title_en }}</td>
												<td>{{ $product->unit_price }}</td>
												<td>{{ $product->qty }}</td>
											</tr>
										@endforeach
								</tbody>
								<tfoot>
									<tr>
										<th>ID</th>
										<th>Product Image</th>
										<th>Product Name</th>
										<th>Sell Price</th>
										<th>Product Quality</th>
									</tr>
								</tfoot>
							</table>
						</div>
						</div>
					</div>
				<!-- /.box-body -->
				</div>
			</div>
		</div>
    </section>
    <!-- /.content -->

@stop
@section('scripts') 
<!-- Select2 -->
<script src="{{ asset('public/admin-assets/plugins/select2/select2.full.min.js') }}"></script>
	<!-- page script -->
<script>
$(".select2").select2();
	$(function () { 
		$('#example2').DataTable({
		  "paging": true,
		  "lengthChange": false,
		  "searching": false,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});
	});
</script>
@stop