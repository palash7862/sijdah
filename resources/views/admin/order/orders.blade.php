@extends('admin.layouts.master')
@section('title') All Orders @stop


@section('styles')
    <style>
        .admin-thumb {
            width: 50px;
        }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Orders
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Orders</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Orders</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>User Name</th>
                                <th>User Email</th>
                                <th>Mobile</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($Orders as $Order)
                                <tr>
                                    <td>{{ $Order->id }}</td>
                                    <td>{{ $Order->user->username }}</td>
                                    <td>{{ $Order->user->email }}</td>
                                    <td>{{ $Order->user->phone }}</td>

                                    <td>{{ $Order->created_at }}</td>

                                    <td>
                                        @if( $Order->status == 'placed' )
                                            <span class="label label-warning">{{ $Order->status }}</span>
                                        @elseif( $Order->status == 'processing' )
                                            <span class="label label-info">{{ $Order->status }}</span>
                                        @elseif( $Order->status == 'shipped' )
                                            <span class="label label-info">{{ $Order->status }}</span>
                                        @elseif( $Order->status == 'delivered' )
                                            <span class="label label-success">{{ $Order->status }}</span>
                                        @elseif( $Order->status == 'cancel' )
                                            <span class="label label-danger">{{ $Order->status }}</span>
                                        @endif
                                    </td>

                                    <td>
                                        <a class="btn btn-app" href="{{route('order.single', array($Order->id))}}">
                                            <i class="fa fa-edit"></i>
                                            View Ditels
                                        </a>
                                        @if( Route::currentRouteName() == 'orders.placed' )
                                            <a class="btn btn-app order-status-up" href="{{route('order.process.up', array($Order->id))}}">
                                                <i class="fa fa-edit"></i>
                                                Provessing
                                            </a>
                                            <a class="btn btn-app order-status-up" href="{{route('order.cancel.up', array($Order->id))}}">
                                                <i class="fa fa-edit"></i>
                                                Cancel
                                            </a>
                                        @elseif(Route::currentRouteName() == 'order.processing')
                                            <a class="btn btn-app order-status-up" href="{{route('order.shipped.up', array($Order->id))}}">
                                                <i class="fa fa-edit"></i>
                                                Shipped
                                            </a>
                                        @elseif(Route::currentRouteName() == 'order.shipped')
                                            <a class="btn btn-app order-status-up" href="{{route('order.delivered.up', array($Order->id))}}">
                                                <i class="fa fa-edit"></i>
                                                Delivered
                                            </a>
                                        @endif
                                        <a class="btn btn-app" href="{{route('order.invoice', array($Order->id))}}">
                                            <i class="fa fa-edit"></i>
                                            View Invoice
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>User Name</th>
                                <th>User Email</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

    <div class="modal fade orderCancel" role="dialog" aria-labelledby="orderCancel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cancel Modal</h4>
                </div>
                <div class="modal-body">
                    <h3> Are You Sure? You Want To Cancel This Order &hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <a href="" class="btn btn-primary calcelConfirm">Save changes</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@stop
@section('scripts')
    <!-- page script -->
    <script>
        $(function () {
                $('.order-status-up').on('click', function (event) {
                        var rlink = $(this).attr('href');
                        event.preventDefault();
                        swal({
                            title: 'Are you sure?',
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, delete it!'
                        }).then((result) => {
                            if (result.value) {
                            window.location.href = rlink;
                        }
                    });
                    });

//            $('#example2').DataTable({
//                "order": [[0, "desc"]]
//            });
//            $('[data-toggle="modal"]').on('click', function () {
//                var url = $(this).data('url');
//                $('.calcelConfirm').attr('href', url);
//            });
        });
    </script>

@stop