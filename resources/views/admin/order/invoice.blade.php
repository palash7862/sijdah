<style>  
    @page { margin: 0px; }
    body { margin: 0px; }
    body {
	font-size: 14px;
	background-image: url("bg.png");
	background-size: 100% 100%;
	background-repeat: no-repeat;
	background-position: 0px 0px;
	
}
    .invoice-box { 
        max-width: 800px;
        margin: auto;  
        font-size: 14px;
        line-height: 22px;
        padding: 30px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555; 
		display: block;
    }  
    .header-top {
	padding-right: 75px;
	padding-left: 20px; 
    }
    
    .header-left {
	text-align: right;
        position: relative;
	margin-bottom: 40px;
        display: block;
    }
    .header-left::before {
	content: "";
	background: #DDD;
	left: 8px;
	right: 0px;
	top: 105px;
	position: absolute;
	height: 1px;
	width: 650px;
	z-index: -1;
    }
    .store-name {
	margin-top: 10px;
	margin-bottom: 10px;
	font-size: 20px;
	font-weight: bold;
        color: #6DB4C8;
    }
    .header-left p {
	font-size: 14px;
	color: #888;
	margin: 0px;
    }
    .header-bottom {
	margin-bottom: 25px;
        padding-left: 25px
    }
    .invoice-no {
	margin-bottom: 15px;
	line-height: 35px;
	font-size: 32px;
	margin-top: 0px;
}
    .invoice-no span {
	background: #6DB4C8;
	color: #fff;
        position: relative;
        z-index: 2;
        line-height: 30px;
        display: inline-block;
    }
    .invoice-no span::before {
	content: "";
	background: #6DB4C8;
	left: -80px;
	top: 0px;
	bottom: 0px;
	position: absolute;
	height: 30px;
	width: 82px;
	z-index: -1;
    }
    .address p {
	margin: 0px;
	line-height: 28px;
    }
    .address .contact {
        line-height: 24px;
    }
    .content{
        width: 100%;  
    }
    .content table {
	width: 100%;
	border: none;
    }
    table thead tr th {
	background: #057696;
	color: #FFFFFF;
	padding: 6px 8px;
	font-size: 14px;
    }
    table tr td, table tr th {
	text-align: left;
	padding: 8px 8px;
	border-bottom: 1px solid #ddd;
    } 
    table tr td {
	font-size: 14px;
	text-align: center;
	color: #888;
    }
    
    table tr th:last-child , table tr td:last-child{
        text-align: right;
    }
    table tbody tr td:first-child {
	width: 40%;
	text-align: left;
	}
    table tfoot tr td {
	border: none;
    }
    table tfoot tr:first-child td {
	padding-top: 30px;
    }
    table tfoot td {
	color: #686765;
	padding: 6px 15px;
    }
    table tfoot td{
        text-align: left;
    }
    table tfoot td:last-child{
        text-align: right;
    }
    table tfoot tr:last-child td:nth-child(2), table tfoot tr:last-child td:nth-child(3) {
            background: #057696;
            color: #ffffff;
            position: relative;
            z-index: 1;
    }
    table tfoot tr:last-child td:nth-child(2)::before {
            content: "";
            position: absolute;
            width: 400px;
            height: 34px;
            left: -185px;
            background: #057696;
            top: 0px;
            z-index: -1;
            transform: skew(-33deg);
    }
    .bottom-content {
	width: 100%;
        display: table; 
        margin-top: 30px;
    }
    .bottom-content .bottom-left {
	width: 30%; 
        display: table-cell;
    }
    .bottom-content .bottom-right { 
	width: 70%;
        display: table-cell;
    }
    .bottom-content p {
	margin: 0px;
    }  
    .bottom-content .bottom-left p:nth-child(2), .bottom-content .bottom-right p:nth-child(2) {
	margin-bottom: 15px;
}
    .footer h2 {
	margin-bottom: 0px;
	text-align: right;
	position: relative;
	z-index: 2;
}
    .footer h2::after {
	content: "";
	position: absolute;
	right: -147px;
	top: -2px;
	width: 150px;
	height: 33px;
	background: #6DB4C8;
	z-index: -1;
}
    .footer h2 span {
	background: #6DB4C8;
	color: #ffffff;
	font-size: 32px;
	line-height: 34px;
	position: relative;
    }
    .copyright {
	display: block;
	width: 100%;
	font-size: 13px;
	text-align: center;
	font-weight: bold;
	margin-bottom: 0px;
}
</style> 
<div class="invoice-box">
    <div class="header">
        <div class="header-top"> 
            <div class="header-left">
                <h5 class="store-name">Dino Store</h5>
                <p>277 Cobblone Road - 3300 Black, Cobblone Country</p>
                <p>+55456-6564 - www.website.com - hello@gmail.com</p>
            </div>
        </div>
        <div class="header-bottom">
            <h1 class="invoice-no"><span>INVOICE</span> # {{$order->id}}</h1>
            <div class="address">
                <p>To : </p>
                <p><b>Slate Rock and Gravel Company</b></p>
                <p class="contact">
                    222 Rocky Way <br>
                    33000 Bedrock cobbleston Country <br>
                    +555787546546 <br>
                    fred@slaterockgravel.bed <br>
                    Attn: Fred Flintstone
                </p>
            </div>
            <address>
                
            </address>
        </div>
    </div>
    <div class="content">
        <table cellspacing="0">
            <thead>
                <tr>
                    <th>ITEM</th>
                    <th>QUANTITY</th>
                    <th>PRICE</th>
                    <th>DISCOUNT</th>
                    <th>TAX</th>
                    <th>LM.TOTAL</th>
                </tr>
            </thead>
            <tbody>
            @foreach($order->orderProduct as $product)
                <tr>
                    <td>{{ $product->product->title_en }}</td>
                    <td>{{ $product->qty }}</td>
                    <td>{{ $product->unit_price }}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3"></td>
                    <td colspan="2"><b>Subtotal:</b></td>  
                    <td>$2,936.00</td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td colspan="2"><b>Tax 1:</b></td>  
                    <td><span>(2%)</span> $4.80</td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td colspan="2"><b>Tax 2:</b></td>  
                    <td><span>(2%)</span> $4.80</td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td colspan="2"><b style="font-size:16px">TOTAL:</b></td>  
                    <td><b style="font-size:16px">$3,074.55</b></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td colspan="2"><b>Paid:</b></td>  
                    <td>0.00</td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td colspan="2"><b style="font-size:16px">AMOUNT DUE</b></td>  
                    <td><b style="font-size:16px">$3,074.55</b></td>
                </tr>
            </tfoot>
        </table>  
    </div> 
    <div class="bottom-content">
        <div class="bottom-left">
            <p><b>Issued on: </b> 03/06/2017</p>
            <p><b>Due onc: </b> 03/06/2017</p>
            <p><b>Currency:</b> USD</p>
            <p><b>P.O</b> # 1/3/4</p>
            <p><b>Net:</b> 21</p>
        </div>
        <div class="bottom-right">
            <p style="text-indent: 50">Lorem Ipsum is simply dummy text of the printing.</p>
            <p style="text-indent: 40">Lorem Ipsum is simply dummy text of the printing.</p> 
            <p style="text-indent: 30">Payment Details:</p>
            <p style="text-indent: 20"><b>*</b> ACC 45549846546</p>
            <p style="text-indent: 10"><b>*</b> IBAN US8654132</p>
            <p style="text-indent: 0"><b>*</b> SWIFT B0655</p>
        </div>
    </div> 
    <div class="footer">
        <h2><span>THANKS!</span></h2>
        <p class="copyright">Write Your @Copywrite Message Hare</p> 
    </div> 
</div> 
