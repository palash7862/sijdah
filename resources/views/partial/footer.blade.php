<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="color-bg">

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5a964f914b401e45400d4459/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->


    <div class="sub-form-row">
        <div class="container">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 no-padding">
                {!! Form::open(['route' => 'subscriber.create', 'class'=>'form-horizontal form-bordered' ]) !!}
                <input placeholder="আমদের  নিউজলেটার  সাবস্ক্রাইব করুন" name="email" data-alt="Subscribe to our newsletter">
                <button class="le-button"><span class="lang-switch" data-en="Subscribe" data-bn="সাবস্ক্রাইব">সাবস্ক্রাইব</span></button>
                {!! Form::close() !!}
            </div>
        </div><!-- /.container -->
    </div><!-- /.sub-form-row -->


    <div class="link-list-row">
        <div class="container no-padding">
            <div class="col-xs-12 col-md-4 ">
                <!-- ============================================================= CONTACT INFO ============================================================= -->
                <div class="contact-info">
                    <div class="footer-logo">
                        <img src="" height="48" width="90" alt="লোগো">
                    </div><!-- /.footer-logo -->

                    <p class="regular-bold lang-switch" data-en="Feel free to contact us via phone or email"
                       data-bn="আমাদের কে ইমেইল অথবা ফোন  করতে পারেন">আমাদের কে ইমেইল অথবা ফোন  করতে পারেন</p>

                    <p class="lang-switch" data-en="@if($address_en){{ $address_en->description }} @endif"
                       data-bn="@if($address_en){{ $address_bn->description }} @endif">
                        @if($address_en){{ $address_bn->description }} @endif
                    </p>

                    <div class="social-icons">
                        <h3 class="lang-switch" data-en="Get in touch" data-bn="সান্নিধ্যে থাকুন">সান্নিধ্যে থাকুন</h3>
                        <ul>
                            <li><a href="@if($facebook){{$facebook->description}} @endif" class="fa fa-facebook"></a></li>
                            <li><a href="@if($twitter){{$twitter->description}} @endif" class="fa fa-twitter"></a></li>
                            <li><a href="@if($pinterest){{$pinterest->description}} @endif" class="fa fa-pinterest"></a></li>
                            <li><a href="@if($linkedin){{$linkedin->description}} @endif" class="fa fa-linkedin"></a></li>
                            <li><a href="@if($stumbleupon){{$stumbleupon->description}} @endif" class="fa fa-stumbleupon"></a></li>
                            <li><a href="@if($dribbble){{$dribbble->description}} @endif" class="fa fa-dribbble"></a></li>
                            <li><a href="@if($vk){{$vk->description}} @endif" class="fa fa-vk"></a></li>
                        </ul>
                    </div><!-- /.social-icons -->

                </div>
                <!-- ============================================================= CONTACT INFO : END ============================================================= -->
            </div>

            <div class="col-xs-12 col-md-8 no-margin">
                <!-- ============================================================= LINKS FOOTER ============================================================= -->
                <div class="link-widget">
                    <div class="widget">
                        @if($footerWidget_1)
                        <h3>{{ json_decode($footerWidget_1->description)->widget_title }}</h3>
                        <ul>
                            @foreach(json_decode($footerWidget_1->description)->title as $key => $val)
                                <li><a href="{{ json_decode($footerWidget_1->description)->url[$key] }}">{{ $val }}</a>
                                </li>
                            @endforeach
                        </ul>
                        @endif
                    </div><!-- /.widget -->
                </div><!-- /.link-widget -->

                <div class="link-widget">
                    <div class="widget">
                        @if($footerWidget_2)
                        <h3>{{ json_decode($footerWidget_2->description)->widget_title }}</h3>
                        <ul>
                            @foreach(json_decode($footerWidget_2->description)->title as $key => $val)
                                <li><a href="{{ json_decode($footerWidget_2->description)->url[$key] }}">{{ $val }}</a>
                                </li>
                            @endforeach

                        </ul>
                        @endif
                    </div><!-- /.widget -->
                </div><!-- /.link-widget -->

                <div class="link-widget">
                    <div class="widget">
                        @if($footerWidget_3)
                        <h3>{{ json_decode($footerWidget_3->description)->widget_title }}</h3>
                        <ul>
                            @foreach(json_decode($footerWidget_3->description)->title as $key => $val)
                                <li><a href="{{ json_decode($footerWidget_3->description)->url[$key] }}">{{ $val }}</a>
                                </li>
                            @endforeach
                        </ul>
                        @endif
                    </div><!-- /.widget -->
                </div><!-- /.link-widget -->
                <!-- ============================================================= LINKS FOOTER : END ============================================================= -->
            </div>
        </div><!-- /.container -->
    </div><!-- /.link-list-row -->
</footer><!-- /#footer -->
<!-- ============================================================= FOOTER : END ============================================================= -->