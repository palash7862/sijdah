<head>
        <!-- Meta -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

        <title>@yield('title') | @if($sitename){{ $sitename->description }}@endif</title>

        <!-- Meta -->
@yield('metas')

<!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">

        <!-- Customizable CSS -->

        <link rel="stylesheet" href="{{ asset('assets/css/colors/green.css')}}">
        <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{ asset('assets/css/owl.transitions.css')}}">
        <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css')}}">

        <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
        <!-- Icons/Glyphs -->
        <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css')}}">

        <!-- Favicon -->
        <link rel="shortcut icon" href=" @if($favicon) {{asset(getImageSizeById($favicon->description,'full'))}} @endif">

        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/side.css')}}">

        <!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
        <!--[if lt IE 9]>
        <script src="{{ asset('assets/js/html5shiv.js"></scrip')}}t>
                <script src="{{ asset('assets/js/respond.min.js"></scrip')}}t>
        <![endif]-->

        <link rel="stylesheet" href="{{ asset('assets/product_slider/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{ asset('assets/product_slider/css/owl.theme.default.min.css')}}">
        <!--main style css-->
        <link rel="stylesheet" href="{{ asset('assets/product_slider/css/style.css')}}">
        <!--Responsive css-->
        <link rel="stylesheet" href="{{ asset('assets/product_slider/css/responsive.css')}}">
        <link rel="stylesheet" href="{{ asset('assets/js/modernizr.js')}}">
        <!--Sweet Alert css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
        <!--Custom css-->
        @yield('styles')

</head>