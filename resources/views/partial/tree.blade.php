<div id="collapse_{{$category->id}}" class="accordion-body collapse">
	<div class="accordion-inner">
		<ul>
			@foreach($childs as $child)
			<li>
				<div class="accordion-heading">
					<a class="lang-switch" href="#collapse_{{$child->id}}" data-toggle="collapse" data-en="{{ $child->name_en }}" data-bn="{{ $child->name_bn }}">{{ $child->name_bn }}</a>
				</div>
				@if(count($child->childs))
				    @include('partial.tree',['childs' => $child->childs,'category'=>$child])
				@endif<!-- /.accordion-body -->
			</li>
			@endforeach
		</ul>
	</div><!-- /.accordion-inner -->
</div>