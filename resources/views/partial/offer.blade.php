<section class="offer-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <div class="headphone-title-text">
                    <h5>limited time offer</h5>
                    <h2>premium Audio</h2>
                    <h1><strong>quality headphones</strong></h1>
                </div>
                <div class="offer-timer">
                    <div class="offer-time-counter">
                        <div class="offer-time-countdown" data-countdown="2018/06/31"></div>
                    </div>
                </div>
                <div class="headphone-button">
                    <a href="#" class="head-btn">read more</a>
                    <a href="#" class="head-btn head-op">Add to cart</a>
                </div>
            </div>
            <div class="col-md-7">
                <div class="product-slider-area responsive owl-carousel">
                    <div class="product-single text-center">
                        <div class="product-area">
                            <img src="assets/product_slider/img/headphone-1.jpg" class="img-responsive" alt="">
                            <div class="new-product"><span>New</span></div>
                            <div class="product-overlay">
                                <div class="cart-button">
                                    <a href="#"">Add to cart</a>
                                </div>
                            </div>
                        </div>
                        <div class="product-details">
                            <h6>cupidata nonsunt proidun</h6>
                            <h4>Marketplace</h4>
                            <h5>$250</h5>
                        </div>
                    </div>
                    <div class="product-single text-center">
                        <div class="product-area">
                            <img src="assets/product_slider/img/headphone-2.jpg" class="img-responsive" alt="">
                            <div class="discount"><span>-50%</span></div>
                            <div class="product-overlay">
                                <div class="cart-button">
                                    <a href="#"">Add to cart</a>
                                </div>
                            </div>
                        </div>
                        <div class="product-details">
                            <h6>cupidata nonsunt proidun</h6>
                            <h4>Marketplace</h4>
                            <h5><span>$100</span>$250</h5>
                        </div>
                    </div>
                    <div class="product-single text-center">
                        <div class="product-area">
                            <img src="assets/product_slider/img/headphone-3.jpg" class="img-responsive" alt="">
                            <div class="product-overlay">
                                <div class="cart-button">
                                    <a href="#"">Add to cart</a>
                                </div>
                            </div>
                        </div>
                        <div class="product-details">
                            <h6>cupidata nonsunt proidun</h6>
                            <h4>Marketplace</h4>
                            <h5>$250</h5>
                        </div>
                    </div>
                    <div class="product-single text-center">
                        <div class="product-area">
                            <img src="assets/product_slider/img/headphone-1.jpg" class="img-responsive" alt="">
                            <div class="product-overlay">
                                <div class="cart-button">
                                    <a href="#"">Add to cart</a>
                                </div>
                            </div>
                        </div>
                        <div class="product-details">
                            <h6>cupidata nonsunt proidun</h6>
                            <h4>Marketplace</h4>
                            <h5>$250</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>