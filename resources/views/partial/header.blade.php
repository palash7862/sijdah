<!-- ======================== TOP NAVIGATION ========================= -->
<nav class="top-bar animate-dropdown">
    <div class="container">
        <div class="col-xs-12 col-sm-6 no-margin">

            <ul class="lng">
                <li><a onclick="event.preventDefault()" href="#" class="lang-btn" data-locale="bn">বাং</a></li>
                <li><a onclick="event.preventDefault()" href="#" class="lang-btn" data-locale="en">EN</a></li>
                <li><i class="fa fa-phone"></i></li>
                <li class="lang-digit" data-val="@if($phone){{ $phone->description }} @endif">@if($phone){{ enToBn($phone->description) }} @endif</li>
            </ul>

        </div><!-- /.col -->
    </div><!-- /.container -->
</nav><!-- /.top-bar -->
<!-- ========================== TOP NAVIGATION : END =========================== -->

<!-- ============================== HEADER ============================================== -->
<header class="no-padding-bottom header-alt">
    <div class="container no-padding">
        <div class="mobile-arrow">
            <div id="mobile-menu-switch" class="inner-arrow">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="logo-call col-md-3 logo-holder">
            <div class="logo">
                <a href="{{url('/')}}"> <img src="@if($logo){{ asset(getImageSizeById($logo->description,'full')) }} @endif" style="height: 48px;width: 90px"></a>
            </div>
        </div><!-- /.logo-holder -->


<!-- ============================ SEARCH AREA ======================================== -->
        <div class="search-call col-md-6 top-search-holder no-margin mobile-hidden ">

            <div class="search-area">
                <form>
                    <div class="control-group">
                        <input class="search-field" placeholder="পণ্য খুঁজুন" id="search-input" autocomplete="off"  data-alt="Search for an item" />
                        <a class="search-button" href="#"></a>
                    </div>
                </form>
            </div><!-- /.search-area -->
            <div id="search-results">
                
            </div>

        </div><!-- /.top-search-holder -->
 <!-- ========================= SEARCH AREA : END ===================================== -->


        <div class="cart-call col-sm-3 col-md-3 top-cart-row no-margin">
            <div class="login-box-wapper">
                @if(Auth::check())
                    <form method="POST" action="{{route('logout')}}">

                        <div class="dropdown">

                            <button class="my-account lang-switch" type="button" data-toggle="dropdown" data-en="My account" data-bn="একাউন্ট">একাউন্ট<span class="caret"></span></button>

                            <ul class="dropdown-menu">
                                <li><a href="{{route('user.profile')}}" class="lang-switch" data-en="profile" data-bn="প্রোফাইল">প্রোফাইল</a></li>
                                <li>
                                    <form method="post" action="{{route('logout')}}">
                                        {{csrf_field()}}

                                        <button class="logout lang-switch" data-en="logout" data-bn="লগ আউট">লগ আউট</button>
                                    </form>
                                </li>

                            </ul>
                        </div>

                    </form>

                @else
                <a href="#" class="login-reg-text lang-switch" data-en="Login / Register" data-bn="লগইন / রেজিস্টার">লগইন / রেজিস্টার</a>
                <div class="login-reg-box">
                    <h3 class="login-title"><span class="lang-switch" data-en="Sign in" data-bn="সাইন ইন">সাইন ইন</span>
                        <a href="{{route('register')}}" class="create-account-link lang-switch" data-en="Create an Account" data-bn="নতুন একাউন্ট তৈরী করুন">নতুন একাউন্ট তৈরী করুন</a>
                    </h3>
                    <form class="login-form" action="{{route('login')}}" method="POST">
                        {{ csrf_field() }}
                        <p>
                            <label class="lang-switch" data-en="Username or email" data-bn="ইউজার নেম / ইমেইল">ইউজার নেম / ইমেইল</label>
                            <input type="text" name="email"  required autofocus>
                        </p>
                        <p>
                            <label class="lang-switch" data-en="Password" data-bn="পাসওয়ার্ড">পাসওয়ার্ড</label>
                            <input type="password" name="password" required>
                        </p>
                        <p class="submit">
                            <label class="lang-switch" data-en="Username or email" data-bn="ইউজার নেম / ইমেইল">ইউজার নেম / ইমেইল </label>
                            <input type="submit" name="Login" value="Login" >
                        </p>
                        <p class="remember">
                            <label>
                                <span><input type="checkbox" name="remember"><span class="lang-switch" data-en="Remember me" data-bn="মনে রাখুন">মনে রাখুন</span></span>
                                <a href="" class="lost-link lang-switch" data-en="Lost your password?" data-bn="পাসওয়ার্ড ভুলে গেছেন?">পাসওয়ার্ড ভুলে গেছেন?</a>
                            </label>
                        </p>
                    </form>
                    <span class="social-login-title lang-switch" data-en="Or login with" data-bn="লগিন করুন">লগিন করুন</span>
                    <div class="social-btns">
                        <a href="{{route('fb.login')}}" class="social-btn fb lang-switch" data-en="Facebook" data-bn="ফেসবুক">ফেসবুক</a>
                        <a href="{{route('google.login')}}" class="social-btn ggl lang-switch" data-en="Google" data-bn="গুগল">গুগল</a>
                    </div>
                </div>
                @endif
            </div>
            <div class="top-cart-row-container">
                <div class="top-cart-holder dropdown animate-dropdown">
                    <div class="basket">

                        <a class="dropdown-toggle" data-toggle="modal" data-target="#myModal2" href="#">
                            <div class="basket-item-count">
                                <span class="count lang-digit" id="product-count" data-val="{{ \Cart::content()->count() }}">{{ enToBn(\Cart::content()->count()) }}</span>
                                <img src="{{asset('assets/images/icon-cart.png')}}" alt="" />
                            </div>

                            <div class="total-price-basket">
                                <span class="lbl lang-switch" data-en="your cart:" data-bn="ব্যাগ:">ব্যাগ:</span>
                                <span class="total-price">
                                    <span class="sign">৳ </span><span class="value lang-digit" data-val="{{(int)\Cart::subtotal()}}" id="cart-subtotal">{{enToBn((int)\Cart::subtotal())}}</span>
                                </span>
                            </div>
                        </a>

                        <!-- right Modal -->
                        <div class="cart-model modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title lang-switch" id="myModalLabel2" data-en="Shopping Cart" data-bn="ব্যাগ">ব্যাগ</h4>
                                    </div>

                                    <div class="modal-body">
                                        <div class="modal-body-product">
                                            @foreach(\Cart::content() as $product)
                                                <div class="basket-item">
                                                    <div class="inner-item">
                                                        <div class="thumb">
                                                            <img alt="" src="{{ asset($product->options->featuredImg) }}" />
                                                        </div>
                                                        <div class="title lang-switch" data-en="{{ $product->name}}" data-bn="{{ $product->options->title_bn}}">{{ $product->options->title_bn}}</div>
                                                        <div class="price lang-digit" data-val=" ৳ {{ $product->subtotal}}"> ৳ {{ enToBn($product->subtotal)}}</div>
                                                    </div>
                                                    <a class="close-btn btn-remove-item" data-pid="{{ $product->id }}" ></a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div><!-- /modal-body -->
                                    <div class="modal-footer">
                                        <a href="{{ route('cart') }}" class="cart-view le-button inverse lang-switch" data-en="View cart" data-bn="ব্যাগ দেখুন">ব্যাগ দেখুন</a>
                                        <a href="{{ route('cart') }}" class="clear-cart le-button lang-switch" data-en="Checkout" data-bn="পরিশোধ করুন">পরিশোধ করুন</a>
                                    </div>
                                </div><!-- modal-content -->
                            </div><!-- modal-dialog -->
                        </div><!-- Right modal -->
                    </div><!-- /.basket -->
                </div><!-- /.top-cart-holder -->
            </div><!-- /.top-cart-row-container -->
            <!-- ============================== SHOPPING CART DROPDOWN : END ======================= -->
        </div><!-- /.top-cart-row -->
    </div><!-- /.container -->

    <!-- ========================================= NAVIGATION ========================================= -->

    <div id="main-navigation-warp" class="">
        <div class="container">
            <div class="navigation-inner">
                <div class="cat-nav-wrapper">
                    <a class="cd-dropdown-trigger" href="#0">
                        <i class="fa fa-bars" aria-hidden="true"></i><span class="lang-switch" data-en="Browse Categories" data-bn="ক্যাটাগরি ব্রাউজ করুন">ক্যাটাগরি ব্রাউজ করুন</span>
                    </a>
                    <nav class="cat-nav-warp ">
                        <ul class="cd-dropdown-content">
                            <li><a href=""><i class="fa fa-frown-o" aria-hidden="true"></i>পোশাক</a>
                                <div class="mega-menu-warp">
                                    <div class="mega-container">
                                        <div class="mega-cal-3">
                                            <div class="mega-img-box">
                                                <img src="{{asset('assets/images/menu-product-1-118x118.jpg')}}">
                                            </div>
                                            <h6 class="mega-cal-title">CLOCKS</h6>
                                            <ul class="mega-submenu">
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                            </ul>
                                        </div>
                                        <div class="mega-cal-3">
                                            <div class="mega-img-box">
                                                <img src="{{asset('assets/images/menu-product-1-118x118.jpg')}}">
                                            </div>
                                            <h6 class="mega-cal-title">CLOCKS</h6>
                                            <ul class="mega-submenu">
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                            </ul>
                                        </div>
                                        <div class="mega-cal-3">
                                            <div class="mega-img-box">
                                                <img src="{{asset('assets/images/menu-product-1-118x118.jpg')}}">
                                            </div>
                                            <h6 class="mega-cal-title">CLOCKS</h6>
                                            <ul class="mega-submenu">
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                            </ul>
                                        </div>
                                        <div class="mega-cal-3">
                                            <div class="mega-img-box">
                                                <img src="{{asset('assets/images/menu-product-1-118x118.jpg')}}">
                                            </div>
                                            <h6 class="mega-cal-title">CLOCKS</h6>
                                            <ul class="mega-submenu">
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                                <li class=""><a href="">Mantel Clocks</a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li><a href=""><i class="fa fa-frown-o" aria-hidden="true"></i>Gallery</a></li>
                            <li><a href=""><i class="fa fa-frown-o" aria-hidden="true"></i>Page 1</a></li>
                            <li><a href=""><i class="fa fa-frown-o" aria-hidden="true"></i>Page 2</a></li>
                            <li><a href=""><i class="fa fa-frown-o" aria-hidden="true"></i>Page 3</a></li>
                            <li><a href=""><i class="fa fa-frown-o" aria-hidden="true"></i>Page 3</a></li>
                            <li><a href=""><i class="fa fa-frown-o" aria-hidden="true"></i>Page 3</a></li>
                        </ul> <!-- .cd-dropdown-content -->
                    </nav> <!-- .cd-dropdown -->
                </div> <!-- .cd-dropdown-wrapper -->
                <div class="site-page-nav">
                    <ul class="main-menu">
                        <li><a href="{{route('siteroot')}}" class="lang-switch" data-en="Home" data-bn="নীড়">নীড়</a> </li>
                        <li><a href="{{route('shop')}}" class="lang-switch" data-en="Shop" data-bn="বাজার">বাজার</a></li>
                        <li><a href="{{route('cart')}}" class="lang-switch" data-en="Content" data-bn="কার্ট">কার্ট</a> </li>
                        <li><a href="#" class="lang-switch" data-en="Future" data-bn="অন্যান্য">অন্যান্য</a> </li>
                        <li><a href="#" class="lang-switch" data-en="Faq" data-bn="সচরাচর প্রশ্নসমুহ">সচরাচর প্রশ্নসমুহ</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /.megamenu-vertical -->
    <!-- ============================ NAVIGATION : END ========================== -->
</header>
<!-- ==================== HEADER : END ============================== -->