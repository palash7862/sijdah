@foreach($sections as $section)
<section class="p-categories">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="p-cat-area">
                    <div class="cat-head">
                        <h3 class="lang-switch" data-en="{{ $section->title_en  }}" data-bn="{{ $section->title_bn }}">{{ $section->title_bn }}</h3>
                    </div>

                    <div class="cat-tab">
                        <ul class="nav nav-tabs">
                            @foreach($section->categories as $key => $category)
                            <li @if($key==0) class="active"@endif><a data-toggle="tab" href="#sec_{{$section->id}}_cat_{{$category->id}}" class="lang-switch" data-en="{{$category->name_en}}" data-bn="{{$category->name_bn}}">{{$category->name_bn}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="p-cat-product">
                    <div class="tab-content">

                        @foreach($section->categories as $key => $category)
                        <div id="sec_{{$section->id}}_cat_{{$category->id}}" class="tab-pane fade @if($key==0)in active @endif">
                            <div class="multi-item owl-carousel">

                                @foreach(App\Category::find($category->id)->products as $product)
                                <div class="product-single text-center">
                                    <div class="product-area">
                                        <a href="{{route('product.single', $product->id)}}">
                                        <img src="{{asset(get_image_size('product-thumb',$product->futureImageSrc($product->featured_img_id)))}}" class="img-responsive" alt="">
                                        </a>
                                        <div class="discount"><span class="lang-digit" data-val="{{(int)(($product->dis_price-$product->reg_price)*100/$product->reg_price)}}%">{{ enToBn((int)(($product->dis_price-$product->reg_price)*100/$product->reg_price))}}%</span></div>
                                        <div class="product-overlay">
                                            <div class="cart-button">
                                                <a href="#" data-pid="{{$product->id}}" class="lang-switch btn-add-item" data-bn="ব্যাগে যোগ করুন" data-en="Add to cart">ব্যাগে যোগ করুন</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-details">
                                        <h4 class="lang-switch" data-en="{{ $product->title_en }}" data-bn="{{ $product->title_bn }}">{{ $product->title_bn }}</h4>
                                        <h5 class="lang-switch" data-en="{{$category->name_en}}" data-bn="{{$category->name_bn}}">{{$category->name_bn}}</h5>
                                        <h5><span class="lang-digit" data-val="৳ {{ (int)$product->reg_price }}">৳ {{ enToBn((int)$product->reg_price) }}</span><strong class="lang-digit" data-val="৳ {{ (int)$product->dis_price }}">৳ {{ enToBn((int)$product->dis_price) }}</strong></h5>
                                    </div>
                                </div>
                                @endforeach
                                
                            </div>
                        </div>
                        @endforeach
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endforeach