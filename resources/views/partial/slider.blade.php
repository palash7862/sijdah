<div id="top-banner-and-menu" class="homepage2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-slider owl-carousel">

                    @if(count($sliders))
                    @foreach(json_decode($sliders->first()->description)->title as $key => $title)
                    <div class="single-slide-item">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="slider-test">
                                    <div class="slider-test-inner">
                                        <h1>{{$title}}</h1>
                                        <p>{{ json_decode($sliders->first()->description)->description[$key] }}</p>
                                        <a href="{{route('shop')}}" class="boxed-btn">Shop Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="slider-img-table">
                                    <div class="slider-img-table-cell">
                                        <img class="slider-img" src="{{asset(getImageSizeById(json_decode($sliders->first()->description)->sliderId[$key],'product-thumb'))}}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>
</div><!-- /.homepage2 -->