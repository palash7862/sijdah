<div id="mobile-menu-nav" class="mobile-menu-nav">
    <div class="nav-close"><i class="fa fa-times" aria-hidden="true"></i></div>
    <div class="tab-list">
        <a href="" data-classname="mainnavlist" class="lang-switch" data-en="Main Menu" data-bn="মেনু">মেনু</a>
        <a href="" data-classname="catnavlist" class="lang-switch" data-en="Category" data-bn="ক্যাটাগরি">ক্যাটাগরি</a>
    </div>
    <div class="tab-content">
        <ul class="mainnavlist">
            <li><a href="">Home</a> </li>
            <li><a href="">Home</a> </li>
            <li><a href="">Home</a> </li>
            <li><a href="">Home</a> </li>
            <li><a href="">Home</a> </li>
            <li><a href="">Home</a> </li>
            <li><a href="">Home</a> </li>
            <li><a href="">Home</a> </li>
            <li><a href="">Home</a> </li>
        </ul>
        <ul class="catnavlist">
            <li><a href="">Category</a> </li>
            <li><a href="">Category</a> </li>
            <li><a href="">Category</a> </li>
            <li><a href="">Category</a> </li>
            <li><a href="">Category</a> </li>
            <li><a href="">Category</a> </li>
            <li><a href="">Category</a> </li>
            <li><a href="">Category</a> </li>
            <li><a href="">Category</a> </li>
        </ul>
    </div>
</div>