@extends('layouts.master')

@section('title','Home')

@section('content')

	@if($imgId != 0)
		<a id="test-popup-link" href="{{asset(getImageSizeById($imgId,'full')) }}"></a>
	@endif

	@include('partial.slider')
	@include('partial.popular')
@endsection

@section('styles')
<!-- Magnific Popup core CSS file -->
<link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}">
<style type="text/css">
	/* overlay at start */
.mfp-fade.mfp-bg {
  opacity: 0;

  -webkit-transition: all 0.5s ease-out;
  -moz-transition: all 0.5s ease-out;
  transition: all 0.5s ease-out;
}
/* overlay animate in */
.mfp-fade.mfp-bg.mfp-ready {
  opacity: 0.8;
}
/* overlay animate out */
.mfp-fade.mfp-bg.mfp-removing {
  opacity: 0;
}

/* content at start */
.mfp-fade.mfp-wrap .mfp-content {
  opacity: 0;

  -webkit-transition: all 0.15s ease-out;
  -moz-transition: all 0.15s ease-out;
  transition: all 0.15s ease-out;
}
/* content animate it */
.mfp-fade.mfp-wrap.mfp-ready .mfp-content {
  opacity: 1;
}
/* content animate out */
.mfp-fade.mfp-wrap.mfp-removing .mfp-content {
  opacity: 0;
}
</style><!-- ./Magnific Popup core CSS file -->
@endsection

@section('scripts')
<!-- Magnific Popup core JS file -->
<script src="{{ asset('assets/js/jquery.magnific-popup.js')}}"></script>
<script>
$(document).ready(function() {
  	$('#test-popup-link').magnificPopup({
  	type: 'image',
  	removalDelay: 500,
  	mainClass: 'mfp-fade'
	});

  	setTimeout(function() {
  	    $('#test-popup-link').delay(3000).trigger('click');
  	  }, 5000);
  	
 });
</script>

@endsection


