<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('payment_id')->nullable();
            $table->enum('payment_type', ['cash', 'online']);
            $table->integer('products_price');
            $table->decimal('discount', 8, 2)->default(0.0);
            $table->string('coupon_code')->nullable();
            $table->integer('shipping_cost')->default(0);
            $table->integer('total_price');
            $table->enum('status', ['placed', 'processing', 'shipped', 'delivered', 'cancel']);
            $table->enum('address_type', ['billing', 'shiping']);
            $table->string('ship_parson')->nullable();
            $table->string('ship_phone')->nullable();
            $table->string('ship_city')->nullable();
            $table->string('ship_area')->nullable();
            $table->text('ship_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
