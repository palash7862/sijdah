<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en');
            $table->string('title_bn');
            $table->enum('product_type', ['Book', 'Clothing', 'Food & Groceries']);
            $table->integer('publisher_id')->nullable();
            $table->string('page_number')->nullable();
            $table->string('clothing_type')->nullable();
            $table->integer('weight')->nullable();
            $table->longText('size')->nullable();
            $table->integer('stock')->nullable();
            $table->decimal('reg_price', 8, 2);
            $table->decimal('dis_price', 8, 2)->nullable();
            $table->decimal('dil_price', 8, 2)->nullable();
            $table->integer('featured_img_id')->nullable();
            $table->mediumText('description_en')->nullable();
            $table->mediumText('description_bn')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
