<?php

use Illuminate\Database\Seeder;
use App\Color;

class ColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat1 = new Color();
        $cat1->name = 'Black';
        $cat1->save();

        $cat2 = new Color();
        $cat2->name         = 'White';
        $cat2->save();

        $cat3 = new Color();
        $cat3->name = 'Green';
        $cat3->save();
    }
}
