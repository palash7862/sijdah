var cartContent;
var shipping_charge;
var discount;
var curl = window.location.origin;

var finalEnlishToBanglaNumber={'0':'০','1':'১','2':'২','3':'৩','4':'৪','5':'৫','6':'৬','7':'৭','8':'৮','9':'৯'};
 
String.prototype.getDigitBanglaFromEnglish = function() {
    var retStr = this;
    for (var x in finalEnlishToBanglaNumber) {
         retStr = retStr.replace(new RegExp(x, 'g'), finalEnlishToBanglaNumber[x]);
    }
    return retStr;
};

$(document).ready(function () {
    update_ship_charge();
    get_cart_content();
})

function get_cart_content() {
    $.getJSON(curl + "/cart/all/", function (data) {
        cartContent = data;
    });
}

function update_ship_charge() {
    shipping_charge = $('#input-shipping option:selected').attr('charge');
}


function update_cart(id, action) {

    if (action == 'add') {
        var url = curl + "/cart/add/" + id;
    }
    else if (action == 'increase') {
        var url = curl + "/cart/increase/" + id;
    }
    else if (action == 'remove') {
        var url = curl + "/cart/remove/" + id;
    }
    else if (action == 'reduce') {
        var url = curl + "/cart/reduce/" + id;
    }
    else if (action == 'destroy') {
        var url = curl + "/cart/destroy";
    }

    $.ajax({
        type: "GET",
        dataType: 'JSON',
        url: url,
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('.modal-body-product').empty();
            var cnt = 0;
            var subTotal = 0;
            $.each(data, function (key, val) {
                cnt++;
                subTotal += val.subtotal;

                var curTitle = val.name;
                var curSubtotal = val.subtotal + '';
                if ($('html').attr('lang') == 'bn') {
                    curTitle = val.options.title_bn;
                    curSubtotal = curSubtotal.getDigitBanglaFromEnglish();
                }

                $('.modal-body-product').append(
                    '<div class="basket-item">' +
                    '<div class="row">' +
                    '<div class="col-xs-4 col-sm-4 no-margin text-center">' +
                    '<div class="thumb">' +
                    '<img alt="" src="/' + val.options.featuredImg + '" />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-xs-8 col-sm-8 no-margin">' +
                    '<div class="title lang-switch" data-en="' + val.name + '" data-bn="' + val.options.title_bn + '">' + curTitle + '</div>' +
                    '<div class="price lang-digit" data-val=" ৳ ' + curSubtotal + '"> ৳ ' + curSubtotal + '</div>' +
                    '</div>' +
                    '</div>' +
                    '<a class="close-btn btn-remove-item" data-pid="' + val.id + '"></a>' +
                    '</div>'
                );
            });
            /********** update Header Cart section **************/
            $('#product-count').data('val', cnt);
            $('#cart-subtotal').data('val', subTotal);

            if(cnt==0){
                shipping_charge=0;

            }

            cnt = cnt + '';
            subTotal = subTotal + '';

            if ($('html').attr('lang') == 'bn') {
                cnt = cnt.getDigitBanglaFromEnglish();
                subTotal = subTotal.getDigitBanglaFromEnglish();
            }

            $('#product-count').text(cnt);
            $('#cart-subtotal').text(subTotal);
            //$("#myModal2").modal('show');

            /***** Update Cart Detail Section(only for Cart Page) **********/
            cartContent = data;

            update_cart_content();

        },
        error: function (req, status, err) {
            swal({
                type: 'error',
                title: 'Oops...',
                text: err,
            });
        }
    });
}


function update_cart_content() {
    var data = cartContent;
    $('#items-holder').empty();
    var subTotal = 0;
    $.each(data, function (key, val) {
        subTotal += val.subtotal;

        var curTitle = val.name;
        var subtotal = val.subtotal + '';
        if ($('html').attr('lang') == 'bn') {
            curTitle = val.options.title_bn;
            subtotal = subtotal.getDigitBanglaFromEnglish();
        }

        $('#items-holder').append(
            '<div class="row no-margin cart-item">' +
            '<div class="col-xs-12 col-sm-2 no-margin">' +
            '<a href="#" class="thumb-holder">' +
            '<img class="lazy" alt="" src="/' + val.options.featuredImg + '"/>' +
            '</a>' +
            '</div>' +

            '<div class="col-xs-12 col-sm-5 ">' +
            '<div class="title">' +
            '<a href="#">' + curTitle + '</a>' +
            '</div>' +
            '</div>' +

            '<div class="col-xs-12 col-sm-3 no-margin">' +
            '<div class="quantity">' +
            '<div class="le-quantity">' +
            '<form>' +
            '<a class="minus btn-reduce-item" data-pid="' + val.id + '"></a>' +
            '<input name="quantity" readonly="readonly" type="text" value="' + val.qty + '" />' +
            '<a class="plus btn-increase-item" data-pid="' + val.id + '"></a>' +
            '</form>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12 col-sm-2 no-margin cart-item-close-div">'+
            '<div class="price lang-digit" data-val="৳ '+ subtotal +'">৳ '+ subtotal+
            '</div>'+
            '<a href="#" class="close-btn btn-remove-item" data-pid="'+ val.id+'"><i class="fa fa-times" aria-hidden="true"></i></a>'+
            '</div>'+
            '</div>'
        );
    });

    $('#cart-subtotal-2').data('val', subTotal);
    $('#cart-discount').data('val', 0);
    //on each change discount will 0, No  change to shipping charge

    var cartTotal = (parseInt(subTotal) + parseInt(shipping_charge));
    $('#cart-total').data('val', cartTotal);

    discount = 0;

    subT = subTotal + ''; 
    cartT = cartTotal + '';
    dis = discount + '';
    ship = shipping_charge + '';

    if ($('html').attr('lang') == 'bn') {
        subT = subT.getDigitBanglaFromEnglish();
        ship = ship.getDigitBanglaFromEnglish();
        dis = dis.getDigitBanglaFromEnglish();
        cartT = cartT.getDigitBanglaFromEnglish();
    }

    $('#cart-subtotal-2').text('' + subT);
    $('#cart-shipping').text('' + ship);
    $('#cart-discount').text('' + dis);
    $('#cart-total').text('' + cartT);
}

$('.btn-add-item').live("click", function (e) {
    e.preventDefault();
    var id = $(this).attr("data-pid");
    update_cart(id, 'add');
});

$('.btn-increase-item').live("click", function (e) {
    e.preventDefault();
    var id = $(this).attr("data-pid");
    update_cart(id, 'increase');
});

$('.btn-remove-item').live("click", function (e) {
    e.preventDefault();
    var id = $(this).attr("data-pid");
    update_cart(id, 'remove');
});

$('.btn-reduce-item').live("click", function (e) {
    e.preventDefault();
    var id = $(this).attr("data-pid");
    update_cart(id, 'reduce');
});

$('#input-shipping').on("change", function (e) {
    update_ship_charge();
    update_cart_content();

    var slug = 'ship_dhaka';
    if ($('#input-shipping option:selected').attr('type') != "1")
        slug = 'ship_outside';
    $.ajax({
        type: "GET",
        url: curl + '/setArea/' + slug
    });

});

$('#btn-coupon-code').live("click", function (e) {
    e.preventDefault();
    var code = $('#input-coupon-code').val();

    var url = curl + "/coupon/" + code;

    $.ajax({
        type: "GET",
        dataType: 'JSON',
        url: url,
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            var subTotal = parseInt($('#cart-subtotal').text());

            if (data.type == 'fixed') {
                total = subTotal - parseInt(data.amount) + 50;
                total = Math.max(0, parseInt(total));
                $('#cart-total').text("" + total);
                $('#cart-discount').empty();
                $('#cart-discount').text('-' + data.amount);

                swal({
                    type: 'success',
                    title: 'Coupon has been Applied',
                    text: '',
                });
            }
            else if (data.type == 'percentage') {
                var discount = parseInt(subTotal) * (parseInt(data.amount)) / 100;
                total = parseInt(subTotal) - discount + 50;
                total = Math.max(0, parseInt(total));
                $('#cart-total').text("" + total);
                $('#cart-discount').empty();
                $('#cart-discount').text('-' + discount);

                swal({
                    type: 'success',
                    title: 'Coupon has been Applied',
                    text: '',
                });
            }
            else {
                swal({
                    type: 'warning',
                    title: 'Oops...',
                    text: 'Coupon cannot found or expired!',
                });
            }
        },
        error: function (req, status, err) {
            swal({
                type: 'warning',
                title: 'Oops...',
                text: err,
            });
        }
    });

});


/*
* **************************************************************************************
* **********************************   *************************************************
* **************************************************************************************
*/

(function($) {
var division = [
    {
        "id": "1",
        "name": "Barisal",
        "bn_name": "বরিশাল"
    },
    {
        "id": "2",
        "name": "Chittagong",
        "bn_name": "চট্টগ্রাম"
    },
    {
        "id": "3",
        "name": "Dhaka",
        "bn_name": "ঢাকা"
    },
    {
        "id": "4",
        "name": "Khulna",
        "bn_name": "খুলনা"
    },
    {
        "id": "5",
        "name": "Rajshahi",
        "bn_name": "রাজশাহী"
    },
    {
        "id": "6",
        "name": "Rangpur",
        "bn_name": "রংপুর"
    },
    {
        "id": "7",
        "name": "Sylhet",
        "bn_name": "সিলেট"
    }
];

var distick = [
    {
        "id": "1",
        "division_id": "3",
        "name": "Dhaka",
        "bn_name": "ঢাকা",
    },
    {
        "id": "2",
        "division_id": "3",
        "name": "Faridpur",
        "bn_name": "ফরিদপুর",
    },
    {
        "id": "3",
        "division_id": "3",
        "name": "Gazipur",
        "bn_name": "গাজীপুর",
    },
    {
        "id": "4",
        "division_id": "3",
        "name": "Gopalganj",
        "bn_name": "গোপালগঞ্জ",
    },
    {
        "id": "5",
        "division_id": "3",
        "name": "Jamalpur",
        "bn_name": "জামালপুর",
    },
    {
        "id": "6",
        "division_id": "3",
        "name": "Kishoreganj",
        "bn_name": "কিশোরগঞ্জ"
    },
    {
        "id": "7",
        "division_id": "3",
        "name": "Madaripur",
        "bn_name": "মাদারীপুর"
    },
    {
        "id": "8",
        "division_id": "3",
        "name": "Manikganj",
        "bn_name": "মানিকগঞ্জ"
    },
    {
        "id": "9",
        "division_id": "3",
        "name": "Munshiganj",
        "bn_name": "মুন্সিগঞ্জ"
    },
    {
        "id": "10",
        "division_id": "3",
        "name": "Mymensingh",
        "bn_name": "ময়মনসিং",
    },
    {
        "id": "11",
        "division_id": "3",
        "name": "Narayanganj",
        "bn_name": "নারায়াণগঞ্জ",
    },
    {
        "id": "12",
        "division_id": "3",
        "name": "Narsingdi",
        "bn_name": "নরসিংদী",
    },
    {
        "id": "13",
        "division_id": "3",
        "name": "Netrokona",
        "bn_name": "নেত্রকোনা",
    },
    {
        "id": "14",
        "division_id": "3",
        "name": "Rajbari",
        "bn_name": "রাজবাড়ি",
    },
    {
        "id": "15",
        "division_id": "3",
        "name": "Shariatpur",
        "bn_name": "শরীয়তপুর",
    },
    {
        "id": "16",
        "division_id": "3",
        "name": "Sherpur",
        "bn_name": "শেরপুর",
    },
    {
        "id": "17",
        "division_id": "3",
        "name": "Tangail",
        "bn_name": "টাঙ্গাইল",
    },
    {
        "id": "18",
        "division_id": "5",
        "name": "Bogra",
        "bn_name": "বগুড়া",
    },
    {
        "id": "19",
        "division_id": "5",
        "name": "Joypurhat",
        "bn_name": "জয়পুরহাট",
    },
    {
        "id": "20",
        "division_id": "5",
        "name": "Naogaon",
        "bn_name": "নওগাঁ",
    },
    {
        "id": "21",
        "division_id": "5",
        "name": "Natore",
        "bn_name": "নাটোর",
    },
    {
        "id": "22",
        "division_id": "5",
        "name": "Nawabganj",
        "bn_name": "নবাবগঞ্জ",
    },
    {
        "id": "23",
        "division_id": "5",
        "name": "Pabna",
        "bn_name": "পাবনা",
    },
    {
        "id": "24",
        "division_id": "5",
        "name": "Rajshahi",
        "bn_name": "রাজশাহী",
    },
    {
        "id": "25",
        "division_id": "5",
        "name": "Sirajgonj",
        "bn_name": "সিরাজগঞ্জ",
    },
    {
        "id": "26",
        "division_id": "6",
        "name": "Dinajpur",
        "bn_name": "দিনাজপুর",
    },
    {
        "id": "27",
        "division_id": "6",
        "name": "Gaibandha",
        "bn_name": "গাইবান্ধা",
    },
    {
        "id": "28",
        "division_id": "6",
        "name": "Kurigram",
        "bn_name": "কুড়িগ্রাম",
    },
    {
        "id": "29",
        "division_id": "6",
        "name": "Lalmonirhat",
        "bn_name": "লালমনিরহাট",
    },
    {
        "id": "30",
        "division_id": "6",
        "name": "Nilphamari",
        "bn_name": "নীলফামারী",
    },
    {
        "id": "31",
        "division_id": "6",
        "name": "Panchagarh",
        "bn_name": "পঞ্চগড়",
    },
    {
        "id": "32",
        "division_id": "6",
        "name": "Rangpur",
        "bn_name": "রংপুর",
    },
    {
        "id": "33",
        "division_id": "6",
        "name": "Thakurgaon",
        "bn_name": "ঠাকুরগাঁও",
    },
    {
        "id": "34",
        "division_id": "1",
        "name": "Barguna",
        "bn_name": "বরগুনা",
    },
    {
        "id": "35",
        "division_id": "1",
        "name": "Barisal",
        "bn_name": "বরিশাল",
    },
    {
        "id": "36",
        "division_id": "1",
        "name": "Bhola",
        "bn_name": "ভোলা",
    },
    {
        "id": "37",
        "division_id": "1",
        "name": "Jhalokati",
        "bn_name": "ঝালকাঠি",
    },
    {
        "id": "38",
        "division_id": "1",
        "name": "Patuakhali",
        "bn_name": "পটুয়াখালী",
    },
    {
        "id": "39",
        "division_id": "1",
        "name": "Pirojpur",
        "bn_name": "পিরোজপুর",
    },
    {
        "id": "40",
        "division_id": "2",
        "name": "Bandarban",
        "bn_name": "বান্দরবান",
    },
    {
        "id": "41",
        "division_id": "2",
        "name": "Brahmanbaria",
        "bn_name": "ব্রাহ্মণবাড়িয়া",
    },
    {
        "id": "42",
        "division_id": "2",
        "name": "Chandpur",
        "bn_name": "চাঁদপুর",
    },
    {
        "id": "43",
        "division_id": "2",
        "name": "Chittagong",
        "bn_name": "চট্টগ্রাম",
    },
    {
        "id": "44",
        "division_id": "2",
        "name": "Comilla",
        "bn_name": "কুমিল্লা",
    },
    {
        "id": "45",
        "division_id": "2",
        "name": "Coxs Bazar",
        "bn_name": "কক্স বাজার",
    },
    {
        "id": "46",
        "division_id": "2",
        "name": "Feni",
        "bn_name": "ফেনী",
    },
    {
        "id": "47",
        "division_id": "2",
        "name": "Khagrachari",
        "bn_name": "খাগড়াছড়ি",
    },
    {
        "id": "48",
        "division_id": "2",
        "name": "Lakshmipur",
        "bn_name": "লক্ষ্মীপুর",
    },
    {
        "id": "49",
        "division_id": "2",
        "name": "Noakhali",
        "bn_name": "নোয়াখালী",
    },
    {
        "id": "50",
        "division_id": "2",
        "name": "Rangamati",
        "bn_name": "রাঙ্গামাটি",
    },
    {
        "id": "51",
        "division_id": "7",
        "name": "Habiganj",
        "bn_name": "হবিগঞ্জ",
    },
    {
        "id": "52",
        "division_id": "7",
        "name": "Maulvibazar",
        "bn_name": "মৌলভীবাজার",
    },
    {
        "id": "53",
        "division_id": "7",
        "name": "Sunamganj",
        "bn_name": "সুনামগঞ্জ",
    },
    {
        "id": "54",
        "division_id": "7",
        "name": "Sylhet",
        "bn_name": "সিলেট",
    },
    {
        "id": "55",
        "division_id": "4",
        "name": "Bagerhat",
        "bn_name": "বাগেরহাট",
    },
    {
        "id": "56",
        "division_id": "4",
        "name": "Chuadanga",
        "bn_name": "চুয়াডাঙ্গা",
    },
    {
        "id": "57",
        "division_id": "4",
        "name": "Jessore",
        "bn_name": "যশোর",
    },
    {
        "id": "58",
        "division_id": "4",
        "name": "Jhenaidah",
        "bn_name": "ঝিনাইদহ",
    },
    {
        "id": "59",
        "division_id": "4",
        "name": "Khulna",
        "bn_name": "খুলনা",
    },
    {
        "id": "60",
        "division_id": "4",
        "name": "Kushtia",
        "bn_name": "কুষ্টিয়া",
    },
    {
        "id": "61",
        "division_id": "4",
        "name": "Magura",
        "bn_name": "মাগুরা",
    },
    {
        "id": "62",
        "division_id": "4",
        "name": "Meherpur",
        "bn_name": "মেহেরপুর",
    },
    {
        "id": "63",
        "division_id": "4",
        "name": "Narail",
        "bn_name": "নড়াইল",
    },
    {
        "id": "64",
        "division_id": "4",
        "name": "Satkhira",
        "bn_name": "সাতক্ষীরা",
    }
];

    if($('#selectDivision').length > 0){
        var html = '';
        $.each(division, function (key, value) {
            html +='<option value="'+value.id+'">'+value.name+'</option>';
        });
        $('#selectDivision').append(html);

        $('#selectDivision').on('change', function (){
            var currentDivision = $(this).val();
            var currentDiviName = $("#selectDivision option:selected").text();
            $(this).next('input[name=billingCity]').attr('value', currentDiviName);
            var dHtml = '';
            $.each(distick, function (key, value) {
                if(currentDivision == value.division_id){
                    dHtml +='<option value="'+value.name+'">'+value.name+'</option>';
                }
            });
            $('#selectDistic').empty().append(dHtml);
        });

        $('#selectDistic').on('change', function (){
            var currentame = $("#selectDistic option:selected").text();
            $(this).next('input[name=billingArea]').attr('value', currentame);
        });
    }


    if($('#selectShipDivision').length > 0){
        var html = '';
        $.each(division, function (key, value) {
            html +='<option value="'+value.id+'">'+value.name+'</option>';
        });
        $('#selectShipDivision').append(html);

        $('#selectShipDivision').on('change', function (){
            var currentDivision = $(this).val();
            var currentDiviName = $("#selectShipDivision option:selected").text();
            $(this).next('input[name=shipCity]').attr('value', currentDiviName);
            var dHtml = '';
            $.each(distick, function (key, value) {
                if(currentDivision == value.division_id){
                    dHtml +='<option value="'+value.name+'">'+value.name+'</option>';
                }
            });
            $('#selectShipDistic').empty().append(dHtml);
        });

        $('#selectShipDistic').on('change', function (){
            var currentame = $("#selectShipDistic option:selected").text();
            $(this).next('input[name=shipArea]').attr('value', currentame);
        });
    }



    $(".checkoutForm").on('submit', function(e){
    var address =  true;
    if($('input[name=addressType]').is(':checked')){

        $('.ship-address-block, .billing-address').find('input, select, textarea').each(function (index, value) {
            if( ($.trim($(value).val().toLowerCase())== '') ||
                ($.trim($(value).val().toLowerCase())=='select country') ){
                address = false;
                $(value).addClass('error');
                console.log($(value).val().toLowerCase());
            }else{
                $(value).removeClass('error');
            }
        });
        if(address !== true){
            e.preventDefault();
        }
    }else{
        $('.billing-address').find('input, select, textarea').each(function (index, value) {
            if( ($.trim($(value).val().toLowerCase())== '') ||
                ($.trim($(value).val().toLowerCase())=='select country') ){
                address = false;
                $(value).addClass('error');
                console.log($(value));
            }else{
                $(value).removeClass('error');
            }
        });
        if(address !== true){
            e.preventDefault();
        }
    }
});

})(jQuery);






























