$('#search-results').slideUp();
$('#search-input').keyup(function(){
    var term = $('#search-input').val();

    var host = window.location.hostname;
    var port = window.location.port;
    var protocol = window.location.protocol;

    if(host== "127.0.0.1")
        host += ":"+port;

    var url = protocol + "//" +host + "/search/all/"+term;

    if(term.length == 0) {
        $('#search-results').slideUp();
        $('#search-results').empty();
    }else
    {

        $('#search-results').empty();

        $.ajax({
            type: "GET",
            dataType: 'JSON',
            url: url,
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $('#search-results').slideDown();
                $.each(data,function(key,val){
                    //console.log(data);
                    //alert(val.title_en);
                    var title = val.title_en;
                    var price = val.dis_price + '';
                    if($('html').attr('lang') == 'bn'){
                        title = val.title_bn;
                        price = price.getDigitBanglaFromEnglish();
                    }


                    $('#search-results').append( 
                    '<div class="product-single ">'+ 
                        '<a href="'+ protocol + "//" +host+'/product/'+val.id +'">'+ 
                            '<img src="'+ val.featured_img_id +'" class="" height="42" width="42" alt="">'+ 
                            '<h4 class="lang-switch" data-bn="'+ val.title_bn +'" data-en="' + val.title_en +'">' + title +'</h4>'+
                            '<h5 class="text-danger"> ৳ ' + price +'</h5>'+ 
                        '</a>'+ 
                    '</div>' 
                    );
                });                

            },
            error: function(req, status, err) {
                swal({
                    type: 'error',
                    title:'Oops...',
                    text: err,
                });
            }
        });

    }
});

$(window).click(function(e) {
    if (e.target.id == "search-results" || $(e.target).parents("#search-results").length){
        //do nothing
    }
    else{
        $('#search-results').empty();
        $('#search-results').slideUp();
    }
});