    $('.multi-item').owlCarousel({
        loop:false,
        margin:10,
        items:5,
        dots:false,
        nav:true,
        navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        responsive:{
            0:{
                items:1
            },
            450:{
                items:2,
                nav:true,
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });
    $('.responsive').owlCarousel({
        loop: false,
        margin: 20,
        responsiveClass:true,
        nav:true,
        dots:false,
        navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        responsive:{
            0:{
                items:1,
                nav:true
            },
            450:{
                items:2,
            },
            600:{
                items:3,
            },
            1000:{
                items:3,
            }
        }
    })

    /*Countdown Timer*/
    if($('.offer-time-countdown').length){  
      $('.offer-time-countdown').each(function() {
      var $this = $(this), finalDate = $(this).data('countdown');
      $this.countdown(finalDate, function(event) {
        var $this = $(this).html(event.strftime('' + '<div class="counter-column"><div class="inner"><span class="count">%D</span>Days</div></div> ' + '<div class="counter-column"><div class="inner"><span class="count">%H</span>Hr</div></div>  ' + '<div class="counter-column"><div class="inner"><span class="count">%M</span>Min</div></div>  ' + '<div class="counter-column"><div class="inner"><span class="count">%S</span>Sc</div></div>'));
      });
     });
    }