$(document).ready(function(){
    if($('html').attr('lang') == 'en')
    {
        update_language();
    }

    $('.lang-btn').on("click",function(e){
        e.preventDefault();
        var lang = $(this).data('locale');
        $('html').attr('lang',lang);
        
        $('.lang-switch').each(function(){
            var txt = $(this).data(lang);
            $(this).text(txt);
        });

        transformDigit(lang);


        var host = window.location.hostname;
        var port = window.location.port;
        var protocol = window.location.protocol;

        if(host== "127.0.0.1")
            host += ":"+port;

        var url = protocol + "//" +host + "/setLanguage/"+lang;

        $.ajax({
            type: "GET",
            url: url
        });
    });

    function transformDigit(lang){
        var finalEnlishToBanglaNumber={'0':'০','1':'১','2':'২','3':'৩','4':'৪','5':'৫','6':'৬','7':'৭','8':'৮','9':'৯'};
         
        String.prototype.getDigitBanglaFromEnglish = function() {
            var retStr = this;
            for (var x in finalEnlishToBanglaNumber) {
                 retStr = retStr.replace(new RegExp(x, 'g'), finalEnlishToBanglaNumber[x]);
            }
            return retStr;
        };

        $('.lang-digit').each(function(){
            var txt = $(this).data('val')+'';
            if(lang == 'bn')
                $(this).text( txt.getDigitBanglaFromEnglish() );
            else
                $(this).text(txt);
        });
    }

    function update_language()
    {
        lang = $('html').attr('lang');
        $('.lang-switch').each(function(){
            var txt = $(this).data(lang);
            $(this).text(txt);
        });
        transformDigit(lang);
    }
});




