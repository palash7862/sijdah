var finalEnlishToBanglaNumber={'0':'০','1':'১','2':'২','3':'৩','4':'৪','5':'৫','6':'৬','7':'৭','8':'৮','9':'৯'};
 
String.prototype.getDigitBanglaFromEnglish = function() {
    var retStr = this;
    for (var x in finalEnlishToBanglaNumber) {
         retStr = retStr.replace(new RegExp(x, 'g'), finalEnlishToBanglaNumber[x]);
    }
    return retStr;
};


function update_cart(id,action)
{
    var curl = window.location.origin;

    if(action=='add'){
        var url = curl + "/cart/add/"+ id;
    }
    else if(action=='remove'){
        var url = curl + "/cart/remove/"+ id;
    }
    else if(action == 'destroy'){
        var url = curl + "/cart/destroy";
    }

    $.ajax({
        type: "GET",
        dataType: 'JSON',
        url: url,
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('.modal-body-product').empty();
            var cnt = 0;
            var subTotal = 0;
            $.each(data,function(key,val){
                cnt++;
                subTotal += val.subtotal;

                var curTitle = val.name;
                var curSubtotal = val.subtotal + '';
                if($('html').attr('lang') == 'bn')
                {
                    curTitle= val.options.title_bn;
                    curSubtotal = curSubtotal.getDigitBanglaFromEnglish();
                }

                $('.modal-body-product').append(
                '<div class="basket-item">'+
                    '<div class="inner-item">'+
                        '<div class="thumb">'+
                            '<img alt="" src="/'+ val.options.featuredImg +'" />'+
                        '</div>'+
                        '<div class="title lang-switch" data-en="'+val.name +'" data-bn="'+ val.options.title_bn+'">'+curTitle +'</div>'+
                        '<div class="price lang-digit" data-val=" ৳ '+ curSubtotal +'"> ৳ '+ curSubtotal +'</div>'+
                    '</div>'+
                    '<a class="close-btn btn-remove-item" data-pid="'+ val.id +'"></a>'+
                '</div>'
                );
            });

            $('#product-count').data('val',cnt);
            $('#cart-subtotal').data('val',subTotal);

            cnt = cnt + '';
            subTotal = subTotal + '';

            if($('html').attr('lang') == 'bn')
            {
                cnt= cnt.getDigitBanglaFromEnglish();
                subTotal = subTotal.getDigitBanglaFromEnglish();
            }

            $('#product-count').text(cnt);
            $('#cart-subtotal').text(subTotal);
            $("#myModal2").modal('show');

        },
        error: function(req, status, err) {
            swal({
                type: 'error',
                title:'Oops...',
                text: err,
            });
        }
    });
}

$('.btn-add-item').live("click",function(e){
    e.preventDefault();
    var id = $(this).attr("data-pid");
    update_cart(id,'add');
});

$('.btn-remove-item').live("click",function(e){
    e.preventDefault();
    var id = $(this).attr("data-pid");
    update_cart(id,'remove');
});