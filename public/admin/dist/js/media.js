Dropzone.autoDiscover = false;
$(document).ready(function () {
    var currentElement = null;
    $(".add-page-image, .add-image").on('click', function (event) {
        event.preventDefault();
        currentElement = $(this);
        var fream = $('.media-fream-warp');
        fream.slideDown();
        $("body").addClass("overflow-hidden");
        var getMediaUrl = fream.data("url");
        $.ajax({
            url: getMediaUrl, //this is your uri
            type: 'get', //this is your method
            dataType: 'json',
            success: function (response) {
                fream.find(".media-all").empty();
                var html = "";
                for (var i = 0; i < response.data.length; i++) {
                    var id = response.data[i].id;
                    html += '<div class="single-media" data-id="' + id + '"><img  src="' + response.data[i].source + '" alt="" ></div>';
                }
                fream.find(".media-all").append(html);
            }
        });
    });
    $(".media-all").find(".single-media").live('click', function () {
        $(this).addClass("active").siblings().removeClass("active");
    });
    $(".media-fream-warp").find(".media-insert").live('click', function () {
        var mediaWarp = $(this).parents(".footer").prev(".media-all");
        var mediaId = mediaWarp.find(".single-media.active").data('id');
        var mediasrc = mediaWarp.find(".single-media.active").children("img").attr('src');
        if(currentElement.hasClass('add-image')){
            currentElement.parents('.form-group').find('.post-future-image').empty();
            currentElement.parents('.form-group')
                .find('.post-future-image')
                .append('<img src="'+mediasrc+'"><input type="hidden" name="featured_img_id" value="' + mediaId + '">');
        }else {
            //if ( (mediaId > 0) && (mediasrc.length > 0)){
            $(".post-thumb-field > table")
                .find("tbody")
                .append('<tr><td><img src="' + mediasrc + '"><input type="hidden" name="product_media[]" value="' + mediaId + '"></td><td><button class="porduct-media">Remove</button></td></tr>');
        }
        $(".media-fream-warp").slideUp();
        $("body").removeClass("overflow-hidden");
        $(this).unbind("click");
    });
    $(".post-thumb-field > table").find(".porduct-media").live('click', function (event) {
        event.preventDefault();
        $(this).parents("tr").remove();
    });
    $(".media-fream-warp .media-close-shadow").live('click', function () {
        $(this).parent('.media-fream-warp').slideUp();
        $("body").removeClass("overflow-hidden");
        $(".media-fream-warp").find(".media-insert").unbind("click");
    });

});