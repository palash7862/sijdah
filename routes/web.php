<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/json', 'ProfileController@division');

Route::get('/', [
    'as'            => 'siteroot',
    'uses'          => 'HomeController@index'
]);

Route::get('/shop', [
    'as'            => 'shop',
    'uses'          => 'HomeController@shop'
]);
Route::get('/category/{id}', [
    'as'            => 'category.shop',
    'uses'          => 'HomeController@categoryShop'
]);

Route::get('/writer/{id}', [
    'as'            => 'writer.shop',
    'uses'          => 'HomeController@writerShop'
]);

Route::get('/publisher/{id}', [
    'as'            => 'publisher.shop',
    'uses'          => 'HomeController@publisherShop'
]);

Route::post('/comment', [
    'as'            => 'book.comment',
    'middleware'    => 'roles',
    'roles'         => ['Admin', 'Basic', 'Staff', 'Diller' ],
    'uses'          => 'HomeController@comment'
]);

Route::get('/product/{id}', [
    'as'            => 'product.single',
    'uses'          => 'HomeController@singleProduct'
]);


Route::get('/wishlist', function () {
    return view('wishlist');
});
Route::get('/order-history', function () {
    return view('order-history');
});
Route::get('/track-order', function () {
    return view('track-order');
});


/*
 ****************************************
 *   Route for Cart
 *****************************************
 */

Route::get('cart/add/{id}','CartController@addProduct');
Route::get('cart/increase/{id}','CartController@increaseProduct');
Route::get('cart/remove/{id}','CartController@removeProduct');
Route::get('cart/reduce/{id}','CartController@reduceProduct');
Route::get('cart/destroy','CartController@destroy');
Route::get('cart/all','CartController@all');


/*
 ****************************************
 *   Route for searching
 *****************************************
 */
Route::get('search','SearchController@filter');
Route::get('search/all/{term}','SearchController@findAll');
Route::get('search/{cat}/{term}','SearchController@findInCategory');





/*
 * **********************************************
 *      Route Order Relatiead
 * ********************************************
 */
//Route::prefix('order')->group( function () {
Route::group(array('prefix' => 'order' ), function () {
    Route::get('/', [
        'as'            => 'cart',
        'uses'          => 'Admin\OrderController@cartProcess'
    ]);

    Route::post('/', [
        'as'            => 'cart.address',
        'middleware'    => 'roles',
        'roles'         => ['Admin', 'Basic', 'Staff', 'Diller' ],
        'uses'          => 'Admin\OrderController@addressProcess'
    ]);

    Route::get('/checkout', [
        'as'            => 'checkout',
        'middleware'    => 'roles',
        'roles'         => ['Admin', 'Basic', 'Staff', 'Diller' ],
        'uses'          => 'Admin\OrderController@checkout'
    ]);

    Route::post('/process', [
        'as'            => 'order.create',
        'middleware'    => 'roles',
        'roles'         => ['Admin', 'Basic', 'Staff', 'Diller' ],
        'uses'          => 'Admin\OrderController@orderProcess'
    ]);
});

/*
 * **********************************************
 *      Route Start For Payment Relatiead
 * ********************************************
 */
Route::group(array('prefix' => 'payment' ), function () {
    Route::post('sslvalidation', [
        'as'            => 'pay.val.front',
        'uses'          => 'Admin\OrderController@sslValidation'
    ]);
    Route::post('sslfaild', [
        'as'            => 'pay.faild.front',
        'uses'          => 'Admin\OrderController@sslFaild'
    ]);
    Route::post('sslcancel', [
        'as'            => 'pay.cancel.front',
        'uses'          => 'Admin\OrderController@sslCancel'
    ]);
});


/*
 * **********************************************
 *      Route Admin Relatiead
 * ********************************************
 */
Route::prefix('adminpanel')->group( function () {
//Route::group(['prefix' => 'adminpanel', 'as' => 'dashboard' ], function () {

    Route::get('/', [
        'as'            => 'dashboard',
        'middleware'    => 'roles',
        'roles'         => ['Admin'],
        'uses'          => 'Admin\UserController@dashboard'
    ]);

    Route::prefix('product')->group( function () {
    //Route::group(['prefix' => 'product'], function () {
        /*
         * **********************************************
         *      Route For Product
         * ********************************************
         */
        Route::get('/all', [
            'as'            => 'product.all',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ProductController@gatAll'
        ]);
        Route::get('/productcreate', [
            'as'            => 'product.create',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ProductController@create'
        ]);
        Route::post('/productcreate', [
            'as'            => 'product.create',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ProductController@store'
        ]);
        Route::get('/productedit/{id}', [
            'as'            => 'product.edit',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ProductController@edit'
        ]);
        Route::post('/productupdate', [
            'as'            => 'product.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ProductController@update'
        ]);
        Route::get('/productdelete/{id}', [
            'as'            => 'product.delete',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ProductController@delete'
        ]);

        /*
         * **********************************************
         *      Route For Writer
         * ********************************************
         */
        Route::get('/writer', [
            'as'            => 'writer.all',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\WritersController@getAll'
        ]);
        Route::get('/writeredit/{id}', [
            'as'            => 'writer.edit',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\WritersController@edit'
        ]);
        Route::post('/writercreate', [
            'as'            => 'writer.create',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\WritersController@create'
        ]);
        Route::post('/writerupdate', [
            'as'            => 'writer.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\WritersController@update'
        ]);
        Route::get('/writerdelete/{id}', [
            'as'            => 'writer.delete',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\WritersController@delete'
        ]);

        /*
         * ********************************************
         *      Route For Publisher
         * ********************************************
         */
        Route::get('/publisher', [
            'as'            => 'publisher.all',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\PublisherController@getAll'
        ]);
        Route::get('/publisheredit/{id}', [
            'as'            => 'publisher.edit',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\PublisherController@edit'
        ]);
        Route::post('/publishercreate', [
            'as'            => 'publisher.create',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\PublisherController@create'
        ]);
        Route::post('/publisherupdate', [
            'as'            => 'publisher.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\PublisherController@update'
        ]);
        Route::get('/publisherdelete/{id}', [
            'as'            => 'publisher.delete',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\PublisherController@delete'
        ]);

        /*
         * **********************************************
         *      Route For Category
         * ********************************************
         */
        Route::get('/category', [
            'as'            => 'cats.all',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\CategoryController@gatAll'
        ]);
        Route::get('/categoryedit/{id}', [
            'as'            => 'cats.edit',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\CategoryController@edit'
        ]);
        Route::post('/categorycreate', [
            'as'            => 'cats.create',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\CategoryController@create'
        ]);
        Route::post('/categoryupdate', [
            'as'            => 'cats.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\CategoryController@update'
        ]);
        Route::get('/categorydelete/{id}', [
            'as'            => 'cats.delete',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\CategoryController@delete'
        ]);

        /*
         * **********************************************
         *      Route For Color
         * ********************************************
         */
        Route::get('/color', [
            'as'            => 'color.all',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ColorController@gatAll'
        ]);
        Route::get('/coloredit/{id}', [
            'as'            => 'color.edit',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ColorController@edit'
        ]);
        Route::post('/colorcreate', [
            'as'            => 'color.create',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ColorController@create'
        ]);
        Route::post('/colorupdate', [
            'as'            => 'color.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ColorController@update'
        ]);
        Route::get('/colordelete/{id}', [
            'as'            => 'color.delete',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ColorController@delete'
        ]);
    });


    /*
	 * **********************************************
	 *      Route For page model
	 * ********************************************
	 */
	Route::group(array('prefix' => 'page' ), function () {
		Route::get('/allpage', [
			'as'            => 'all.page',
			'middleware'    => 'roles',
			'roles'         => ['Admin'],
			'uses'          => 'Admin\PageController@getAll'
		]);

		Route::get('/createpage', [
			'as'            => 'create.page',
			'middleware'    => 'roles',
			'roles'         => ['Admin'],
			'uses'          => 'Admin\PageController@create'
		]);

		Route::post('/store', [
			'as'            => 'store.page',
			'middleware'    => 'roles',
			'roles'         => ['Admin'],
			'uses'          => 'Admin\PageController@store'
		]);

		Route::get('/editpage/{id}', [
			'as'            => 'edit.page',
			'middleware'    => 'roles',
			'roles'         => ['Admin'],
			'uses'          => 'Admin\PageController@edit'
		]);

		Route::post('/update', [
			'as'            => 'update.page',
			'middleware'    => 'roles',
			'roles'         => ['Admin'],
			'uses'          => 'Admin\PageController@update'
		]);


		Route::get('/pagedelete/{id}', [
			'as'            => 'page.delete',
			'middleware'    => 'roles',
			'roles'         => ['Admin'],
			'uses'          => 'Admin\PageController@delete'
		]); 
	});

    /*
     * **********************************************
     *      Route For User Model
     * ********************************************
     */
    Route::group(array('prefix' => 'user' ), function () {
        Route::get('/', [
            'as'            => 'user.all',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\UserController@gatAll'
        ]);

        Route::get('userAdd', [
            'as'            => 'user.add',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\UserController@add'
        ]);

        Route::post('userCreate', [
            'as'            => 'user.create',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\UserController@create'
        ]);

        Route::get('userEdit/{id}', [
            'as'   => 'user.edit',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses' => 'Admin\UserController@edit'
        ]);

        Route::post('userUpdate', [
            'as'   => 'user.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses' => 'Admin\UserController@update'
        ]);

        Route::get('userDelete/{id}', [
            'as'   => 'user.delete',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses' => 'Admin\UserController@delete'
        ]);
    }); 
    
	/*
     * **********************************************
     *      Route For Home Sections
     * ********************************************
     */
    Route::group(array('prefix' => 'sections' ), function () { 
        Route::get('/', [
            'as'            => 'section.all',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SectionController@index'
        ]);
        Route::get('/edit/{id}', [
            'as'            => 'section.edit',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SectionController@edit'
        ]);
        Route::post('/create', [
            'as'            => 'section.create',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SectionController@create'
        ]);
        Route::post('/update', [
            'as'            => 'section.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SectionController@update'
        ]);
        Route::get('/delete/{id}', [
            'as'            => 'section.delete',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SectionController@delete'
        ]);
    });


    /********** Coupon Route ***************/
    Route::group(array('prefix' => 'coupon' ), function () {

        Route::get('/', [
            'as'            => 'coupon.all',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\CouponController@index'
        ]);
        Route::get('/edit/{id}', [
            'as'            => 'coupon.edit',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\CouponController@edit'
        ]);
        Route::post('/create', [
            'as'            => 'coupon.create',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\CouponController@store'
        ]);
        Route::post('/update', [
            'as'            => 'coupon.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\CouponController@update'
        ]);
        Route::get('/delete/{id}', [
            'as'            => 'coupon.delete',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\CouponController@destroy'
        ]);
    });

	/*
     * **********************************************
     *      Route For Media Model
     * ********************************************
     */
    Route::group(array('prefix' => 'media' ), function () {
        Route::get('/', array(
            'as'            => 'upload',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\MediaController@view'
        ));

        Route::get('/delete/{id}', array(
            'as'            => 'upload.delete',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\MediaController@delete'
        ));

        Route::post('/upload', array(
            'as'            => 'upload.add',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\MediaController@upload'
        ));

        Route::get('/getall', array(
            'as'            => 'media.all',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\MediaController@getAll'
        ));
    });

    Route::group(array('prefix' => 'orders' ), function () {
        Route::get('/', [
            'as'            => 'orders.placed',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\OrderController@gatAll'
        ]);

        Route::get('/processing', [
            'as' => 'order.processing',
            'middleware' => 'roles',
            'roles' => ['Admin'],
            'uses' => 'Admin\OrderController@getOrderProcessing'
        ]);

        Route::get('/shipped', [
            'as' => 'order.shipped',
            'middleware' => 'roles',
            'roles' => ['Admin'],
            'uses' => 'Admin\OrderController@getOrderShipped'
        ]);

        Route::get('/delivered', [
            'as' => 'order.delivered',
            'middleware' => 'roles',
            'roles' => ['Admin'],
            'uses' => 'Admin\OrderController@getOrderDelivered'
        ]);

        Route::get('/cancel', [
            'as' => 'order.cancel',
            'middleware' => 'roles',
            'roles' => ['Admin'],
            'uses' => 'Admin\OrderController@getOrderCancel'
        ]);

        Route::get('/Order/{id}', [
            'as'            => 'order.single',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\OrderController@gatSingleOrder'
        ]);

        Route::get('/invoice/{id}', [
            'as'            => 'order.invoice',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\OrderController@exportInvoice'
        ]);



        /*---------------------- Update Status ---------------------*/
        Route::get('processingup/{id}', [
            'as' => 'order.process.up',
            'middleware' => 'roles',
            'roles' => ['Admin'],
            'uses' => 'Admin\OrderController@orderProcessing'
        ]);

        Route::get('shipped/{id}', [
            'as' => 'order.shipped.up',
            'middleware' => 'roles',
            'roles' => ['Admin'],
            'uses' => 'Admin\OrderController@orderShipped'
        ]);

        Route::get('delivered/{id}', [
            'as' => 'order.delivered.up',
            'middleware' => 'roles',
            'roles' => ['Admin'],
            'uses' => 'Admin\OrderController@orderDelivered'
        ]);

        Route::get('cancel/{id}', [
            'as' => 'order.cancel.up',
            'middleware' => 'roles',
            'roles' => ['Admin'],
            'uses' => 'Admin\OrderController@orderCancel'
        ]);

    });


    Route::group(array('prefix' => 'report' ), function () {
        Route::get('/writer', [
            'as'            => 'writer.list',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ReportController@writerAll'
        ]);

        Route::get('/writerbooksales/{id}', [
            'as'            => 'writer.sales',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ReportController@writerSales'
        ]);

        Route::get('/writerbooksalesexport/{id}', [
            'as'            => 'writer.sales.export',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ReportController@writerSalesExport'
        ]);

        Route::get('/publishers', [
            'as'            => 'publisher.list',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ReportController@publisherAll'
        ]);

        Route::get('/publisherbooksales/{id}', [
            'as'            => 'publisher.sales',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ReportController@publisherSales'
        ]);

        Route::get('/publisherbooksalesexport/{id}', [
            'as'            => 'publisher.sales.export',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ReportController@publisherSalesExport'
        ]);

        Route::get('/datesales/', [
            'as'            => 'date.view',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ReportController@dateSales'
        ]);

        Route::post('/datesales/', [
            'as'            => 'date.sales',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ReportController@dateSalesResult'
        ]);

        Route::get('/datesalesexport/{newdate}/{olddate}', [
            'as'            => 'date.sales.export',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\ReportController@dateSalesExport'
        ]);
    });

    /*
     * **********************************************
     *      Route For Settings
     * ********************************************
     */
    Route::group(array('prefix' => 'settings' ), function () {
        Route::get('/general', array(
            'as'            => 'general.index',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SettingsController@general'
        ));
        Route::post('/general', array(
            'as'            => 'general.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SettingsController@generalUpdate'
        ));
        Route::get('/shipping', array(
            'as'            => 'shipping.index',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SettingsController@shipping'
        ));
        Route::post('/shipping', array(
            'as'            => 'shipping.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SettingsController@shippingUpdate'
        ));
        Route::get('/contact', array(
            'as'            => 'contact.index',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SettingsController@contact'
        ));
        Route::post('/contact', array(
            'as'            => 'contact.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SettingsController@contactUpdate'
        ));
        Route::get('/footerWidget', array(
            'as'            => 'footerWidget.index',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SettingsController@footerWidget'
        ));
        Route::post('/footerWidget', array(
            'as'            => 'footerWidget.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SettingsController@footerWidgetUpdate'
        ));
        Route::get('/socialProfile', array(
            'as'            => 'socialProfile.index',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SettingsController@socialProfile'
        ));
        Route::post('/socialProfile', array(
            'as'            => 'socialProfile.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SettingsController@socialProfileUpdate'
        ));
    });

    /****************** Subscribers **************/
    Route::get('/subscriber', [
        'as'            => 'subscriber.index',
        'middleware'    => 'roles',
        'roles'         => ['Admin'],
        'uses'          => 'Admin\SubscriberController@index'
    ]);
    Route::get('/subscriber/delete/{id}', [
            'as'            => 'subscriber.delete',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SubscriberController@delete'
    ]);


    /**************** Promotions *******************/
    Route::resource('/promotion','Admin\PromoController');

    /**********   Slider ******************/
    Route::get('/slider', [
        'as'            => 'slider.index',
        'middleware'    => 'roles',
        'roles'         => ['Admin'],
        'uses'          => 'Admin\SliderController@index'
    ]);
    Route::post('/slider', [
            'as'            => 'slider.update',
            'middleware'    => 'roles',
            'roles'         => ['Admin'],
            'uses'          => 'Admin\SliderController@update'
    ]);


    /********* Ajax ***********************/  
    Route::prefix('ajax')->group( function () {
        Route::post('/productthumb', array(
            'as'            => 'upload.product.thumb',
            'uses'          => 'Admin\MediaController@uploadProductThumb'
        ));
        Route::prefix('order')->group( function () {

        });
    });

});


/*
 * **********************************************
 *      Route User Profile Related
 * ********************************************
 */
Route::group(array('prefix' => 'user' ), function () {

    Route::get('profile', [
        'as'            => 'user.profile',
        'middleware'    => 'roles',
        'roles'         => ['basic', 'Admin'],
        'uses'          => 'ProfileController@userProfile'
    ]);

    Route::get('editprofile', [
        'as'            => 'edit.profile',
        'middleware'    => 'roles',
        'roles'         => ['basic', 'Admin'],
        'uses'          => 'ProfileController@editProfile'
    ]);

    Route::post('updateprofile', [
        'as'            => 'update.profile',
        'middleware'    => 'roles',
        'roles'         => ['basic', 'Admin'],
        'uses'          => 'ProfileController@updateprofile'
    ]);

    Route::get('changepassword', [
        'as'            => 'change.password',
        'middleware'    => 'roles',
        'roles'         => ['basic', 'Admin'],
        'uses'          => 'ProfileController@changepassword'
    ]);

    Route::post('updatepassword', [
        'as'            => 'update.password',
        'middleware'    => 'roles',
        'roles'         => ['basic', 'Admin'],
        'uses'          => 'ProfileController@upatepassword'
    ]);

    Route::get('orders', [
        'as'            => 'user.orders',
        'middleware'    => 'roles',
        'roles'         => ['basic', 'Admin'],
        'uses'          => 'ProfileController@orders'
    ]);

    Route::get('order/{id}', [
        'as'            => 'user.order.detail',
        'middleware'    => 'roles',
        'roles'         => ['basic', 'Admin'],
        'uses'          => 'ProfileController@orderDetail'
    ]);

});

/************** Subscription Route **********/
Route::post('/subscribe',['as'=>'subscriber.create','uses'=>'Admin\SubscriberController@subscribe']);


/**********Custom 404 Template****************/
Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);

Route::get('/coupon/{code}','Admin\CouponController@search');



Route::get('auth/facebook', [
    'as'    => 'fb.login',
    'uses'  => 'Auth\SocialAuthController@fbRedirectToProvider'
]);
Route::get('auth/facebook/callback', [
    'as'    => 'fb.login.callback',
    'uses'  => 'Auth\SocialAuthController@fbHandleProviderCallback'
]);

Route::get('auth/google', [
    'as'    => 'google.login',
    'uses'  => 'Auth\SocialAuthController@googleRedirectToProvider'
]);
Route::get('auth/google/callback', [
    'as'    => 'google.login.callback',
    'uses'  => 'Auth\SocialAuthController@googleHandleProviderCallback'
]);

/*********** Set Coockie for ************************/
Route::get('/setLanguage/{lang}','CookieController@setLanguage');
Route::get('/setArea/{area}','CookieController@setArea');

Auth::routes();

Route::get('test',function(){
    return '<html><head><style src="../../public_html/aa.css"></style></head></html>';
});

//Route::get('/home', 'HomeController@index')->name('home');
/*
Route::get('/palash', function (){

//    $writerByProductSell = DB::table('writers')
//                ->select('writers.name', 'products.id', 'products.title_en',
//                    DB::raw('SUM(orders_details.qty) as total_sales'),
//                    DB::raw('SUM(orders_details.unit_price) as total_amount'))
//                ->leftJoin('product_writers', 'product_writers.writer_id',  '=', 'writers.id')
//                ->leftJoin('products', 'products.id', '=', 'product_writers.product_id')
//                ->leftJoin('orders_details', 'orders_details.product_id', '=', 'products.id')
//                ->groupBy('products.id')
//                ->where('writers.id', 4)
//                ->get();
//    dd($writerByProductSell);

//    $PublisherByProductSell = DB::table('publishers')
//        ->select('publishers.name', 'products.id', 'products.title_en', DB::raw('SUM(orders_details.qty) as total_sales'))
//        ->leftJoin('product_publishers', 'product_publishers.publisher_id',  '=', 'publishers.id')
//        ->leftJoin('products', 'products.id', '=', 'product_publishers.product_id')
//        ->leftJoin('orders_details', 'orders_details.product_id', '=', 'products.id')
//        ->groupBy('products.id')
//        ->where('publishers.id', 4)
//        ->get();
//    dd($PublisherByProductSell);

//    $date = new DateTime('2018-01-24');
//    $date->sub(new DateInterval('P7D'));
//    echo $date->format('Y-m-d') . "\n";
//    echo  '<pre>';
//
//    $nowDate = Carbon\Carbon::now();
//    $oldDate = Carbon\Carbon::now()->subDays(2)->toDateString();
//
//    $PublisherByProductSell = DB::table('products')
//        ->select('products.title_en',
//            DB::raw('SUM(orders_details.qty) as total_sales'))
//        ->leftJoin('orders_details', 'orders_details.product_id',  '=', 'products.id')
//        ->groupBy('products.id')
//        //->whereBetween('orders_details.created_at', ['2018-02-22', '2018-02-25'])
//        ->whereBetween('orders_details.created_at', [$oldDate, $nowDate])
//        ->get();
//    dd($PublisherByProductSell);
});
    $writerByProductSell = DB::table('writers')
                ->select('writers.name', 'products.id', 'products.title_en',
                    DB::raw('SUM(orders_details.qty) as total_sales'),
                    DB::raw('SUM(orders_details.unit_price) as total_amount'))
                ->leftJoin('product_writers', 'product_writers.writer_id',  '=', 'writers.id')
                ->leftJoin('products', 'products.id', '=', 'product_writers.product_id')
                ->leftJoin('orders_details', 'orders_details.product_id', '=', 'products.id')
                ->groupBy('products.id')
                ->where('writers.id', 4)
                ->get();
    dd($writerByProductSell);

    $PublisherByProductSell = DB::table('publishers')
        ->select('publishers.name', 'products.id', 'products.title_en',
                DB::raw('SUM(orders_details.qty) as total_sales'))
        ->leftJoin('product_publishers', 'product_publishers.publisher_id',  '=', 'publishers.id')
        ->leftJoin('products', 'products.id', '=', 'product_publishers.product_id')
        ->leftJoin('orders_details', 'orders_details.product_id', '=', 'products.id')
        ->groupBy('products.id')
        ->where('publishers.id', 4)
        ->get();
    dd($PublisherByProductSell);

    $date = new DateTime('2018-01-24');
    $date->sub(new DateInterval('P7D'));
    echo $date->format('Y-m-d') . "\n";
    echo  '<pre>';

    $nowDate = Carbon\Carbon::now();
    $oldDate = Carbon\Carbon::now()->subDays(2)->toDateString();

    $PublisherByProductSell = DB::table('products')
        ->select('products.title_en',
            DB::raw('SUM(orders_details.qty) as total_sales'))
        ->leftJoin('orders_details', 'orders_details.product_id',  '=', 'products.id')
        ->groupBy('products.id')
        //->whereBetween('orders_details.created_at', ['2018-02-22', '2018-02-25'])
        ->whereBetween('orders_details.created_at', [$oldDate, $nowDate])
        ->get();
    dd($PublisherByProductSell);
});

*/
