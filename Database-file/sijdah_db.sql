-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2018 at 06:39 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sijdah_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_bn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name_en`, `name_bn`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Select Parent', 'Category  one', 'Category  one', NULL, '2018-02-20 15:21:20', '2018-02-20 15:21:20'),
(2, 'Select Parent', 'Category  two', 'Category  two', NULL, '2018-02-20 15:21:37', '2018-02-20 15:21:37'),
(7, '', 'Category  Three', 'Category  Three', NULL, '2018-02-20 16:48:04', '2018-02-20 16:48:04'),
(8, '', 'Category  four', 'Category  four', NULL, '2018-02-20 16:48:13', '2018-02-20 16:48:13'),
(9, '', 'Category  five', 'Category  five', NULL, '2018-02-20 16:48:26', '2018-02-20 16:48:26');

-- --------------------------------------------------------

--
-- Table structure for table `category_media`
--

CREATE TABLE `category_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `color_code`, `created_at`, `updated_at`) VALUES
(1, 'Black', '#000', '2018-02-15 21:54:45', '2018-02-15 21:55:42'),
(2, 'White', '#FFFFFF', '2018-02-15 21:55:12', '2018-02-15 21:55:12');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `massage` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `couponcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('percentage','fixed') COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `expired_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `title`, `couponcode`, `type`, `amount`, `expired_date`, `created_at`, `updated_at`) VALUES
(1, 'gift coupon', 'palash', 'fixed', '1000.00', '2018-02-28', '2018-02-21 10:28:21', '2018-02-21 10:28:21');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `source` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `title`, `description`, `source`, `created_at`, `updated_at`) VALUES
(13, '8c519d71-cb8c-4ed8-b0e2-874e41a2d3ab-original', '8c519d71-cb8c-4ed8-b0e2-874e41a2d3ab-original', 'storage/2018/02/151898426917092.png', '2018-02-18 20:04:29', '2018-02-18 20:04:29'),
(14, '25511689_23843019889300302_4135314771673088000_n', '25511689_23843019889300302_4135314771673088000_n', 'storage/2018/02/151898426966944.png', '2018-02-18 20:04:30', '2018-02-18 20:04:30'),
(17, 'blog-pos', 'blog-pos', 'storage/2018/02/151898792877705.jpeg', '2018-02-18 21:05:28', '2018-02-18 21:05:28'),
(18, '35144_10650505_1969296_376a9d78_imag', '35144_10650505_1969296_376a9d78_imag', 'storage/2018/02/151913794175736.jpeg', '2018-02-20 14:45:42', '2018-02-20 14:45:42'),
(19, '462574-1i2ytK146764246', '462574-1i2ytK146764246', 'storage/2018/02/151913794276273.jpeg', '2018-02-20 14:45:42', '2018-02-20 14:45:42'),
(20, 'a-cover', 'a-cover', 'storage/2018/02/151913794285004.jpeg', '2018-02-20 14:45:43', '2018-02-20 14:45:43'),
(21, 'downloa', 'downloa', 'storage/2018/02/151913794428456.jpeg', '2018-02-20 14:45:44', '2018-02-20 14:45:44'),
(22, 'Enchantment-Book-Cover-Best-Seller', 'Enchantment-Book-Cover-Best-Seller', 'storage/2018/02/151913794487120.jpeg', '2018-02-20 14:45:45', '2018-02-20 14:45:45'),
(23, 'how-to-book-cove', 'how-to-book-cove', 'storage/2018/02/151913794527000.jpeg', '2018-02-20 14:45:45', '2018-02-20 14:45:45'),
(24, 'origina', 'origina', 'storage/2018/02/151913794641179.jpeg', '2018-02-20 14:45:47', '2018-02-20 14:45:47'),
(25, 'original-book-cove', 'original-book-cove', 'storage/2018/02/151913794730238.jpeg', '2018-02-20 14:45:48', '2018-02-20 14:45:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2018_02_05_131641_create_categories_table', 2),
(6, '2018_02_05_132105_create_colors_table', 2),
(7, '2018_02_05_183618_create_products_table', 3),
(8, '2018_02_06_192958_create_product_category_table', 4),
(9, '2018_02_06_193049_create_product_color_table', 4),
(10, '2018_02_08_180320_add_color_code_to_colors_table', 5),
(11, '2018_02_08_193627_modify_name_culum_to_category_table', 6),
(12, '2018_02_09_052330_create_writers_table', 6),
(13, '2018_02_09_095322_create_publishers_table', 6),
(14, '2018_02_09_213151_create_media_table', 7),
(15, '2018_02_09_214024_create_product_writers_table', 7),
(16, '2018_02_09_214131_create_product_publishers_table', 7),
(17, '2018_02_09_214236_create_product_media_table', 7),
(18, '2018_02_09_214451_create_category_media_table', 7),
(20, '2018_02_11_200007_column_modify_to_categories_table', 8),
(22, '2018_02_12_105013_column_modify_to_products_table', 9),
(23, '2018_02_11_190931_create_comments_table', 10),
(24, '2018_02_11_193307_create_orders_table', 10),
(25, '2018_02_11_195621_create_orders_details_table', 10),
(27, '2018_02_12_145648_add_thumb_fature_id_to_products_table', 11),
(36, '2018_02_13_162128_create_sections_table', 12),
(37, '2018_02_14_181611_create_pages_table', 12),
(38, '2018_02_15_172730_create_coupons_table', 12),
(39, '2018_02_16_174553_create_shoppingcarts_table', 12),
(40, '2018_02_21_105838_modify_to_users_table', 13),
(41, '2018_02_21_110102_modify_to_orders_table', 13),
(42, '2018_02_21_193352_create_payment_table', 14);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `payment_type` enum('cash','online') COLLATE utf8mb4_unicode_ci NOT NULL,
  `products_price` int(11) NOT NULL,
  `discount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_price` int(11) NOT NULL,
  `status` enum('padding','ondelivery','success','cancel') COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_type` enum('billing','shiping') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ship_parson` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ship_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ship_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ship_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ship_area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ship_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ship_address` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders_details`
--

CREATE TABLE `orders_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_bn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_en` longtext COLLATE utf8mb4_unicode_ci,
  `content_bn` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `tran_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `val_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_tran_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_issuer_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tran_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_bn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_type` enum('Book','Clothing','FoodGroceries') COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clothing_type` text COLLATE utf8mb4_unicode_ci,
  `weight` int(11) DEFAULT NULL,
  `size` longtext COLLATE utf8mb4_unicode_ci,
  `stock` int(11) NOT NULL,
  `review` double(5,2) NOT NULL DEFAULT '0.00',
  `reg_price` decimal(8,2) NOT NULL,
  `dis_price` decimal(8,2) NOT NULL,
  `dil_price` decimal(8,2) DEFAULT NULL,
  `featured_img_id` int(11) DEFAULT NULL,
  `description_en` mediumtext COLLATE utf8mb4_unicode_ci,
  `description_bn` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title_en`, `title_bn`, `product_type`, `page_number`, `clothing_type`, `weight`, `size`, `stock`, `review`, `reg_price`, `dis_price`, `dil_price`, `featured_img_id`, `description_en`, `description_bn`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Product One', 'Product One', 'Book', '40', NULL, NULL, NULL, 999, 0.00, '555.00', '666.00', '777.00', 22, '<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '<p>এই গণিতে দুর্বলতার বিষয়টি আমাদের শিক্ষার্থীদের আত্মবিশ্বাসে অত্যন্ত নেতিবাচক ভূমিকা রাখে। যখনই যুক্তি-বুদ্ধি প্রয়োগের বিষয় আসে, যখনই গাণিতিক বিশ্লেষণের বিষয় আসে, তখনই তারা ধরে নেয় যে, সেই জিনিসটি তাদের দিয়ে হবে না। তাই তো আমরা হরহামেশাই এমন প্রশ্ন পাই, &ldquo;ভাই, আমার প্রোগ্রামিং শেখার অনেক ইচ্ছা। কিন্তু আমি তো গণিতে দুর্বল। আমি কি প্রোগ্রামিং শিখতে পারব?&rdquo;। কেবল প্রোগ্রামিং নয়, বিজ্ঞান ও প্রকৌশলে জাতি হিসেবে আমাদের পিছিয়ে পড়ার অন্যতম কারণ হচ্ছে গণিতে দুর্বলতা কিংবা দক্ষতার অভাব।<br />\r\n<br />\r\nগণিতে দক্ষতা অর্জন করতে তিনটি জিনিস প্রয়োজন। সেগুলো হচ্ছে পড়া, উপলব্ধি করা ও অনুশীলন করা। গণিত বইতে কেবল অনুশীলনীর প্রশ্নগুলোর উত্তর বা সমাধান করলেই হবে না, বরং বই ভালোভাবে পড়তে হবে। আর পড়ার সময় গল্পের বই কিংবা খবরের কাগজ পড়ার মতো পড়লে হবে না, পড়ার সঙ্গে সঙ্গে কী পড়ছি, সেটি উপলব্ধি করার চেষ্টা করতে হবে। তো আমাদের স্কুলের গণিত বইগুলোতে অনুশীলনী আছে, অনুশীলনীর আগে আলোচনাও আছে (যদিও অনেক শিক্ষার্থীই সেই আলোচনা পড়ে না)। যেই জিনিসটি দরকার, সেটি হচ্ছে সবকিছুকে একই সুতোয় গাঁথা। গণিতের কোন জিনিসটি কেন শিখছি, সেটি কী কাজে লাগছে-এই বিষয়টি উপলব্ধি করা অত্যন্ত জরুরি।<br />\r\n<br />\r\nএই বইয়ের মাধ্যমে আমরা চেষ্টা করেছি যেন শিক্ষার্থীরা গণিতের মৌলিক ধারণাগুলো উপলব্ধি করতে পারে। সংখ্যা কীভাবে এল, মানুষ কীভাবে গুণতে শিখলো, সেই আলোচনা থেকে শুরু করে আমরা বীজগণিতের ধারণা, ঐকিক নিয়ম, উৎপাদক ও মৌলিক সংখ্যা, পরিসংখ্যান, সম্ভাব্যতা, সেট, ফাংশন, লগারিদম ইত্যাদি বিষয় নিয়ে কোথাও সংক্ষিপ্ত, আর কোথাও বিস্তারিত আলোচনা করেছি। আমরা কিন্তু বইতে গণিত শেখানোর চেষ্টা করি নি। তাই এই বই পড়ার সঙ্গে সঙ্গে কিংবা পড়ার পরে শিক্ষার্থীদের উচিত হবে স্কুলের গণিত বইগুলো পড়া এবং অনুশীলন করা। তাহলে এই বই পড়ে কী লাভ হবে? এই বইটি পড়ার পরে স্কুলের গণিত বইগুলো আর রহস্যময় কিংবা দূর্বোধ্য মনে হবে না। বরং তখন শিক্ষার্থীরা জানবে তারা কোন জিনিসটি কেন শিখছে এবং শেখার জন্য জোর প্রচেষ্টা চালাবে। আর বইতে গণিতের কঠিন বাংলা শব্দগুলোকে সহজ ভাষায় পাঠকের সঙ্গে পরিচয় করিয়ে দিয়েছি-তাই সেই বাংলা শব্দগুলো গণিত শেখার পথে বাধা হয়ে দাঁড়াতে পারবে না।</p>', '2018-02-20 16:51:00', '2018-02-20 16:53:02', NULL),
(2, 'Product tow', 'Product tow', 'Book', '50', NULL, NULL, NULL, 654, 0.00, '654.00', '5465.00', '6546.00', 23, '<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '<p>এই গণিতে দুর্বলতার বিষয়টি আমাদের শিক্ষার্থীদের আত্মবিশ্বাসে অত্যন্ত নেতিবাচক ভূমিকা রাখে। যখনই যুক্তি-বুদ্ধি প্রয়োগের বিষয় আসে, যখনই গাণিতিক বিশ্লেষণের বিষয় আসে, তখনই তারা ধরে নেয় যে, সেই জিনিসটি তাদের দিয়ে হবে না। তাই তো আমরা হরহামেশাই এমন প্রশ্ন পাই, &ldquo;ভাই, আমার প্রোগ্রামিং শেখার অনেক ইচ্ছা। কিন্তু আমি তো গণিতে দুর্বল। আমি কি প্রোগ্রামিং শিখতে পারব?&rdquo;। কেবল প্রোগ্রামিং নয়, বিজ্ঞান ও প্রকৌশলে জাতি হিসেবে আমাদের পিছিয়ে পড়ার অন্যতম কারণ হচ্ছে গণিতে দুর্বলতা কিংবা দক্ষতার অভাব।<br />\r\n<br />\r\nগণিতে দক্ষতা অর্জন করতে তিনটি জিনিস প্রয়োজন। সেগুলো হচ্ছে পড়া, উপলব্ধি করা ও অনুশীলন করা। গণিত বইতে কেবল অনুশীলনীর প্রশ্নগুলোর উত্তর বা সমাধান করলেই হবে না, বরং বই ভালোভাবে পড়তে হবে। আর পড়ার সময় গল্পের বই কিংবা খবরের কাগজ পড়ার মতো পড়লে হবে না, পড়ার সঙ্গে সঙ্গে কী পড়ছি, সেটি উপলব্ধি করার চেষ্টা করতে হবে। তো আমাদের স্কুলের গণিত বইগুলোতে অনুশীলনী আছে, অনুশীলনীর আগে আলোচনাও আছে (যদিও অনেক শিক্ষার্থীই সেই আলোচনা পড়ে না)। যেই জিনিসটি দরকার, সেটি হচ্ছে সবকিছুকে একই সুতোয় গাঁথা। গণিতের কোন জিনিসটি কেন শিখছি, সেটি কী কাজে লাগছে-এই বিষয়টি উপলব্ধি করা অত্যন্ত জরুরি।<br />\r\n<br />\r\nএই বইয়ের মাধ্যমে আমরা চেষ্টা করেছি যেন শিক্ষার্থীরা গণিতের মৌলিক ধারণাগুলো উপলব্ধি করতে পারে। সংখ্যা কীভাবে এল, মানুষ কীভাবে গুণতে শিখলো, সেই আলোচনা থেকে শুরু করে আমরা বীজগণিতের ধারণা, ঐকিক নিয়ম, উৎপাদক ও মৌলিক সংখ্যা, পরিসংখ্যান, সম্ভাব্যতা, সেট, ফাংশন, লগারিদম ইত্যাদি বিষয় নিয়ে কোথাও সংক্ষিপ্ত, আর কোথাও বিস্তারিত আলোচনা করেছি। আমরা কিন্তু বইতে গণিত শেখানোর চেষ্টা করি নি। তাই এই বই পড়ার সঙ্গে সঙ্গে কিংবা পড়ার পরে শিক্ষার্থীদের উচিত হবে স্কুলের গণিত বইগুলো পড়া এবং অনুশীলন করা। তাহলে এই বই পড়ে কী লাভ হবে? এই বইটি পড়ার পরে স্কুলের গণিত বইগুলো আর রহস্যময় কিংবা দূর্বোধ্য মনে হবে না। বরং তখন শিক্ষার্থীরা জানবে তারা কোন জিনিসটি কেন শিখছে এবং শেখার জন্য জোর প্রচেষ্টা চালাবে। আর বইতে গণিতের কঠিন বাংলা শব্দগুলোকে সহজ ভাষায় পাঠকের সঙ্গে পরিচয় করিয়ে দিয়েছি-তাই সেই বাংলা শব্দগুলো গণিত শেখার পথে বাধা হয়ে দাঁড়াতে পারবে না।</p>', '2018-02-20 16:52:30', '2018-02-20 16:53:22', NULL),
(3, 'Product Three', 'Product Three', 'Book', '59', NULL, NULL, NULL, 788, 0.00, '454.00', '458.00', '877.00', 24, '<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '<p>এই গণিতে দুর্বলতার বিষয়টি আমাদের শিক্ষার্থীদের আত্মবিশ্বাসে অত্যন্ত নেতিবাচক ভূমিকা রাখে। যখনই যুক্তি-বুদ্ধি প্রয়োগের বিষয় আসে, যখনই গাণিতিক বিশ্লেষণের বিষয় আসে, তখনই তারা ধরে নেয় যে, সেই জিনিসটি তাদের দিয়ে হবে না। তাই তো আমরা হরহামেশাই এমন প্রশ্ন পাই, &ldquo;ভাই, আমার প্রোগ্রামিং শেখার অনেক ইচ্ছা। কিন্তু আমি তো গণিতে দুর্বল। আমি কি প্রোগ্রামিং শিখতে পারব?&rdquo;। কেবল প্রোগ্রামিং নয়, বিজ্ঞান ও প্রকৌশলে জাতি হিসেবে আমাদের পিছিয়ে পড়ার অন্যতম কারণ হচ্ছে গণিতে দুর্বলতা কিংবা দক্ষতার অভাব।<br />\r\n<br />\r\nগণিতে দক্ষতা অর্জন করতে তিনটি জিনিস প্রয়োজন। সেগুলো হচ্ছে পড়া, উপলব্ধি করা ও অনুশীলন করা। গণিত বইতে কেবল অনুশীলনীর প্রশ্নগুলোর উত্তর বা সমাধান করলেই হবে না, বরং বই ভালোভাবে পড়তে হবে। আর পড়ার সময় গল্পের বই কিংবা খবরের কাগজ পড়ার মতো পড়লে হবে না, পড়ার সঙ্গে সঙ্গে কী পড়ছি, সেটি উপলব্ধি করার চেষ্টা করতে হবে। তো আমাদের স্কুলের গণিত বইগুলোতে অনুশীলনী আছে, অনুশীলনীর আগে আলোচনাও আছে (যদিও অনেক শিক্ষার্থীই সেই আলোচনা পড়ে না)। যেই জিনিসটি দরকার, সেটি হচ্ছে সবকিছুকে একই সুতোয় গাঁথা। গণিতের কোন জিনিসটি কেন শিখছি, সেটি কী কাজে লাগছে-এই বিষয়টি উপলব্ধি করা অত্যন্ত জরুরি।<br />\r\n<br />\r\nএই বইয়ের মাধ্যমে আমরা চেষ্টা করেছি যেন শিক্ষার্থীরা গণিতের মৌলিক ধারণাগুলো উপলব্ধি করতে পারে। সংখ্যা কীভাবে এল, মানুষ কীভাবে গুণতে শিখলো, সেই আলোচনা থেকে শুরু করে আমরা বীজগণিতের ধারণা, ঐকিক নিয়ম, উৎপাদক ও মৌলিক সংখ্যা, পরিসংখ্যান, সম্ভাব্যতা, সেট, ফাংশন, লগারিদম ইত্যাদি বিষয় নিয়ে কোথাও সংক্ষিপ্ত, আর কোথাও বিস্তারিত আলোচনা করেছি। আমরা কিন্তু বইতে গণিত শেখানোর চেষ্টা করি নি। তাই এই বই পড়ার সঙ্গে সঙ্গে কিংবা পড়ার পরে শিক্ষার্থীদের উচিত হবে স্কুলের গণিত বইগুলো পড়া এবং অনুশীলন করা। তাহলে এই বই পড়ে কী লাভ হবে? এই বইটি পড়ার পরে স্কুলের গণিত বইগুলো আর রহস্যময় কিংবা দূর্বোধ্য মনে হবে না। বরং তখন শিক্ষার্থীরা জানবে তারা কোন জিনিসটি কেন শিখছে এবং শেখার জন্য জোর প্রচেষ্টা চালাবে। আর বইতে গণিতের কঠিন বাংলা শব্দগুলোকে সহজ ভাষায় পাঠকের সঙ্গে পরিচয় করিয়ে দিয়েছি-তাই সেই বাংলা শব্দগুলো গণিত শেখার পথে বাধা হয়ে দাঁড়াতে পারবে না।</p>', '2018-02-20 16:56:14', '2018-02-20 17:47:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `product_id`, `category_id`, `created_at`, `updated_at`) VALUES
(5, 1, 7, NULL, NULL),
(6, 1, 9, NULL, NULL),
(7, 2, 1, NULL, NULL),
(8, 2, 8, NULL, NULL),
(12, 3, 1, NULL, NULL),
(13, 3, 2, NULL, NULL),
(14, 3, 8, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_color`
--

CREATE TABLE `product_color` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_media`
--

CREATE TABLE `product_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_media`
--

INSERT INTO `product_media` (`id`, `product_id`, `media_id`, `created_at`, `updated_at`) VALUES
(3, 1, 22, NULL, NULL),
(4, 2, 23, NULL, NULL),
(6, 3, 24, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_publishers`
--

CREATE TABLE `product_publishers` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `publisher_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_publishers`
--

INSERT INTO `product_publishers` (`id`, `product_id`, `publisher_id`, `created_at`, `updated_at`) VALUES
(5, 1, 4, NULL, NULL),
(6, 1, 5, NULL, NULL),
(7, 2, 1, NULL, NULL),
(8, 2, 5, NULL, NULL),
(11, 3, 4, NULL, NULL),
(12, 3, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_writers`
--

CREATE TABLE `product_writers` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `writer_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_writers`
--

INSERT INTO `product_writers` (`id`, `product_id`, `writer_id`, `created_at`, `updated_at`) VALUES
(5, 1, 4, NULL, NULL),
(6, 1, 7, NULL, NULL),
(7, 2, 3, NULL, NULL),
(8, 2, 6, NULL, NULL),
(11, 3, 4, NULL, NULL),
(12, 3, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `publishers`
--

CREATE TABLE `publishers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `publishers`
--

INSERT INTO `publishers` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, '{\"en\":\"Publisher one\",\"bn\":\"Publisher one\"}', '', '2018-02-12 06:32:36', '2018-02-12 06:32:36'),
(4, '{\"en\":\"Publisher tow\",\"bn\":\"Publisher tow\"}', '', '2018-02-20 16:47:13', '2018-02-20 16:47:13'),
(5, '{\"en\":\"Publisher three\",\"bn\":\"Publisher three\"}', '', '2018-02-20 16:47:19', '2018-02-20 16:47:19'),
(6, '{\"en\":\"Publisher four\",\"bn\":\"Publisher four\"}', '', '2018-02-20 16:47:26', '2018-02-20 16:47:26');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_bn` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `title_en`, `title_bn`, `categories`, `created_at`, `updated_at`) VALUES
(17, 'Book Section', '  বই সেকশন', 'a:3:{i:0;s:1:\"3\";i:1;s:1:\"4\";i:2;s:1:\"5\";}', '2018-02-21 05:47:02', '2018-02-21 05:47:02'),
(18, 'Food Section', ' খাবার সেকশন', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}', '2018-02-21 05:57:02', '2018-02-21 05:57:02'),
(20, 'Cloth', 'পোশাক', 'a:3:{i:0;s:1:\"7\";i:1;s:1:\"8\";i:2;s:1:\"9\";}', '2018-02-21 15:42:52', '2018-02-21 15:42:52');

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcarts`
--

CREATE TABLE `shoppingcarts` (
  `id` int(10) UNSIGNED NOT NULL,
  `userId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` enum('Basic','Diller','Staff','Admin') COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `phone`, `level`, `password`, `remember_token`, `picture`, `billing_country`, `billing_city`, `billing_area`, `billing_postcode`, `billing_address`, `created_at`, `updated_at`) VALUES
(1, 'palash7862', 'palashkhan.pk@gmail.com', '01853730155', 'Admin', '$2y$10$13DhVhnlVRGkgdaDGVwvGeEFvGk/b6/ZxeDjAjbKw.GhRGUevO1Hm', 'ydDhWiLvIGa0LfcYTXRffPJkNQGAQXYNus1RFVqMobyXDIOLSQnOUezXss0w', NULL, 'Bangladesh', 'fcvbcbvbcb', 'cbvcbvc', 'cbvcvbcv', 'cvbcvbbcv', '2018-02-06 11:16:28', '2018-02-21 12:24:58'),
(2, 'palashkhan pm', 'palashkhan.pm@gmail.com', '01981807397', 'Basic', '$2y$10$rzBGHRU2SwqaF3Yr48dKhOoBJClA7jnUt/g4sVjs9ZjUxrTgI94rW', 'HUI6ThNqe2LGtROhf7bkpaiyFMn5ZqHo2IEY0Bh3TT11pRwuSAsbqP0EKTYq', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-09 14:05:17', '2018-02-09 14:05:17');

-- --------------------------------------------------------

--
-- Table structure for table `writers`
--

CREATE TABLE `writers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `writers`
--

INSERT INTO `writers` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(3, '{\"en\":\"writer one\",\"bn\":\"writer one\"}', NULL, '2018-02-12 06:08:22', '2018-02-12 06:08:22'),
(4, '{\"en\":\"writer tow\",\"bn\":\"writer tow\"}', NULL, '2018-02-12 06:08:38', '2018-02-12 06:08:38'),
(6, '{\"en\":\"writer three\",\"bn\":\"writer three\"}', NULL, '2018-02-20 16:46:47', '2018-02-20 16:46:47'),
(7, '{\"en\":\"writer four\",\"bn\":\"writer four\"}', NULL, '2018-02-20 16:46:57', '2018-02-20 16:46:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_media`
--
ALTER TABLE `category_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_details`
--
ALTER TABLE `orders_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_color`
--
ALTER TABLE `product_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_media`
--
ALTER TABLE `product_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_publishers`
--
ALTER TABLE `product_publishers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_writers`
--
ALTER TABLE `product_writers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publishers`
--
ALTER TABLE `publishers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shoppingcarts`
--
ALTER TABLE `shoppingcarts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- Indexes for table `writers`
--
ALTER TABLE `writers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `category_media`
--
ALTER TABLE `category_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `orders_details`
--
ALTER TABLE `orders_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `product_color`
--
ALTER TABLE `product_color`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_media`
--
ALTER TABLE `product_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product_publishers`
--
ALTER TABLE `product_publishers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `product_writers`
--
ALTER TABLE `product_writers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `publishers`
--
ALTER TABLE `publishers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `shoppingcarts`
--
ALTER TABLE `shoppingcarts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `writers`
--
ALTER TABLE `writers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
