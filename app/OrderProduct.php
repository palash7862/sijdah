<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'orders_details';

    protected $fillable = [
        'order_id', 'product_id', 'qty',
        'unit_price', 'discount'
    ];

    public function product(){
        return $this->hasOne('App\Product', 'id', 'product_id');
    }
}
