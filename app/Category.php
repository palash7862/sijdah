<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'parent_id',
        'name_en',
        'name_bn',
        'description'
    ];

    public static function gatAll(){
        $cats = self::all();
        return $cats;
    }

    public static function getParentById($id){
        $cats = self::find($id);
        return $cats;
    }

    public function products(){
        return $this->belongsToMany('App\Product', 'product_category', 'category_id', 'product_id');
    }
    public function childs() {
        return $this->hasMany('App\Category','parent_id','id') ;
    }
}
