<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Section;
use App\Category;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);


//        View::composer(['index'], function ($view) {
//            $sections = Section::all();
//            foreach ($sections as $section) {
//                $catIds = unserialize($section->categories);
//                $categories = Category::find($catIds);
//                $section->categories = $categories;
//            }
//            $view->with('sections', $sections);
//        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $whitelist = array(
            '127.0.0.1',
            '::1'
        );

        /*if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
            $this->app->bind('path.public', function() {
                return base_path().'/../public_html';
            });
        }*/
    }
}
