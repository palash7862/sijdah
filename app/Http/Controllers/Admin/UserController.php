<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;

class UserController extends Controller
{

    protected $rules = ['Admin', 'Staff', 'Diller', 'Basic'];


    public function dashboard(){
        return view('admin.index');
    }

    public function gatAll(){
        User::paginate();
        $users = User::all();
        //$users = User::with('roles')->get();
        return view('admin.user.all', compact('users'));
    }

    public function add()
    {
        $roles = $this->rules;
        return view('admin.user.create', compact('roles'));
    }

    public function create(Request $request){
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'email'    => 'required',
            'phone'    => 'required',
            'status'   => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('user.add')
                ->withErrors($validator)
                ->withInput();
        }else {
            $user = User::create([
                'username'          => $data['username'],
                'email'             => $data['email'],
                'phone'             => $data['phone'],
                'password'          => bcrypt($data['password']),
                'level'             => $data['rule'],
                'billing_city'      => isset($data['billing_city']) ? $data['billing_city'] : '',
                'billing_area'      => isset($data['billing_area']) ? $data['billing_area'] : '',
                'billing_address'   => isset($data['billing_address']) ? $data['billing_address'] : '',
                'status'            => $data['status']
            ]);
        }
        return redirect()->route('user.all');
    }

    public function edit($id){
        $users = User::where('id', $id)->get();
        $roles = $this->rules;
        return view('admin.user.edit')
            ->with('users', $users)
            ->with('roles', $roles);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'userId'    => 'required',
            'username'  => 'required',
            'email'     => 'required',
            'phone'     => 'required',
            'status'    => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('user.add')
                ->withErrors($validator)
                ->withInput();
        }else {
            if ($data['userId']) {
                $user = User::find($data['userId']);
                if ($user) {
                    if (isset($data['password']) && ($data['password'] != "")) {
                        $data['password'] = bcrypt($data['password']);
                    } else {
                        $data['password'] = $user->password;
                    }
                    $user->username         = $data['username'];
                    $user->phone            = $data['phone'];
                    $user->password         = $data['password'];
                    $user->level            = $data['rule'];
                    $user->billing_city     = isset($data['billing_city']) ? $data['billing_city'] : '';
                    $user->billing_area     = isset($data['billing_area']) ? $data['billing_area'] : '';
                    $user->billing_address  = isset($data['billing_address']) ? $data['billing_address'] : '';
                    $user->status           = $data['status'];
                    $user->save();

                    return redirect()->route('user.all');
                }
            } else {
                return redirect()->route('user.all');
            }
        }
    }

    public function delete($id)
    {

        $user = User::where('id', $id)->first();
        if ($user) {
            $user->delete();
            return redirect()->route('user.all');
        } else {
            return redirect()->route('user.all');
        }
    }

}
