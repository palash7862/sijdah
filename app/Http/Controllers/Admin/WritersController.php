<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Writer;

class WritersController extends Controller
{
    public function getAll(){
        $writers = Writer::all();
        //return $writers;
        return view('admin.writer.writer', compact('writers'));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request){

        $validator = Validator::make($request->all(), [
            'name_en'       => 'required|max:190',
            'name_bn'       => 'required|max:190',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('writer.all')
                ->withErrors($validator)
                ->withInput();
        }else{
            $data = $request->all();
            $name['en'] = $data['name_en'];
            $name['bn'] = $data['name_bn'];
            $user = Writer::create([
                'name'         => json_encode($name),
                'description'  => $data['description'],
            ]);
            return redirect()->route('writer.all');
        }
    }

    public function edit($id){
        $writers = Writer::all();
        $swriter  = Writer::where('id', $id)->get();
        return view('admin.writer.writer', compact('writers', 'swriter'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(), [
            'name_en'          => 'required|max:190',
            'name_bn'          => 'required|max:190',
            'description'     => 'string|max:500',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('writer.all')
                ->withErrors($validator)
                ->withInput();
        }else{
            $data = $request->all();
            $writer = Writer::find($data['writer_id']);
            $name['en'] = $data['name_en'];
            $name['bn'] = $data['name_bn'];
            if($writer){
                $writer->name          = json_encode($name);
                $writer->description   = $data['description'];
                $writer->save();
                return redirect()->route('writer.all');
            }
        }
    }

    public function delete($id){
        $writer = Writer::where('id', $id)->first();
        if($writer){
            $writer->delete();
            return redirect()->route('writer.all');
        }else{
            return redirect()->route('writer.all');
        }
    }
}
