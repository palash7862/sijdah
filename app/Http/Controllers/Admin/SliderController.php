<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Setting;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Setting::where('name','slider')->get();
    	return view('admin.slider.slider',compact('sliders'));
    }

    public function update(Request $request)
    {
    	$data = $request->all();

    	$validator = Validator::make($request->all(), [
            'sliderId.*' => 'required|numeric',
            'title.*' => 'required|string',
            'description.*' => 'required|string'
        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('slider.index')
                ->withErrors($validator)
                ->withInput();
        }else{
            $slider = Setting::where('name','slider')->get();
            $slider = $slider->first();
            if($slider){
                $slider->description = json_encode($data);
                $slider->save();
            }
            else
            {

                Setting::create([
                    'name'=>'slider',
                    'description' => json_encode($data)
                ]);
            }
            
        }

        return redirect()->route('slider.index');
    }
}
