<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Setting;
use App\Media;

class SettingsController extends Controller
{
    public function general()
    {
        $sitename = Setting::where('name','sitename')->first();
        $watermark = Setting::where('name','watermark')->first();
        $logo = Setting::where('name','logo')->first();
        $favicon = Setting::where('name','favicon')->first();

        return view('admin.settings.general',compact(['logo','watermark','sitename','favicon']));
    }

    public function generalUpdate(Request $request)
    {
        $data =  $request->all();

        $validator = Validator::make($request->all(), [
            'sitename'  => 'required|string',
            'logo'      => 'required|numeric',
            'favicon'   => 'required|numeric',
            'watermark' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('general.index')
                ->withErrors($validator)
                ->withInput();
        }else{
            $sitename = Setting::where('name','sitename')->first();
            if($sitename){
                $sitename->description = $data['sitename'];
                $sitename->save();
            }else{
                Setting::create(['name'=>'sitename','description' => $data['sitename']]);
            }

            $logo = Setting::where('name','logo')->first();
            if($logo){
                $logo->description = $data['logo'];
                $logo->save();
            }else{
                Setting::create(['name'=>'logo','description' => $data['logo']]);
            }

            $favicon = Setting::where('name','favicon')->first();
            if($favicon){
                $favicon->description = $data['favicon'];
                $favicon->save();
            }else{
                Setting::create(['name'=>'favicon','description' => $data['favicon']]);
            }

            $watermark = Setting::where('name','watermark')->first();
            if($watermark){
                $watermark->description = $data['watermark'];
                $watermark->save();
            }else{
                Setting::create(['name'=>'watermark','description' => $data['watermark']]);
            }
        }
        return redirect()->route('general.index');
    }

    public function shipping()
    {
    	$ship_dhaka = Setting::where('name','ship_dhaka')->first();
        $ship_outside = Setting::where('name','ship_outside')->first();
    	return view('admin.settings.shipping',compact('ship_dhaka','ship_outside'));
    }
    public function shippingUpdate(Request $request)
    {
    	$data =  $request->all();

    	$validator = Validator::make($request->all(), [
            'ship_dhaka'         => 'required|numeric',
            'ship_outside'       => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('shipping.index')
                ->withErrors($validator)
                ->withInput();
        }else{
            $ship_dhaka = Setting::where('name','ship_dhaka')->first();
            if($ship_dhaka){
                $ship_dhaka->description = $data['ship_dhaka'];
                $ship_dhaka->save();
            }else{
                Setting::create(['name'=>'ship_dhaka','description' => $data['ship_dhaka']]);
            }

            $ship_outside = Setting::where('name','ship_outside')->first();
            if($ship_outside){
                $ship_outside->description = $data['ship_outside'];
                $ship_outside->save();
            }else{
                Setting::create(['name'=>'ship_outside','description' => $data['ship_outside']]);
            }
            
        }
    	return redirect()->route('shipping.index');
    }



    public function contact()
    {
    	$phone = Setting::where('name','phone')->first();
        $email = Setting::where('name','email')->first();
        $address_en = Setting::where('name','address_en')->first();
        $address_bn = Setting::where('name','address_bn')->first();

    	return view('admin.settings.contact',compact('phone','email','address_en','address_bn'));
    }

    public function contactUpdate(Request $request)
    {
    	$data =  $request->all();

    	$validator = Validator::make($request->all(), [
            'phone'         => 'required',
            'email'       	=> 'required|email',
            'address_en' 	=> 'required',
            'address_bn' 	=> 'required'
        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('contact.index')
                ->withErrors($validator)
                ->withInput();
        }else{
            $phone = Setting::where('name','phone')->first();
            if($phone){
                $phone->description = $data['phone'];
                $phone->save();
            }else{
                Setting::create(['name'=>'phone','description' => $data['phone']]);
            }

            $email = Setting::where('name','email')->first();
            if($email){
                $email->description = $data['email'];
                $email->save();
            }else{
                Setting::create(['name'=>'email','description' => $data['email']]);
            }

            $address_en = Setting::where('name','address_en')->first();
            if($address_en){
                $address_en->description = $data['address_en'];
                $address_en->save();
            }else{
                Setting::create(['name'=>'address_en','description' => $data['address_en']]);
            }

            $address_bn = Setting::where('name','address_bn')->first();
            if($address_bn){
                $address_bn->description = $data['address_bn'];
                $address_bn->save();
            }else{
                Setting::create(['name'=>'address_bn','description' => $data['address_bn']]);
            }            
        }
    	return redirect()->route('contact.index');
    }


    public function footerWidget()
    {
    	$footerWidget_1 = Setting::where('name','footerWidget_1')->first();
    	$footerWidget_2 = Setting::where('name','footerWidget_2')->first();
    	$footerWidget_3 = Setting::where('name','footerWidget_3')->first();

    	return view('admin.settings.footerWidget',compact('footerWidget_1','footerWidget_2','footerWidget_3'));
    }

    public function footerWidgetUpdate(Request $request)
    {
    	$data = $request->all();
    	$widget_id = $data['widget_id'];
 
    	$validator = Validator::make($request->all(), [
    		'widget_id'    => 'required',
    		'widget_title' => 'required',
            'title.*'      => 'required',
            'url.*'        => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('footerWidget.index')
                ->withErrors($validator)
                ->withInput();
        }else{
            $footerWidget = Setting::where('name','footerWidget_'.$widget_id)->first();
            if($footerWidget){
            	$footerWidget->description = json_encode($data);
            	$footerWidget->save();
            }
            else
            {

            	Setting::create([
            		'name'=>'footerWidget_'.$widget_id,
            		'description' => json_encode($data)
            	]);
            }
            
        }
    	return redirect()->route('footerWidget.index');
    }

    public function socialProfile()
    {
        $facebook = Setting::where('name','facebook')->first();
        $twitter = Setting::where('name','twitter')->first();
        $pinterest = Setting::where('name','pinterest')->first();
        $linkedin = Setting::where('name','linkedin')->first();
        $stumbleupon = Setting::where('name','stumbleupon')->first();
        $dribbble = Setting::where('name','dribbble')->first();
        $vk = Setting::where('name','vk')->first();

        return view('admin.settings.socialProfile',compact('facebook','twitter','pinterest','linkedin',
            'stumbleupon','stumbleupon','stumbleupon','dribbble','vk'));
    }
    public function socialProfileUpdate(Request $request)
    {
        $data = $request->all();
 
        $facebook = Setting::where('name','facebook')->first();
        if($facebook){
            $facebook->description = $data['facebook'];
            $facebook->save();
        }else{
            Setting::create(['name'=>'facebook','description' => $data['facebook']]);
        }

        $twitter = Setting::where('name','twitter')->first();
        if($twitter){
            $twitter->description = $data['twitter'];
            $twitter->save();
        }else{
            Setting::create(['name'=>'twitter','description' => $data['twitter']]);
        }

        $pinterest = Setting::where('name','pinterest')->first();
        if($pinterest){
            $pinterest->description = $data['pinterest'];
            $pinterest->save();
        }else{
            Setting::create(['name'=>'pinterest','description' => $data['pinterest']]);
        }

        $linkedin = Setting::where('name','linkedin')->first();
        if($linkedin){
            $linkedin->description = $data['linkedin'];
            $linkedin->save();
        }else{
            Setting::create(['name'=>'linkedin','description' => $data['linkedin']]);
        }

        $stumbleupon = Setting::where('name','stumbleupon')->first();
        if($stumbleupon){
            $stumbleupon->description = $data['stumbleupon'];
            $stumbleupon->save();
        }else{
            Setting::create(['name'=>'stumbleupon','description' => $data['stumbleupon']]);
        }

        $dribbble = Setting::where('name','dribbble')->first();
        if($dribbble){
            $dribbble->description = $data['dribbble'];
            $dribbble->save();
        }else{
            Setting::create(['name'=>'dribbble','description' => $data['dribbble']]);
        } 
        
        $vk = Setting::where('name','vk')->first();
        if($vk){
            $vk->description = $data['vk'];
            $vk->save();
        }else{
            Setting::create(['name'=>'vk','description' => $data['vk']]);
        }     
        
        return redirect()->route('socialProfile.index');
    }
}
