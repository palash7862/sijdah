<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class MediaController extends Controller
{

    private $fileExtension      = null;

    private $fileOrgName        = null;

    private $fileTitle          = null;

    private $fileNewName        = null;

    private $fileNewNameWithEx  = null;

    private $fileDistinition    = null;

    private $imageWithDistion   = null;

    public function view(){
        $mediaAll = Media::all();
        return view('admin.media.media', compact('mediaAll'));
    }

    public function upload(Request $request){
        $files = $request->file('files');
        if($request->hasFile('files')){
            foreach ($files as $file){
                $this->fileInit($file);
                $status = $this->saveFile($file);
                $i = 1;
                foreach (Media::allImageSize as $key=>$value){
                    echo $i;
                    $array = explode("x",$value);
                    $this->saveAsFile($array[0], $array[1]);
                    $i++;
                }
                if($status === TRUE){
                    $this->insert($this->fileTitle, $this->imageWithDistion);
                }
            }
        }
    }

    protected function insert($imageName, $location){
        $location = str_replace('\\', '/', $location);
        $medial = Media::create([
            'title'         => $imageName,
            'description'   => $imageName,
            'source'        => $location,
        ]);
        return $medial;
    }

    protected function fileInit($file){
        $year = date('Y');
        $month = date('m');
        $this->fileExtension        = $file->extension();
        $this->fileOrgName          = $file->getClientOriginalName();
        $this->fileTitle            = substr($this->fileOrgName, 0, -(strlen($this->fileExtension)+1));
        //$fileName                   = str_slug(substr($this->fileOrgName, 0, -(strlen($this->fileExtension)+1)));
        $this->fileNewName          = time().rand(11111, 99999);
        $this->fileNewNameWithEx    = $this->fileNewName.'.'.$this->fileExtension; 
        $this->fileDistinition      = 'storage/'.$year.'/'.$month;

    }

    protected function saveFile($file){
        if(!is_null( $this->fileNewNameWithEx)){
            $file->move($this->fileDistinition, $this->fileNewNameWithEx);
            $this->imageWithDistion = $this->fileDistinition.'/'.$this->fileNewNameWithEx;
            return TRUE;
        }
        return FALSE;
    }

    protected function saveAsFile($width, $hight){
        if(isset($width) && isset($hight)) {
            if (!is_null($this->imageWithDistion)) {
                $year = date('Y');
                $month = date('m');
                $cropImgWithDis = public_path() . '/storage' . DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR . $month . DIRECTORY_SEPARATOR . $this->fileNewName . "{$width}x{$hight}." . $this->fileExtension;
                $saveAsFileName = $cropImgWithDis;
                //print_r(storage_path('app\\'.$this->imageWithDistion));
                //var_dump($this->imageWithDistion);
                //$image = new ImageResize(public_path() . DIRECTORY_SEPARATOR . $this->imageWithDistion);
                //$image->crop($width, $hight, ImageResize::CROPCENTER);
                //$image->save($saveAsFileName);
                $img = Image::make($this->imageWithDistion)->fit($width, $hight);//->crop($width, $hight);
                $img->insert('storage/watermark.png', 'bottom-right');
                $img->save($saveAsFileName);
            }
        }
    }

    public function getAll(){
        $mediaData = array();
        $mediaAll = Media::all();
        $i = 0;
        foreach ($mediaAll as $media){
            $mediaData[$i]['id']      =  $media->id;
            $mediaData[$i]['source']  =  asset(get_image_size('media-thumb', $media->source));
//            $mediaData[$media->id]['id']      =  $media->id;
//            $mediaData[$media->id]['source']['orginal']  =  asset($media->source);
//            foreach (Media::allImageSize as $key=>$value) {
//                $keyN = str_replace('-', '', $key);
//                $mediaData[$media->id]['source'][$keyN] = asset(get_image_size($key, $media->source));
//            }
            $i++;
        }
        return response()->json([
            'success' => true,
            'data'    => $mediaData
        ]);
    }

    public function delete($id){
        $media = Media::find($id);
        $file = public_path().'/'.$media->source;
        \File::delete($file);
        $allSize = Media::allImageSize;
        $imgEx = explode(".", $media->source);
        $imgEx = end($imgEx);
        $imgNameWithDis = substr($media->source, 0, -(strlen($imgEx)+1));
        foreach ($allSize as $key => $value){
            $file = $imgNameWithDis.$value.'.'.$imgEx;
            \File::delete($file);
            //var_dump();
        }
        $media->delete();
        return redirect()->route('upload');
    }
}
