<?php

namespace App\Http\Controllers\Admin;

use App\Color;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Product;
use App\Category;
use App\Writer;
use App\Publisher;

class ProductController extends Controller
{
    public function gatAll(){
        $products = Product::all();
        return view('admin.product.all', compact('products'));
    }

    public function create(){
        $cats   = Category::all();
        $colors = Color::all();
        $writers = Writer::all();
        $publishers = Publisher::all();
        return view('admin.product.create', compact('cats', 'colors', 'writers', 'publishers'));
    }

    public function store(Request $request){
        $data = $request->all();
        if($this->productValidator($request)){

            if( $data['product_type'] == 'Book' ){
                    $this->productBookStore($data);
            }
            if( $data['product_type'] == 'Clothing' ){
                    $this->productClothingStore($data);
            }
            if( $data['product_type'] == 'FoodGroceries' ){
                    $this->productFoodGroceriesStore($data);
            }
            return redirect()->route('product.create');
        }
    }

    public function edit($id){
        $products  = Product::where('id', $id)->with('colors')->get();
        $cats     = Category::all();
        $colors = Color::all();
        $writers = Writer::all();
        $publishers = Publisher::all();
        return view('admin.product.edit')
            ->with('products', $products)
            ->with('colors', $colors)
            ->with('writers', $writers)
            ->with('publishers', $publishers)
            ->with('cats', $cats);
    }

    public function update(Request $request){

        $data = $request->all();
        if(!empty($data['product_id'])) {
            if ($this->productValidator($request)) {
                if ($data['product_type'] == 'Book') {
                        $this->productBookUpdate($data);
                }
                if ($data['product_type'] == 'Clothing') {
                        $this->productClothingUpdate($data);
                }
                if ($data['product_type'] == 'FoodGroceries') {
                        $this->productFoodGroceriesUpdate($data);
                }
                return redirect()->route('product.create');
            }
        }
    }

    public function delete($id){

        $product   = Product::find($id);
        if($product ){
            if ($product->product_type == 'Book'){
                $product->categories()->detach();
                $product->writers()->detach();
                $product->medias()->detach();
            }
            if ($product->product_type == 'Clothing'){
                $product->categories()->detach();
                $product->colors()->detach();
                $product->medias()->detach();
            }
            if ($product->product_type == 'FoodGroceries'){
                $product->categories()->detach();
                $product->medias()->detach();
            }
            $product->delete();
            return redirect()->route('product.all');
        }else{
            return redirect()->route('product.all');
        }
    }

    protected function productBookStore($data){
        if(!empty($data['featured_img_id'])){
            $featured_img_id = $data['featured_img_id'];
        }else{
            $featured_img_id = $data['product_media'][0];
        }
        $product = Product::create([
            'title_en'          => $data['title_en'],
            'title_bn'          => $data['title_bn'],
            'product_type'      => 'Book',
            'publisher_id'      => $data['publisher_id'],
            'page_number'       => $data['page_number'],
            'stock'             => $data['stock'],
            'reg_price'         => (float)$data['reg_price'],
            'dis_price'         => isset($data['dis_price']) ? (float)$data['dis_price'] : (float)'',
            'dil_price'         => isset($data['dil_price']) ? (float)$data['dil_price'] : (float)'',
            'featured_img_id'   => $featured_img_id,
            'description_en'    => isset($data['description_en']) ? $data['description_en'] : '',
            'description_bn'    => isset($data['description_bn']) ? $data['description_bn'] : '',
        ]);

        if (isset($data['cats']) && (count($data['cats']) > 0)) {
            foreach ($data['cats'] as $cat) {
                $product->categories()->attach($cat);
            }
        }

        if (isset($data['writers']) && (count($data['writers']) > 0)) {
            foreach ($data['writers'] as $writer) {
                $product->writers()->attach($writer);
            }
        }

        if (isset($data['product_media']) && (count($data['product_media']) > 0)) {
            foreach ($data['product_media'] as $media) {
                $product->medias()->attach($media);
            }
        }
        return TRUE;
    }

    protected function productBookUpdate($data){
        $product = Product::find($data['product_id']);
        if($product){
            if(!empty($data['featured_img_id'])){
                $featured_img_id = $data['featured_img_id'];
            }else{
                $featured_img_id = $data['product_media'][0];
            }
            $product->title_en      = $data['title_en'];
            $product->title_bn      = $data['title_bn'];
            $product->product_type  = 'Book';
            $product->publisher_id  = $data['publisher_id'];
            $product->page_number   = $data['page_number'];
            $product->stock         = $data['stock'];
            $product->reg_price     = (float)$data['reg_price'];
            $product->dis_price     = isset($data['dis_price']) ? (float)$data['dis_price'] : (float)'';
            $product->dil_price     = isset($data['dil_price']) ? (float)$data['dil_price'] : (float)'';
            $product->featured_img_id   = $featured_img_id;
            $product->description_en    = isset($data['description_en']) ? $data['description_en'] : '';
            $product->description_bn    = isset($data['description_bn']) ? $data['description_bn'] : '';
            $product->save();

            $product->categories()->detach();
            $product->writers()->detach();
            $product->medias()->detach();

            if (isset($data['cats']) && (count($data['cats']) > 0)) {
                foreach ($data['cats'] as $cat) {
                    $product->categories()->attach($cat);
                }
            }

            if (isset($data['writers']) && (count($data['writers']) > 0)) {
                foreach ($data['writers'] as $writer) {
                    $product->writers()->attach($writer);
                }
            }

            if (isset($data['product_media']) && (count($data['product_media']) > 0)) {
                foreach ($data['product_media'] as $media) {
                    $product->medias()->attach($media);
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    protected function productClothingStore($data){

        if(!empty($data['featured_img_id'])){
            $featured_img_id = $data['featured_img_id'];
        }else{
            $featured_img_id = $data['product_media'][0];
        }
        $product = Product::create([
            'title_en'          => $data['title_en'],
            'title_bn'          => $data['title_bn'],
            'product_type'      => 'Clothing',
            'clothing_type'     => serialize($data['clothing_type']),
            'size'              => $data['size'],
            'stock'             => $data['stock'],
            'reg_price'         => (float)$data['reg_price'],
            'dis_price'         => isset($data['dis_price']) ? (float)$data['dis_price'] : (float)'',
            'dil_price'         => isset($data['dil_price']) ? (float)$data['dil_price'] : (float)'',
            'featured_img_id'   => $featured_img_id,
            'description_en'    => isset($data['description_en']) ? $data['description_en'] : '',
            'description_bn'    => isset($data['description_bn']) ? $data['description_bn'] : '',
        ]);
        if (isset($data['colors']) && (count($data['colors'])> 0)){
            foreach ($data['colors'] as $color){
                $product->colors()->attach($color);
            }
        }
        if (isset($data['cats']) && (count($data['cats'])> 0)){
            foreach ($data['cats'] as $cat){
                $product->categories()->attach($cat);
            }
        }
        return TRUE;
    }

    protected function productClothingUpdate($data){
        $product = Product::find($data['product_id']);
        if($product){

            if(!empty($data['featured_img_id'])){
                $featured_img_id = $data['featured_img_id'];
            }else{
                $featured_img_id = $data['product_media'][0];
            }

            $product->title_en      = $data['title_en'];
            $product->title_bn      = $data['title_bn'];
            $product->product_type  = 'Clothing';
            $product->clothing_type = serialize($data['clothing_type']);
            $product->size          = $data['size'];
            $product->stock         = $data['stock'];
            $product->reg_price     = (float)$data['reg_price'];
            $product->dis_price     = isset($data['dis_price']) ? (float)$data['dis_price'] : (float)'';
            $product->dil_price     = isset($data['dil_price']) ? (float)$data['dil_price'] : (float)'';
            $product->featured_img_id   = $featured_img_id;
            $product->description_en    = isset($data['description_en']) ? $data['description_en'] : '';
            $product->description_bn    = isset($data['description_bn']) ? $data['description_bn'] : '';
            $product->save();

            $product->colors()->detach();
            if (isset($data['colors']) && (count($data['colors'])> 0)){
                foreach ($data['colors'] as $color){
                    $product->colors()->attach($color);
                }
            }

            $product->categories()->detach();
            if (isset($data['cats']) && (count($data['cats'])> 0)){
                foreach ($data['cats'] as $cat){
                    $product->categories()->attach($cat);
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    protected function productFoodGroceriesStore($data){
        if(!empty($data['featured_img_id'])){
            $featured_img_id = $data['featured_img_id'];
        }else{
            $featured_img_id = $data['product_media'][0];
        }
        $product = Product::create([
            'title_en'          => $data['title_en'],
            'title_bn'          => $data['title_bn'],
            'product_type'      => 'FoodGroceries',
            'weight'            => $data['weight'],
            'stock'             => $data['stock'],
            'reg_price'         => (float)$data['reg_price'],
            'dis_price'         => isset($data['dis_price']) ? (float)$data['dis_price'] : (float)'',
            'dil_price'         => isset($data['dil_price']) ? (float)$data['dil_price'] : (float)'',
            'featured_img_id'   => $featured_img_id,
            'description_en'    => isset($data['description_en']) ? $data['description_en'] : '',
            'description_bn'    => isset($data['description_bn']) ? $data['description_bn'] : '',
        ]);
        if (isset($data['cats']) && (count($data['cats'])> 0)){
            foreach ($data['cats'] as $cat){
                $product->categories()->attach($cat);
            }
        }
        return TRUE;
    }

    protected function productFoodGroceriesUpdate($data){

        $product = Product::find($data['product_id']);

        if($product){
            if(!empty($data['featured_img_id'])){
                $featured_img_id = $data['featured_img_id'];
            }else{
                $featured_img_id = $data['product_media'][0];
            }
            $product->title_en      = $data['title_en'];
            $product->title_bn      = $data['title_bn'];
            $product->product_type  = 'FoodGroceries';
            $product->weight        = $data['weight'];
            $product->stock         = $data['stock'];
            $product->reg_price     = (float)$data['reg_price'];
            $product->dis_price     = isset($data['dis_price']) ? (float)$data['dis_price'] : (float)'';
            $product->dil_price     = isset($data['dil_price']) ? (float)$data['dil_price'] : (float)'';
            $product->featured_img_id   = $featured_img_id;
            $product->description_en    = isset($data['description_en']) ? $data['description_en'] : '';
            $product->description_bn    = isset($data['description_bn']) ? $data['description_bn'] : '';
            $product->save();
            $product->categories()->detach();
            if (isset($data['cats']) && (count($data['cats'])> 0)){
                foreach ($data['cats'] as $cat){
                    $product->categories()->attach($cat);
                }
            }

            return TRUE;
        }
        return FALSE;
    }

    protected function productValidator($request){
        $validator = Validator::make($request->all(), [
            'title_en'              => 'required|max:190',
            'title_bn'              => 'required|max:190',
            'product_type'          => 'required|string',
            'page_number'           => 'nullable|numeric',
            'clothing_type.*.en'    => 'nullable|string',
            'clothing_type.*.bd'    => 'nullable|string',
            'weight'                => 'nullable|numeric',
            'stock'                 => 'nullable|numeric',
            'reg_price'             => 'required',
            'dis_price'             => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('product.create')
                ->withErrors($validator)
                ->withInput();
        }
        return TRUE;
    }

}
