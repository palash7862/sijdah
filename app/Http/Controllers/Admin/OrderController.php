<?php

namespace App\Http\Controllers\Admin;

use Barryvdh\DomPDF\PDF as PDF;
use DateTimeZone;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\User;
use App\Order;
use App\Payment;
use App\OrderProduct;
use App\Setting;

class OrderController extends Controller
{

    protected $cartSubtotal = null;

    protected $discunt = null;

    protected $totalAmount = null;

    protected $shippingCharge = null;

    protected  $paymentType = 'online';

    protected $couponCode = null;

    protected $addressType = 'bill';

    public function gatAll(){
        $Orders = Order::where('status', 'placed')->orderBy('id', 'desc')->get();
        return view('admin.order.orders', compact('Orders'));
    }

    public function getOrderProcessing(){
        $Orders = Order::where('status', 'processing')->orderBy('id', 'desc')->get();
        return view('admin.order.orders', compact('Orders'));
    }

    public function getOrderShipped(){
        $Orders = Order::where('status', 'shipped')->orderBy('id', 'desc')->get();
        return view('admin.order.orders', compact('Orders'));
    }

    public function getOrderDelivered(){
        $Orders = Order::where('status', 'delivered')->orderBy('id', 'desc')->get();
        return view('admin.order.orders', compact('Orders'));
    }

    public function getOrderCancel(){
        $Orders = Order::where('status', 'cancel')->orderBy('id', 'desc')->get();
        return view('admin.order.orders', compact('Orders'));
    }

    public function gatSingleOrder($id){
        $Order = Order::find($id);
        return view('admin.order.order', compact('Order'));
    }

    public function exportInvoice($id){
        $order = Order::find($id);
//        $pdf = app('dompdf.wrapper');
//        $pdf->loadView('invoice');
        //return $pdf->download('invoice.pdf');


        //->setPaper('a4')->setWarnings(false)
        $pdf = \PDF::loadView('admin.order.invoice', compact('order'));
        return $pdf->download('invoice.pdf');
        //return redirect()->route('orders');
    }

    public function orderProcess(Request $request){
        $data = $request->all();
        $user  = Auth::user();

        if(Auth::check()){
            //$data['paymentType'] = 'cash';
            if($data['paymentType'] == 'cash'){
                $this->insertCashOrder($user, $request);
                //\Cart::destroy();
                \Session::flash('OrderPlace', 'Order Complated');
                return redirect()->route('cart', array('stap'=>5));
            }else{
                $dhaka = Setting::where('name','ship_dhaka')->first();
                $outside = Setting::where('name','ship_outside')->first();
                if(session()->has('area')){
                    if($dhaka && $outside){   
                        if(!strcmp(session('area'),"ship_dhaka"))
                            $shippingCharge = $dhaka->description;
                        else
                            $shippingCharge = $outside->description;
                    }
                }else {
                    $shippingCharge = $dhaka->description;
                }
                $products = \Cart::content();
                $cartSubtotal = \Cart::subtotal();
                $discunt = $this->discunt($cartSubtotal);
                $totalAmount = ($cartSubtotal + $shippingCharge) - $discunt;
                $tranId = $this->tranId();
                session(['tranId'=>$tranId]);
                return view('orderprocess', compact('totalAmount', 'tranId'));
            }
        }
    }

    protected function discunt($cartSubtotal){
        if(session()->has('coupon')){
            $coupon = session('coupon');

            if($coupon->type == 'fixed')
                $discount = (int)$coupon->amount;
            else
                $discount = (int)(($cartSubtotal*$coupon->amount)/100);
        }
        else {
            $discount = 0;
        }
        return $discount;
    }

    public function OrderAddressCheck($user, Request $request){
        $data = $request->all();
        var_dump($data);
        if(isset($data['addressType'])) {

            $validator = Validator::make($request->all(), [
                'shipParson'    => 'required',
                'shipCity'      => 'required',
                'shipArea'      => 'required',
                'shipPhone'     => 'required',
                'shipAddress'   => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()
                    ->route('checkout')
                    ->withErrors($validator)
                    ->withInput();
            }else {

                $this->addressType = 'ship';
                if( empty($user->billing_city) || empty($user->billing_area) || empty($user->billing_address) ){
                    User::updateAddress($user->id, $request);
                    $address = array(
                        'shipParson'    => $data['shipParson'],
                        'shipCity'      => $data['shipCity'],
                        'shipArea'      => $data['shipArea'],
                        'shipPhone'     => $data['shipPhone'],
                        'shipAddress'   => $data['shipAddress']
                    );
                    session(["orderAddress"=>$address, 'addressType'=>'ship']);
                    return TRUE;
                }else{
                    $address = array(
                        'shipParson'    => $data['shipParson'],
                        'shipCity'      => $data['shipCity'],
                        'shipArea'      => $data['shipArea'],
                        'shipPhone'     => $data['shipPhone'],
                        'shipAddress'   => $data['shipAddress']
                    );
                    session(["orderAddress"=>$address, 'addressType'=>'ship']);
                    return TRUE;
                }
            }

        }else{
            if( empty($user->billing_city) ||
                empty($user->billing_area) ||
                empty($user->billing_address) ){
                User::updateAddress($user->id, $request);
                session(['addressType'=>'bill']);
                return TRUE;
            }else{
                session(['addressType'=>'bill']);
                return TRUE;
            }
        }

    }

    protected function insertCashOrder($user, $request){
        $data = $request->all();
        $addressType = session('addressType');
        $shippingCharge = 0;
        $dhaka = Setting::where('name','ship_dhaka')->first();
        $outside = Setting::where('name','ship_outside')->first();
        if(session()->has('area')){
            if($dhaka && $outside){
                if(!strcmp(session('area'),"ship_dhaka"))
                    $shippingCharge = $dhaka->description;
                else
                    $shippingCharge = $outside->description;
            }
        }else {
            $shippingCharge = $dhaka->description;
        }
        $products = \Cart::content();
        $cartSubtotal = \Cart::subtotal();
        $discunt = $this->discunt($cartSubtotal);
        $totalAmount = ($cartSubtotal + $shippingCharge) - $discunt;

        $orderData = [
            'customer_id'       => $user->id,
            'payment_type'      => 'cash',
            'products_price'    => (int)$cartSubtotal,
            'discount'          => (float)$discunt,
            'coupon_code'       => isset(session('coupon')->couponcode) ? session('coupon')->couponcode : '',
            'shipping_cost'     => $shippingCharge,
            'total_price'       => (int)$totalAmount,
            'status'            => 'padding',
        ];
        if ($addressType == 'ship'){
            $address = session('orderAddress');
            $orderData['address_type']      = 'shiping';
            $orderData['ship_parson']       = $address['shipParson'];
            $orderData['ship_phone']        = $address['shipPhone']; 
            $orderData['ship_city']         = $address['shipCity'];
            $orderData['ship_area']         = $address['shipArea']; 
            $orderData['ship_address']      = $address['shipAddress'];
        }else{
            $orderData['address_type']      = 'billing';
        }
        $order = Order::create($orderData);
        if($order){
            $this->insertOrderProduct($order->id, $products);
        }
    }

    protected function insertOnlinePayOrder($user, $paymentId){
        $addressType = session('addressType');
        $shippingCharge = 0;
        $dhaka = Setting::where('name','ship_dhaka')->first();
        $outside = Setting::where('name','ship_outside')->first();
        if(session()->has('area')){
            if($dhaka && $outside){
                if(!strcmp(session('area'),"ship_dhaka"))
                    $shippingCharge = $dhaka->description;
                else
                    $shippingCharge = $outside->description;
            }
        }else {
            $shippingCharge = $dhaka->description;
        }
        $products = \Cart::content();
        $cartSubtotal = \Cart::subtotal();
        $discunt = $this->discunt($cartSubtotal);
        $totalAmount = ($cartSubtotal + $shippingCharge) - $discunt;
        $orderData = [
            'customer_id'       => $user->id,
            'payment_id'        => $paymentId,
            'payment_type'      => 'online',
            'products_price'    => (int)$cartSubtotal,
            'discount'          => (float)$discunt,
            'coupon_code'       => session()->exists('coupon') ? session('coupon') : null,
            'shipping_cost'     => $shippingCharge,
            'total_price'       => (int)$totalAmount,
            'status'            => 'padding',
        ];

        if ($addressType == 'ship'){
            $address = session('orderAddress');
            $orderData['address_type']      = 'shiping';
            $orderData['ship_parson']       = $address['shipParson'];
            $orderData['ship_phone']        = $address['shipPhone'];
            $orderData['ship_city']         = $address['shipCity'];
            $orderData['ship_area']         = $address['shipArea'];
            $orderData['ship_address']      = $address['shipAddress'];
        }else{
            $orderData['address_type']      = 'billing';
        }
        $order = Order::create($orderData);
        if($order){
            $this->insertOrderProduct($order->id, $products);
            //return view('ordersuccess');
        }
    }

    protected function insertOrderProduct($orderId, $products){
            if($orderId){
                //echo "<pre>";
                //print_r($products);
                foreach ($products as $pds){
                    OrderProduct::create([
                        'order_id'      => $orderId,
                        'product_id'    => $pds->id,
                        'qty'           => $pds->qty,
                        'unit_price'    => $pds->price,
                        'discount'      => 0
                    ]);
                }
            }
    }

    public function tranId(){
        $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
        $trnId = $dt->format("Y").$dt->format("m").$dt->format("d").$dt->format("H").$dt->format("i").$dt->format("s");
        $trnId .= "-".rand(189,999);
        return $trnId;
    }

    public function sslValidation(Request $request){
        //echo '<pre>';
        //print_r($_POST);

        $sslData = $request->all();
        if( isset($sslData['status']) && ($sslData['status'] == "VALID")  && isset($sslData['val_id'])){

            $tran_id        = urlencode($sslData['tran_id']);
            $val_id         = urlencode($sslData['val_id']);
            $store_id       = urlencode('test_thyrocarebd');
            $store_passwd   = urlencode('test_thyrocarebd@ssl');

            $requested_url = ("https://sandbox.sslcommerz.com/validator/api/validationserverAPI.php?val_id=".$val_id."&store_id=".$store_id."&store_passwd=".$store_passwd."&v=1&format=json");


            $handle = curl_init();
            curl_setopt($handle, CURLOPT_URL, $requested_url);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);

            $result = curl_exec($handle);

            $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

            if( ($code == 200) && !( curl_errno($handle)) ){
                curl_close( $handle);
                $result = json_decode($result);
                if( $result->status == "VALID" ){
                    $Payment = $this->createPayment($result);
                    if(Auth::check()){
                        $user  = Auth::user();
                        $order = $this->insertOnlinePayOrder($user, $Payment->id);
                    }
                    //$Products = Cart::getContent()->toJson();
                    //if($Order){
                        //Mail::to($user->email)->send(new OrderShipped($Order));
                        //return redirect()->route('thanks')->with('message', 'You are successfully Send Order.');
                    //}
                    \Session::flash('OrderPlace', 'Order Completed');
                    return redirect()->route('cart', array('stap'=>5));
                }

            } else {
                echo "Failed to connect with SSLCOMMERZ";
            }
        }
    }

    public function sslFaild(){
        return view('orderfaild');
    }

    public function sslCancel(){
        return view('ordercancel');
    }

    protected function createPayment($result){
//        echo '<pre>';
//        print_r($result);
        $Payment = Payment::create([
            'tran_id'               => $result->tran_id,
            'val_id'                => $result->val_id,
            'bank_tran_id'          => $result->bank_tran_id,
            'card_type'             => $result->card_type,
            'card_issuer_country'   => $result->card_issuer_country,
            'currency'              => $result->currency,
            'currency_amount'       => $result->currency_amount,
            'tran_date'             => $result->tran_date
        ]);
        if($Payment){
            return $Payment;
        }
        return false;
    }

    public function checkout(){
        if (Auth::check()){
            return view('checkout');
        }
        return redirect()->route('login');
    }

    public function orderProcessing($id){
        $order = Order::find($id);
        $order->status = 'processing';
        $order->save();
        return Redirect::back();;
    }

    public function orderShipped($id){
        $order = Order::find($id);
        $order->status = 'shipped';
        $order->save();
        return Redirect::back();;
    }

    public function orderDelivered($id){
        $order = Order::find($id);
        $order->status = 'delivered';
        $order->save();
        return Redirect::back();;
    }

    public function orderCancel($id){
        $order = Order::find($id);
        $order->status = 'cancel';
        $order->save();
        return Redirect::back();;
    }

    public function cartProcess(Request $request){

        $data = $request->all();
        $stap = isset($data['stap']) ? $data['stap'] : '';
        if ($stap == 2 && Auth::check() == false){
            return redirect()->route('login');
        }elseif ($stap == 3 && Auth::check() == false ){ 
            return redirect()->route('login');
        }elseif ($stap == 3 && Auth::check() == true  && \Cart::count() == 0){
            return redirect()->route('shop');
        }elseif ($stap == 5 && ( Auth::check() == false  || \Session::has('OrderPlace') != true )){
            return redirect()->route('cart');
        }

        $products = \Cart::content();
        $cartSubtotal= \Cart::subtotal();

        if(session()->has('coupon')){
            $coupon = session('coupon');
            if($coupon->type == 'fixed')
                $discount = (int)$coupon->amount;
            else
                $discount = (int)(($cartSubtotal*$coupon->amount)/100);
        } else{
            $discount = 0;
        }


        $dhaka = Setting::where('name','ship_dhaka')->first();
        $outside = Setting::where('name','ship_outside')->first();
        if(session()->has('area')){

            $area = session('area');
            if($area == 'ship_dhaka')
                $shippingCharge = $dhaka->description;
            else
                $shippingCharge = $outside->description;
        } else {
            $shippingCharge = $dhaka->description;
        }

        $cartTotal = (int)$cartSubtotal + $shippingCharge - $discount;
        return view('cart',compact('products','cartSubtotal','shippingCharge','discount','cartTotal'));

    }

    public function addressProcess(Request $request){
        $user  = Auth::user();

        if($this->OrderAddressCheck($user,  $request)){
            return redirect()->route('cart', array('stap'=> 4));
        }
    }
}
