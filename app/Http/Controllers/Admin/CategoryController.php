<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Category;

class CategoryController extends Controller
{
    public function gatAll(){
        $cats = Category::all();
        $parents = Category::whereNotNull('parent_id')->get();
        return view('admin.category.cat', compact('cats', 'parents'));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request){

        $validator = Validator::make($request->all(), [
            'parent_id'     => 'nullable',
            'name_en'       => 'required|max:190',
            'name_bn'       => 'required|max:190',
        ]);

        if ($validator->fails()) {
            //var_dump($validator->messages());
            return redirect()
                ->route('cats.all')
                ->withErrors($validator)
                ->withInput();
        }else{
            $data = $request->all();
            $user = Category::create([
                'parent_id'      => isset($data['parent_id']) ? $data['parent_id'] : '',
                'name_en'        => $data['name_en'],
                'name_bn'        => $data['name_bn'],
                'description'    => $data['description'],
            ]);
            return redirect()->route('cats.all');
        }
    }

    public function edit($id){
        $cats    = Category::where('id', $id)->get();
        $parents = Category::whereNotNull('parent_id')->get();
        return view('admin.category.cat', compact('cats', 'parents'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(), [
            'catName'       => 'required|numeric',
            'parent_id'     => 'nullable|alpha_num',
            'name_en'       => 'required|max:190',
            'name_bn'       => 'required|max:190',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cats.all','')
                ->withErrors($validator)
                ->withInput();
        }else{
            $data = $request->all();
            $cat = Category::find($data['catName']);
            if($cat){
                $cat->parent_id     = $data['parent_id'];
                $cat->name_en       = $data['name_en'];
                $cat->name_bn       = $data['name_bn'];
                $cat->description   = $data['description'];
                $cat->save();
                return redirect()->route('cats.all');
            }
        }
    }

    public function delete($id){
        $cat   = Category::where('id', $id)->first();
        if($cat){
            $cat->delete();
            return redirect()->route('cats.all');
        }else{
            return redirect()->route('cats.all');
        }
    }
}
