<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Publisher;

class PublisherController extends Controller
{
    public function getAll(){
        $publishers = Publisher::all();
        return view('admin.publisher.publisher', compact('publishers'));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request){

        $validator = Validator::make($request->all(), [
            'name.en'          => 'required',
            'name.bn'          => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('publisher.all')
                ->withErrors($validator)
                ->withInput();
        }else{
            $data = $request->all();
            $user = Publisher::create([
                'name'         => json_encode($data['name']),
                'description'  => isset($data['description']) ? $data['description'] : '',
            ]);
            return redirect()->route('publisher.all');
        }
    }

    public function edit($id){
        $publishers = Publisher::all();
        $spublisher  = Publisher::where('id', $id)->get();
        return view('admin.publisher.publisher', compact('publishers', 'spublisher'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(), [
            'name.en'          => 'required',
            'name.bn'          => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('publisher.all')
                ->withErrors($validator)
                ->withInput();
        }else{
            $data = $request->all();
            $publisher = Publisher::find($data['publisher_id']);
            if($publisher){
                $publisher->name          = json_encode($data['name']);
                $publisher->description   = $data['description'];
                $publisher->save();
                return redirect()->route('publisher.all');
            }
        }
    }

    public function delete($id){
        $publisher = Publisher::where('id', $id)->first();
        if($publisher){
            $publisher->delete();
            return redirect()->route('publisher.all');
        }else{
            return redirect()->route('publisher.all');
        }
    }
}
