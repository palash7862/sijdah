<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Writer;
use App\Publisher;
use Maatwebsite\Excel\Facades\Excel;
//use Maatwebsite\Excel\Excel;



class ReportController extends Controller
{

    public function writerAll(){
        $writers = Writer::all();
        return view('admin.reports.writersales', compact('writers'));
    }

    public function writerSales($id){
        $writerSell = DB::table('writers')
            ->select('writers.id', 'writers.name', 'products.id as product_id', 'products.title_en',
                DB::raw('SUM(orders_details.qty) as total_sales'),
                DB::raw('SUM(orders_details.unit_price) as total_amount'))
            ->leftJoin('product_writers', 'product_writers.writer_id',  '=', 'writers.id')
            ->leftJoin('products', 'products.id', '=', 'product_writers.product_id')
            ->leftJoin('orders_details', 'orders_details.product_id', '=', 'products.id')
            ->groupBy('products.id')
            ->where('writers.id', $id)
            ->orderBy('total_sales', 'desc')
            ->get();
        return view('admin.reports.writersales', compact('writerSell'));
        //dd($writerSell);
    }

    public function writerSalesExport($id){
        $writerSell = DB::table('writers')
            ->select('writers.id', 'writers.name', 'products.id as product_id', 'products.title_en',
                DB::raw('SUM(orders_details.qty) as total_sales'),
                DB::raw('SUM(orders_details.unit_price) as total_amount'))
            ->leftJoin('product_writers', 'product_writers.writer_id',  '=', 'writers.id')
            ->leftJoin('products', 'products.id', '=', 'product_writers.product_id')
            ->leftJoin('orders_details', 'orders_details.product_id', '=', 'products.id')
            ->groupBy('products.id')
            ->where('writers.id', $id)
            ->orderBy('total_sales', 'desc')
            ->get();
        Excel::create('Filename', function($excel ) use($writerSell){
            // Set the title
            $excel->setTitle('Our new awesome title');

            // Chain the setters
            $excel->setCreator('Maatwebsite')
                ->setCompany('Maatwebsite');

            // Call them separately
            $excel->setDescription('A demonstration to change the file properties');

            $excel->sheet('Sheetname', function($sheet) use($writerSell) {
                $sheet->row(1, array(
                    'Writer Name', 'Product Name', 'Total Sales', 'Total Amount'
                ));
                $i = 2;
                foreach ($writerSell as $writer){
                    $sheet->row($i, array(
                        json_decode($writer->name)->bn,
                        $writer->title_en,
                        $writer->total_sales,
                        $writer->total_amount
                    ));
                    $i++;
                }
            });
        })->download('csv');
    }

    public function publisherAll(){
        $publishers = Publisher::all();
        return view('admin.reports.publishersales', compact('publishers'));
    }

    public function publisherSales($id){
        $publisherSell = DB::table('publishers')
            ->select('publishers.id', 'publishers.name', 'products.id as product_id', 'products.title_en',
                DB::raw('SUM(orders_details.qty) as total_sales'),
                DB::raw('SUM(orders_details.unit_price) as total_amount'))
            ->leftJoin('product_publishers', 'product_publishers.publisher_id',  '=', 'publishers.id')
            ->leftJoin('products', 'products.id', '=', 'product_publishers.product_id')
            ->leftJoin('orders_details', 'orders_details.product_id', '=', 'products.id')
            ->groupBy('products.id')
            ->where('publishers.id', $id)
            ->orderBy('total_sales', 'desc')
            ->get();
        return view('admin.reports.publishersales', compact('publisherSell'));
        //dd($writerSell);
    }

    public function publisherSalesExport($id){
        $publisherSell = DB::table('publishers')
            ->select('publishers.id', 'publishers.name', 'products.id as product_id', 'products.title_en',
                DB::raw('SUM(orders_details.qty) as total_sales'),
                DB::raw('SUM(orders_details.unit_price) as total_amount'))
            ->leftJoin('product_publishers', 'product_publishers.publisher_id',  '=', 'publishers.id')
            ->leftJoin('products', 'products.id', '=', 'product_publishers.product_id')
            ->leftJoin('orders_details', 'orders_details.product_id', '=', 'products.id')
            ->groupBy('products.id')
            ->where('publishers.id', $id)
            ->orderBy('total_sales', 'desc')
            ->get();
        Excel::create('Filename', function($excel ) use($publisherSell){
            // Set the title
            $excel->setTitle('Our new awesome title');

            // Chain the setters
            $excel->setCreator('Maatwebsite')
                ->setCompany('Maatwebsite');

            // Call them separately
            $excel->setDescription('A demonstration to change the file properties');

            $excel->sheet('Sheetname', function($sheet) use($publisherSell) {
                $sheet->row(1, array(
                    'Publisher Name', 'Product Name', 'Total Sales', 'Total Amount'
                ));
                $i = 2;
                foreach ($publisherSell as $publisher){
                    $sheet->row($i, array(
                        json_decode($publisher->name)->en,
                        $publisher->title_en,
                        $publisher->total_sales,
                        $publisher->total_amount
                    ));
                    $i++;
                }
            });
        })->download('csv');
    }

    public function dateSales(){
        return view('admin.reports.datesales');
    }

    public function dateSalesResult(Request $request){
        $date = $request->all();
        $nowDate = $date['from_date'];
        $oldDate = $date['to_date'];

            $products = DB::table('products')
                    ->select('products.id', 'products.title_en',
                        DB::raw('SUM(orders_details.qty) as total_sales'),
                        DB::raw("(SELECT GROUP_CONCAT(writers.name SEPARATOR '|') as wri  FROM product_writers INNER JOIN writers ON writers.id = product_writers.writer_id WHERE product_writers.product_id = products.id ) as writersName") )
                    ->leftJoin('orders_details', 'orders_details.product_id',  '=', 'products.id')
                    ->groupBy('products.id')
                    //->whereBetween('orders_details.created_at', ['2018-02-22', '2018-02-25'])
                    ->whereBetween('orders_details.created_at', [$oldDate, $nowDate])
                    ->orderBy('total_sales', 'desc')
                    ->get();
        return view('admin.reports.datesales', compact('products', 'nowDate', 'oldDate'));
        //dd($products);
    }

    public function dateSalesExport($newdate, $olddate){
        $products = DB::table('products')
            ->select('products.id', 'products.title_en',
                DB::raw('SUM(orders_details.qty) as total_sales'),
                DB::raw("(SELECT GROUP_CONCAT(writers.name SEPARATOR '|') as wri  FROM product_writers INNER JOIN writers ON writers.id = product_writers.writer_id WHERE product_writers.product_id = products.id ) as writersName") )
            ->leftJoin('orders_details', 'orders_details.product_id',  '=', 'products.id')
            ->groupBy('products.id')
            //->whereBetween('orders_details.created_at', ['2018-02-22', '2018-02-25'])
            ->whereBetween('orders_details.created_at', [$olddate, $newdate])
            ->orderBy('total_sales', 'desc')
            ->get();
        Excel::create('Filename', function($excel ) use($products){
            // Set the title
            $excel->setTitle('Our new awesome title');

            // Chain the setters
            $excel->setCreator('Maatwebsite')
                ->setCompany('Maatwebsite');

            // Call them separately
            $excel->setDescription('A demonstration to change the file properties');

            $excel->sheet('Sheetname', function($sheet) use($products) {
                $sheet->row(1, array(
                    'Product Name', 'Writers Name', 'Total Sales'
                ));
                $i = 2;
                foreach ($products as $product){
                    $writerArray = explode('|', $product->writersName);
                    $writerName = "";
                    foreach ($writerArray as $writer){
                        $writerName .= json_decode($writer)->en.', ';
                    }
                    $sheet->row($i, array(
                        $product->title_en,
                        $writerName,
                        $product->total_sales,
                    ));
                    $i++;
                }
            });
        })->download('csv');
    }
}
