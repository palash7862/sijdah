<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Color;

class ColorController extends Controller
{
    public function gatAll(){
        $colors = Color::all();
        return view('admin.color.color', compact('colors'));
    }

    public function create(Request $request){

        $validator = Validator::make($request->all(), [
            'name'          => 'required|max:190',
            'color_code'    => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('color.all')
                ->withErrors($validator)
                ->withInput();
        }else{
            $data = $request->all();
            Color::create([
                'name'           => $data['name'],
                'color_code'     => $data['color_code'],
            ]);
            return redirect()->route('color.all');
        }
    }

    public function edit($id){
        $colors = Color::all();
        $color    = Color::where('id', $id)->get();
        return view('admin.color.color', compact('color', 'colors'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(), [
            'name'          => 'required|max:190',
            'color_code'    => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('color.all')
                ->withErrors($validator)
                ->withInput();
        }else{
            $data = $request->all();
            $color = Color::find($data['colorId']);
            if($color){
                $color->name        = $data['name'];
                $color->color_code  = $data['color_code'];
                $color->save();
                return redirect()->route('color.all');
            }
        }
    }

    public function delete($id){
        $color   = Color::where('id', $id)->first();
        if($color){
            $color->delete();
            return redirect()->route('color.all');
        }else{
            return redirect()->route('color.all');
        }
    }
}
