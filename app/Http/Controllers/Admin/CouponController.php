<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Coupon;
use Carbon\Carbon;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::all();
        return view('admin.coupon.coupon',compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'       => 'required|max:190',
            'couponcode'  => 'required|alpha_num|max:190',
            'amount'      => 'required|numeric',
            'expired_date'=> 'required|date'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('coupon.all')
                ->withErrors($validator)
                ->withInput();
        }else{
            $data = $request->all();
            $user = Coupon::create($data);
            return redirect()->route('coupon.all');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cp = Coupon::find($id);
        $coupons = Coupon::all();
        return view('admin.coupon.coupon',compact('cp','coupons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'       => 'required|max:190',
            'couponcode'  => 'required|alpha_num|max:190',
            'amount'      => 'required|numeric',
            'expired_date'=> 'required|date'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('coupon.all')
                ->withErrors($validator)
                ->withInput();
        }else{
            $data = $request->all();
            $coupon = Coupon::find($data['coupon_id']);
            if($coupon){
                $coupon->title        = $data['title'];
                $coupon->couponcode   = $data['couponcode'];
                $coupon->type        = $data['type'];
                $coupon->amount        = $data['amount'];
                $coupon->expired_date        = $data['expired_date'];
                $coupon->save();
                return redirect()->route('coupon.all');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coupon = Coupon::where('id', $id)->first();
        if($coupon){
            $coupon->delete();
            return redirect()->route('coupon.all');
        }else{
            return redirect()->route('coupon.all');
        }
    }

    public function search($code)
    {
        $now = Carbon::now('Asia/Dhaka');
        $coupon = Coupon::where('couponcode',trim($code))->where('expired_date','>=',$now->toDateString())->get();
        session(["coupon"=>$coupon[0]]);

        return $coupon[0];
    }
}
