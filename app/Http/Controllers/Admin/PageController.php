<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Page;
use Validator;

class PageController extends Controller
{
    //


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(){
        return view('admin.page.create');
    }

    /**
     * @param Request $request
     */
    public function store(Request $request){

        $formdata =  $request->all();

        $validator = Validator::make($formdata, [
            'title.en'    => 'required',
            'title.bn'    => 'required',
            'slug'        => 'required',
            'content.en'  => 'required',
            'content.bn'  => 'required',
        ]);

        if ($validator->fails()){
            return redirect()
                ->route('create.page')
                ->withErrors($validator)
                ->withInput();
        }else{
            $user = Page::create([
                'title_en'      => $formdata['title']['en'],
                'title_bn'      => $formdata['title']['bn'],
                'slug'          => $formdata['slug'],
                'content_en'    => $formdata['content']['en'],
                'content_bn'    => $formdata['content']['bn'],

            ]);
            $request->session()->flash('status', 'Your Record Has been Added');
            return redirect()->route('create.page');
        }
    }

    public function edit($id){
        $page = Page::where('id',$id)->get();
        return view('admin.page.edit', compact('page'));
    }

    public function update(Request $request){
        $formdata = $request->all();
        $validator = Validator::make($formdata, [
            'title.en'    => 'required',
            'title.bn'    => 'required',
            'content.en'  => 'required',
            'content.bn'  => 'required'
        ]);

        if ($validator->fails()){
            return redirect()
                ->route('edit.page', ['id'=>$formdata['page_id']])
                ->withErrors($validator)
                ->withInput();
        }else{

            $page = Page::find($formdata['page_id']);

            if($page){
                $page->title_en         = $formdata['title']['en'];
                $page->title_bn         = $formdata['title']['bn'];
                $page->content_en       = $formdata['content']['en'];
                $page->content_bn       = $formdata['content']['bn'];
                $page->save();
                return redirect()->route('all.page');
            }
        }
    }

    public function getAll(){
        $data = DB::table('pages')->get();
        return view('admin.page.all', ['page' => $data]);
    }

    public function delete($id){
        DB::table('pages')->where('id', '=', $id)->delete();
        return redirect()->route('all.page');
    }

}
