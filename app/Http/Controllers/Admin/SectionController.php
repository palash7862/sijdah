<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Section;
use App\Category;

class SectionController extends Controller
{
    public function index(){
        $sections = Section::all();
        $categories = Category::all();
        return view('admin.homeSections.homeSections',compact('sections','categories'));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request){

        $validator = Validator::make($request->all(), [
            'title_en'       => 'required|max:190',
            'title_bn'       => 'required|max:200',
            'categories'  => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('section.all')
                ->withErrors($validator)
                ->withInput();
        }else{
            $data = $request->all();

            $data['categories'] = serialize($data['categories']);
            
            $cat  = Section::create($data);
            return redirect()->route('section.all');           
        }
    }

    public function edit($id){
        $sections = Section::all();
        $ssection  = Section::where('id', $id)->get();
        $categories = Category::all();
        return view('admin.homeSections.homeSections', compact('sections', 'ssection','categories'));
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(), [
            'title_en'       => 'required|max:190',
            'title_bn'       => 'required|max:200',
            'categories'  => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('section.all')
                ->withErrors($validator)
                ->withInput();
        }else{
            $data = $request->all();
            $section = Section::find($data['section_id']);
            if($section){
                $section->title_en     = $data['title_en'];
                $section->title_bn     = $data['title_bn'];
                $section->categories   = serialize($data['categories']);
                $section->save();
                return redirect()->route('section.all');
            }
        }
    }

    public function delete($id){
        $section = Section::where('id', $id)->first();
        if($section){
            $section->delete();
            return redirect()->route('section.all');
        }else{
            return redirect()->route('section.all');
        }
    }
}
