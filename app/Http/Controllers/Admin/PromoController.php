<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Promo;
use Carbon\Carbon;
use App\Media;
use DB;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $curDate = Carbon::now('Asia/Dhaka');
        $nextDate = Carbon::tomorrow('Asia/Dhaka');
        $promotions = DB::table('promos')
        ->select('promos.id as id','promos.title as title','promos.status as status','promos.starts_at as starts_at','promos.ends_at as ends_at','media.source as m_src')
        ->leftJoin('media', 'media.id',  '=', 'promos.source')
        ->orderBy('promos.updated_at','DESC')
        ->get();

        //dd($promotions);
        return view('admin.promotions.promo',compact('promotions','curDate','nextDate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formdata =  $request->all();

        $validator = Validator::make($formdata, [
            'title'    => 'required',
            'source'    => 'required',
            'starts_at' => 'required',
            'ends_at'  => 'required',
            'status'  => 'required|in:active,inactive',
        ]);

        if ($validator->fails()){
            return redirect()
                ->route('promotion.index')
                ->withErrors($validator)
                ->withInput();
        }else{
            $promo = Promo::create($formdata);
            $request->session()->flash('status', 'Your Record Has been Added');
            return redirect()->route('promotion.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promotions = DB::table('promos')
        ->select('promos.id as id','promos.title as title','promos.status as status','promos.starts_at as starts_at','promos.ends_at as ends_at','media.source as m_src')
        ->leftJoin('media', 'media.id',  '=', 'promos.source')
        ->orderBy('promos.updated_at','DESC')
        ->get();

        $spromotion = Promo::where('id',$id)->first();
        $imgId = $spromotion->source;
        $img = Media::where('id',$imgId)->first();

        return view('admin.promotions.promo',compact('promotions','spromotion','img'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $formdata =  $request->all();

        $validator = Validator::make($formdata, [
            'title'    => 'required',
            'source'    => 'required',
            'starts_at' => 'required',
            'ends_at'  => 'required',
            'status'  => 'required|in:active,inactive',
        ]);

        if ($validator->fails()){
            return redirect()
                ->route('promotion.index')
                ->withErrors($validator)
                ->withInput();
        }else{
            $promo = Promo::where('id',$id)->first();
            $promo->title = $formdata['title'];
            $promo->source = $formdata['source'];
            $promo->starts_at = $formdata['starts_at'];
            $promo->ends_at = $formdata['ends_at'];
            $promo->status = $formdata['status'];
            $promo->save();

            $request->session()->flash('status', 'Your Record Has been updated');
           
        }
        return redirect()->route('promotion.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $promo = Promo::where('id',$id)->first();
        $promo->delete();
        return redirect()->route('promotion.index');

    }
}
