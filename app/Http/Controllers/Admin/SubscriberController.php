<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Subscriber;

class SubscriberController extends Controller
{
	public function index()
	{
		$subscribers = Subscriber::all();
		return view('admin.subscriber.subscriber',compact('subscribers'));
	}
    public function subscribe(Request $request)
    {
    	$data = $request->all();

    	$validator = Validator::make($request->all(), [
            'email'  => 'required|email'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }else{
        	$email = $data['email'];
        	$sub = Subscriber::where('email',$email)->first();
        	if($sub)
        	{
        		$sub->status = 'active';
        		$sub->save();
        	}
        	else{
        		Subscriber::create(['email'=>$email,'status'=>'active']);
        	}
            return redirect()->back();
        }
    }

    public function delete($id)
    {
    	$subscriber = Subscriber::where('id', $id)->first();
        if($subscriber){
            $subscriber->delete();
            return redirect()->route('subscriber.index');
        }else{
            return redirect()->route('subscriber.index');
        }
    }
}
