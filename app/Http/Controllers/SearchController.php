<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Searchy;
use App\Media;
use DB;

class SearchController extends Controller
{
    public function filter(Request $request, Product $products)
    {
        $products = $products->newQuery(); 
        $products = DB::table('products')
       ->select('products.*','media.source as source',
        DB::raw('SUM(orders_details.qty) as total_sales'), 
        DB::raw("(SELECT GROUP_CONCAT(writers.name SEPARATOR '|') as wri  FROM product_writers INNER JOIN writers ON writers.id = product_writers.writer_id WHERE product_writers.product_id = products.id ) as writersName"), 
        DB::raw("(SELECT GROUP_CONCAT(writers.id SEPARATOR '|') as wri  FROM product_writers INNER JOIN writers ON writers.id = product_writers.writer_id WHERE product_writers.product_id = products.id ) as writersId"))
       ->leftJoin('orders_details', 'orders_details.product_id',  '=', 'products.id')
       ->leftJoin('media','media.id','=','products.featured_img_id')
       ->groupBy('products.id');

        if ($request->has('type')) {
            $products->whereIn('product_type', $request->input('type'));
        }

        if ($request->has('rating')) {
            if((float)$request->input('rating') > 0.0)
                $products->where('review','>=', $request->input('rating'));
        }

        if ($request->has('pricebetween')) {
            $price = explode(',',$request->input('pricebetween'));
            $products->whereBetween('dis_price', [$price[0], $price[1]]);
        }

        if ($request->has('orderby')) {
            $order = $request->input('orderby');

            if($order=='rating')
                $products->orderBy('review','DESC');
            else if ($order == 'pricehl') 
                $products->orderBy('dis_price','DESC');
            else if ($order == 'pricelh') 
                $products->orderBy('dis_price','ASC');
            else if ($order == 'new')
                $products->orderBy('created_at','DESC');
            else if ($order == 'old')
                $products->orderBy('created_at','ASC');
            else if ($order == 'popular')
                $products->orderBy('total_sales','DESC');
            
        }
        if ($request->has('pagination')) {
            $products->paginate($request->input('pagination'));
        }
        return $products->get();
    }

    public function findAll($term)
    {
        if (strlen($term) == strlen(utf8_decode($term))) //IF not utf-8
        {
            $products = Searchy::driver('fuzzy')->search('products')->fields('title_en')->query($term)->get();
            foreach ($products as $product) {
                $img = Media::find($product->featured_img_id);
                $product->featured_img_id = get_image_size('product-thumb',$img->source);
            }

            return json_encode($products);
        }
        else
        {
            $products = Searchy::driver('ufuzzy')->search('products')->fields('title_bn')->query($term)->get();
            foreach ($products as $product) {
                $img = Media::find($product->featured_img_id);
                $product->featured_img_id = get_image_size('product-thumb',$img->source);
            }
            return json_encode($products);
        }
    }

    public function findInCategory($cat,$term)
    {
    	if (strlen($term) == strlen(utf8_decode($term))) //IF not utf-8
    	{
    	    $products = Searchy::driver('fuzzy')->search('products')->fields('title_en')->query($term)->getQuery()->where('product_type',$cat)->get();
            foreach ($products as $product) {
                $img = Media::find($product->featured_img_id);
                $product->featured_img_id = get_image_size('product-thumb',$img->source);
            }
    		return json_encode($products);
    	}
    	else
    	{
    		$products = Searchy::driver('ufuzzy')->search('products')->fields('title_bn')->query($term)->getQuery()->where('product_type',$cat)->get();

            foreach ($products as $product) {
                $img = Media::find($product->featured_img_id);
                $product->featured_img_id = get_image_size('product-thumb',$img->source);
            }
            
    		return json_encode($products);
    	}
    }

}
