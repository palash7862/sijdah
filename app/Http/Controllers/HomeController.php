<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Publisher;
use App\Writer;
use Illuminate\Http\Request;
use App\Product;
use App\Section;
use App\Category;
use Cookie;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Promo;
use Carbon\Carbon;
use DB;
use App\Media;
use App\Setting;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $sections = Section::all();
        foreach ($sections as $section) {
            $catIds = unserialize($section->categories);
            $categories = Category::find($catIds);
            $section->categories = $categories;
        }

        $imgId = 0;
        if(  !(session()->has('promoshowed'))  )
        {
            $now = Carbon::now('Asia/Dhaka');
            session(['promoshowed'=>true]);
            $img = Promo::where('status','active')
                            ->where('ends_at','>=',$now)
                            ->where('starts_at','<=',$now)
                            ->orderBy('created_at')
                            ->get();
            if(count($img)){
                $imgId = $img->first()->source;
            }
        }

        $sliders = Setting::where('name','slider')->get();

        return view('index',compact('sections','imgId','sliders'));
    }

    public function singleProduct($id){
        if (isset($id)) {
            $product = Product::find($id);

            $comments = Comments::where('product_id', $product->id)->get();
            $avg = Comments::where('product_id', $product->id)->avg('rating');
            $avg = round($avg);
            return view('single-product', compact('product','comments','avg'));
        }
        return redirect()->route('shop');
    }

    public function shop(){
        
        $products = Product::paginate(16); 
       
        $categories = Category::whereNull('parent_id')->get();
        return view('shop', compact('products','categories'));
    }
    public function categoryShop($id){

        $category = Category::find($id);
        if($category && ($category->products->count() > 0)){
            $products = $category->products()->paginate(15);
        }else{
            $products = false;
        }
        $categories = Category::whereNull('parent_id')->get();
        return view('catshop', compact('category', 'products','categories'));
    }

    public function writerShop($id){

        $writer = Writer::find($id);
        if($writer && ($writer->products->count() > 0)){
            $products = $writer->products()->paginate(15);
        }else{
            $products = false;
        }
        $categories = Category::whereNull('parent_id')->get();
        return view('writershop', compact('writer', 'products','categories'));
    }

    public function publisherShop($id){

        $publisher = Publisher::find($id);
        if($publisher && ($publisher->products->count() > 0)){
            $products = $publisher->products()->paginate(15);
        }else{
            $products = false;
        }
        $categories = Category::whereNull('parent_id')->get();
        return view('publishershop', compact('publisher', 'products','categories'));
    }

    public function shopCategory($id){
        
        $products = Category::find($id)->products;

        $categories = Category::whereNull('parent_id')->get();
        return view('shop', compact('products','categories'));
    }

    public function comment(Request $request)
    {
        $formdata = $request->all();
        $id = $formdata['product_id'];
        $product = Product::find($id);

        $validator = Validator::make($formdata, [
            'comment' => 'required | max:200'
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors('msg','Your coment wont publish')
                ->withInput();
        } else {
            $comment = Comments::create([
                'product_id'    => $formdata['product_id'],
                'user_id'       => Auth::id(),
                'rating'        => $formdata['rating'],
                'massage'       => $formdata['comment']
            ]);

        }
        //return view('single-product',compact('product'));
        return Redirect::back();
    }

}
