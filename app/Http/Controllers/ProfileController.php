<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use App\Order;
use App\OrderProduct;

class ProfileController extends Controller
{
    //
    public function userProfile(){
        return view('profile.userprofile');
    }

    public function editProfile(){
        return view('profile.editProfile');
    }

    public function updateProfile(Request $request){

        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'name'           => 'required',
            'username'       => 'required',
            'email'          => 'required',
            'phone'          => 'required',
            'address'        => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('edit.profile')
                ->withErrors($validator)
                ->withInput();
        }else{
            $userId = $user = Auth::user()->id;
            $user = User::find($userId);
            if($user){
                $user->name      = $data['name'];
                $user->username  = $data['username'];
                $user->phone     = $data['phone'];
                $user->address   = $data['address'];
                $user->save();
                return redirect()->route('user.profile');
            }
        }
    }

    public function changepassword(){
        return view('profile.changepassword');
    }

    public function upatepassword(Request $request){

        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'current_password'  => 'required',
            'new_password'      => 'required|min:6',
            'confirm_password'  => 'required|same:new_password',
        ]);

        if ($validator->fails()){
            return redirect()
                ->route('change.password')
                ->withErrors($validator)
                ->withInput();
        }else{
            $userId = Auth::user()->id;
            $user = User::find($userId);

            if(!Hash::check($data['current_password'], $user->password)){
                return redirect()
                    ->back()
                    ->with('error','The specified password does not match the database password');
            }else{
                if($user){
                    $user->password  = bcrypt($data['new_password']);
                    $user->save();
                    return redirect()->route('user.profile');
                }
            }
        }

    }

    public function orders(){
        $user_id = Auth::user()->id;
        $orders = Order::where('customer_id',$user_id)->orderBy('created_at', 'desc')->get();
        return view('profile.order',compact('orders'));
    }
    public function orderDetail($id){
        $user_id = Auth::user()->id;
        $products = OrderProduct::where('order_id',$id)->get();
        return view('profile.order-detail',compact('products'));
    }

  
}
