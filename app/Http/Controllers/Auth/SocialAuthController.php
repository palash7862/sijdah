<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Socialite;
use App\User;

class SocialAuthController extends Controller
{


    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function fbRedirectToProvider(){
        return Socialite::driver('facebook')->redirect();
    }

    public function googleRedirectToProvider(){
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function fbHandleProviderCallback(){
        $userData = [];
        $user = Socialite::driver('facebook')->user();
        //echo "<pre>";
        print_r($user);
        $userData['username']   = $user->name;
        $userData['email']      = $user->email;
        $userData['password']   = $user->id;
        $userExits = User::where('email', $user->email)->first();
        if ($userExits){
            Auth::login($userExits, true);
             return redirect()->route('siteroot');
        }else{
            $user = $this->userCreate($userData);
            Auth::login($user, true);
            return redirect()->route('siteroot');
        }
    }

    public function googleHandleProviderCallback(){
        $userData = [];
        $user = Socialite::driver('google')->user();
//        echo "<pre>";
//        print_r($user);
        $userData['username']   = $user->name;
        $userData['email']      = $user->email;
        $userData['password']   = $user->id;
        $userExits = User::where('email', $user->email)->first();
        if ($userExits){
            Auth::login($userExits, true);
            return redirect()->route('siteroot');
        }else{
            $user = $this->userCreate($userData);
            Auth::login($user, true);
            return redirect()->route('siteroot');
        }
    }

    protected function userCreate($data){
        return User::create([
            'username'  => $data['username'],
            'email'     => $data['email'],
            'level'     => 'basic',
            'password'  => bcrypt($data['password']),
        ]);
    }
}
