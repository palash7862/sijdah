<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cookie;
use Illuminate\Http\Response;

class CookieController extends Controller
{
    public function setLanguage($lang)
    {

    	session(['lang'=>$lang]);

    	$cookie = Cookie::make('lang',$lang,1000000);
    	$response = new Response();
    	$response->withCookie($cookie);
    	return $response;
    }
    public function setArea($area)
    {

    	session(['area'=>$area]);

    	$cookie = Cookie::make('area',$area,1000000);
    	$response = new Response();
    	$response->withCookie($cookie);
    	return $response;
    }
}
