<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Shoppingcart;
use Auth;
use App\Media;
use App\Setting;

class CartController extends Controller
{
    public function all()
    {
        return \Cart::content();
    }
    public function addProduct($id)
    {
        $product = Product::find($id);
        $featureImg = Media::find($product->featured_img_id)->source;
        $featureImg = get_image_size('media-thumb', $featureImg); //get 200x200 img


            \Cart::add($id, $product->title_en, 1, $product->dis_price, [
                "featuredImg" => $featureImg,
                "dil_price" => $product->dil_price,
                "product_type" => $product->product_type,
                "title_bn" => $product->title_bn,
                "size" => $product->size
            ]);

            if (Auth::check()) {
                $cnt = Shoppingcart::where('userId', Auth::id())->where('productId', $id)->count();
                if ($cnt)
                    Shoppingcart::where('userId', Auth::id())->where('productId', $id)->increment('qty');
                else {
                    Shoppingcart::create([
                        'userId' => Auth::id(),
                        'productId' => $id,
                        'qty' => 1,
                    ]);
                }
            }
            session()->forget('coupon');

        return \Cart::content();
    }

    public function increaseProduct($id)
    {
        $row = \Cart::content()->where('id', $id);
        if (sizeof($row)) {
            $rowId = key(json_decode($row));
            $qty = \Cart::get($rowId)->qty;

            $product = Product::find($id);
            if ($product->stock > 0) {
                \Cart::update($rowId, (int)$qty + 1);
            }

            if (Auth::check()) {
                $cnt = Shoppingcart::where('userId', Auth::id())->where('productId', $id)->count();
                if ($cnt)
                    Shoppingcart::where('userId', Auth::id())->where('productId', $id)->decrement('qty');
            }
        }
        session()->forget('coupon');
        return \Cart::content();
    }

    public function removeProduct($id)
    {
        $row = \Cart::content()->where('id', $id);
        if (sizeof($row)) {
            $rowId = key(json_decode($row));
            \Cart::remove($rowId);

            if (Auth::check())
                Shoppingcart::where('userId', Auth::id())->where('productId', $id)->delete();

        }
        session()->forget('coupon');
        return \Cart::content();
    }

    public function reduceProduct($id)
    {
        $row = \Cart::content()->where('id', $id);
        if (sizeof($row)) {
            $rowId = key(json_decode($row));
            $qty = \Cart::get($rowId)->qty;

            \Cart::update($rowId, (int)$qty - 1);

            if (Auth::check()) {
                $cnt = Shoppingcart::where('userId', Auth::id())->where('productId', $id)->count();
                if ($cnt)
                    Shoppingcart::where('userId', Auth::id())->where('productId', $id)->decrement('qty');
            }
        }
        session()->forget('coupon');
        return \Cart::content();
    }

    public function destroy()
    {
        \Cart::destroy();
        return \Cart::content();

    }


}
