<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() === null){
            return redirect()->route('login');
        }
        $actions = $request->route()->getAction();
        $roles = isset($actions['roles']) ? $actions['roles'] : null;
//        var_dump($roles);
//        var_dump($request->user()->level);
//        exit(0);
        if($request->user()->matchLavel($request, $roles )){
            return $next($request);
        }
        return redirect()->route('home');
    }
}
