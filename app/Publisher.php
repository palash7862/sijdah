<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    protected $table = 'publishers';

    protected $fillable = [
        'name',
        'description'
    ];

    public static function getAll(){
        $writers = self::all();
        return $writers;
    }

    public function products(){
        return $this->hasMany('App\Product',  'publisher_id', 'id');
    }
}
