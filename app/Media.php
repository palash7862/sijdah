<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';

    protected $fillable = [
        'title', 'description', 'source'
    ];

    const  allImageSize = [
        'media-thumb'   => '200x200',
        'cart-thumb'    => '75x75',
        'product-thumb' => '390x490'
    ];


}
