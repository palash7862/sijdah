<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';

    protected $fillable = [
        'name',
        'description'
    ];

    public static function getAll(){
        $writers = self::all();
        return $writers;
    }
}
