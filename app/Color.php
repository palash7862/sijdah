<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $table = 'colors';

    protected $fillable = [
        'name', 'color_code'
    ];

    public static function gatAll(){
        $color = self::all();
        return $color;
    }
}
