<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable =['title','couponcode','type','amount','expired_date'];
}
