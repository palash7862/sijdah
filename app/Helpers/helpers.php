<?php

if (! function_exists('get_image_size')) {
    function get_image_size($thumbName, $imgDistanion)
    {
        $allSize = App\Media::allImageSize;
        if (!is_null($thumbName)){

            if (array_key_exists($thumbName, $allSize)){
                $imgEx = explode(".", $imgDistanion);
                $imgEx = end($imgEx);
                $imgNameWithDis = substr($imgDistanion, 0, -(strlen($imgEx)+1));
                return $imgNameWithDis.$allSize[$thumbName].'.'.$imgEx;
            }
            return 'Invalid Size Key';
        }
    }
}

if (! function_exists('getImageSizeById')) {
    function getImageSizeById($id, $thumbName)
    {
        $amedia= App\Media::find($id);
        $imgDistanion = $amedia->source;
        if($thumbName == 'full'){
            return $amedia->source;
        }else{
            $allSize = App\Media::allImageSize;

            if (!is_null($thumbName)){

                if (array_key_exists($thumbName, $allSize)){
                    $imgEx = explode(".", $imgDistanion);
                    $imgEx = end($imgEx);
                    $imgNameWithDis = substr($imgDistanion, 0, -(strlen($imgEx)+1));
                    return $imgNameWithDis.$allSize[$thumbName].'.'.$imgEx;
                }
                return 'Invalid Size Key';
            }
        } 
    }
}

if (! function_exists('getSettingByCul')) {
    function getSettingByCul($calName = null){
		if(!empty($calName) || $calName !== null){
			$contact = App\Setting::where('name', $calName)->first();
			if($contact){
				return $contact;
			}
			return false;
		} 
		return false;
    }
}

if (! function_exists('enToBn')) {
    function enToBn($en){
        return App\BanglaConverter::en2bn($en);
    }

}