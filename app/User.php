<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Validator;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'phone',
        'level',
        'password',
        'picture',
        'billing_city',
        'billing_area',
        'billing_address',
        'verify_code',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function matchLavel($request, $roles){
        if(is_array($roles)){
            foreach ($roles as $role){
                if( strtolower($role) == strtolower($request->user()->level)){
                    return TRUE;
                }
            }
            return FALSE;
        } else {
            if($roles == $request->user()->level ){
                return TRUE;
            }
        }
        return FALSE;
    }

    public static function updateAddress($uid, $request){
        $data = $request->all();
        if($uid) {
            $validator = Validator::make($request->all(), [
                'billingAddress'    => 'required|',
                'billingCity'       => 'required',
                'billingArea'       => 'required'
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route('color.all')
                    ->withErrors($validator)
                    ->withInput();
            }else{
                $user = self::find($uid);
                $user->billing_city     = $data['billingCity'];
                $user->billing_area     = $data['billingArea'];
                $user->billing_address  = $data['billingAddress'];
                $user->save();
                return TRUE;
            }
        }else{
            return FALSE;
        }
    }

}
