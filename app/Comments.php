<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    //
    protected $table  = 'comments';

    protected $fillable = [
        'product_id',
        'user_id',
        'rating',
        'massage'
    ];

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
