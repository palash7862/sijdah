<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Writer extends Model
{
    protected $table = 'writers';

    protected $fillable = [
        'name',
        'description'
    ];

    public static function getAll(){
        $writers = self::all();
        return $writers;
    }

    public function products(){
        return $this->belongsToMany('App\Product', 'product_writers', 'writer_id', 'product_id');
    }

}
