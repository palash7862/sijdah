<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Category;
use App\Color;
use App\Media;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';

    protected $fillable = [
        'title_en', 'title_bn', 'product_type', 'writer', 'publisher', 'page_number',
        'clothing_type', 'weight', 'size', 'stock', 'review',
        'reg_price', 'dis_price', 'dil_price', 'featured_img_id', 'description_en', 'description_bn'
    ];

    protected $dates = ['deleted_at'];

    public static function getColorBy($colorObj){
        $dara = array();
        if(is_object($colorObj)){
            //$inc = 0;
            foreach ($colorObj->categories as $color){
                $dara[] = $color->id;
                //$dara[$inc]['name'] = $color->name;
                //$inc++;
            }
            return $dara;
        }
        return false;
    }

    public function categories(){
        return $this->belongsToMany('App\Category', 'product_category', 'product_id', 'category_id');
    }

    public function publishers(){
        return $this->hasOne('App\Publisher', 'id', 'publisher_id' );
    }

    public function writers(){
        return $this->belongsToMany('App\Writer', 'product_writers', 'product_id', 'writer_id');
    }

    public function medias(){
        return $this->belongsToMany('App\Media', 'product_media', 'product_id', 'media_id');
    }

    public function colors(){
        return $this->belongsToMany('App\Color', 'product_color', 'product_id', 'color_id');
    }

    public function featuredImage(){
        return $this->belongsTo('App\Media','id','featured_img_id');
    }

    public function getCategoriesIdByProduct($productObj){
        $categoryArray = [];
        foreach($productObj->categories as $cats){
            $categoryArray[]= $cats->id;
        }
        return $categoryArray;
    }

    public function getWritersIdByProduct($productObj){
        $writerArray = [];
        foreach($productObj->writers as $writer){
            $writerArray[]= $writer->id;
        }
        return $writerArray;
    }

    public function getPublishersIdByProduct($productObj){
        $publisherArray = [];
        foreach($productObj->publishers as $pub){
            $publisherArray[]= $pub->id;
        }
        return $publisherArray;
    }

    public function getColorIdByProduct($productObj){
        $colorArray = [];
        foreach($productObj->colors as $color){
            $colorArray[]= $color->id;
        }
        return $colorArray;
    }

    public function futureImageSrc($id){
        $media = Media::find($id);
        return $media->source;
    }
}
