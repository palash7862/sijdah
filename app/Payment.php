<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payment';

    protected $fillable = [
        'tran_id', 'val_id', 'bank_tran_id',
        'card_type', 'card_issuer_country',
        'currency', 'currency_amount', 'tran_date'
    ];
}
