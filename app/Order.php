<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'customer_id', 'payment_id', 'payment_type', 'products_price', 'discount',
        'coupon_code', 'shipping_cost', 'total_price', 'status', 'address_type', 'ship_parson',
        'ship_phone', 'ship_city', 'ship_area', 'ship_address'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'customer_id', 'id');
    }

    public function payment(){
        return $this->belongsTo('App\Payment', 'payment_id', 'id');
    }

    public function orderProduct(){
        return $this->hasMany('App\OrderProduct', 'order_id', 'id');
    }
}
